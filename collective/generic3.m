addpath(genpath('../ipic3d_toolbox'));

%clearvars -except Ncyc_ini Ncyc_max dir results_dir fraciz Ygsm


dir='/data1/gianni/generic3/';
global Lx Ly Lz Xgsmrange Ygsmrange Zgsmrange dx dy dz XLEN YLEN ZLEN initial_time Nx Ny Nz Dt



initial_time=0

e=1.6e-19;
mu0 = 4*pi*1e-7;
code_E = 1;
code_B = 1;
%code_B=code_B *1e9; % to convert from Tesla to nT
code_J = 1;
%code_J = code_J*4*pi;
%code_J = code_J*1e9; % to convert to nA/m^2
code_V = 1;
%code_V=code_V/1e3; %to convert to Km/s
code_T = 1;
code_n = 1;
code_dp=1;

code_E = 2060.21;
code_B = 6.87213e-06;
%code_B=code_B *1e9; % to convert from Tesla to nT
code_J = 1.20082e-05;
%code_J = code_J*4*pi;
%code_J = code_J*1e9; % to convert to nA/m^2
code_V = 2.99792e+08;
%code_V=code_V/1e3; %to convert to Km/s
code_T = 1.50326e-10;
code_n = 0.25;
code_dp = 4.5541e+05; %ion skin depth for code unit conversion
e=1.6e-19;
mp=1.6726e-27;
eps0=8.8542e-12;
c=2.9979e8;
n0=code_n*1e6;
wp=sqrt(n0*e^2/mp/eps0);
mu0 = 4*pi*1e-7;

filename=[dir 'settings.hdf'];
Dt=.5;
B0=.0097;
Lx=100;
Ly=50;
Lz=66.667;
%Nx=1280/4;
%Ny=3840/4;
%Nz=1;

Nx = 400;
Ny = 200;
Nz = 280;

XLEN=40;
YLEN=10;
ZLEN=14;
qom=-256;
mratio=256;
vthi=.045
vthe=.0063
Nprocs=XLEN*YLEN*ZLEN

vthr=vthe;

Xgsmrange= [0 Lx];
Zgsmrange= [0 Ly];
Ygsmrange= [0 Lz];
Xgsmrange= [-38.2, -7];
Zgsmrange= [-10.4, 10.4];
Ygsmrange= [-7.8, 7.8];

dx=Lx/Nx;
dy=Ly/Ny;
dz=Lz/Nz;

xc=linspace(0, Lx, Nx);
yc=linspace(0, Ly, Ny);
zc=linspace(0, Lz, Nz);
