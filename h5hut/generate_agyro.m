close all
addpath(genpath('../../matanak')); % Point to the directory where the iPic3D toolbox is
%dir='/data1/gianni/HRmaha3D3/vtk/'; %directory where the files are
clear all

must_read=true;
leggo='h5'
if(must_read)

sim_name='mercury17'
switch sim_name
case 'mercury17'
Mercury17;
case_name='Mercury';
cycle = 1600;
zcode = Lz/2;
case 'tred77'
TRED77;
case_name='GEM';
cycle = 15000;
zcode = Lz/2;
case 'tred81'
tred81;
case_name='GEM';
cycle = 18000;
zcode = Lz/2;
    case 'tred82'
tred82;
case_name='GEM';
cycle = 18000;
zcode = Lz/2;
case 'turbo'
turbo;
case_name='DoubleHarris';
cycle = 20000;
zcode = Lz/2;
case 'AH'
generic;
case_name='AH';
cycle =4000;
zcode = Lz/2;
case 'HRmaha3D3'
HRmaha3D3;
leggo='gda';
    case_name='GEM';
%dir='/data1/gianni/HRmaha3D3/h5/'; cycle= 80002; 
cycle = 24000
%cycle = 80002;  % for h5
%cycle = 80000  % for vtk binary
ncycle = num2str(cycle)%,'%06d');
% for HRmaha3D1:
time=60*(cycle/75000.0*Dt/.125); %*4 %times four to correct for change in dt between 2D and 3D
% for HRmaha3D1.v2:
% time=60*(cycle/75000.0) *2 %times two to correct for change in dt between 2D and 3D
%ADD initial time of the RUN
time=time+initial_time; %(03*60+48)*60
case 'UHRmaha3D3'
UHRmaha3D3;
leggo='vtk';
    case_name='GEM';
%dir='/data1/gianni/HRmaha3D3/h5/'; cycle= 80002; 
cycle = 48000
%cycle = 80002;  % for h5
%cycle = 80000  % for vtk binary
ncycle = num2str(cycle)%,'%06d');
% for HRmaha3D1:
time=60*(cycle/75000.0*Dt/.125); %*4 %times four to correct for change in dt between 2D and 3D
% for HRmaha3D1.v2:
% time=60*(cycle/75000.0) *2 %times two to correct for change in dt between 2D and 3D
%ADD initial time of the RUN
time=time+initial_time; %(03*60+48)*60
case '7feb09'
FEB09;
cycle=18000
case_name='MHDUCLA'
%cycle = 80000  % for vtk binary
% for HRmaha3D1:
time=60*(cycle/75000.0*Dt/.125); %*4 %times four to correct for change in dt between 2D and 3D
% for HRmaha3D1.v2:
% time=60*(cycle/75000.0) *2 %times two to correct for change in dt between 2D and 3D
%ADD initial time of the RUN
time=time+initial_time; %(03*60+48)*60
otherwise
print('no recognised case selected')
end

% Prepare string
ntime = num2str(cycle,'%06d');
ncycle = num2str(cycle,'%06d');


import_h5_binvtk
end

ll=[1,1,1]*9;
Pxx1=smooth3(Pexx,'box',ll);
Pyy1=smooth3(Peyy,'box',ll);
Pzz1=smooth3(Pezz,'box',ll);
Pxy1=smooth3(Pexy,'box',ll);
Pxz1=smooth3(Pexz,'box',ll);
Pyz1=smooth3(Peyz,'box',ll);
   

[nx ny nz]= size(Pexx)

disp(["doing calculation"])
for i=1:nx
    ["doing " num2str(i) " of " num2str(nx)]
for iy=1:ny
for k=1:nz

p(1,1)=(Pxx1(i,iy,k));
p(1,2)=(Pxy1(i,iy,k));
p(1,3)=(Pxz1(i,iy,k));
p(2,2)=(Pyy1(i,iy,k));
p(2,3)=(Pyz1(i,iy,k));
p(3,3)=(Pzz1(i,iy,k));
p(2,1)=p(1,2);
p(3,1)=p(1,3);
p(3,2)=p(2,3);

b(1)=(Bx(i,iy,k));
b(2)=(By(i,iy,k));
b(3)=(Bz(i,iy,k));

b=reshape(b,3,1);
if sum(b.^2) >0
    
b=b./sqrt(sum(b.^2));

%%%%%%%%%%
% Scudder
%%%%%%%%%%

for l=1:3
N1(l,:)=cross(b,p(l,:));
end

for l=1:3
N(:,l)=cross(b,N1(:,l));
end

lamb=sort(eig(N));
lambda1(i,iy,k)=lamb(1);
lambda2(i,iy,k)=lamb(2);
lambda3(i,iy,k)=lamb(3);

Agyro(i,iy,k)= 2*(lamb(3)-lamb(2))/(lamb(3)+lamb(2));


%%%%%%%%%%%
% Aunai
%%%%%%%%%%%

Tr=(p(1,1)+p(2,2)+p(3,3));
Ppar=b'*p*b;
Pper=(Tr-Ppar)/2;
G=eye(3,3)*Pper+(Ppar-Pper)*kron(b,b');
N=p-G;
Agyro_aunai(i,iy,k)=sqrt(sum(N(:).^2))./Tr;

%%%%%%%%%%%
% Swisdak
%%%%%%%%%%%

I2=p(1,1)*p(2,2)+p(1,1)*p(3,3)+p(2,2)*p(3,3);
I2=I2-(p(1,2).^2+p(1,3).^2+p(2,3).^2);
Q=1-4*I2./((Tr-Ppar).*(Tr+3*Ppar));
Nongyro_swisdak(i,iy,k)=sqrt(Q);
% The following formula form Swisdak paper is actually wrong
%Nongyro_aunai(i,k)=sqrt(8*(p(1,2).^2+p(1,3).^2+p(2,3).^2))./(Ppar+2*Pper);

else
   
    Agyro(i,iy,k)= 0;
    Agyro_aunai(i,iy,k)=0;
    Nongyro_swisdak(i,iy,k)=0;
end

end
end
end

savevtk_bin(Agyro,['Agyro_xyz_cycle' ncycle '.vtk'],'Agyro',dx,dy,dz)
savevtk_bin(Agyro_aunai,['Agyro_aunai_xyz_cycle' ncycle '.vtk'],'Agyro',dx,dy,dz)
savevtk_bin(Nongyro_swisdak,['Nongyro_swisdak_xyz_cycle' ncycle '.vtk'],'Agyro',dx,dy,dz)

% Agyro_sm=smooth3D(Agyro,6);
% Agyro_aunai_sm=smooth3D(Agyro_aunai,6);
% Nongyro_swisdak_sm=smooth3D(Nongyro_swisdak,6);
% 
% savevtk_bin(Agyro_sm,['AgyroSM_xyz_cycle' ncycle '.vtk'],'Agyro',dx,dy,dz)
% savevtk_bin(Agyro_aunai_sm,['AgyroSM_aunai_xyz_cycle' ncycle '.vtk'],'Agyro',dx,dy,dz)
% savevtk_bin(Nongyro_swisdak_sm,['NongyroSM_swisdak_xyz_cycle' ncycle '.vtk'],'Agyro',dx,dy,dz)

opath=fn
h5create(opath,'/Step#0/Block/Agyro/0',[Nx, Ny, Nz]);
h5write(opath,'/Step#0/Block/Agyro/0',Agyro)
h5create(opath,'/Step#0/Block/Agyro_aunai/0',[Nx, Ny, Nz]);
h5write(opath,'/Step#0/Block/Agyro_aunai/0',Agyro_aunai)
h5create(opath,'/Step#0/Block/Nongyro_swisdak/0',[Nx, Ny, Nz]);
h5write(opath,'/Step#0/Block/Nongyro_swisdak/0',Nongyro_swisdak)
h5create(opath,'/Step#0/Block/Nongyro_aunai/0',[Nx, Ny, Nz]);
h5write(opath,'/Step#0/Block/Nongyro_aunai/0',Nongyro_swisdak)

