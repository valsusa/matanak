#!/bin/bash
cd maxp_frame
convert  Bn_combo*.png Bn_combo.gif
convert  Bl_combo*.png Bl_combo.gif
convert  Bm_combo*.png Bm_combo.gif
convert  Bn0*.png Bn.gif
convert  Bl0*.png Bl.gif
convert  Bm0*.png Bm.gif

convert Tepar_combo*.png Tepar_combo.gif
convert Tepar0*.png Tepar.gif
convert Teper1_combo*.png Teper1_combo.gif
convert Teper10*.png Teper1.gif
convert Teper2_combo*.png Teper2_combo.gif
convert Teper20*.png Teper2.gif

convert Tipar_combo*.png Tipar_combo.gif
convert Tipar0*.png Tipar.gif
convert Tiper1_combo*.png Tiper1_combo.gif
convert Tiper10*.png Tiper1.gif
convert Tiper2_combo*.png Tiper2_combo.gif
convert Tiper20*.png Tiper2.gif

convert deltay*.png deltaycode.gif
convert ZGSM*.png Zgsm.gif
convert BZ*.png Bz_xygsm.gif
convert EY*.png Eyrate_xygsm.gif
