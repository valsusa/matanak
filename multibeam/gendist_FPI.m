close all
clear all

load bimodal_ions_FAC.mat           %small box
%load bimodal_ions_FAC_bigbox.mat    %big box
[nx,ny,nz]=size(fcutoff);
vmax=560                             %for small box
%vmax=560*3.75;                       %for big box

%fcutoff = fcutoff + permute(fcutoff,[2 3 1]);
fcutoff = fcutoff + circshift(fcutoff,100);
dv=2*vmax/(nx-1);
v1D=-vmax:dv:vmax;
[vvx,vvy,vvz]=meshgrid(v1D,v1D,v1D);
f1D=fcutoff(:);
fmin=min(f1D(f1D>0));
fmax=max(f1D);
%f1D(f1D<fmax/100)=0;

%Distribution cannot equal zero for method to work
%so set zeros (if the exist) to small fraction of 
%smallest nonzero value.
%f1D(f1D==0)=fmin/1000;      
vx1D=vvx(:)'; vy1D=vvy(:)'; vz1D=vvz(:)';

Np=1e5;          %Number of randomly distributed particles

Ng=size(f1D,1);
ranarr=rand(4,Np);
fcum=cumsum(f1D);
fcum=Ng*fcum/fcum(Ng);
Pg=interp1(fcum',1:Ng,Ng*ranarr(1,:));
Pg=1+floor(Pg);
xp=vx1D(Pg)+dv*ranarr(2,:)-dv/2;
yp=vy1D(Pg)+dv*ranarr(3,:)-dv/2;
zp=vz1D(Pg)+dv*ranarr(4,:)-dv/2;

means=[mean(xp) mean(yp) mean(zp)]
figure(1)
scatter3(xp(1:100:Np),yp(1:100:Np),zp(1:100:Np))
daspect([1 1 1])

X=[xp;yp;zp]';
options = statset('MaxIter',1000);

NGMM=20
for i=1:NGMM
GMModel = fitgmdist(X,i,'RegularizationValue',0.1,'Options',options);
GMModel.ComponentProportion
GMModel.mu
AIC(i)=GMModel.AIC;
BIC(i)=GMModel.BIC;
 figure(1)
 hold on
 xc=GMModel.mu(:,1)
 yc=GMModel.mu(:,2)
 zc=GMModel.mu(:,3)
 plot3(xc,yc,zc,'*')
 pause(.1)
end
figure(2)
subplot(2,1,1)
plot(1:NGMM,AIC)
title('AIC')
axis tight
xlabel('number of clusters','fontsize',15)
set(gca,'fontsize',15)
subplot(2,1,2)
plot(1:NGMM,BIC)
axis tight
title('BIC')
xlabel('number of clusters','fontsize',15)
set(gca,'fontsize',15)
print('-dpng','aicbic.png')

GMModel = fitgmdist(X,4,'RegularizationValue',0.1,'Options',options);
xc=GMModel.mu(:,1)
 yc=GMModel.mu(:,2)
 zc=GMModel.mu(:,3)
 figure(100)
plot3(xp(1:100:Np),yp(1:100:Np),zp(1:100:Np),'.')
hold on
 plot3(xc,yc,zc,'*')

 