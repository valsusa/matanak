clear all
close all

options = statset('MaxIter',1000);
rng(1); % For reproducibility

X=[];
NC=5;
v0x=30*rand(NC,1);v0y=zeros(NC,1);v0z=zeros(NC,1);
for ncomp= 1:NC
Np=100000;
k_kappa=200;
%k_kappa must exceed 1.5

uth=sqrt(2);
vth=uth;

apar = uth*uth*2.0*(k_kappa-1.5)/k_kappa;
aperp = vth*vth*2.0*(k_kappa-1.5)/k_kappa;
theta = 2.0 * pi * rand(Np,1);
vpar=sqrt(k_kappa*apar.*(rand(Np,1).^(-1.0/(k_kappa-0.5))-1.0)).*cos(theta);
vperp=sqrt(k_kappa*aperp.*(1.0+vpar.*vpar./(k_kappa*apar+1e-20)).*((1.0-rand(Np,1)).^(-1.0/k_kappa)-1.0));

theta = 2*pi*rand(Np,1);
vpkx=v0x(ncomp)+vpar;
vpky=v0y(ncomp)+vperp .* cos(theta);
vpkz=v0z(ncomp)+vperp .* sin(theta);
close all
X=[X;vpkx,vpky,vpkz];
end
plot3(X(:,1),X(:,2),X(:,3),'.')
axis equal
pause

NGMM=10
for i=1:NGMM
    disp(['Beams=' num2str(i)])
GMModel = fitgmdist(X,i,'RegularizationValue',0.1,'Options',options);
%GMModel.ComponentProportion
AIC(i)=GMModel.AIC;
BIC(i)=GMModel.BIC;
end
figure
subplot(2,1,1)
plot(1:NGMM,AIC)
title('AIC')
subplot(2,1,2)
plot(1:NGMM,BIC)
title('BIC')
figure
subplot(2,1,1)
plot(2:NGMM,diff(AIC),2:NGMM,zeros(NGMM-1,1))
title('\delta AIC')
subplot(2,1,2)
plot(2:NGMM,diff(BIC),2:NGMM,zeros(NGMM-1,1))
title('\delta BIC')

GMModel = fitgmdist(X,NC,'RegularizationValue',0.1,'Options',options);
GMModel.ComponentProportion



