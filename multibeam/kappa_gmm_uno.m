%clear all
%close all

options = statset('MaxIter',1000);
rng(1); % For reproducibility

Np=100000
k_kappa=2
%k_kappa must exceed 1.5

uth=sqrt(2);
vth=uth;
apar = uth*uth*2.0*(k_kappa-1.5)/k_kappa;
aperp = vth*vth*2.0*(k_kappa-1.5)/k_kappa;
theta = 2.0 * pi * rand(Np,1);
vpar=sqrt(k_kappa*apar.*(rand(Np,1).^(-1.0/(k_kappa-0.5))-1.0)).*cos(theta);
vperp=sqrt(k_kappa*aperp.*(1.0+vpar.*vpar./(k_kappa*apar+1e-10)).*((1.0-rand(Np,1)).^(-1.0/k_kappa)-1.0));

theta = 2*pi*rand(Np,1);
vpkx=vpar;
vpky=vperp .* cos(theta);
vpkz=vperp .* sin(theta);
close all
plot3(vpkx,vpky,vpkz,'.')
X=[vpkx,vpky,vpkz];

NGMM=10
for i=1:NGMM
GMModel = fitgmdist(X,i,'RegularizationValue',0.1,'Options',options);
GMModel.ComponentProportion
GMModel.mu
AIC(i)=GMModel.AIC;
BIC(i)=GMModel.BIC;
end
figure
subplot(2,1,1)
plot(1:NGMM,AIC)
title('AIC')
axis tight
xlabel('number of clusters','fontsize',15)
set(gca,'fontsize',15)
subplot(2,1,2)
plot(1:NGMM,BIC)
axis tight
title('BIC')
xlabel('number of clusters','fontsize',15)
set(gca,'fontsize',15)
print('-dpng','aicbic.png')

GMModel = fitgmdist(X,3,'RegularizationValue',0.1,'Options',options);
GMModel.ComponentProportion



