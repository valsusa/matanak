kappa=2;
close all
for kappa=[2,4,8,100]
    kappa
v=linspace(-10,10,100);
kT=1;
wk = sqrt((2*kappa-3)*kT/kappa);
f= gamma(kappa+1)/2/pi/(kappa*wk.^2).^(3/2)/gamma(kappa-1/2)/gamma(3/2).*(1+v.^2/kappa/wk.^2).^(-kappa-1);
semilogy(v,f./max(f))


hold on
end
legend('2','4','8','max')

figure(2)
for kappa=0:.1:1
E=-linspace(0,10,100);
f=(sqrt(1+kappa^2.*E.^2)+kappa*E).^(1/kappa)
loglog(-E,f)
hold on
end