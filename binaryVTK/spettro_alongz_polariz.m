function [] = spettro_alongz_polariz(x,y,z,Ex,Ey,Ez, Bx, By, Bz, Nx, Ny, Nz,Lx,Ly,Lz,qom,vthi,vthe,B)


close all
%h1=figure(1)
%set(h1,'Position',[40 78 1040 420])

cmap=load('rainbow_cm.mat');cmap=cmap.cmap;
%cmap=load('gist_ncar.mat');
%cmap=cmap.cm_cool_hot_2

f1 = Ey + i*Bz;
f2 = Ey + i *Ez;

ff1 = fft(f1(:,:,1:end-1),[],3);ff1=fftshift(ff1,3);
ff2 = fft(f2(:,:,1:end-1),[],3);ff2=fftshift(ff2,3);
figure(1)
imagesc(log(abs(squeeze((ff1(:,round(end/2),:))))))
figure(2)
imagesc(log(abs(squeeze(mean(ff2,2)))))
B0=mean(mean(B,3),2);
max_val=-7;%log(max(max(fEx(10:end-10,:))));
min_val=-10;

x1d=10:Nx-10; x1d=x1d/Nx*Lx;

mz=0:round(Nz/2); kz=mz*2*pi/Lz;

rhoi=mean(mean(vthi,3),2)./B0;
rhoi = mean(mean(vthi./B,3),2);
rhoe=mean(mean(vthe./B/abs(qom),3),2);
rholh=sqrt(rhoi.*rhoe);

% subplot(2,3,1)
% pcolor(x1d,kz,log(fEx(10:end-10,:)'));colorbar;shading interp 
% caxis([-10 max_val])
% colormap(jet)
% xlabel('x/d_i')
% ylabel('k_z d_i')
% ylim([0 20])

grafo(fEx,1,'Spectrum Ex',cmap)
grafo(fEz,2,'Spectrum Ey',cmap)
grafo(fEy,3,'Spectrum Ez',cmap)



fEx = fft(cEx(:,:,1:end-1),[],3);fEx=squeeze(mean(abs(fEx(:,:,1:round(Nz/2)+1)),2));
fEy = fft(cEy(:,:,1:end-1),[],3);fEy=squeeze(mean(abs(fEy(:,:,1:round(Nz/2)+1)),2));
fEz = fft(cEz(:,:,1:end-1),[],3);fEz=squeeze(mean(abs(fEz(:,:,1:round(Nz/2)+1)),2));

grafo(fEx,4,'Spectrum dBx/dt',cmap)
grafo(fEz,5,'Spectrum dBy/dt',cmap)
grafo(fEy,6,'Spectrum dBz/dt',cmap)

print -dpng figura

function [] = grafo(fEx,pos,varname,cmap)
subplot(2,3,pos)
pcolor(x1d,kz,log(fEx(10:end-10,:)'));colorbar;shading interp 
caxis([-10 max_val])
xlabel('x/d_i')
ylabel('k_y d_i')
ylim([0 20])
hold on 
%plot(x1d, ones(size(x1d))/rhoi,'k','linewidth',2)


plot(x1d, 1./rhoi(10:end-10),'k','linewidth',2)
plot(x1d, 1./rhoe(10:end-10),'k:','linewidth',2)
plot(x1d, 1./rholh(10:end-10),'k--','linewidth',2)
title(varname)
colormap(flipud(cmap))
set(gca, 'YScale', 'log')
end

end