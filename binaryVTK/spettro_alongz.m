function [] = spettro_alongz(x,y,z,Ex,Ey,Ez, Bx, By, Bz,  Nx, Ny, Nz,Lx,Ly,Lz,qom,vthi,vthe,B)


close all
h1=figure(1)
set(h1,'Position',[40 78 1040 220])

cmap=load('rainbow_cm.mat');cmap=cmap.cmap;
%cmap=load('gist_ncar.mat');
%cmap=cmap.cm_cool_hot_2


[cEx,cEy,cEz]=compute_curl(x,y,z,Ex,Ey,Ez);

fEx = fft(Ex(:,:,1:end-1),[],3);fEx=squeeze(mean(abs(fEx(:,:,1:round(Nz/2)+1)),2));
fEy = fft(Ey(:,:,1:end-1),[],3);fEy=squeeze(mean(abs(fEy(:,:,1:round(Nz/2)+1)),2));
fEz = fft(Ez(:,:,1:end-1),[],3);fEz=squeeze(mean(abs(fEz(:,:,1:round(Nz/2)+1)),2));

B0=mean(mean(B,3),2);
max_val=-7;%log(max(max(fEx(10:end-10,:))));
min_val=-10;
spx = 1;
spy = 3;


x1d=10:Nx-10; x1d=x1d/Nx*Lx;

mz=0:round(Nz/2); kz=mz*2*pi/Lz;

rhoi=mean(mean(vthi,3),2)./B0;
rhoi = mean(mean(vthi./B,3),2);
rhoe=mean(mean(vthe./B/abs(qom),3),2);
rholh=sqrt(rhoi.*rhoe);

% subplot(2,3,1)
% pcolor(x1d,kz,log(fEx(10:end-10,:)'));colorbar;shading interp 
% caxis([-10 max_val])
% colormap(jet)
% xlabel('x/d_i')
% ylabel('k_z d_i')
% ylim([0 20])

grafo(fEx,spx, spy, 1,'Spectrum Ex',cmap)
grafo(fEz,spx, spy, 2,'Spectrum Ey',cmap)
grafo(fEy,spx, spy, 3,'Spectrum Ez',cmap)
print -dpng figura1

h1=figure(2)
set(h1,'Position',[40 78 1040 220])

fEx = fft(cEx(:,:,1:end-1),[],3);fEx=squeeze(mean(abs(fEx(:,:,1:round(Nz/2)+1)),2));
fEy = fft(cEy(:,:,1:end-1),[],3);fEy=squeeze(mean(abs(fEy(:,:,1:round(Nz/2)+1)),2));
fEz = fft(cEz(:,:,1:end-1),[],3);fEz=squeeze(mean(abs(fEz(:,:,1:round(Nz/2)+1)),2));

grafo(fEx,spx, spy, 1,'Spectrum dBx/dt',cmap)
grafo(fEz,spx, spy, 2,'Spectrum dBy/dt',cmap)
grafo(fEy,spx, spy, 3,'Spectrum dBz/dt',cmap)
print -dpng figura2

close all
h3=figure(3)
%set(h2,'Position',[40 78 1040 420])
[Sx, Sy, Sz] = cross_prod(Ex, Ey, Ez, Bx/4/pi, By/4/pi, Bz/4/pi);
S=sqrt(Sx.^2+Sy.^2+Sz.^2);
dx=Lx/Nx;
dy=Ly/Ny;
dz=Lz/Nz;

[x,y,z]=meshgrid(0:dx:Lx-dx,0:dy:Ly-dy,0:dz:Lz-dz);
radius=0.01;
divS = compute_div(x,y,z,Sx,Sy,Sz,radius, 0.);

fdivS = fft(divS(:,:,1:end-1),[],3);fdivS=squeeze(mean(abs(fdivS(:,:,1:round(Nz/2)+1)),2));

max_val=0;
grafo(fdivS,1, 1, 1,'Spectrum div(S)',cmap)

print -dpng figura3

function [] = grafo(fEx,spx, spy, pos,varname,cmap)
subplot(spx,spy,pos)
pcolor(x1d,kz,log(fEx(10:end-10,:)'));colorbar;shading interp 
if(max_val ~= 0) 
    caxis([-10 max_val])
end    
xlabel('x/d_i')
ylabel('k_y d_i')
ylim([0 20])
hold on 
%plot(x1d, ones(size(x1d))/rhoi,'k','linewidth',2)


plot(x1d, 1./rhoi(10:end-10),'k','linewidth',2)
plot(x1d, 1./rhoe(10:end-10),'k:','linewidth',2)
plot(x1d, 1./rholh(10:end-10),'k--','linewidth',2)
title(varname)
colormap(flipud(cmap))
set(gca, 'YScale', 'log')
end

end