dx=Lx/Nx; dy=Ly/Ny; dz=Lz/Nz;
ri=vthi/mean(B(:));
re=ri/sqrt(abs(qom));
rhoLH=sqrt(re*ri);

%xflow='inflow'
close all
soglia=AAz(:,:,:)./max(AAz(:));
s_0=.5;c=.1;
% -() for inflow +() for outflow
switch xflow
    case 'outflow'
        st=.5*(1+tanh(+(soglia-s_0)./c^2));
    otherwise
        st=.5*(1+tanh(-(soglia-s_0)./c^2));  
end


wci=(mean(B(:).*st(:))/mean(st(:)));
wce=wci*abs(qom);
ri=vthi/wci;
re=vthe/wce;
rhoLH=sqrt(re*ri);

Qe=Qex.^2+Qey.^2+Qez.^2; Qe=sqrt(Qe);
%bbb=Qe.*st;
%figure(1)
%imagesc(bbb(:,:,1))

   
E=sqrt(Ex.*Ex+Ey.*Ey+Ez.*Ez);
[autX1,autY1,autZ1,volX,volY,volZ]=compute_strct_funct_directional(st,E);%divS);

p_avg=mean(E,3);
dp=E;
for k=1:Nz
    dp(:,:,k)=E(:,:,k)-p_avg;
end
[autX1d,autY1d,autZ1d,volX,volY,volZ]=compute_strct_funct_directional(st,dp);%divS);
%bbb=S.*st;
[autX2,autY2,autZ2,volX,volY,volZ]=compute_strct_funct_directional(st,B);
p_avg=mean(B,3);
dp=B;
for k=1:Nz
    dp(:,:,k)=B(:,:,k)-p_avg;
end
[autX2d,autY2d,autZ2d,volX,volY,volZ]=compute_strct_funct_directional(st,dp);%divS);

%bbb=Jex.*Ex+Jey.*Ey+Jez.*Ez;%.*st;

[autX3,autY3,autZ3,volX,volY,volZ]=compute_strct_funct_directional(st,Ve); % JedotE);
p_avg=mean(Ve,3);
dp=B;
for k=1:Nz
    dp(:,:,k)=Ve(:,:,k)-p_avg;
end
[autX3d,autY3d,autZ3d,volX,volY,volZ]=compute_strct_funct_directional(st,dp);%divS);


%

%[autX4,autY4,autZ4,volX,volY,volZ]=compute_strct_funct_directional(st, JidotE)
close all
h=figure(2);
set(h,'Position', [103 3 827 665])
splt(2,autX1,autY1, autZ1, Nx, Ny, Nz, dx, dy, dz,  'S_E')%'S_{divS}^2')

%splt(2,autX2,autY2, autZ2, Nx, Ny, Nz, dx, dy, dz,  'S_S^2')
splt(3,autX3,autY3, autZ3, Nx, Ny, Nz, dx, dy, dz, 'S_{Ve}')%'S_{JeE}^2')
splt(1,autX2,autY2, autZ2, Nx, Ny, Nz, dx, dy, dz, 'S_B') %{JiE}^2')
%print('-dpng', '-r300', ['structure_ele' xflow])
print('-dpng', '-r300', ['structure_combo' xflow])

close all
h=figure(3);
set(h,'Position', [103 3 827 665])
splt(2,autX1d,autY1d, autZ1d, Nx, Ny, Nz, dx, dy, dz,  'S_{dE}')%'S_{divS}^2')

%splt(2,autX2,autY2, autZ2, Nx, Ny, Nz, dx, dy, dz,  'S_S^2')
splt(3,autX3d,autY3d, autZ3d, Nx, Ny, Nz, dx, dy, dz, 'S_{dVe}')%'S_{JeE}^2')
splt(1,autX2d,autY2d, autZ2d, Nx, Ny, Nz, dx, dy, dz, 'S_{dB}') %{JiE}^2')
%close all

%Qi=Qix.^2+Qiy.^2+Qiz.^2; Qi=sqrt(Qi);
%bbb=Qi.*st;
%figure(1)
%imagesc(bbb(:,:,1))



%[autX1,autY1,autZ1,volX,volY,volZ]=compute_strct_funct_directional(st,Jix,Jiy,Jiz);

%bbb=S.*st;
%[autX2,autY2,autZ2,volX,volY,volZ]=compute_strct_funct_directional(st,Sx,Sy,Sz);

%bbb=Jix.*Ex+Jiy.*Ey+Jiz.*Ez;%.*st;
%[autX3,autY3,autZ3,volX,volY,volZ]=compute_strct_funct_directional(st,bbb);

%[autX4,autY4,autZ4,volX,volY,volZ]=compute_strct_funct_directional(st, divS)

%h=figure(2);
%set(h,'Position', [103 3 827 665])
%splt(3,autX1,autY1, autZ1, Nx, Ny, Nz, dx, dy, dz,  'S_{Ji}^2')
%splt(2,autX2,autY2, autZ2, Nx, Ny, Nz, dx, dy, dz,  'S_S^2')
%splt(3,autX3,autY3, autZ3, Nx, Ny, Nz, dx, dy, dz, 'S_{JiE}^2')
%splt(4,autX4,autY4, autZ4, Nx, Ny, Nz, dx, dy, dz, 'S_{divS}^2')
%print('-dpng', '-r300', ['structure_ion' xflow])
print('-dpng', '-r300', ['structure_dcombo' xflow])

save([sim_name '_' xflow '_structures.mat'],'Nx','Ny','Nz','-regexp','^aut');

function [hax]=splt(nplt,autX1,autY1, autZ1, Nx, Ny, Nz, dx, dy, dz, tit1)
nplt
hax=subplot(3,1,nplt)
nsx=round(Nx/2)
nsy=round(Ny/2)
nsz=round(Nz/2)
size(autX1)
size(autY1)
size(autZ1)
loglog((0:nsx-1)*dx,autX1(1:nsx),(0:nsy-1)*dy,autY1(1:nsy),(0:nsz-1)*dz,autZ1(1:nsz))
hold on 
p=polyfit(log(1./(1:3)/dx), log(autZ1(2:4)'),1)
n=p(1);
loglog((1:10)*dx,(1./(1:10)/dx).^n.*(dx)^n*autZ1(2))
xlabel('$\ell/d_i$','interpreter','latex','fontsize',12)
title(tit1)
%ylim([1e-17 1e-14])
%xlim([1e-2 1e2])
%line([1 1]/rhoi,get(hax,'YLim'),'Color','k')
%line([1 1]/rhoLH,get(hax,'YLim'),'Color','y')
legend('l_x','l_y','l_z',['n=' num2str(-n)],'location','eastoutside')
grid on
end
