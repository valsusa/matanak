addpath(genpath('..'));
close all


leggi=true;



if(leggi)
    %dir ='/Users/glapenta/Desktop/tred65/';
    dir ='~/Dropbox/Shared/tred65/';
    cycle = 18000;
    [agyro,Nx,Ny,Nz] = read_binVTK_scalar(dir,'Agyro',cycle,0);
    [Bx, By, Bz,Nx,Ny,Nz] = read_binVTK_vector(dir,'B',cycle);
    [Ex, Ey, Ez,Nx,Ny,Nz] = read_binVTK_vector(dir,'E',cycle);
    [Jex, Jey, Jez, Nx,Ny,Nz] = read_binVTK_vector(dir,'Je',cycle);
    [Jix, Jiy, Jiz, Nx,Ny,Nz] = read_binVTK_vector(dir,'Ji',cycle);
    [rhoe,rhoi,Nx,Ny,Nz] = read_binVTK_multiscalar(dir,'rho',cycle);
    B2 = Bx.*Bx + By.*By + Bz.*Bz;
    E2 = Ex.*Ex + Ey.*Ey + Ez.*Ez;
    EdotB = Ex.*Bx + Ey.*By + Ez.*Bz;
    JdotE = Ex.*(Jex+Jix) + Ey.*(Jey+Jiy) + Ez.*(Jez+Jiz);
    JedotE = Ex.*(Jex) + Ey.*(Jey) + Ez.*(Jez);
    vE = sqrt(B2.*E2 -EdotB.^2)./E2;
    je = Jex.*Jex + Jey.*Jey + Jez.*Jez;
    jpar = (Jex.*Bx + Jey.*By + Jez.*Bz)./sqrt(B2);
    aag=smooth3D(agyro,2);
    Bref = max(sqrt(B2(:)))/2;
    Vex = Jex./rhoe;
    Vey = Jey./rhoe;
    Vez = Jez./rhoe;
    
    Vix = Jix./rhoi;
    Viy = Jiy./rhoi;
    Viz = Jiz./rhoi;
    
    varef=Bref./sqrt(4*pi*max(rhoi(:)))/2;
end

%[code_n, code_J, code_V, code_T, code_E, code_B, momentum_corrector, dp, de, wpp] =   code_units(256, 0, 1, 10);
tred65
B0_phys = 20e-9 ; %Tesla
code_BSI= 20e-9 /B0;
density = 1; %particles per cc
density = density* 1e6;
vA_phys = B0_phys / sqrt(density* mu0)
vA_code = B0;
code_nSI = 4*pi*density;
code_ESI = code_BSI* cphys

Lx = 40;
Ly = 15;
Lz = 10;
dx = Lx/Nx;
dy = Ly/Ny;
dz = Lz/Nz;

di=1
de=1/sqrt(256)

[i,j,k]=ndgrid(1:Nx,1:Ny,1:Nz);

xp = 400.35;
yp = 100.5;
zp = 93.2;

sz=size(vE);
vEm=vE;
valmax=15; valtr=1; % erano 1 e .5
vEm(vEm>valmax)=valmax;
vEm(1:100,:,:)=valmax;vEm(Nx-(0:99),:,:)=valmax;
vEm(:,1:20,:)=valmax;vEm(:,Ny-(0:19),:)=valmax;
vEm(:,:,1:20)=valmax;vEm(:,:,Nz-(0:19))=valmax;

ic=find(vEm<valtr)

nc = max(size(ic))


[vEmval iorder]=sort(vEm(ic))

Bsc1 =[]; Bsc2 =[];


fai_plots=false
if(fai_plots)
iplot=0
lpos=[]; ltype=[];heig=[];jeig=[];
for icount=1:nc
    
    
    %[caz ic]=min(vEm(:))
    [xp yp zp]=ind2sub(sz,ic(iorder(icount)));
    
    Position=[xp*dx yp*dy zp*dz]
    [n1 n2]= size(lpos);
    if(n1>1)
        dist=min(sqrt(sum((lpos-repmat(Position,n1,1)).^2,2)))
    else
        dist=Lx
    end
    if (abs(Position(1)-Lx/2)>2 &dist>de) % was >2 and >1
        lpos=[lpos;Position];
        iplot=iplot+1
        bxp = interpn(i,j,k,Bx,xp, yp, zp);
        byp = interpn(i,j,k,By,xp, yp, zp);
        bzp = interpn(i,j,k,Bz,xp, yp, zp);
        vxp = interpn(i,j,k,Vex,xp, yp, zp);
        vyp = interpn(i,j,k,Vey,xp, yp, zp);
        vzp = interpn(i,j,k,Vez,xp, yp, zp);
        exp = interpn(i,j,k,Ex,xp, yp, zp);
        eyp = interpn(i,j,k,Ey,xp, yp, zp);
        ezp = interpn(i,j,k,Ez,xp, yp, zp);
        
        
        bp = [bxp,byp,bzp]; ep = [exp,eyp,ezp];
        bpe = (bp*ep')/(ep*ep')*ep;
        vf = cross(ep,bp)/(ep*ep');
        
        % To use PErp1 and Perp2 as defined by MMS (with respect to Perp Drift of
        % the species)
        %[perp1x, perp1y,perp1z,  perp2x, perp2y, perp2z] = perp_plane(bxp, byp, bzp, vxp, vyp, vzp);
        
        % To define perp1 as Eperp and perp2 as BxEperp
        [perp1x, perp1y,perp1z,  perp2x, perp2y, perp2z] = perp_plane(exp, eyp, ezp, bxp, byp, bzp);
        
        span = 3
        skip = 2
        [eta,xi] = meshgrid(-span:.2:span,-span:.2:span);
        
        xperp = xp + perp1x .*eta + perp2x .* xi;
        yperp = yp + perp1y .*eta + perp2y .* xi;
        zperp = zp + perp1z .*eta + perp2z .* xi;
        
        dbx = interpn(i,j,k,Bx,xperp,yperp,zperp) - 0*bxp;
        dby = interpn(i,j,k,By,xperp,yperp,zperp) - 0*byp;
        dbz = interpn(i,j,k,Bz,xperp,yperp,zperp) - 0*bzp;
        Bp2 = bxp.^2 + byp.^2 + bzp.^2;
        
        dex = interpn(i,j,k,Ex,xperp,yperp,zperp);
        dey = interpn(i,j,k,Ey,xperp,yperp,zperp);
        dez = interpn(i,j,k,Ez,xperp,yperp,zperp);
        
        [zx,zy,zz]=cross_prod(vf(1),vf(2),vf(3),dex,dey,dez);
        gamma = 1/sqrt(1-vf*vf');
        dbfx = gamma * (dbx - zx);
        dbfy = gamma * (dby - zy);
        dbfz = gamma * (dbz - zz);
        
        
        
        
        vex = interpn(i,j,k,Vex,xperp,yperp,zperp);
        vey = interpn(i,j,k,Vey,xperp,yperp,zperp);
        vez = interpn(i,j,k,Vez,xperp,yperp,zperp);
        
        vix = interpn(i,j,k,Vix,xperp,yperp,zperp);
        viy = interpn(i,j,k,Viy,xperp,yperp,zperp);
        viz = interpn(i,j,k,Viz,xperp,yperp,zperp);
        
        
        jx = interpn(i,j,k,Jex+Jix,xperp,yperp,zperp);
        jy = interpn(i,j,k,Jey+Jiy,xperp,yperp,zperp);
        jz = interpn(i,j,k,Jez+Jiz,xperp,yperp,zperp);
        
        
        vfx = vex-vf(1);
        vfy = vey-vf(2);
        vfz = vez-vf(3);
        
        [zx,zy,zz]=cross_prod(vf(1),vf(2),vf(3),dbx,dby,dbz);
        defx = gamma * (dbx + zx);
        defy = gamma * (dby + zy);
        defz = gamma * (dbz + zz);
        
        
        [Epx,Epy,Epz] = cross_prod(vex,vey,vez,dbx,dby,dbz);
        
        Eslip = (Epx - dex).^2 + (Epy - dey).^2 + (Epz - dez).^2;
        Vslip = (vex - Epx/Bp2).^2 + (vey - Epx/Bp2).^2 + (vez - Epx/Bp2).^2 ;
        Vslip = Vslip./sqrt(vex.^2+vey.^2+vez.^2);
        Epar = (dbx .* dex + dby .* dey + dbz .* dez)./sqrt(Bp2);
        Emod = (exp .* dex + eyp .* dey + ezp .* dez)./sqrt(exp.*exp + eyp.*eyp + ezp.*ezp);
        Epx = Epx + dex;
        Epy = Epy + dey;
        Epz = Epz + dez;
        
        
        
        
        
        phipar=false
        if(phipar)
            
            global Nx Ny Nz Bx By Bz verso
            
            for is = 1:prod(size(xperp))
                Opt    = odeset('Events', @myEvent);
                tspan=linspace(0,1e5,1000);
                xst=[xperp(is), yperp(is), zperp(is)];
                verso=1;
                [t1,sol1]=ode45(@ode_lines,tspan,xst,Opt);
                verso=-1;
                [t2,sol2]=ode45(@ode_lines,tspan,xst,Opt);
                sol=[flipud(sol2); sol1];
                %plot(sol(:,1),sol(:,2))
                hold on
                xs = sol(:,1);
                ys = sol(:,2);
                zs = sol(:,3);
                pb(is)=int_line(xs,ys,zs, dx, dy, dz, Bx,By,Bz,Ex,Ey,Ez);
            end
            pb=reshape(pb,31,31);
        end
        
        %save("todos.mat")
        %return
        
        %pb=potalongb(xperp*dx,yperp*dy,zperp*dz,dx,dy,dz,Bx,By,Bz,Ex,Ey,Ez);
        
        
        
        agp = interpn(i,j,k,aag,xperp,yperp,zperp);
        
        LIp = interpn(i,j,k,vE,xperp,yperp,zperp);
        
        rhoep = interpn(i,j,k,rhoe,xperp,yperp,zperp);
        
        jEp = Epx .* jx + Epy .* jy + Epz .* jz;
        %jEp = (dex .* (vex - vf(1)*0) + dey .* (vey -vf(2)*0) + dez .* (vez - vf(3)*0)).*rhoep;
        
        dbeta = dbfx.* perp1x + dbfy.* perp1y + dbfz.* perp1z;
        dbxi = dbfx.* perp2x + dbfy.* perp2y + dbfz.* perp2z;
        
        % attention order switched in B to account for exchange of index
        az=vecpot(eta(1,:),xi(:,1),dbxi,dbeta);
        %az=vecpot_planet(dbeta,dbxi,dx,dy);
        
        
        
        [FX,FY]=gradient(az,.2*dx);
        [Hxx,Hxy]=gradient(FX,.2*dx);
        [Hyx,Hyy]=gradient(FY,.2*dx);
        [nxh,nyh]=size(Hxx);
        ihctr = round(nxh/2);
        jhctr = round(nyh/2);
        H=[Hxx(ihctr,jhctr) Hxy(ihctr,jhctr); Hyx(ihctr,jhctr) Hyy(ihctr,jhctr)]
        lh=eig(H);
        
        % This works in 2D but in 3D one better looks at the eigenvalues of the
        % JAcobian as explained in Lau and Finn.
        heig = [heig ; lh'];
        % if(lh(1)*lh(2)<0)
        %     disp('x-point')
        %     labelh='x'
        % else
        %     disp('o-point')
        %     labelh='o'
        % end
        % ltype=[ltype;labelh];
        
        veta = vex.* perp1x + vey.* perp1y + vez.* perp1z;
        vxi = vex.* perp2x + vey.* perp2y + vez.* perp2z;
        
        vieta = vix.* perp1x + viy.* perp1y + viz.* perp1z;
        vixi = vix.* perp2x + viy.* perp2y + viz.* perp2z;
        
        
        
        
        % More complete view for investigation
        %localB_figure
        
        % Simplified presentation for publication
        localB_figure_new
        
        h1000=figure(1000)
        set(h1000,'Position', [362 578 968 369])
        if(iplot==1)
            %pcolor(mean(Bz,3)');shading interp
            pcolor(linspace(0,Lx,Nx),linspace(0,Ly,Ny),Vex(:,:,Nz/2)');shading interp
            set(gca,'fontsize',[15])
            axis tight
            axis equal
            colorbar
            xlabel('x/d_i','fontsize',[15])
            ylabel('y/d_i','fontsize',[15])
            cmap=buildcmap('bcgwyrm');
            colormap(cmap)
            caxis([-.2 .2])
            hold on
        end
        print_lab(iplot,labelh,xp*dx,yp*dy,14)
        h1001= figure(1001)
        set(h1001,'Position', [229 218 1210 229])
        if(iplot==1)
            %pcolor(mean(Bz,3)');shading interp
            subplot(1,2,1)
            pcolor(linspace(0,Lx,Nx),linspace(0,Ly,Ny),Vex(:,:,Nz/2)');shading interp
            set(gca,'fontsize',[15])
            axis equal
            xlabel('x/d_i','fontsize',[15])
            ylabel('y/d_i','fontsize',[15])
            % xlim([0 Lx/2])
            %  ylim([Ly/2 Ly])
            xlim([5 11])
            ylim([10. 13])
            cmap=buildcmap('bcgwyrm');
            colormap(cmap)
            colorbar
            caxis([-.2 .2])
            hold on
            subplot(1,2,2)
            pcolor(linspace(0,Lx,Nx),linspace(0,Ly,Ny),Vex(:,:,Nz/2)');shading interp
            set(gca,'fontsize',[15])
            axis equal
            xlabel('x/d_i','fontsize',[15])
            ylabel('y/d_i','fontsize',[15])
            %     xlim([Lx/2 Lx])
            %     ylim([0 Ly/2])
            xlim([31 37])
            ylim([2.5 5.5])
            colorbar
            caxis([-.2 .2])
            
            hold on
        end
        subplot(1,2,1)
        if(xp*dx<Lx/2 && yp*dy>Ly/2)
            print_lab(iplot,labelh,xp*dx,yp*dy,12)
        end
        subplot(1,2,2)
        if(xp*dx>Lx/2 && yp*dy<Ly/2)
            print_lab(iplot,labelh,xp*dx,yp*dy,12)
        end
        pause(.1)
    end
end
end

squashing_matlab = false;
if(squashing_matlab)
    [npos, dummy]=size(lpos);
    figure(1000)
    for ix=1:npos
        [Qp(ix),Qm(ix)]=squashing(lpos(ix,1),lpos(ix,2),lpos(ix,3),dx,dy,dz,Bx,By,Bz);
        pause(.1)
    end
    figure(1000)
    for ix=1:npos
        [Qprf(ix),Qmrf(ix)]=squashing(lpos(ix,1),lpos(ix,2)+1,lpos(ix,3)+1,dx,dy,dz,Bx,By,Bz);
        pause(.1)
    end
    
    
    [npos, dummy]=size(lpos);
    figure(1000)
    for ix=1:npos
        [pb(ix)]=potalongb(lpos(ix,1),lpos(ix,2),lpos(ix,3),dx,dy,dz,Bx,By,Bz,Ex,Ey,Ez);
        pause(.1)
    end
    figure(1000)
    for ix=1:npos
        [pbde(ix)]=potalongb(lpos(ix,1),lpos(ix,2)+1,lpos(ix,3)+1,dx,dy,dz,Bx,By,Bz,Ex,Ey,Ez);
        pause(.1)
    end
end

ftr=[];
fr=[];
fr_je=[];
fr_jdotE=[];
fr_jedotE=[];
ftot=[];
for itr=-1:.1:5
    tr=10^itr;
    ftr=[ftr;tr];
    tot=sum(vE(:)<tr);
    ftot=[ftot;tot/Nx/Ny/Nz];
    ii= vE(:)<tr;
    fr=[fr;sum(aag(ii)>.2)./tot];
    fr_jdotE=[fr_jdotE;sum(JdotE(ii))./tot];
    fr_jedotE=[fr_jedotE;sum(JedotE(ii))./tot];
    fr_je=[fr_je;sum(je(ii))./tot];
end
figure(2000)
semilogx(ftr,ftot,ftr,fr)
legend('V_L<V','[A>.2|V_L<V]')
xlabel('V','fontsize',[15])
ylabel('Probability','fontsize',[15])
set(gca,'fontsize',[15])
print('-dpng','-r600','figure2000.png')


figure(2001)
yyaxis left
semilogx(ftr,ftot)
ylabel('Probability','fontsize',[15])
yyaxis right
semilogx(ftr,fr_je)
%legend('V_L<V','\sum_{V_L<V} j\cdot E')
xlabel('V','fontsize',[15])
name=ylabel('$\langle |je|^2 \rangle_{V_L<V}$','fontsize',[15])
set(name,'Interpreter','latex');
set(gca,'fontsize',[15])
print('-dpng','-r600','figure2001.png')


figure(2002)
yyaxis left
semilogx(ftr,ftot)
ylabel('Probability','fontsize',[15])
yyaxis right
semilogx(ftr,fr_jdotE)
hold on
semilogx(ftr,fr_jedotE)
nn=legend('Probability','$\langle {\bf j} \cdot {\bf E}\rangle_{V_L<V}$', '$\langle {\bf j}_e \cdot {\bf E}\rangle_{V_L<V}$')
xlabel('V','fontsize',[15])
name=ylabel('$ {\bf j} \cdot {\bf E}$','fontsize',[15])
set(nn,'Interpreter','latex');
set(name,'Interpreter','latex');
set(gca,'fontsize',[15])
print('-dpng','-r600','figure2002.png')


figure(2003)
yyaxis left
semilogx(ftr,ftot)
ylabel('Probability','fontsize',[15])
yyaxis right
semilogx(ftr,fr_jedotE)
%legend('V_L<V','\sum_{V_L<V} j\cdot E')
xlabel('V','fontsize',[15])
name=ylabel('$\langle {\bf j}_e \cdot {\bf E}\rangle_{V_L<V}$','fontsize',[15])
set(name,'Interpreter','latex');
set(gca,'fontsize',[15])
print('-dpng','-r600','figure2003.png')

for k=1:length(lpos)
    %fprintf('%i & %c & %8.2f & %8.2f & %8.2f & %8.2f \\\\ \n', k,  ltype(k), lpos(k,1), lpos(k,2), lpos(k,3), sign(jeig(k,1)*jeig(k,2)) )
    fprintf('%i & %c & %8.2f & %8.2f & %8.2f & %8.2f & %8.2f \\\\ \n', k,  ltype(k), lpos(k,1), lpos(k,2), lpos(k,3), Bsc2(k), Bsc1(k)*di )
end

save('3dnulls.mat','ltype', 'lpos', 'Bsc2')

num_S=sum(ltype=='S')
num_C=sum(ltype=='C')
num_A=sum(ltype=='A')
num_R=sum(ltype=='R')
num_P=sum(ltype=='P')
num_N=sum(ltype=='N')

figure(1000)
print('-dpng','-r600','figure1000.png')
figure(1001)
print('-dpng','-r600','figure1001.png')

function [perp1x, perp1y,perp1z,  perp2x, perp2y, perp2z] = perp_plane_david(Bx, By, Bz, Jex, Jey, Jez)
B2D=sqrt(Bx.^2+By.^2);
B=sqrt(Bx.^2+By.^2+Bz.^2);

perp2x=Bz.*Bx./(B.*B2D);
perp2y=Bz.*By./(B.*B2D);
perp2z=-B2D./B;


perp1x = By./B2D;
perp1y = -Bx./B2D;
perp1z = 0;

end

function [perp1x, perp1y,perp1z,  perp2x, perp2y, perp2z] = perp_plane(Bx, By, Bz, Jex, Jey, Jez)
B2=Bx.^2+By.^2+Bz.^2;
B=sqrt(B2);


JdotB = Jex.*Bx+ Jey.*By +Jez.*Bz;
perp1x = Jex - Bx .*JdotB./B2 ;
perp1y = Jey - By .*JdotB./B2 ;
perp1z = Jez - Bz .*JdotB./B2 ;

perp1 = sqrt(perp1x.^2+perp1y.^2+perp1z.^2);
perp1x = perp1x ./ perp1;
perp1y = perp1y ./ perp1;
perp1z = perp1z ./ perp1;

[perp2x,perp2y,perp2z]=cross_prod(Bx./B,By./B,Bz./B,perp1x,perp1y,perp1z)

end

function print_lab(iplot,labelh,xp,yp,fsz)
str=[labelh '_{' num2str(iplot) '}']
T = text(xp,yp,str)
if(labelh=='S')
    set(T,'Color',[0.3010, 0.7450, 0.9330]) %[0 0 0])
end
if(labelh=='C')
    set(T,'Color',[0.6350, 0.0780, 0.1840]	)%[1 0 0])
end
if(labelh=='R')
    set(T,'Color',[0.25, 0.25, 0.25])%[1 1 1])
end
if(labelh=='A')
    set(T,'Color',[0.4940, 0.1840, 0.5560]) %[0 0 0])
end
if(labelh=='N')
    set(T,'Color',[0.8500, 0.3250, 0.0980]	)%[1 0 0])
end
if(labelh=='P')
    set(T,'Color',[0, 0.4470, 0.7410])%[1 1 1])
end
set(T,'fontsize',fsz)
end
