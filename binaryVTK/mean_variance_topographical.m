% dir='tred86'
% %dirtop=['~/Desktop/outflow/' dir]
% dirtop=['./' dir]
% load([dirtop '/' dir '_medie.mat']);
dir=sim_name
load([dir '/' dir '_medie.mat']);


xrange=.5*(xrange(2:end)+xrange(1:end-1));

figure(1)
plot(xrange,divS_mean,xrange,JiE_mean+JeE_mean,xrange,dBdt_mean,xrange,divS_mean+JiE_mean+JeE_mean+dBdt_mean)
legend('divS','JE','dBdt','balance','location','South')


%plot(xrange,divS_std,xrange,JiE_std+JeE_std,xrange,dBdt_std,xrange,divS_std+JiE_std+JeE_std+dBdt_std)
%legend('divS','JE','dBdt','balance')

close all
h=figure(2)
set(h,'Position', [24 138 945 480])
subplot(1,2,1)
plot(xrange,divS_mean,xrange,JiE_mean,xrange,JeE_mean,xrange,dBdt_mean,xrange,divQe_mean,xrange,divQi_mean) %xrange,divS_mean+JiE_mean+JeE_mean+dBdt_mean,
%legend('divS','Ji.E','Je.E','dW_Bdt','divQ_e','divQ_i', 'location','South') %,'balance'
grid on
xlabel('\Psi','fontsize',14)
ylabel('Mean','fontsize',14)
set(gca,'fontsize',14)
xlim([xrange(5),xrange(end-5)])
subplot(1,2,2)
plot(xrange,divS_std,xrange,JiE_std,xrange,JeE_std,xrange,dBdt_std,xrange,divQe_std,xrange,divQi_std)
legend('divS','Ji.E','Je.E','dW_Bdt','divQ_e','divQ_i','location','SouthEast','fontsize',10)
grid on
xlabel('\Psi','fontsize',14)
ylabel('STD','fontsize',14)
set(gca,'fontsize',14)
xlim([xrange(5),xrange(end-5)])

print([dir '/mean_std_topografic.png'],'-dpng','-r300')
close all

figure(3)
subplot(1,2,1)
plot(xrange,divS_mean,xrange,JiE_mean,xrange,JeE_mean,xrange,dBdt_mean,xrange,divS_mean+JiE_mean+JeE_mean+dBdt_mean,xrange,divQi_mean,xrange,divQe_mean)
legend('divS','JiE','JeE','dBdt','balance','divQi','divQe', 'location','SouthWest')
grid on

subplot(1,2,2)
plot(xrange,divS_std,xrange,JiE_std,xrange,JeE_std,xrange,dBdt_std,xrange,divQi_std,xrange,divQe_std)
legend('divS','JiE','JeE','dBdt','divQi','divQe','location','NorthWest')
grid on
close all

h=figure(4)
set(h,'Position', [24 138 945 480])
subplot(1,2,1)
semilogy(xrange,beta_i_mean,'--',xrange,beta_e_mean)
legend('\beta_i','\beta_e', 'location','SouthEast')
grid on
xlabel('\Phi','fontsize',15)
ylabel('<\beta>','fontsize',15)
set(gca,'fontsize',15)
subplot(1,2,2)
semilogy(xrange,beta_i_std,'--',xrange,beta_e_std)
% legend('\beta_i','\beta_e', 'location','NorthWest')
grid on
xlabel('\Phi','fontsize',15)
ylabel('\beta_{STD}','fontsize',15)
set(gca,'fontsize',15)
print([dir '/mean_std_beta_topografic.png'],'-dpng','-r300')
close all
