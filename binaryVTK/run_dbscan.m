close all
addpath(genpath('~/Matlab/matanak/'));


[cmap]=buildcmap('kbcwgyr');
colormap(cmap)

addpath(genpath('..'));

leggi=true;
if(leggi)
dir ='~/Desktop/tred65/';
cycle = 18000;
[Jex, Jey, Jez,Nx,Ny,Nz] = read_binVTK_vector(dir,'Je',cycle);
[Ex, Ey, Ez,Nx,Ny,Nz] = read_binVTK_vector(dir,'E',cycle);
JedotE=Ex.*Jex+Ey.*Jey+Ez.*Jez;
[agyro,Nx,Ny,Nz] = read_binVTK_scalar(dir,'Agyro',cycle,0);
clear rhoe
Je=sqrt(Jex.^2+Jey.^2+Jez.^2);
clear Jex Jey Jez 
[Jix, Jiy, Jiz,Nx,Ny,Nz] = read_binVTK_vector(dir,'Ji',cycle);
JidotE=Ex.*Jix+Ey.*Jiy+Ez.*Jiz;
clear Jix Jiy Jiz Ex Ey Ez
end


Lx = 40;
Ly = 15;
Lz = 10;
dx = Lx/Nx;
dy = Ly/Ny;
dz = Lz/Nz;

di=1/dx
de=1/dx/sqrt(256)


Jehalf=Je(1:round(Nx/2),1:round(Ny/2),1:Nz);
agyro_half=agyro(1:round(Nx/2),1:round(Ny/2),1:Nz);
JedotE_half=JedotE(1:round(Nx/2),1:round(Ny/2),1:Nz);
JidotE_half=JidotE(1:round(Nx/2),1:round(Ny/2),1:Nz);


Je_max=max(Jehalf(:).^2);
Jsat = tanh(Jehalf.^2./Je_max*10);

is=Jsat>.8;
    
[Nx,Ny,Nz]=size(Jehalf);
    
    
[i,j,k]=ndgrid(1:Nx,1:Ny,1:Nz);

figure(1)
imagesc(Jsat(:,:,1)'); colorbar
% hold on
% plot3(i(is),j(is),k(is),'k.','markersize',.1)

X=[i(is),j(is),k(is)];

figure(2)
imagesc(Jsat(:,:,1)'); colorbar
hold on
plot(X(:,1),X(:,2),'.','markersize',.1)

%Extract only region in a cone around the x-axis
%ilim = X(:,1).^2<X(:,2).^2*2;
%size(X)
%X=X(ilim,:);
%size(X)


    % old values
        epsilon = 5;
        minpts = 5;
    % new values
%     minpts = 2*2; % 2 times the dimensionality of the space
%     epsilon = 1/dx/sqrt(mass_ratio) % electron skin depth
%     epsilon = max(epsilon, 5);
% 
   %idx=dbscan(X,epsilon,minpts);
   
   Np=max(size(X))
   disp('calling DBscan')
   [idx, corepts] = dbscan(X(1:Np,:),epsilon,minpts);
   disp(['Number of Clusters = ', num2str(max(idx))])
   Y=X(idx>3,:);
   Npy=max(size(Y));
   idy=idx(idx>3);
   figure(100)
   scatter3(Y(:,1),Y(:,2),Y(:,3),ones(Npy,1),idy,'filled')
   axis equal
   
   figure(1001)
gscatter(X(1:Np,1),X(1:Np,2),idx)
axis equal
figure(1002)
gscatter(Y(:,1),Y(:,2),idy)
axis equal
figure(1003)
gscatter(Y(:,1),Y(:,3),idy)
axis equal
figure(1004)
gscatter(Y(:,2),Y(:,3),idy)
axis equal


nbin = max(idx)
hbin=[];
hcov=[];
sizebin=[];
for ib=1:nbin
    xp=X(idx==ib,1);
    yp=X(idx==ib,2);
    zp=X(idx==ib,3);
    ap_mean=0;np=max(size(xp));
    jedotE_mean=0;
    jidotE_mean=0;
    for ip= 1:np
    ap_mean=agyro_half(xp(ip),yp(ip),zp(ip));
    jedotE_mean=JedotE_half(xp(ip),yp(ip),zp(ip));
    jidotE_mean=JidotE_half(xp(ip),yp(ip),zp(ip));
    end
    ap_mean=ap_mean/np;
    sizebin(ib)=max(size(xp));
    hbin(ib,:)=[mean(xp) mean(yp) mean(zp) std(xp) std(yp) std(zp) ap_mean jedotE_mean jidotE_mean];
    hcov(ib,:)=eig(cov(xp,yp))';
end  

figure(102)
scatter(hbin(:,4),hbin(:,5),log10(sizebin)*100,hbin(:,7),'filled')
hold on 
plot(hbin(:,4),hbin(:,4),'k')
%xlim([0 15])
%ylim([0 15])
colormap(cmap)
set(gca,'xscale','log')
set(gca,'yscale','log')
xline(di,'-.','di')
xline(de,'-.','de')
yline(di,'-.','di')
yline(de,'-.','de')
xlabel('\sigma_x','fontsize',20)
ylabel('\sigma_y','fontsize',20)
set(gca,'fontsize',15)
colorbar
print('-dpng','current_scales_xy.png')


figure(103)
scatter(hbin(:,4),hbin(:,6),log10(sizebin)*100,hbin(:,7),'filled')
hold on 
plot(hbin(:,4),hbin(:,4),'k')
%xlim([0 15])
%ylim([0 15])
colormap(cmap)
set(gca,'xscale','log')
set(gca,'yscale','log')
xline(di,'-.','di')
xline(de,'-.','de')
yline(di,'-.','di')
yline(de,'-.','de')
xlabel('\sigma_x','fontsize',20)
ylabel('\sigma_z','fontsize',20)
set(gca,'fontsize',15)
colorbar
print('-dpng','current_scales_agyro_xz.png')


figure(104)
scatter(hbin(:,4),hbin(:,5),log10(sizebin)*100,hbin(:,8),'filled')
hold on 
plot(hbin(:,4),hbin(:,4),'k')
%xlim([0 15])
%ylim([0 15])
colormap(cmap)
set(gca,'xscale','log')
set(gca,'yscale','log')
xline(di,'-.','di')
xline(de,'-.','de')
yline(di,'-.','di')
yline(de,'-.','de')
xlabel('\sigma_x','fontsize',20)
ylabel('\sigma_y','fontsize',20)
set(gca,'fontsize',15)
colorbar
print('-dpng','current_scales_jedotE_xy.png')


figure(105)
scatter(hbin(:,4),hbin(:,6),log10(sizebin)*100,hbin(:,8),'filled')
hold on 
plot(hbin(:,4),hbin(:,4),'k')
%xlim([0 15])
%ylim([0 15])
colormap(cmap)
set(gca,'xscale','log')
set(gca,'yscale','log')
xline(di,'-.','di')
xline(de,'-.','de')
yline(di,'-.','di')
yline(de,'-.','de')
xlabel('\sigma_x','fontsize',20)
ylabel('\sigma_z','fontsize',20)
set(gca,'fontsize',15)
colorbar
caxis([-3 3]*1e-7)
[cmap]=buildcmap('cbkrm');
colormap(cmap)
print('-dpng','current_scales_jedotE_xz.png')


figure(105)
scatter(hbin(:,4),hbin(:,6),log10(sizebin)*100,hbin(:,9),'filled')
hold on 
plot(hbin(:,4),hbin(:,4),'k')
%xlim([0 15])
%ylim([0 15])
colormap(cmap)
set(gca,'xscale','log')
set(gca,'yscale','log')
xline(di,'-.','di')
xline(de,'-.','de')
yline(di,'-.','di')
yline(de,'-.','de')
xlabel('\sigma_x','fontsize',20)
ylabel('\sigma_z','fontsize',20)
set(gca,'fontsize',15)
colorbar
caxis([-1 1]*1e-7)
[cmap]=buildcmap('cbkrm');
colormap(cmap)
print('-dpng','current_scales_jidotE_xz.png')


save('june20')
