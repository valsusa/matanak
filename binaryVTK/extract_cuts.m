function [] = extract_cuts(xcut,Lx, Ly, Lz, A,titolo,bufferx,buffery,minval,maxval)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
 
bufferz = buffery
close all
h1=figure(1)
set(h1,'Position',[40 78 840 320])

Ncut=max(size(xcut));
[Nx Ny Nz] = size(A);
spx=1;spy=Ncut;
if(Ncut>2)
    spx = floor(sqrt(Ncut));
    spy = Ncut/spx;
end


%minval=min(A(round(xcut/Lx*Nx),:,3:end-2));minval=min(minval(:));
%maxval=max(A(round(xcut/Lx*Nx),:,3:end-2));maxval=max(maxval(:));
%maxval=sqrt(abs(prod([maxval,minval])))
%minval=-maxval

for ip = 1:Ncut
    if(minval==maxval)
        rescale=false
    minval=min(A(round(xcut(ip)/Lx*Nx),buffery:end-buffery,3:end-2));minval=min(minval(:));
    maxval=max(A(round(xcut(ip)/Lx*Nx),buffery:end-buffery,3:end-2));maxval=max(maxval(:));

        if(minval<0)
        maxval=sqrt(abs(prod([maxval,minval])))
        minval=-maxval;
    end 
    end
    subplot(spx, spy, ip) 
    b=squeeze( A(round(xcut(ip)/Lx*Nx),:,3:end-2));
    pcolor((bufferz:Nz-bufferz+1)/Nz*Lz, (buffery:Ny-buffery)/Ny*Ly,b(buffery:end-buffery,bufferz:Nz-bufferz+1))
    shading interp
    if(rescale) caxis([minval,maxval]); end
    colorbar
    load gist_ncar.mat
colormap(flipud(gist_ncar))
    title(['x/d_i = ', num2str(xcut(ip))])
    xlabel('z/d_i')
    ylabel('y/d_i')
end



print([titolo '_extract_cuts.png'],'-dpng')
end


