
%hfig = figure(iplot)
%set(hfig,'Position', [1 1 (387-150)*2 2000])

fntsz = 20
th=linspace(0,2*pi,60); R=2; StepMax=300;
th=linspace(0,2*pi,40); R=2; StepMax=300;
startx = R*sin(th).*rand(size(th));
starty = R*cos(th).*rand(size(th));

%nrow=4;ncol=1;
%ax(1)=subplot(nrow,ncol,1);
if(ishandle(iplot)) 
    close(iplot)
end    
ax(1)= figure(iplot)
pcolor(eta*dx/de,xi*dx/de,agp); shading interp;colorbar
colormap(ax(1),parula)
hold on
hlines_fw = streamline(eta,xi,dbeta,dbxi,startx,starty,[0.1,300]);
hlines_bw = streamline(eta,xi,-dbeta,-dbxi,startx,starty,[0.1,300]);
set(hlines_fw,'LineWidth',1,'Color','k')
set(hlines_bw,'LineWidth',1,'Color','k')
%title('a) Agyrotropy','fontsize',fntsz)
%title(['x/d_i = ' num2str(xp*dx,'%5.2f') '  y/d_i = ' num2str(yp*dy,'%5.2f') '  z/d_i = ' num2str(zp*dx,'%5.2f')])
xlabel('\xi_1/d_e','fontsize',fntsz)
ylabel('\xi_2/d_e','fontsize',fntsz)
set(gca,'fontsize',fntsz)
axis equal
xlim([min(eta(:)) max(eta(:))]*dx/de)
ylim([min(xi(:)) max(xi(:))]*dx/de)

saveas(ax(1),['1recon_site', num2str(iplot,'%06d'), '.png'],'png')
close(iplot)

%ax(2)=subplot(nrow,ncol,2);
ax(2)= figure(iplot)
pcolor(eta*dx/de,xi*dx/de,log10(LIp)); shading interp;colorbar
%pcolor(eta*dx/de,xi*dx/de,1-tanh(LIp)); shading interp;colorbar
cmap=buildcmap('rbgyw');
colormap(ax(2),cmap)
hold on
%contour(eta*dx/de,xi*dx/de,az,60,'k');
quiver(eta(1:skip:end,1:skip:end)*dx/de,xi(1:skip:end,1:skip:end)*dx/de,dbeta(1:skip:end,1:skip:end),dbxi(1:skip:end,1:skip:end),2,'k')
%title('b) Lorentz','fontsize',fntsz)
%set(get(h(2),'label'),'string','[p/cm^3]');
%title(h(2),'[p/cm^3]');
%title(['x/d_i = ' num2str(xp*dx,'%5.2f') '  y/d_i = ' num2str(yp*dy,'%5.2f') '  z/d_i = ' num2str(zp*dx,'%5.2f')])
xlabel('\xi_1/d_e')
ylabel('\xi_2/d_e','fontsize',fntsz)
set(gca,'fontsize',fntsz)
axis equal
xlim([min(eta(:)) max(eta(:))]*dx/de)
ylim([min(xi(:)) max(xi(:))]*dx/de)

saveas(ax(2),['2recon_site', num2str(iplot,'%06d'), '.png'],'png')
close(iplot)

%ax(3)=subplot(nrow,ncol,3);
ax(3)= figure(iplot)
% pcolor(eta*dx,xi*dx,dbxi.^2+dbeta.^2); shading interp; colorbar
% title('b) \delta B')
% xlabel('\eta/d_i')
% ylabel('\xi/d_i')
vele = sqrt(veta.^2+vxi.^2);
%valf = max(max(sqrt(dbeta.^2+dbxi.^2)))./sqrt(-rhoep*4*pi);
valf = mean(sqrt(bxp.^2+byp.^2+bzp.^2),'all')./sqrt(-rhoep*4*pi);
%pcolor(eta*dx/de,xi*dx/de,-rhoep*code_nSI*1e-6); shading interp;h(2)=colorbar
pcolor(eta*dx/de,xi*dx/de,vele./valf); shading interp;h(2)=colorbar
colormap(ax(3),parula)
hold on
%contour(eta*dx/de,xi*dx/de,az,60,'k');
quiver(eta(1:skip:end,1:skip:end)*dx/de,xi(1:skip:end,1:skip:end)*dx/de,veta(1:skip:end,1:skip:end),vxi(1:skip:end,1:skip:end),2,'k')
%title('c) V_e/V_A','fontsize',fntsz)
xlabel('\xi_1/d_e','fontsize',fntsz)
ylabel('\xi_2/d_e','fontsize',fntsz)
set(gca,'fontsize',fntsz)
axis equal
xlim([min(eta(:)) max(eta(:))]*dx/de)
ylim([min(xi(:)) max(xi(:))]*dx/de)

saveas(ax(3),['3recon_site', num2str(iplot,'%06d'), '.png'],'png')
close(iplot)

%ax(4)=subplot(nrow,ncol,4);
ax(4)=figure(iplot);
% pcolor(eta*dx,xi*dx,dbxi.^2+dbeta.^2); shading interp; colorbar
% title('b) \delta B')
% xlabel('\eta/d_i')
% ylabel('\xi/d_i')
B0=max(sqrt(dbxi(:).^2+dbeta(:).^2));
va=B0./sqrt(-4*pi*max(rhoep(:)));
%pcolor(eta*dx/de,xi*dx/de,Emod./va/B0); shading interp;colorbar
pcolor(eta*dx/de,xi*dx/de,Emod*code_ESI*1e3); shading interp; h(4)=colorbar
%caxis([-5 5]*1e-7)
%cmap=buildcmap('bcgwyrm');
%colormap(ax(4),cmap)
colormap(ax(4),parula)
hold on
%contour(eta*dx/de,xi*dx/de,az,60,'k');
quiver(eta(1:skip:end,1:skip:end)*dx/de,xi(1:skip:end,1:skip:end)*dx/de, ...
    veta(1:skip:end,1:skip:end)-vieta(1:skip:end,1:skip:end),vxi(1:skip:end,1:skip:end)-vixi(1:skip:end,1:skip:end),2,'k')
%title('d) E_{\xi_3}','fontsize',fntsz)
title(h(4),'[mV/m]','fontsize',fntsz)
xlabel('\xi_1/d_e','fontsize',fntsz)
ylabel('\xi_2/d_e','fontsize',fntsz)
set(gca,'fontsize',fntsz)
axis equal
xlim([min(eta(:)) max(eta(:))]*dx/de)
ylim([min(xi(:)) max(xi(:))]*dx/de)

saveas(ax(4),['4recon_site', num2str(iplot,'%06d'), '.png'],'png')
close(ax(4))

[g11 g12] = gradient(dbeta,.2*dx);
[g21 g22] = gradient(dbxi,.2*dx);
Jac=[g11(ihctr,jhctr) g12(ihctr,jhctr); g21(ihctr,jhctr) g22(ihctr,jhctr)];
lj=eig(Jac);
jeig = [jeig ; lj' trace(Jac) (trace(Jac))/norm(Jac) Jac(2,1)*Jac(1,2) Jac(2,1) Jac(1,2)];

scale = mean(sqrt(bxp.^2+byp.^2+bzp.^2),'all')/mean(sqrt(g11.^2+g12.^2+g21.^2+g22.^2),'all');
scale2 = mean(sqrt(bxp.^2+byp.^2+bzp.^2),'all')/Bref;

Bsc1 =[Bsc1; scale]; 
Bsc2 =[Bsc2; scale2];

% if (abs(imag(lj(1)))>0)
%     disp('o-point')
%     labelh='o'
% elseif (lj(1)*lj(2)<0)
%     disp('x-point')
%     labelh='x'
% else
%     disp('3D-point')
%     labelh='*'
% end    
if(abs(real(lj(1)))<1e-10 && abs(real(lj(2)))<1e-10)
   disp('center')
   labelh='C'
elseif (real(lj(1))*real(lj(2))<0 && abs(imag(lj(1)))<1e-10)
   disp('saddle')
   labelh='S'   
elseif (real(lj(1))>0 && real(lj(2))>0 && abs(imag(lj(1)))<1e-10)
   disp('repelling')
   labelh='R'   
elseif (real(lj(1))<0 && real(lj(2))<0 && imag(lj(1))>1e-10)
   disp('attracting focus')
   labelh='N'  
elseif (real(lj(1))<0 && real(lj(2))<0 && abs(imag(lj(1)))<1e-10)
   disp('attracting')
   labelh='A'    
elseif (real(lj(1))>0 && real(lj(2))>0 && imag(lj(1))>1e-10)
   disp('repelling focus')
   labelh='P'     
else
  disp('this cannot happen')
  stop
end  
ltype=[ltype;labelh];

if(ishandle(1)) 
    close(1)
end 
hfig=figure(1)
sgtitle({[labelh '-point:  ' '(x,y,z)/d_i =(' num2str(Position(1),'%5.2f'),  ...
    ',' num2str(Position(2),'%5.2f'), ...
    ',' num2str(Position(3),'%5.2f'), '),' 'v_E/c=' num2str(vf*vf','%5.2f') ] ...
    %['v_E/c=' num2str(vf*vf','%5.2f') '  ||B||/||\nabla B||='  num2str(scale,'%5.2f') , '  ||B||/Bmax='  num2str(scale2,'%5.2f')]},'fontsize',fntsz)
    ['\lambda_J=(' num2str(lj(1),'%5.2e') ',' num2str(lj(2),'%5.2e') ')']},'fontsize',10)
  
saveas(hfig,['Trecon_site', num2str(iplot,'%06d'), '.eps'],'eps')
