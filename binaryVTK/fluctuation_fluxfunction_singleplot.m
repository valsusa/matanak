close all
%clear all
!rm *.png
addpath(genpath('~/matanak'));

global bufferx buffery 

for isim=1:1
 list_sim="tred81_ecsim";
%list_sim=["tred81_ecsim";  "tred81_ecsim_first"   ]
list_sim=["tred60" ;"tred81"; "tred82"; "tred86"]
%list_sim="7feb09"
list_sim="tred86"
%list_sim=["tred81"; "tred82"; "tred86"]
sim_name=char(list_sim(isim))
unix('rm *.png')

must_read=true;
leggo='h5';

fluctuations = true
structure = false
spettro = false
slices = true
means = true

electrons = 0;
radius=0.01;


if(must_read)



switch sim_name
case 'tred60'
TRED60;
leggo='vtk'
case_name='GEM';
cycle = 18000;
%cycle = 20010;
zcode = Lz/2;
case 'tred65'
tred65;
leggo='vtk'
%case_name='GEM';
cycle = 18000;
%cycle = 20010;
zcode = Lz/2;
case 'tred77'
TRED77;
case_name='GEM';
cycle = 15000;
zcode = Lz/2;
    case 'tred80'
tred80;
case_name='GEM';
cycle = 18000;
zcode = Lz/2;
case 'tred81'
tred81;
case_name='GEM';
cycle = 18000;
zcode = Lz/2;
case 'tred81_ecsim'
tred81ecsim;
case_name='GEM';
cycle = 16000;% to be used only for tred81_Ecsim
cycle=8000;% to be used only for tred81_Ecsim_wDiv
zcode = Lz/2;
case 'tred81_ecsim_first'
tred81ecsim_first;
case_name='GEM';
cycle = 8000;
zcode = Lz/2;
case 'tred82'
tred82;
case_name='GEM';
cycle = 20000;
zcode = Lz/2;
    case 'tred86'
tred86;
case_name='GEM';
cycle = 16000;
zcode = Lz/2;
    case 'tred88'
tred88;
case_name='GEM';
cycle = 5000;
zcode = Lz/2;
    case 'tred89'
tred89;
case_name='GEM';
cycle = 4000;
zcode = Lz/2;
    case 'tred90'
tred90;
case_name='GEM';
cycle = 18000;
zcode = Lz/2;
case 'AH'
generic;
case_name='AH';
cycle =4000;
zcode = Lz/2;
case 'HRmaha3D3'
HRmaha3D3;
leggo='gda';
    case_name='GEM';
dir='/data1/gianni/HRmaha3D3/h5/'; cycle= 80002; ncycle = num2str(cycle,'%06d');
cycle = 80002;  % for h5
%cycle = 80000  % for vtk binary
% for HRmaha3D1:
time=60*(cycle/75000.0*Dt/.125); %*4 %times four to correct for change in dt between 2D and 3D
% for HRmaha3D1.v2:
% time=60*(cycle/75000.0) *2 %times two to correct for change in dt between 2D and 3D
%ADD initial time of the RUN
time=time+initial_time; %(03*60+48)*60
case '7feb09'
FEB09;
cycle=18000
case_name='MHDUCLA'
%cycle = 80000  % for vtk binary
% for HRmaha3D1:
time=60*(cycle/75000.0*Dt/.125); %*4 %times four to correct for change in dt between 2D and 3D
% for HRmaha3D1.v2:
% time=60*(cycle/75000.0) *2 %times two to correct for change in dt between 2D and 3D
%ADD initial time of the RUN
time=time+initial_time; %(03*60+48)*60
    case 'generic3'
        generic3
        cycle=8000
        case_name='MHDUCLA'
        zcode = Lz/2;
otherwise
print('no recognised case selected')
end

% Prepare string
ntime = num2str(cycle,'%06d');
ncycle = num2str(cycle,'%06d');


import_h5_binvtk



[X Y] = meshgrid(dx/2:dx:Lx-dx/2,dy/2:dy:Ly-dy/2);

[X Y] = meshgrid(0:dx:Lx-dx,0:dy:Ly-dy);

[Sx, Sy, Sz] = cross_prod(Ex, Ey, Ez, Bx/4/pi, By/4/pi, Bz/4/pi);
S=sqrt(Sx.^2+Sy.^2+Sz.^2);

JedotE=Jex.*Ex+Jey.*Ey+Jez.*Ez;
JidotE=Jix.*Ex+Jiy.*Ey+Jiz.*Ez;

if(strcmp(leggo,'vtk'))
    [x,y,z]=meshgrid(0:dx:Lx-dx,0:dy:Ly-dy,0:dz:Lz-dz);
else    
    
    [x,y,z]=meshgrid(0:dx:Lx,0:dy:Ly,0:dz:Lz);
end    


[dBx,dBy,dBz]=compute_curl(x,y,z,Ex,Ey,Ez);
dBdt=-(Bx.*dBx+By.*dBy+dBz.*Bz)/4/pi;

divS = compute_div(x,y,z,Sx,Sy,Sz,radius, 0.);
%radius=0.0;
%Qex=smooth3D(Qex,6);Qex=smooth3D(Qex,6);Qex=smooth3D(Qex,6);
%Qey=smooth3D(Qey,6);Qey=smooth3D(Qey,6);Qey=smooth3D(Qey,6);
%Qez=smooth3D(Qez,6);Qez=smooth3D(Qez,6);Qez=smooth3D(Qez,6);

%Qix=smooth3D(Qex,6);Qex=smooth3D(Qex,6);Qex=smooth3D(Qex,6);
%Qiy=smooth3D(Qiy,6);Qiy=smooth3D(Qiy,6);Qiy=smooth3D(Qiy,6);
%Qiz=smooth3D(Qiz,6);Qiz=smooth3D(Qiz,6);Qiz=smooth3D(Qiz,6);

divQe = compute_div(x,y,z,Qex,Qey,Qez,radius, 0.);
divQi = compute_div(x,y,z,Qix,Qiy,Qiz,radius, 0.);

beta_e=8*pi*(Pexx+Peyy+Pezz)./B.^2;
beta_i=8*pi*(Pixx+Piyy+Pizz)./B.^2;

UPex = (Jex.*Pexx + Jey.*Pexy + Jez.*Pexz)./rhoe;
UPey = (Jex.*Pexy + Jey.*Peyy + Jez.*Peyz)./rhoe;
UPez = (Jex.*Pexz + Jey.*Peyz + Jez.*Pezz)./rhoe;
UPix = (Jix.*Pixx + Jiy.*Pixy + Jiz.*Pixz)./rhoi;
UPiy = (Jix.*Pixy + Jiy.*Piyy + Jiz.*Piyz)./rhoi;
UPiz = (Jix.*Pixz + Jiy.*Piyz + Jiz.*Pizz)./rhoi;

[Uthe, Ubulke, divQbulke, divQenthe, divQhfe,  udivPe, PgradVe, ugradpe, pdivve, divUPe] = compute_energy_balance( ...
    rhoe, Jex, Jey, Jez,... 
    Qbulkex, Qbulkey, Qbulkez, Qenthex, Qenthey, Qenthez, Qhfex, Qhfey, Qhfez, ...
    Pexx, Peyy, Pezz, Pexy, Pexz, Peyz, x, y, z, dx, dy, dz, qom, radius,0.);
[Uthi, Ubulki, divQbulki, divQenthi, divQhfi,  udivPi, PgradVi, ugradpi, pdivvi, divUPi] = compute_energy_balance( ...
    rhoi, Jix, Jiy, Jiz,... 
    Qbulkix, Qbulkiy, Qbulkiz, Qenthix, Qenthiy, Qenthiz, Qhfix, Qhfiy, Qhfiz, ...
    Pixx, Piyy, Pizz, Pixy, Pixz, Piyz, x, y, z, dx, dy, dz, 1.0, radius,0.);


[JexBx, JexBy, JexBz] = cross_prod(Jex, Jey, Jez, Bx, By, Bz);


switch sim_name
    
case 'extred82'
    list_value=[-.02, -.01, 0, .005, .01, .015, .02] %tred82
 case 'extred86'
    list_value=[-.02, -.01, 0, .005, .01, .015, .02] %tred86
    case 'extred81'
    list_value=-.02:.01:.04 %tred81
    otherwise
    list_value=[];
end

bufferx = round(Nx/20);
buffery = round(Ny/20);
xc=linspace(0, Lx, Nx);
yc=linspace(0, Ly, Ny);
AAz=zeros(size(Bx));
for kr=1:Nz
AAz(:,:,kr)=vecpot(xc,yc,Bx(:,:,kr),By(:,:,kr));
AAz(:,:,kr)=AAz(:,:,kr)-AAz(round(Nx/2),round(Ny/2),kr);
end

differ=true
switch sim_name
    case {'7feb09','generic3','tred81','tred82','tred86'}
    disp('AAz replaced by Jix')
    AAz=abs(Bx);
    AAz=Bx.^2+By.^2;
    AAz=beta_e+beta_i;
    differ=false
    otherwise
        %do nothing
end

figure(1)
imagesc(xc,yc,mean(AAz,3)')
imagesc(xc,yc,squeeze(AAz(:,:,round(Nz/2)))')
%contourf(xc,yc,mean(AAz,3)',20)
%hold on; contour(xc,yc,mean(AAz,3)',list_value,'m','linewidth',2)
load cm_new; colormap(cm_kbwrk)
load gist_ncar; colormap(gist_ncar)
%colormap default
colorbar
xlabel('x')
ylabel('y')
cmax=max(max(abs(mean(AAz,3))));
%caxis([-cmax cmax])
caxis([0 cmax])
xlim([5 Lx-5])
axis equal 
axis tight
print('-dpng','-r300',[ncycle '_Phi'])

% 
% [X,Y,Z]=ndgrid(1:Nx,1:Ny,1:Nz);
% figure
% plot3(AAz(:),Y(:),S(:),'.')
% figure
% plot(AAz(:),S(:),'.')

colormap hsv
close all

end

if (fluctuations)
    [xrange,rhoe2_mean,rhoe2_std]=figura(AAz,rhoe.^2,2,'rhoe2',ncycle, differ, list_value);
[xrange,rhoi2_mean,rhoi2_std]=figura(AAz,rhoi.^2,2,'rhoi2',ncycle, differ, list_value);
[xrange,rhoe_mean,rhoe_std]=figura(AAz,rhoe,2,'rhoe',ncycle, differ, list_value);
[xrange,rhoi_mean,rhoi_std]=figura(AAz,rhoi,2,'rhoi',ncycle, differ, list_value);
[xrange,Ve2_mean,Ve2_std]=figura(AAz,Ve.^2,2,'Ve2',ncycle, differ, list_value);
[xrange,Vi2_mean,Vi2_std]=figura(AAz,Vi.^2,2,'Vi2',ncycle, differ, list_value);
[xrange,S_mean,S_std]=figura(AAz,S,2,'S',ncycle, differ, list_value);
[xrange,divS_mean,divS_std]=figura(AAz,divS,2,'divS',ncycle, differ, list_value);
[xrange,divQe_mean,divQe_std]=figura(AAz,divQe,2,'divQe',ncycle, differ, list_value);
[xrange,divQi_mean,divQi_std]=figura(AAz,divQi,2,'divQi',ncycle, differ, list_value);
[xrange,B2_mean,B2_std]=figura(AAz,(Bx.*Bx+By.*By+Bz.*Bz)/8/pi,2,'B2',ncycle, differ, list_value);
[xrange,B_mean,B_std]=figura(AAz,sqrt(Bx.*Bx+By.*By+Bz.*Bz),2,'B',ncycle, differ, list_value);
[xrange,E2_mean,E2_std]=figura(AAz,(Ex.*Ex+Ey.*Ey+Ez.*Ez)/8/pi,2,'E2',ncycle, differ, list_value);

[xrange,dBdt_mean,dBdt_std]=figura(AAz,dBdt,2,'dBdt',ncycle, differ, list_value);
[xrange,Ubulke_mean,Ubulke_std]=figura(AAz,0.5*(Jex.^2+Jey.^2+Jez.^2)./rhoe/qom,2,'Ubulke',ncycle, differ, list_value);
[xrange,Uthe_mean,uthe_std]=figura(AAz,0.5*(Pexx+Peyy+Pezz),2,'Uthe',ncycle, differ, list_value);
[xrange,Ubulki_mean,Ubulki_std]=figura(AAz,0.5*(Jix.^2+Jiy.^2+Jiz.^2)./rhoi,2,'Ubulki',ncycle, differ, list_value);
[xrange,Uthi_mean,Uthi_std]=figura(AAz,0.5*(Pixx+Piyy+Pizz),2,'Uthi',ncycle, differ, list_value);
%figura(AAz,log10(S),3,'Slog',ncycle, differ, list_value)
[xrange,Sx_mean,Sx_std]=figura(AAz,Sx,4,'Sx',ncycle, differ, list_value);
[xrange,Sy_mean,Sy_std]=figura(AAz,Sy,5,'Sy',ncycle, differ, list_value);
[xrange,Sz_mean,Sz_std]=figura(AAz,Sz,6,'Sz',ncycle, differ, list_value);
[xrange,Qex_mean,Qex_std]=figura(AAz,Qex,4,'Qex',ncycle, differ, list_value);
[xrange,Qey_mean,Qey_std]=figura(AAz,Qey,4,'Qey',ncycle, differ, list_value);
[xrange,Qez_mean,Qez_std]=figura(AAz,Qez,4,'Qez',ncycle, differ, list_value);
[xrange,Qix_mean,Qix_std]=figura(AAz,Qix,4,'Qix',ncycle, differ, list_value);
[xrange,Qiy_mean,Qiy_std]=figura(AAz,Qiy,4,'Qiy',ncycle, differ, list_value);
[xrange,Qiz_mean,Qiz_std]=figura(AAz,Qiz,4,'Qiz',ncycle, differ, list_value);
%figura(AAz,UPex,4,'uPex',ncycle, differ, list_value)
%figura(AAz,UPey,4,'uPey',ncycle, differ, list_value)
%figura(AAz,UPez,4,'uPez',ncycle, differ, list_value)
%figura(AAz,UPix,4,'uPix',ncycle, differ, list_value)
%figura(AAz,UPiy,4,'uPiy',ncycle, differ, list_value)
%figura(AAz,UPiz,4,'uPiz',ncycle, differ, list_value)

[xrange,beta_e_mean,beta_e_std]=figura(AAz,beta_e,4,'beta_e',ncycle, differ, list_value);
[xrange,beta_i_mean,beta_i_std]=figura(AAz,beta_i,4,'beta_i',ncycle, differ, list_value);

[xrange,pthetae_mean,pthetae_std]=figura(AAz,pdivve,4,'pthetae',ncycle, differ, list_value);
[xrange,pthetai_mean,pthetai_std]=figura(AAz,pdivvi,4,'pthetai',ncycle, differ, list_value);
[xrange,PiDe_mean,PiDe_std]=figura(AAz,PgradVe-pdivve,4,'PiDe',ncycle, differ, list_value);
[xrange,PiDi_mean,PiDi_std]=figura(AAz,PgradVi-pdivvi,4,'PiDi',ncycle, differ, list_value);

[xrange,JeE_mean,JeE_std]=figura(AAz,JedotE,4,'JeE',ncycle, differ, list_value);
[xrange,JiE_mean,JiE_std]=figura(AAz,JidotE,4,'JiE',ncycle, differ, list_value);

        
        Epx = Ex + (Jey.*Bz - Jez.*By)./rhoe;
        Epy = Ey + (Jez.*Bx - Jex.*Bz)./rhoe;
        Epz = Ez + (Jex.*By - Jey.*Bx)./rhoe;
        
        JdotEp=(Jex+Jix).*Epx + (Jey+Jiy).*Epy + (Jez+Jiz).*Epz;
[xrange,JEp_mean,JEp_std]=figura(AAz,JdotEp,4,'JEp',ncycle, differ, list_value);

save([sim_name '_medie.mat'],'xrange','-regexp','_mean$','_std$')
end
%
%   Structure function calculation - still experimental
%

if(structure)
xflow='inflow'
   structure_function
xflow='outflow'
   structure_function
end
%
% Spectrum along z
%
   %vthe_loc = sqrt((Pexx+Peyy+Pezz)/3./rhoe.*qom);
   %vthi_loc = sqrt((Pixx+Piyy+Pizz)/3./rhoi);
 if(spettro)  
     close all
   spettro_alongz(x,y,z,Ex,Ey,Ez, Bx, By, Bz, Nx, Ny, Nz,Lx,Ly,Lz,qom,vthi,vthe,B0*ones(size(B)))
 end 
 if (slices) 
     close all
     JedotE=Jex.*Ex+Jey.*Ey+Jez.*Ez;
JidotE=Jix.*Ex+Jiy.*Ey+Jiz.*Ez;
        Epx = Ex + (Jey.*Bz - Jez.*By)./rhoe;
        Epy = Ey + (Jez.*Bx - Jex.*Bz)./rhoe;
        Epz = Ez + (Jex.*By - Jey.*Bx)./rhoe;
        
        JdotEp=(Jex+Jix).*Epx + (Jey+Jiy).*Epy + (Jez+Jiz).*Epz;
%         extract_cuts([8, 20],Lx,Ly, Lz,log10(beta_e+beta_i),'beta_log',bufferx,buffery,0, 0, true, 1)
%      extract_cuts([8, 20],Lx,Ly, Lz,JedotE,'JeE',bufferx,buffery,0, 0, true, 1)
%      extract_cuts([8, 20],Lx,Ly, Lz,JidotE,'JiE',bufferx,buffery,0, 0, true, 1)
%      extract_cuts([8, 20],Lx,Ly, Lz,JdotEp,'JdotEp',bufferx,buffery,0, 0, true, 1)
%      extract_cuts([8, 20],Lx,Ly, Lz,dBdt,'dW_Bdt',bufferx,buffery,0, 0, true, 1)
%    extract_cuts([8, 20],Lx,Ly, Lz,divS,'divS',bufferx,buffery,0, 0, true, 1)
%    extract_cuts([8, 20],Lx,Ly, Lz,divQi,'divQi',bufferx,buffery,0,0)
%    extract_cuts([8, 20],Lx,Ly, Lz,divQe,'divQe',bufferx,buffery,0,0)
%    extract_cuts([8, 20],Lx,Ly, Lz,sqrt(Jex.^2+Jey.^2+Jez.^2),'Je',bufferx,buffery,0,2.5e-3*0)
%    extract_cuts_maxp(Lx,Ly, Lz,log10(beta_e+beta_i),Pipar,'beta_log',bufferx,buffery,0, 0, true, 1)
   extract_cuts_maxp(Lx,Ly, Lz,divS,Pipar,'divS',bufferx,buffery,0, 0, 1, 1)
   extract_cuts_maxp(Lx,Ly, Lz,JedotE,Pipar,'JedotE',bufferx,buffery,0, 0, 1, 1)
   extract_cuts_maxp(Lx,Ly, Lz,JidotE,Pipar,'JidotE',bufferx,buffery,0, 0, 1, 1)
   extract_cuts_maxp(Lx,Ly, Lz,JdotEp,Pipar,'JdotEp',bufferx,buffery,0, 0, 1, 1)
   extract_cuts_maxp(Lx,Ly, Lz,dBdt,Pipar,'dWBdt',bufferx,buffery,0, 0, 1, 1)
   extract_cuts_maxp(Lx,Ly, Lz,divQi,Pipar,'divQi',bufferx,buffery,0,0, 1, 1)
   extract_cuts_maxp(Lx,Ly, Lz,divQe,Pipar,'divQe',bufferx,buffery,0,0, 1, 1)   
   extract_cuts_maxp(Lx,Ly, Lz,divQhfi,Pipar,'divQhfi',bufferx,buffery,0,0, 1, 1)
   extract_cuts_maxp(Lx,Ly, Lz,divQhfe,Pipar,'divQhfe',bufferx,buffery,0,0, 1, 1)
   extract_cuts_maxp(Lx,Ly, Lz,5/3*pdivve,Pipar,'pthetae53',bufferx,buffery,0,0, 1, 1)
   extract_cuts_maxp(Lx,Ly, Lz,5/3*pdivvi,Pipar,'pthetai53',bufferx,buffery,0,0, 1, 1)
   extract_cuts_maxp(Lx,Ly, Lz,PgradVe-pdivve,Pipar,'PiDe',bufferx,buffery,0,0, 1, 1)
   extract_cuts_maxp(Lx,Ly, Lz,PgradVi-pdivvi,Pipar,'PiDi',bufferx,buffery,0,0, 1, 1)
   extract_cuts_maxp(Lx,Ly, Lz,PgradVe,Pipar,'PgradVe',bufferx,buffery,0,0, 1, 1)
   extract_cuts_maxp(Lx,Ly, Lz,PgradVi,Pipar,'PgradVi',bufferx,buffery,0,0, 1, 1)
   extract_cuts_maxp(Lx,Ly, Lz,sqrt(Jex.^2+Jey.^2+Jez.^2),Pipar,'Je',bufferx,buffery,0,0,-1, 5)
   extract_cuts_maxp(Lx,Ly, Lz,Jex,Pipar,'Jex',bufferx,buffery,0,0, 1, 1)
   extract_cuts_maxp(Lx,Ly, Lz,sqrt(Jix.^2+Jiy.^2+Jiz.^2),Pipar,'Ji',bufferx,buffery,0,0, -1, 5)
extract_cuts_maxp(Lx,Ly, Lz,Jix,Pipar,'Jix',bufferx,buffery,0,0, 1, 1)

 end
 
 unix(['mkdir ' sim_name])
 unix(['mv *.png ' sim_name])
 unix(['mv *.mat ' sim_name])
 if (means)
     mean_variance_topographical;
 end    
end 
 
function [xrange,pmean_dir,pstd_dir] = figura(a1,p1,n,name,prename,differ, list_value)
% MYMEAN Example of a local function.
close all
global bufferx buffery 

a=a1(bufferx:end-bufferx,buffery:end-buffery,:);
p=p1(bufferx:end-bufferx,buffery:end-buffery,:);

ndiv=100;
Np=max(size(a(:)));

if (differ)
    dp=fluct(p);
else
    dp=p;
end    

% figure(n)
% [totnum,nbinu,xrange,urange]=spaziofasi2(a(:),p(:),ones(Np,1),0,min(a(:)),max(a(:)),min(p(:)),max(p(:)),ndiv);
% imagesc(xrange,urange,log10(nbinu))
% xlabel('\Phi')
% ylabel(name)
% colorbar
% colormap hsv
% print('-dpng','-r300',[prename '_' name])
% close(n)

figure(n)
[totnum,nbinu,xrange,urange]=spaziofasi2(a(:),p(:),ones(Np,1),0,min(a(:)),max(a(:)),min(p(:)),max(p(:)),ndiv);
pmean=urange*nbinu./(ones(size(urange))*nbinu);

for j=2:max(size(urange))
ii=(xrange(j-1)<a(:)) & (xrange(j)>a(:));
pmean_dir(j-1)=mean(p(ii));
pstd_dir(j-1)=std(p(ii));
end

[totnum,nbinu,xrange,urange]=spaziofasi2(a(:),dp(:),ones(Np,1),0,min(a(:)),max(a(:)),min(dp(:)),max(dp(:)),ndiv);
pmean2=urange*nbinu./(ones(size(urange))*nbinu);
pstd=sqrt((urange-pmean2).^2*nbinu./(ones(size(urange))*nbinu));

durange=urange(2)-urange(1);
imagesc(xrange,urange,log10(nbinu))
xlabel('\Phi','fontsize',[14])
ylabel(name,'fontsize',[14])
colorbar
%colormap hsv
load gist_ncar.mat
colormap(flipud(gist_ncar))

Ncuts=max(size(list_value))

figure(n+1)
        urka=-20:.1:20;
        semilogy(urka,exp(-urka.^2/2)/sqrt(2*pi),'k--')
hold on
labelle=["normal"];

Nxr=max(size(xrange));

for i=1:Ncuts
%ip=round(ndiv/Ncuts*i-ndiv/Ncuts/2);
ip=(list_value(i)*(Nxr-1)+max(xrange)-min(xrange)*Nxr)/(max(xrange)-min(xrange));
ip=round(ip);
lr=num2str(xrange(ip),'%10.3f\n');
labelle=[labelle;string(lr)];
figure(n)
hold on
plot([xrange(ip) xrange(ip)],[min(urange) max(urange)],'k')
figure(n+1)
sig=sqrt(urange.^2*nbinu(:,ip)/sum(nbinu(:,ip)));
        semilogy(urange/sig,nbinu(:,ip)./sum(durange/sig*nbinu(:,ip)))%,'linewidth',[4])
%ylim([min(nbinu(:,ip)) max(nbinu(:,ip))])
end
ylim([1e-6, 10])
xlim([-20 20])
xlabel(['\Delta' name],'fontsize',[14])
title(name,'fontsize',[14])
legend(labelle)
set(gca,'fontsize',[14])
print('-dpng','-r300',[prename '_mp_' name])
close(n+1)
figure(n)  
set(gca,'fontsize',[14])
print('-dpng','-r300',[prename '_d_' name])
%close(n)

end


function [dp] = fluct(p)
p_avg=mean(p,3);
[Nx , ~, Nz]=size(p);
dp=p;
for k=1:Nz
    dp(:,:,k)=p(:,:,k)-p_avg;
end
end
