function [Ve,Vi]=seven_tetra(Bx,By,Bz,Nx,Ny,Nz, Lx, nome_file, nome)
close all
h=figure(1)
set(h,'Position', [229 30 491 631])
v1=[1,1,1]
v2=[1,-1,-1]
v3=[-1,1,-1]
v4=[-1,-1,1]
Ve=[v1-v1;v2-v1;v3-v1;v4-v1]*2;
Vi=round(Ve(1:end,:)*sqrt(256));
xv=(33:Nx-33)/Nx*Lx;

for i=33:Nx-33
    Bve(i,1:4)=interpn(Bx,Ve(:,1)+i,Ve(:,2)+Ny/2,Ve(:,3)+Nz/2);
    Bvi(i,1:4)=interpn(Bx,Vi(:,1)+i,Vi(:,2)+Ny/2,Vi(:,3)+Nz/2);
end    
subplot(6,1,1)
plot(xv,Bvi(33:end,:)')
title('ion tetrahedron')
ylabel([nome '_x'])
set(gca,'XTick',[])
axis tight
subplot(6,1,2)
plot(xv,Bve(33:end,:)')
%xlabel('x/d_i')
title('electron tetrahedron')
ylabel([nome '_x'])
set(gca,'XTick',[])
axis tight
for i=33:Nx-33
    Bve(i,1:4)=interpn(By,Ve(:,1)+i,Ve(:,2)+Ny/2,Ve(:,3)+Nz/2);
    Bvi(i,1:4)=interpn(By,Vi(:,1)+i,Vi(:,2)+Ny/2,Vi(:,3)+Nz/2);
end   
subplot(6,1,3)
plot(xv,Bvi(33:end,:)')
title('ion tetrahedron')
ylabel([nome '_y'])
set(gca,'XTick',[])
axis tight
subplot(6,1,4)
plot(xv,Bve(33:end,:)')
title('electron tetrahedron')
%xlabel('x/d_i')
ylabel([nome '_y'])
set(gca,'XTick',[])
axis tight

for i=33:Nx-33
    Bve(i,1:4)=interpn(Bz,Ve(:,1)+i,Ve(:,2)+Ny/2,Ve(:,3)+Nz/2);
    Bvi(i,1:4)=interpn(Bz,Vi(:,1)+i,Vi(:,2)+Ny/2,Vi(:,3)+Nz/2);
end   
subplot(6,1,5)
plot(xv,Bvi(33:end,:)')
title('ion tetrahedron')
ylabel([nome '_z'])
set(gca,'XTick',[])
axis tight
subplot(6,1,6)
plot(xv,Bve(33:end,:)')
title('electron tetrahedron')
xlabel('x/d_i')
ylabel([nome '_z'])
axis tight

print('-dpng',nome_file)
end

