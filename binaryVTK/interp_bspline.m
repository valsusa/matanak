function [bxp, byp, bzp] = interp_bspline(x,y,z,Bx,By,Bz)
%
%  Function to interpolate in teh same manner as iPic3D does
%
%  G. Lapenta, 20/10/29
%
[Nx,Ny,Nz]=size(Bx);
i=floor(x);
eta = x - i; 
j=floor(y);
xi = y - j;
k=floor(z);
zeta = z - k;

i = 1+ mod(i-1,Nx-1);
j = 1+ mod(j-1,Ny-1);
k = 1+ mod(k-1,Nz-1);
sz=size(Bx);
bxp = Bx(sub2ind(sz,i,j,k)) .* (1-eta) .* (1-xi) .* (1-zeta) + ...
     Bx(sub2ind(sz,i+1,j,k)) .* eta .* (1-xi) .* (1-zeta) + ...
     Bx(sub2ind(sz,i,j+1,k)) .* (1-eta) .* xi .* (1-zeta) + ...
     Bx(sub2ind(sz,i+1,j+1,k)) .* eta .* xi .* (1-zeta) + ...
     Bx(sub2ind(sz,i,j,k+1)) .* (1-eta) .* (1-xi) .* zeta + ...
     Bx(sub2ind(sz,i+1,j,k+1)) .* eta .* (1-xi) .* zeta + ...
     Bx(sub2ind(sz,i,j+1,k+1)) .* (1-eta) .* xi .* zeta + ...
     Bx(sub2ind(sz,i+1,j+1,k+1)) .* eta .* xi .* zeta;
 byp = By(sub2ind(sz,i,j,k)) .* (1-eta) .* (1-xi) .* (1-zeta) + ...
     By(sub2ind(sz,i+1,j,k)) .* eta .* (1-xi) .* (1-zeta) + ...
     By(sub2ind(sz,i,j+1,k)) .* (1-eta) .* xi .* (1-zeta) + ...
     By(sub2ind(sz,i+1,j+1,k)) .* eta .* xi .* (1-zeta) + ...
     By(sub2ind(sz,i,j,k+1)) .* (1-eta) .* (1-xi) .* zeta + ...
     By(sub2ind(sz,i+1,j,k+1)) .* eta .* (1-xi) .* zeta + ...
     By(sub2ind(sz,i,j+1,k+1)) .* (1-eta) .* xi .* zeta + ...
     By(sub2ind(sz,i+1,j+1,k+1)) .* eta .* xi .* zeta;
  bzp = Bz(sub2ind(sz,i,j,k)) .* (1-eta) .* (1-xi) .* (1-zeta) + ...
     Bz(sub2ind(sz,i+1,j,k)) .* eta .* (1-xi) .* (1-zeta) + ...
     Bz(sub2ind(sz,i,j+1,k)) .* (1-eta) .* xi .* (1-zeta) + ...
     Bz(sub2ind(sz,i+1,j+1,k)) .* eta .* xi .* (1-zeta) + ...
     Bz(sub2ind(sz,i,j,k+1)) .* (1-eta) .* (1-xi) .* zeta + ...
     Bz(sub2ind(sz,i+1,j,k+1)) .* eta .* (1-xi) .* zeta + ...
     Bz(sub2ind(sz,i,j+1,k+1)) .* (1-eta) .* xi .* zeta + ...
     Bz(sub2ind(sz,i+1,j+1,k+1)) .* eta .* xi .* zeta;
 
end