%fluctuation_fluxfunction_singleplot

%save('7feb_data.mat','Jex', 'Jey', 'Jez', 'Jix', 'Jiy', 'Jiz', 'Ex', 'Ey', 'Ez', 'Bx', 'By', 'Bz', 'rhoe', 'rhoi')

%load('7feb_data.mat')

xp=Jex(:);
yp=Jix(:);
zp=JedotE(:);
%[vdf,umin,vmin,wmin,du,dv,dw]=spaziofasi3D_different(xp,yp,zp,ones(size(xp)),100);


%imagesc(log(squeeze(mean(vdf,1))'))
%loglog(squeeze(mean(vdf,1))')

[xrange,beta_e_mean,beta_e_std]=figura(Jix,Jex,4,'Jix','JedotE','0', true, []);

function [xrange,pmean_dir,pstd_dir] = figura(a1,p1,n,namex,name,prename,differ, list_value)
% MYMEAN Example of a local function.
close all
bufferx = 1
buffery = 1
differ=false

a=a1(bufferx:end-bufferx,buffery:end-buffery,:);
p=p1(bufferx:end-bufferx,buffery:end-buffery,:);

ndiv=100;
Np=max(size(a(:)));

if (differ)
    dp=fluct(p);
else
    dp=p;
end    

% figure(n)
% [totnum,nbinu,xrange,urange]=spaziofasi2(a(:),p(:),ones(Np,1),0,min(a(:)),max(a(:)),min(p(:)),max(p(:)),ndiv);
% imagesc(xrange,urange,log10(nbinu))
% xlabel('\Phi')
% ylabel(name)
% colorbar
% colormap hsv
% print('-dpng','-r300',[prename '_' name])
% close(n)

figure(n)
[totnum,nbinu,xrange,urange]=spaziofasi2(a(:),p(:),ones(Np,1),0,min(a(:)),max(a(:)),min(p(:)),max(p(:)),ndiv);
pmean=urange*nbinu./(ones(size(urange))*nbinu);

for j=2:max(size(urange))
ii=(xrange(j-1)<a(:)) & (xrange(j)>a(:));
pmean_dir(j-1)=mean(p(ii));
pstd_dir(j-1)=std(p(ii));
end

[totnum,nbinu,xrange,urange]=spaziofasi2(a(:),dp(:),ones(Np,1),0,min(a(:)),max(a(:)),min(dp(:)),max(dp(:)),ndiv);
pmean2=urange*nbinu./(ones(size(urange))*nbinu);
pstd=sqrt((urange-pmean2).^2*nbinu./(ones(size(urange))*nbinu));

durange=urange(2)-urange(1);
imagesc(xrange,urange,log10(nbinu))
xlabel(namex,'fontsize',[14])
ylabel(name,'fontsize',[14])
colorbar
%colormap hsv
load gist_ncar.mat
colormap(flipud(gist_ncar))

Ncuts=max(size(list_value));

figure(n+1)
        urka=-20:.1:20;
        semilogy(urka,exp(-urka.^2/2)/sqrt(2*pi),'k--')
hold on
labelle=["normal"];

Nxr=max(size(xrange));

for i=1:Ncuts
%ip=round(ndiv/Ncuts*i-ndiv/Ncuts/2);
ip=(list_value(i)*(Nxr-1)+max(xrange)-min(xrange)*Nxr)/(max(xrange)-min(xrange));
ip=round(ip);
lr=num2str(xrange(ip),'%10.3f\n');
labelle=[labelle;string(lr)];
figure(n)
hold on
plot([xrange(ip) xrange(ip)],[min(urange) max(urange)],'k')
figure(n+1)
sig=sqrt(urange.^2*nbinu(:,ip)/sum(nbinu(:,ip)));
        semilogy(urange/sig,nbinu(:,ip)./sum(durange/sig*nbinu(:,ip)))%,'linewidth',[4])
%ylim([min(nbinu(:,ip)) max(nbinu(:,ip))])
end
ylim([1e-6, 10])
xlim([-20 20])
xlabel(['\Delta' name],'fontsize',[14])
title(name,'fontsize',[14])
legend(labelle)
set(gca,'fontsize',[14])
print('-dpng','-r300',[prename '_mp_' name])
close(n+1)
figure(n)  
set(gca,'fontsize',[14])
print('-dpng','-r300',[prename '_d_' name])
%close(n)

end


function [dp] = fluct(p)
p_avg=mean(p,3);
[Nx Ny Nz]=size(p);
dp=p;
for k=1:Nz
    dp(:,:,k)=p(:,:,k)-p_avg;
end
end
