%
% Script to Compute the Suqshing Factor and Field aligned potential 
%
% G. Lapenta 30/11/2020
%
addpath(genpath('..'));
close all

global Nx Ny Nz Bx By Bz verso dx dy dz

clear Qm pb veb

leggi=true;

if leggi 
    %dir ='/Users/glapenta/Desktop/tred65/';
    dir ='~/Dropbox/Shared/tred65/';
cycle = 18000;
[Bx, By, Bz,Nx,Ny,Nz] = read_binVTK_vector(dir,'B',cycle);
[Ex, Ey, Ez,Nx,Ny,Nz] = read_binVTK_vector(dir,'E',cycle);
B2 = Bx.*Bx + By.*By + Bz.*Bz;
E2 = Ex.*Ex + Ey.*Ey + Ez.*Ez;
EdotB = Ex.*Bx + Ey.*By + Ez.*Bz;
vE = sqrt(B2.*E2 -EdotB.^2)./E2; 
vEtanh = 1 - tanh(vE);
%tred65
B0_phys = 20e-9 ; %Tesla
code_BSI= 20e-9 /B0;
density = 1; %particles per cc
density = density* 1e6;
vA_phys = B0_phys / sqrt(density* mu0)
vA_code = B0;
code_nSI = 4*pi*density;
code_ESI = code_BSI* cphys

Lx = 40;
Ly = 15;
Lz = 10;
dx = Lx/Nx;
dy = Ly/Ny;
dz = Lz/Nz;

di=1
de=1/sqrt(256)
end

lpos = [35.80     13.00     6.87;
    33.03, 4.53, 7.67
    0 0 0
    0 0 0
    8.27, 10.73, 5.97]

%
% Using internal Matlab functions [ Very slow]
%

% lpos = has the physical position of teh points of interest where we want to
%        study the squashing 

squashing_matlab = false;

if(squashing_matlab)
    
    [npos, dummy]=size(lpos);
    
    figure(1000)
    for ix=1:npos
        [Qp(ix),Qm(ix)]=squashing(lpos(ix,1),lpos(ix,2),lpos(ix,3),dx,dy,dz,Bx,By,Bz);
        pause(.1)
    end
    
    
    figure(1000)
    for ix=1:npos
        [pb(ix)]=potalongb(lpos(ix,1),lpos(ix,2),lpos(ix,3),dx,dy,dz,Bx,By,Bz,Ex,Ey,Ez);
        pause(.1)
    end
end


squashing_home = true;


if(squashing_home)

ix=5
for icy=1:1
    for icz=1:1
            xp = lpos(ix,1) /dx ;    
yp = lpos(ix,2) /dy + icy-1; 
zp = lpos(ix,3) /dz + icz-1; 

xp= lpos(ix,1) /dx ;
yp = Ny/2;
zp = Nz/2;

%span in celle
span = 50;ds=1;
%span=2; ds = .05
[eta,xi] = meshgrid(-span:ds:span,-span:ds:span);


xperp = xp ;
yperp = yp + eta ;
zperp = zp + xi;

xperp=Nx/4;
[yperp,zperp]=meshgrid(50:Ny-50,1:Nz);
yp=0;zp=0;

[nspany,nspanz]=size(yperp);

yend=zeros(size(yperp));
zend=zeros(size(yperp));
yini=zeros(size(yperp));
zini=zeros(size(yperp));
close all
clear ystart zstart yend zend yini zini Qm Qme Qmi
    for i1 = 1:nspany
         disp(['Done ' num2str(i1) ' of ' num2str(nspany)])
        for i2 = 1:nspanz
        Opt    = odeset('Events', @myEvent);
        tspan=linspace(0,1e5,1e4);
        xst=[xperp, yperp(i1,i2), zperp(i1,i2)];
        verso=1;
        [t1,sol1]=ode45(@ode_lines,[0, 1e5],xst,Opt);
        verso=-1;
        [t2,sol2]=ode45(@ode_lines,[0, 1e5],xst,Opt);
        sol=[flipud(sol2); sol1];
        %plot(sol(:,1),sol(:,2))
      
        xs = sol(:,1);
        ys = sol(:,2);
        zs = sol(:,3);
        plotta = false;
        if(plotta)
        figure(1000)
        plot3(xs,ys,zs)
        hold on
        plot3(xperp, yperp(i1,i2), zperp(i1,i2),'*')
        end
        ystart(i1,i2)= xst(2);
        zstart(i1,i2)= xst(3);
        yend(i1,i2)= ys(end);
        yini(i1,i2)= ys(1);
        zend(i1,i2)= zs(end);
        zini(i1,i2)= zs(1);
        pb(i1,i2)=int_line(xs,ys,zs, dx, dy, dz, Bx,By,Bz,Ex,Ey,Ez);
        
        veb(i1,i2)=int_line_vE(xs,ys,zs, dx, dy, dz, vEtanh);
        
        end
    end   
 
 figure(icy*icz)
 pcolor(((ystart)-yp)*dy,((zstart)-zp)*dz,smooth2D(pb,0)); shading interp;  colorbar
 T=text(yp,zp,'*');set(T,'fontsize',40);set(T,'color','w')
 print('-dpng',['Pot' num2str(icy*icz)])
 
 figure(icy*icz+1000)
 pcolor(((ystart)-yp)*dy,((zstart)-zp)*dz,smooth2D(veb,0)); shading interp; colorbar
 T=text(yp,zp,'*');set(T,'fontsize',40);set(T,'color','w')
 print('-dpng',['Vealong' num2str(icy*icz)])
 hold on
 ii=veb>.01;
 yvE = yperp(ii); zvE = zperp(ii);
 plot(yvE*dy,zvE*dz,'wo')
 
% nsx = (nspanx+1)/2;
% nsy = (nspany+1)/2;

[gyend1, gyend2] = gradient(yend) ;
[gzend1, gzend2] = gradient(zend) ;
[gyini1, gyini2] = gradient(yini) ;
[gzini1, gzini2] = gradient(zini) ;

for ns1=1:nspany
     %disp(['Done ' num2str(ns1) ' of ' num2str(nspany)])
    for ns2=1:nspanz
 
je(1,1)=gyend1(ns1,ns2) ; je(1,2)= gyend2(ns1,ns2);
je(2,1)=gzend1(ns1,ns2) ; je(2,2)= gzend2(ns1,ns2);

ji(1,1)=gyini1(ns1,ns2) ; ji(1,2)= gyini2(ns1,ns2);
ji(2,1)=gzini1(ns1,ns2) ; ji(2,2)= gzini2(ns1,ns2);


jac = je*inv(ji);
jacbac = ji*inv(je);

lam=eig(jac'*jac);
R = lam(2)/lam(1);
Qm(ns1,ns2)=R+1/R;
QmT(ns1,ns2)=trace(jac'*jac)/abs(det(jac));

lam=eig(jacbac'*jacbac);
R = lam(2)/lam(1);
QmINV(ns1,ns2)=R+1/R;

lam=eig(je'*je);
R = lam(2)/lam(1);
Qme(ns1,ns2)=R+1/R;

lam=eig(ji'*ji);
R = lam(2)/lam(1);
Qmi(ns1,ns2)=R+1/R;

%disp(num2str([icy,icz,Qm(icy,icz)]))
    end
end
    end
end
end




figure;imagesc((ystart(1,:)-yp)*dy,(zstart(:,1)-zp)*dz,smooth2D(log10(Qm),1));colorbar
T=text(yp,zp,'*');set(T,'fontsize',40);set(T,'color','w')
print('-dpng',['Sq' num2str(icy*icz)])

figure;imagesc((ystart(1,:)-yp)*dy,(zstart(:,1)-zp)*dz,smooth2D(log10(Qme),1));colorbar
T=text(yp,zp,'*');set(T,'fontsize',40);set(T,'color','w')
print('-dpng',['Sqe' num2str(icy*icz)])

figure;imagesc((ystart(1,:)-yp)*dy,(zstart(:,1)-zp)*dz,smooth2D(log10(Qmi),1));colorbar
T=text(yp,zp,'*');set(T,'fontsize',40);set(T,'color','w')
print('-dpng',['Sqi' num2str(icy*icz)])

figure;imagesc((ystart(1,:)-yp)*dy,(zstart(:,1)-zp)*dz,smooth2D(log10(QmT),1));colorbar
T=text(yp,zp,'*');set(T,'fontsize',40);set(T,'color','w')
print('-dpng',['SqT' num2str(icy*icz)])

figure;pcolor((ystart-yp)*dy,(zstart-zp)*dz,smooth2D(log10(QmINV),1));shading interp; colorbar
T=text(yp,zp,'*');set(T,'fontsize',40);set(T,'color','w')
print('-dpng',['SqINV' num2str(icy*icz)])

function [perp1x, perp1y,perp1z,  perp2x, perp2y, perp2z] = perp_plane(Bx, By, Bz)
B2D=sqrt(Bx.^2+By.^2);
B=sqrt(Bx.^2+By.^2+Bz.^2);

perp2x=Bz.*Bx./(B.*B2D);
perp2y=Bz.*By./(B.*B2D);
perp2z=-B2D./B;


perp1x = By./B2D;
perp1y = -Bx./B2D;
perp1z = 0;

end
