close all
figure(1)
plot(xrange(2:end),B2_std./B2_mean)
%hold on
%plot(xrange(2:end),B_std.^2./B_mean.^2,'--')
xlim([-.02 .04])
hold on
plot(xrange(2:end),E2_std./E2_mean)
%plot(xrange(2:end),rhoe_std./rhoe_mean)
%plot(xrange(2:end),rhoi_std.^2./rhoi_mean.^2)
plot(xrange(2:end),S_std./S_mean)
plot(xrange(2:end),Ve2_std./Ve2_mean)
plot(xrange(2:end),Vi2_std./Vi2_mean)
legend('B^2','E^2','S','Ve^2','Vi^2')
xlabel('\Psi','fontsize',[14])
ylabel('std(x)/mean(x)','fontsize',[14])
set(gca,'fontsize',[14])
print(['meanOstd.png'],'-dpng','-r300')

close(1)
figure(2)
semilogy(xrange(2:end),rhoi2_std./(B2_std*8*pi))
hold on
semilogy(xrange(2:end),rhoi_std.^2./B_std.^2)
xlim([-.02 .04])
xlabel('\Psi','fontsize',[14])
ylabel('\delta n_i^2/\delta B^2','fontsize',[14])
set(gca,'fontsize',[14])
print(['meanOstd_compress.png'],'-dpng','-r300')

