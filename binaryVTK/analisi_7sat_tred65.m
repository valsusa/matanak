addpath(genpath('~/matanak/'));
tred65

leggo=false;

if(leggo)
cycle=18000
[Bx,By,Bz,Nx,Ny,Nz]=read_binVTK_vector(dir,'B',cycle);
[Jx,Jy,Jz,Nx,Ny,Nz]=read_binVTK_vector(dir,'Je',cycle);
end

[Ve, Vi]=seven_tetra(Bx,By,Bz,Nx,Ny,Nz, Lx,'7sat_B.png','B');

[Ve, Vi]=seven_tetra(Jx,Jy,Jz,Nx,Ny,Nz, Lx,'7sat_Je.png','Je');
