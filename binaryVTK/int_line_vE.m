function [potalongb] = int_line_vE(xs,ys,zs, dx, dy, dz, vE)
%
%   Function to integrate along the field lines using iPic3D-like
%   interpolation
%
%   G. Lapenta, 10/29/2020
%


[ves, bys, bzs] = interp_bspline(xs,ys,zs,vE,vE,vE);


ds = sqrt((xs(2:end)-xs(1:end-1)).^2 + (ys(2:end)-ys(1:end-1)).^2 + (zs(2:end)-zs(1:end-1)).^2);


potalongb = max(ves);


end
