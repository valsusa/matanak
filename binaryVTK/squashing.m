function [Qp,Qm] = squashing(xphys,yphys,zphys,dx,dy,dz,Bx,By,Bz)
%
% This function computes the squashing factor using matlab internal matalb
% streamline tracing
%
% G. Lapenta, 29/10/2020
%

[Nx,Ny,Nz]=size(Bx);
%rangey = round(Ny/2):round(Ny/2)+100;
%rangez = Nz/2-10:Nz/2+10;
jct=0;
kct=0;
for js=1:prod(size(yphys));
    istart = xphys/dx;
    jstart = yphys(js)/dy;
    jct=jct+1;
for ks=1:prod(size(zphys));
    kstart = zphys(ks)/dz;
    kct=kct+1;
    [jstart kstart]
ds=1.;
vecs=(-ds:ds:ds);
[sx,sy,sz]=meshgrid(istart,jstart+vecs,kstart+vecs);
XYZ=stream3(permute(Bx,[2 1 3]),permute(By,[2 1 3]),permute(Bz,[2 1 3]),sx,sy,sz);
streamline(XYZ)
%view(3) 
% pause(.1)
nstream = prod(size(sx));
for is=1:nstream
    [i j] =ind2sub([3 3],is);
    C = XYZ(is);
    xstr  =  C{:};
    xs =xstr(:,1);ys =xstr(:,2);zs =xstr(:,3);
    x0(j,i)=xs(1); y0(j,i)=ys(1); z0(j,i)=zs(1); 
    xend(j,i)=xs(end)
    yend(j,i)=ys(end); zend(j,i)=zs(end); 
    
end
[j11 j12]=gradient(yend);
[j21 j22]=gradient(zend);
jac=[j11(2,2) j12(2,2); j21(2,2) j22(2,2)];
lam=eig(jac'*jac);
R = lam(2)/lam(1);
Qp(jct,kct)=0;
if(mean(xend(:))<1 | mean(xend(:))>Nx-1)
    Qp(jct,kct)=R+1/R;
end 


XYZ=stream3(permute(-Bx,[2 1 3]),permute(-By,[2 1 3]),permute(-Bz,[2 1 3]),sx,sy,sz);
streamline(XYZ)
%view(3) 
% pause(.1)
nstream = prod(size(sx));
for is=1:nstream
    [i j] =ind2sub([3 3],is);
    C = XYZ(is);
    xstr  =  C{:};
    xs =xstr(:,1);ys =xstr(:,2);zs =xstr(:,3);
    x0(i,j)=xs(1); y0(i,j)=ys(1); z0(i,j)=zs(1); 
    xend(i,j)=xs(end)
    yend(i,j)=ys(end); zend(i,j)=zs(end); 
end
[j11 j12]=gradient(yend);
[j21 j22]=gradient(zend);
jac=[j11(2,2) j12(2,2); j21(2,2) j22(2,2)];
lam=eig(jac'*jac);
R = lam(2)/lam(1);
Qm(jct,kct)=0;
if(mean(xend(:))<1 | mean(xend(:))>Nx-1)
    Qm(jct,kct)=R+1/R;
end    
end
end

%figure
%Qin=Q(rangey,rangez)
%imagesc(log10(Qin));colorbar

end
