function [A_plane] = extract_cuts_maxp(Lx, Ly, Lz, A,Pi, titolo,bufferx, buffery, minval,maxval,sym_col,colore)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% sym_col=1 symmetric color, 0+/-3std
% sym_col=0 just uses mean+-3 std
% sym_col=-1 0 max
 
bufferz=buffery;
color_choice=colore;
close all
h1=figure(1)
set(h1,'Position',[40 78 840 320])

[Nx Ny Nz] = size(A);


iy_axis=1:Ny;
for ix=1:Nx
for iz=1:Nz
w=Pi(ix,:,iz);

%w=1.0./sqrt(Bx(ix,:,iz).^2+1e-10);

iy_plane(ix,iz) = min(Ny,max(1,round(sum(w.*iy_axis)./sum(w))));

A_plane(ix,iz) = A(ix,iy_plane(ix,iz),iz);

end
end


%minval=min(A(round(xcut/Lx*Nx),:,3:end-2));minval=min(minval(:));
%maxval=max(A(round(xcut/Lx*Nx),:,3:end-2));maxval=max(maxval(:));
%maxval=sqrt(abs(prod([maxval,minval])))
%minval=-maxval

    rescale=true;
    if(minval==maxval)
        
        Amean = mean(A_plane(:),'omitnan');
        if(sym_col==1) Amean=0; end;   
        Astd = std(A_plane(:),'omitnan');
        minval=Amean-3*Astd;
        maxval=Amean+3*Astd
    end 
    if(sym_col==-1)
        minval=0;
        mxval=max(A_plane(:))
    end    
%     if(rescale)
%     minval=min(A_plane(3+bufferx:Nx-2-bufferx,:));minval=min(minval(:));
%     maxval=max(A_plane(3+bufferx:Nx-2-bufferx,:));maxval=max(maxval(:));
%     
%     if(minval<0)
%         maxval=sqrt(abs(prod([maxval,minval])))
%         minval=-maxval;
%     end    
%     end
    
    
    pcolor( (3+bufferx:Nx-2-bufferx)/Nx*Lx,(bufferz:Nz-bufferz+1)/Nz*Lz,A_plane(3+bufferx:Nx-2-bufferx,bufferz:Nz-bufferz+1)')
    shading interp
    if(rescale) caxis([minval,maxval]); end
    colorbar
    title(['MAX P'])
    xlabel('x/d_i')
    ylabel('z/d_i')
    if(color_choice==0)
        colormap jet
elseif (color_choice==1)
        load cm_new
        colormap(cm_kbwrk)
        colormap(redblue(256))
elseif (color_choice==2)
        colormap hot
elseif (color_choice==3)
        load cm_multi4
        colormap(cm_cool_hot_2)
elseif (color_choice==4)
        colormap hsv
elseif (color_choice==5)
        load gist_ncar
        colormap(gist_ncar)       
elseif (color_choice==-1)
        colormap parula
end


print([titolo '_extract_cuts_maxp.png'],'-dpng')


    pcolor( (3+bufferx:Nx-2-bufferx)/Nx*Lx,(bufferz:Nz-bufferz+1)/(Nz-2*bufferz)*Lz,squeeze(A(3+bufferx:Nx-2-bufferx,round(2*Ny/3),bufferz:Nz-bufferz+1))')
    shading interp
    if(rescale) caxis([minval,maxval]); end
    colorbar
    title(['2 Ly/3'])
    xlabel('x/d_i')
    ylabel('z/d_i')
if(color_choice==0)
        colormap jet
elseif (color_choice==1)
        load cm_new
        colormap(cm_kbwrk)
        colormap(redblue(256))
elseif (color_choice==2)
        colormap hot
elseif (color_choice==3)
        load cm_multi4
        colormap(cm_cool_hot_2)
elseif (color_choice==4)
        colormap hsv
elseif (color_choice==5)
        load gist_ncar
        colormap(gist_ncar)       
elseif (color_choice==-1)
        colormap parula
end


print([titolo '_extract_cuts_XZ.png'],'-dpng')


 
    pcolor( (3+bufferx:Nx-2-bufferx)/Nx*Lx,(buffery:Ny-buffery)/Ny*Ly,squeeze(A(3+bufferx:Nx-2-bufferx,buffery:Ny-buffery,round(Nz/2)))')
    shading interp
    if(rescale) caxis([minval,maxval]); end
    colorbar
    title(['Z/d_i=' num2str(Ly/2)])
    xlabel('x/d_i')
    ylabel('y/d_i')
if(color_choice==0)
        colormap jet
elseif (color_choice==1)
        load cm_new
        colormap(cm_kbwrk)
        colormap(redblue(256))
elseif (color_choice==2)
        colormap hot
elseif (color_choice==3)
        load cm_multi4
        colormap(cm_cool_hot_2)
elseif (color_choice==4)
        colormap hsv
elseif (color_choice==5)
        load gist_ncar
        colormap(gist_ncar)       
elseif (color_choice==-1)
        colormap parula
end


print([titolo '_extract_cuts_XY.png'],'-dpng')
end


