figure(iplot)
ax(1)=subplot(2,2,1);
pcolor(eta*dx/de,xi*dx/de,agp); shading interp;colorbar
colormap(ax(1),parula)
hold on
contour(eta*dx/de,xi*dx/de,az,60,'w');
title('a) Agyrotropy')
%title(['x/d_i = ' num2str(xp*dx,'%5.2f') '  y/d_i = ' num2str(yp*dy,'%5.2f') '  z/d_i = ' num2str(zp*dx,'%5.2f')])
xlabel('\eta/d_e')
ylabel('\xi/d_e')

ax(2)=subplot(2,2,2);
% pcolor(eta*dx,xi*dx,dbxi.^2+dbeta.^2); shading interp; colorbar
% title('b) \delta B')
% xlabel('\eta/d_i')
% ylabel('\xi/d_i')
pcolor(eta*dx/de,xi*dx/de,1-tanh(LIp)); shading interp;colorbar
cmap=buildcmap('wygbr');
colormap(ax(2),cmap)
hold on
contour(eta*dx/de,xi*dx/de,az,60,'k');
quiver(eta(1:skip:end,1:skip:end)*dx/de,xi(1:skip:end,1:skip:end)*dx/de,veta(1:skip:end,1:skip:end),vxi(1:skip:end,1:skip:end),2)
title('b) V_\eta,  V_\xi')
xlabel('\eta/d_e')
ylabel('\xi/d_e')

ax(3)=subplot(2,2,3);
quiver(eta(1:skip:end,1:skip:end),xi(1:skip:end,1:skip:end),dbeta(1:skip:end,1:skip:end),dbxi(1:skip:end,1:skip:end),2)
th=linspace(0,2*pi,60); R=2
startx = R*sin(th);
starty = R*cos(th);
%[startx,starty]=meshgrid(-span:.8:span,-span:.8:span);
streamline(eta,xi,dbeta,dbxi,startx,starty)
streamline(eta,xi,-dbeta,-dbxi,startx,starty)
%hold on
%plot(startx,starty,'+')

title('c) \delta B_\eta, \delta B_\xi')
xlabel('\eta/d_e')
ylabel('\xi/d_e')

ax(4)=subplot(2,2,4);
%Tachyon indicator
%vEpl = interpn(i,j,k,vE,xperp,yperp,zperp);
%pcolor(eta,xi,vEpl); shading interp;colorbar
%title('d) vE')

%diva = divergence(eta,xi,dbeta,dbxi);
%pcolor(eta,xi,diva);shading interp; colorbar

%pcolor(eta*dx/de,xi*dx/de,pb); shading interp;colorbar

pcolor(eta*dx/de,xi*dx/de,sqrt(vx.^2+vy.^2+vz.^2)); shading interp;colorbar

colormap(ax(4),parula)
%hold on
%contour(eta*dx/de,xi*dx/de,az,60,'w');
title('d) Density')
xlabel('\eta/d_e')
ylabel('\xi/d_e')

[g11 g12] = gradient(dbeta,.2*dx);
[g21 g22] = gradient(dbxi,.2*dx);
Jac=[g11(ihctr,jhctr) g12(ihctr,jhctr); g21(ihctr,jhctr) g22(ihctr,jhctr)];
lj=eig(Jac);
jeig = [jeig ; lj' trace(Jac) (trace(Jac))/norm(Jac) Jac(2,1)*Jac(1,2) Jac(2,1) Jac(1,2)]

scale = mean(sqrt(bxp.^2+byp.^2+bzp.^2),'all')/mean(sqrt(g11.^2+g12.^2+g21.^2+g22.^2),'all');
scale2 = mean(sqrt(bxp.^2+byp.^2+bzp.^2),'all')/Bref;

Bsc1 =[Bsc1; scale]; 
Bsc2 =[Bsc2; scale2];

sgtitle([labelh '-point:  ' 'x/d_i = ' num2str(Position(1),'%5.2f'),  ...
    '  y/d_i = ' num2str(Position(2),'%5.2f'), ...
    '  z/d_i = ' num2str(Position(3),'%5.2f'), ...
    '  ||B||/||\nabla B||='  num2str(scale,'%5.2f') , '  ||B||/Bmax='  num2str(scale2,'%5.2f')])
