function [potalongb] = potalongb(xphys,yphys,zphys,dx,dy,dz,Bx,By,Bz,Ex,Ey,Ez)
%
%   Function to integrate along the field lines using MATLAB internal
%   functions
%
%   G. Lapenta, 10/29/2020
%

[Nx,Ny,Nz]=size(Bx);
%rangey = round(Ny/2):round(Ny/2)+100;
%rangez = Nz/2-10:Nz/2+10;

for is=1:length(xphys(:))
    istart = xphys(is)/dx;
    jstart = yphys(is)/dy;
    [is length(xphys(:))]


    kstart = zphys(is)/dz;

ds=1.;
[sx,sy,sz]=meshgrid(istart,jstart,kstart);
XYZ=stream3(permute(Bx,[2 1 3]),permute(By,[2 1 3]),permute(Bz,[2 1 3]),sx,sy,sz);
streamline(XYZ)
%view(3) 
% pause(.1)

    C = XYZ;
    xstr  =  C{:};
    xs =xstr(:,1);ys =xstr(:,2);zs =xstr(:,3);
    
    
[i,j,k]=ndgrid(1:Nx,1:Ny,1:Nz);
bxs = interpn(i,j,k,Bx,xs, ys, zs);
bys = interpn(i,j,k,By,xs, ys, zs);
bzs = interpn(i,j,k,Bz,xs, ys, zs);

exs = interpn(i,j,k,Ex,xs, ys, zs);
eys = interpn(i,j,k,Ey,xs, ys, zs);
ezs = interpn(i,j,k,Ez,xs, ys, zs);

b = bxs.*bxs + bys.*bys + bzs.*bzs;
epar = (bxs.*exs + bys.*eys + bzs.*ezs)./sqrt(b);
ds = sqrt((xs(2:end)-xs(1:end-1)).^2 + (ys(2:end)-ys(1:end-1)).^2 + (zs(2:end)-zs(1:end-1)).^2);

potalongb(is) = sum(ds.*(epar(2:end)+epar(1:end-1))/2);

[sx,sy,sz]=meshgrid(istart,jstart,kstart);
XYZ=stream3(-permute(Bx,[2 1 3]),-permute(By,[2 1 3]),-permute(Bz,[2 1 3]),sx,sy,sz);
streamline(XYZ)
%view(3) 
% pause(.1)

    C = XYZ;
    xstr  =  C{:};
    xs =xstr(:,1);ys =xstr(:,2);zs =xstr(:,3);
    
    
[i,j,k]=ndgrid(1:Nx,1:Ny,1:Nz);
bxs = interpn(i,j,k,Bx,xs, ys, zs);
bys = interpn(i,j,k,By,xs, ys, zs);
bzs = interpn(i,j,k,Bz,xs, ys, zs);

exs = interpn(i,j,k,Ex,xs, ys, zs);
eys = interpn(i,j,k,Ey,xs, ys, zs);
ezs = interpn(i,j,k,Ez,xs, ys, zs);

b = bxs.*bxs + bys.*bys + bzs.*bzs;
epar = (bxs.*exs + bys.*eys + bzs.*ezs)./sqrt(b);
ds = sqrt((xs(2:end)-xs(1:end-1)).^2 + (ys(2:end)-ys(1:end-1)).^2 + (zs(2:end)-zs(1:end-1)).^2);

potalongb(is) = potalongb(is) - sum(ds.*(epar(2:end)+epar(1:end-1))/2);

end

end
