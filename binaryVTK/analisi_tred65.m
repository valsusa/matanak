addpath(genpath('~/matanak/'));
tred65

leggo=false;

if(leggo)
cycle=18000
[Bx,By,Bz,Nx,Ny,Nz]=read_binVTK_vector(dir,'B',cycle);
Bx=squeeze(Bx(:,:,Nz/2));By=squeeze(By(:,:,Nz/2));Bz=squeeze(Bz(:,:,Nz/2));
%[Ex,Ey,Ez,Nx,Ny,Nz]=read_binVTK_vector(dir,'E',cycle);
[Jex,Jey,Jez,Nx,Ny,Nz]=read_binVTK_vector(dir,'Je',cycle);
Jex=squeeze(Jex(:,:,Nz/2));Jey=squeeze(Jey(:,:,Nz/2));Jez=squeeze(Jez(:,:,Nz/2));
[Jix,Jiy,Jiz,Nx,Ny,Nz]=read_binVTK_vector(dir,'Ji',cycle);
Jix=squeeze(Jix(:,:,Nz/2));Jiy=squeeze(Jiy(:,:,Nz/2));Jiz=squeeze(Jiz(:,:,Nz/2));

[rhoe,rhoi,~,Ny,Nz]=read_binVTK_multiscalar(dir,'rho',cycle);
rhoe=squeeze(rhoe(:,:,Nz/2));rhoi=squeeze(rhoi(:,:,Nz/2));
Je=sqrt(Jex.^2+Jey.^2+Jez.^2);Jemax=max(Je(:))
Ji=sqrt(Jix.^2+Jiy.^2+Jiz.^2);Jimax=max(Ji(:))
%[Jix,Jiy,Jiz,Nx,Ny,Nz]=read_binVTK_vector(dir,'Ji',cycle);

c=300000;
Jex=Jex./rhoe*c;Jey=Jey./rhoe*c;Jez=Jez./rhoe*c;Je=Je./rhoe*c;
Jix=Jix./rhoi*c;Jiy=Jiy./rhoi*c;Jiz=Jiz./rhoi*c;Ji=Ji./rhoi*c;

end

%[Bmin,jmin]=min(abs(Bx(:,:,Nz/2)),[],2);
%for i=1:Nx
%    Jemin(i)=Je(i,jmin(i),Nz/2);
%    Bmin(i)=Bx(i,jmin(i),Nz/2);
%end
%plot(xc,Jmin/Jmax,xc,Bmin/B0)
%legend('Jmin','Jcut')


subplot(4,1,4)
plot(xc,Jix(:,Ny/2),xc,Jex(:,Ny/2))
%plot(xc,Je(:,Ny/2),xc,Jex(:,Ny/2),xc,Jey(:,Ny/2),xc,Jez(:,Ny/2))
ylabel('Vx[Km/s]')
subplot(4,1,3)
plot(xc,Jiy(:,Ny/2),xc,Jey(:,Ny/2))
%plot(xc,Je(:,Ny/2),xc,Jex(:,Ny/2),xc,Jey(:,Ny/2),xc,Jez(:,Ny/2))
ylabel('Vy[Km/s]')
subplot(4,1,2)
plot(xc,Jiz(:,Ny/2),xc,Jez(:,Ny/2))
ylabel('Vz[Km/s]')
subplot(4,1,1)
plot(xc,Jix(:,Ny/2),xc,Jex(:,Ny/2))
plot(xc,Bx(:,Ny/2)/B0,xc,By(:,Ny/2)/B0,xc,Bz(:,Ny/2)/B0)
ylabel('B/B0')


v1=[1,1,1]
v2=[1,-1,-1]
v3=[-1,1,-1]
v4=[-1,-1,1]
Ve=[v1-v1;v2-v1;v3-v1;v4-v1];


