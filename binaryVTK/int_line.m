function [potalongb] = int_line(xs,ys,zs, dx, dy, dz, Bx,By,Bz,Ex,Ey,Ez)
%
%   Function to integrate along the field lines using iPic3D-like
%   interpolation
%
%   G. Lapenta, 10/29/2020
%

[Nx,Ny,Nz]=size(Bx);
%rangey = round(Ny/2):round(Ny/2)+100;
%rangez = Nz/2-10:Nz/2+10;
    
% ys=ys(xs>10);
% zs=zs(xs>10);
% xs=xs(xs>10);

[bxs, bys, bzs] = interp_bspline(xs,ys,zs,Bx,By,Bz);
[exs, eys, ezs] = interp_bspline(xs,ys,zs,Ex,Ey,Ez);


b = bxs.*bxs + bys.*bys + bzs.*bzs;
epar = (bxs.*exs + bys.*eys + bzs.*ezs)./sqrt(b);
ds = sqrt((xs(2:end)-xs(1:end-1)).^2 + (ys(2:end)-ys(1:end-1)).^2 + (zs(2:end)-zs(1:end-1)).^2);


potalongb = sum(ds*dx.*(epar(2:end)+epar(1:end-1))/2);


end
