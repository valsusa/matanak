
close all
% mancano dx di de

di=1; de=1/sqrt(256); dx = 0.0333

hbin = sortrows(hbin, 7,'descend');
sizebin=hbin(:,11);

dx_off = dx*0
[cmap]=buildcmap('kbcgyrm');
resize_cicles = 50
figure(1)
scatter(hbin(:,4)+dx_off,hbin(:,6)+dx_off,log10(sizebin)*resize_cicles,hbin(:,7),'filled')
hold on 
plot(hbin(:,4),hbin(:,4),'k')
xlim([2e-3 10])
ylim([2e-3 10])
caxis([0.1 .4])
colormap(cmap)
set(gca,'xscale','log')
set(gca,'yscale','log')
xline(di,'-.','di')
xline(de,'-.','de')
yline(di,'-.','di')
yline(de,'-.','de')
xlabel('\sigma_1','fontsize',20)
ylabel('\sigma_3','fontsize',20)
set(gca,'fontsize',15)
colorbar
colormap(cmapp)
title('Agyro')
print('-dpng','current_scales_agyro_13.png')

figure(2)
scatter(hbin(:,4),hbin(:,5),log10(sizebin)*resize_cicles,hbin(:,7),'filled')
hold on 
plot(hbin(:,4),hbin(:,4),'k')
xlim([2e-3 10])
ylim([2e-3 10])
caxis([0.1 .4])
colormap(cmap)
set(gca,'xscale','log')
set(gca,'yscale','log')
xline(di,'-.','di')
xline(de,'-.','de')
yline(di,'-.','di')
yline(de,'-.','de')
xlabel('\sigma_1','fontsize',20)
ylabel('\sigma_2','fontsize',20)
set(gca,'fontsize',15)
colorbar
colormap(cmapp)
title('Agyro')
print('-dpng','current_scales_agyro_12.png')

figure(3)
scatter(hbin(:,4)+dx_off,hbin(:,6)./hbin(:,4),log10(sizebin)*resize_cicles,hbin(:,7),'filled')
%hold on 
%plot(hbin(:,4),hbin(:,4),'k')
%xlim([2e-3 10])
%ylim([1 100])
caxis([0.1 .4])
colormap(cmap)
set(gca,'xscale','log')
set(gca,'yscale','log')
%xline(di,'-.','di')
xline(de,'-.','de')
%xline(dx,'-.','dx')
% yline(di,'-.','di')
% yline(de,'-.','de')
%yline(dx,'-.','dx')
xlabel('\sigma_1','fontsize',20)
ylabel('\sigma_3/\sigma_1','fontsize',20)
set(gca,'fontsize',15)
colorbar
colormap(cmapp)
title('Agyro')
print('-dpng','current_scales_agyro_ar1.png')


figure(4)
scatter(hbin(:,4)+dx_off,hbin(:,5)./hbin(:,4),log10(sizebin)*resize_cicles,hbin(:,7),'filled')
%hold on 
%plot(hbin(:,4),hbin(:,4),'k')
%xlim([2e-3 10])
%ylim([1 100])
caxis([0.1 .4])
colormap(cmap)
set(gca,'xscale','log')
set(gca,'yscale','log')
%xline(di,'-.','di')
xline(de,'-.','de')
% yline(di,'-.','di')
% yline(de,'-.','de')
xlabel('\sigma_1','fontsize',20)
ylabel('\sigma_2/\sigma_1','fontsize',20)
set(gca,'fontsize',15)
colorbar
colormap(cmapp)
title('Agyro')
print('-dpng','current_scales_agyro_ar2.png')

[cmap]=buildcmap('bcgkyrm');
c0 = 3

figure(104)
scatter(hbin(:,4)+dx_off,hbin(:,6)./hbin(:,4),log10(sizebin)*resize_cicles,hbin(:,8),'filled')
%hold on 
%plot(hbin(:,4),hbin(:,4),'k')
%xlim([0 15])
%ylim([0 15])
colormap(cmap)
set(gca,'xscale','log')
set(gca,'yscale','log')
%xline(di,'-.','di')
xline(de,'-.','de')
% yline(di,'-.','di')
% yline(de,'-.','de')
xlabel('\sigma_1','fontsize',20)
ylabel('\sigma_3/\sigma_1','fontsize',20)
set(gca,'fontsize',15)
caxis([-c0 c0]*1e-7)
colorbar
title('JeE')
print('-dpng','current_scales_jedotE_ar1.png')




figure(106)
scatter(hbin(:,4)+dx_off,hbin(:,6)./hbin(:,4),log10(sizebin)*resize_cicles,hbin(:,9),'filled')
%hold on 
%plot(hbin(:,4),hbin(:,4),'k')
%xlim([0 15])
ylim([0 100])
colormap(cmap)
set(gca,'xscale','log')
set(gca,'yscale','log')
%xline(di,'-.','di')
xline(de,'-.','de')
% yline(di,'-.','di')
% yline(de,'-.','de')
xlabel('\sigma_1','fontsize',20)
ylabel('ar_1','fontsize',20)
set(gca,'fontsize',15)
colorbar
caxis([-c0 c0]/3*1e-7)
colormap(cmap)
title('Ji.E')
print('-dpng','current_scales_jidotE_ar1.png')


figure(107)
scatter(hbin(:,4)+dx_off,hbin(:,6)./hbin(:,4),log10(sizebin)*resize_cicles,hbin(:,10),'filled')
%hold on 
%plot(hbin(:,4),hbin(:,4),'k')
%xlim([0 15])
ylim([0 100])
colormap(cmap)
set(gca,'xscale','log')
set(gca,'yscale','log')
%xline(di,'-.','di')
xline(de,'-.','de')
%yline(di,'-.','di')
%yline(de,'-.','de')
xlabel('\sigma_1','fontsize',20)
ylabel('\sigma_3/\sigma_1','fontsize',20)
set(gca,'fontsize',15)
colorbar
caxis([-c0 c0]*1e-7)
colormap(cmap)
title('J.Ep')
print('-dpng','current_scales_jdotEp_ar1.png')

figure(108)
scatter(hbin(:,4)+dx_off,hbin(:,6)./hbin(:,4),log10(sizebin)*resize_cicles,abs(hbin(:,12)),'filled')
%hold on 
%plot(hbin(:,4),hbin(:,4),'k')
%xlim([0 15])
ylim([1 100])
colormap(cmap)
set(gca,'xscale','log')
set(gca,'yscale','log')
%xline(di,'-.','di')
xline(de,'-.','de')
%yline(di,'-.','di')
%yline(de,'-.','de')
xlabel('\sigma_1','fontsize',20)
ylabel('\sigma_3/\sigma_1','fontsize',20)
set(gca,'fontsize',15)
colorbar
caxis([0 8e-5])
title('Epar')
colormap(parula)
print('-dpng','current_scales_Epar_ar1.png')


% figure(110)
% scatter(hbin(:,4)+dx_off,hbin(:,6)./hbin(:,4),log10(sizebin)*resize_cicles,hbin(:,18),'filled')
% %hold on 
% %plot(hbin(:,4),hbin(:,4),'k')
% %xlim([0 15])
% %ylim([1 100])
% colormap(cmap)
% set(gca,'xscale','log')
% set(gca,'yscale','log')
% %xline(di,'-.','di')
% xline(de,'-.','de')
% %yline(di,'-.','di')
% %yline(de,'-.','de')
% xlabel('\sigma_1','fontsize',20)
% ylabel('\sigma_3/\sigma_1','fontsize',20)
% set(gca,'fontsize',15)
% colorbar
% caxis([1 5])
% colormap(hsv(4))
% title(['A:', num2str(sum(hbin(:,18)==1)),'  B:', num2str(sum(hbin(:,18)==2)),...
%        '  As:', num2str(sum(hbin(:,18)==3)),'  Bs:', num2str(sum(hbin(:,18)==4)) ])
% print('-dpng','current_scales_B2_lau_finn_ar1.png')

figure(111)
scatter(hbin(:,4)+dx_off,hbin(:,6),log10(sizebin)*resize_cicles,hbin(:,19),'filled')
%hold on 
%plot(hbin(:,4),hbin(:,4),'k')
%xlim([0 15])
%ylim([1 100])
set(gca,'xscale','log')
set(gca,'yscale','log')
%xline(di,'-.','di')
xline(de,'-.','de')
%yline(di,'-.','di')
%yline(de,'-.','de')
xlabel('\sigma_1','fontsize',20)
ylabel('\sigma_3','fontsize',20)
set(gca,'fontsize',15)
colorbar
%caxis([0 1e-3])
colormap(cmapp)
title('exp(-vE)')
print('-dpng','current_scales_vE_ar1.png')
