
[autX1,autY1,autZ1,volX,volY,volZ]=compute_strct_funct_directional(mask,V);
size(autX1)
size(autY1)
size(autZ1)
close all
figure(100)
elle_x  = (0:nsx-1)*dx;
elle_y  = (0:nsy-1)*dy;
elle_z  = (0:nsz-1)*dz;

loglog(elle_x,autX1(1:nsx),elle_y,autY1(1:nsy),elle_z,autZ1(1:nsz))
hold on 

interval=17:18;
interval2=5:60;
intervalc = 20
p=polyfit(log(1./(interval)/dx), log(autX1(interval+1)'),1);
nx=p(1);
loglog(elle_x(interval2),(1./(interval2)/dx).^nx.*(dx*intervalc)^nx*autX1(intervalc),'k:')


p=polyfit(log(1./(interval)/dy), log(autY1(interval+1)'),1);
ny=p(1);
loglog(elle_y(interval2),(1./(interval2)/dy).^ny.*(dy*intervalc)^ny*autY1(intervalc),'k:')


p=polyfit(log(1./(interval)/dz), log(autZ1(interval+1)'),1);
nz=p(1);
loglog(elle_z(interval2),(1./(interval2)/dz).^nz.*(dz*intervalc)^nz*autZ1(intervalc),'k:')

xlabel('$\ell/d_i$','interpreter','latex','fontsize',12)
%ylim([1e-17 1e-14])
%xlim([1e-2 1e2])
%line([1 1]/rhoi,get(hax,'YLim'),'Color','k')
%line([1 1]/rhoLH,get(hax,'YLim'),'Color','y')
legend('l_x','l_y','l_z',['n_x=' num2str(-nx)],['n_y=' num2str(-ny)],['n_z=' num2str(-nz)],'location','eastoutside')
print('-dpng',[run 'Structure_Function' name_var 'cyc'  ncyc nspecies '.png'])


h5create([run 'cyc' ncyc 'spec' nspecies '.h5'],['/SF_ellex_' name_var 'cyc' ncyc],size(elle_x))
h5write([run 'cyc' ncyc 'spec' nspecies '.h5'],['/SF_ellex_' name_var 'cyc' ncyc],(elle_x))
h5create([run 'cyc' ncyc 'spec' nspecies '.h5'],['/SF_elley_' name_var 'cyc' ncyc],size(elle_y))
h5write([run 'cyc' ncyc 'spec' nspecies '.h5'],['/SF_elley_' name_var 'cyc' ncyc],(elle_y))
h5create([run 'cyc' ncyc 'spec' nspecies '.h5'],['/SF_ellez_' name_var 'cyc' ncyc],size(elle_z))
h5write([run 'cyc' ncyc 'spec' nspecies '.h5'],['/SF_ellez_' name_var 'cyc' ncyc],(elle_z))

h5create([run 'cyc' ncyc 'spec' nspecies '.h5'],['/SF_autX_' name_var 'cyc' ncyc],size(autX1))
h5write([run 'cyc' ncyc 'spec' nspecies '.h5'],['/SF_autX_' name_var 'cyc' ncyc],(autX1))
h5create([run 'cyc' ncyc 'spec' nspecies '.h5'],['/SF_autY_' name_var 'cyc' ncyc],size(autY1))
h5write([run 'cyc' ncyc 'spec' nspecies '.h5'],['/SF_autY_' name_var 'cyc' ncyc],(autY1))
h5create([run 'cyc' ncyc 'spec' nspecies '.h5'],['/SF_autZ_' name_var 'cyc' ncyc],size(autZ1))
h5write([run 'cyc' ncyc 'spec' nspecies '.h5'],['/SF_autZ_' name_var 'cyc' ncyc],(autZ1))