close all
addpath '~/matanak/service'
addpath 'Colormaps'

sizelabel = 12 %22;
sizeaxis = 12% 17;
fntsz=sizelabel;
axssz=sizeaxis;

global sizeaxis sizelabel di de
resize_cicles = 25; % 50

[cmap]=parula;
[cmapp]=buildcmap('kbcw'); %buildcmap('kbcgyrm');
[cmaps] = buildcmap('bwr');
%[cmaps] = colormap(centered('RdYlGn'))


addpath(genpath('..'));

fai_dbscan=true;
read_dbscan=false;
leggi=false;
save_all=false;

set(gca,'fontname','Futura')
set(0, 'DefaultAxesFontName', 'Futura');


Lx = 40;
Ly = 15;
Lz = 10;
dp = 227710.76724846876 %m
code_n= 1000000.0 %[m^-3]
code_V= 299792458.0 %[m/s]
code_J= 4.803204712570472e-05 %[A/m^2]
code_B = 1.3744360177550602e-05 %T
code_E = 4120.455521265211/200 %V/m
code_T = 1.5032776159851256e-10 %K



if(fai_dbscan)


    if(leggi)
        fn ='GEM-Fields_036000.h5';
        fn2 =replace(fn,'Fields','Derived');
        Jex = hdf5read(fn,'/Step#0/Block/Jx_0/0/')+hdf5read(fn,'/Step#0/Block/Jx_2/0/');
        Jey = hdf5read(fn,'/Step#0/Block/Jy_0/0/')+hdf5read(fn,'/Step#0/Block/Jy_2/0/');
        Jez = hdf5read(fn,'/Step#0/Block/Jz_0/0/')+hdf5read(fn,'/Step#0/Block/Jz_2/0/');

        rhoe = hdf5read(fn,'/Step#0/Block/rho_0/0/') + hdf5read(fn,'/Step#0/Block/rho_2/0/');

        Ex = hdf5read(fn,'/Step#0/Block/Ex/0/');%+hdf5read(fn,'/Step#0/Block/Ex_ext/0/');
        Ey = hdf5read(fn,'/Step#0/Block/Ey/0/');%+hdf5read(fn,'/Step#0/Block/Ey_ext/0/');
        Ez = hdf5read(fn,'/Step#0/Block/Ez/0/');%+hdf5read(fn,'/Step#0/Block/Ez_ext/0/');



        JedotE=Ex.*Jex+Ey.*Jey+Ez.*Jez;
        %[agyro,Nx,Ny,Nz] = read_binVTK_scalar(dir,'Agyro',cycle,0);

        agyro = hdf5read(fn2,'/Step#0/Block/Pagy_2/0/');
        figure(303)
        imagesc(squeeze(agyro(:,:,60)));colorbar

        Bx = hdf5read(fn,'/Step#0/Block/Bx/0/');%+hdf5read(fn,'/Step#0/Block/Bx_ext/0/');
        By = hdf5read(fn,'/Step#0/Block/By/0/');%+hdf5read(fn,'/Step#0/Block/By_ext/0/');
        Bz = hdf5read(fn,'/Step#0/Block/Bz/0/');%+hdf5read(fn,'/Step#0/Block/Bz_ext/0/');
        [Nx,Ny, Nz] = size(Bx);




        Epar = Ex.*Bx + Ey.*By + Ez.*Bz;
        B2 = Bx.*Bx + By.*By + Bz.*Bz;
        E2 = Ex.*Ex + Ey.*Ey + Ez.*Ez;
        vE = (E2.*B2- Epar.*Epar)./E2./E2;
        vB = (E2.*B2- Epar.*Epar)./B2./B2;
        Epar = Epar./sqrt(B2);

        Epx = Ex + (Jey.*Bz - Jez.*By)./rhoe;
        Epy = Ey + (Jez.*Bx - Jex.*Bz)./rhoe;
        Epz = Ez + (Jex.*By - Jey.*Bx)./rhoe;

        Je=sqrt(Jex.^2+Jey.^2+Jez.^2);

        Jix = hdf5read(fn,'/Step#0/Block/Jx_1/0/') + hdf5read(fn,'/Step#0/Block/Jx_3/0/');
        Jiy = hdf5read(fn,'/Step#0/Block/Jy_1/0/') + hdf5read(fn,'/Step#0/Block/Jy_3/0/');
        Jiz = hdf5read(fn,'/Step#0/Block/Jz_1/0/') + hdf5read(fn,'/Step#0/Block/Jz_3/0/');


        JidotE=Ex.*Jix+Ey.*Jiy+Ez.*Jiz;
        JdotEp=(Jex+Jix).*Epx + (Jey+Jiy).*Epy + (Jez+Jiz).*Epz;
        Ji=sqrt(Jix.^2+Jiy.^2+Jiz.^2);

        % Vex = Jex./rhoe;
        % Vey = Jey./rhoe;
        % Vez = Jez./rhoe;

        clear  Jey Jez Jix Jiy Jiz Epx Epy Epz Jix Jiy Jiz

        rangex = 1:round(Nx);
        rangey = 1:Ny;
        Jehalf=Je(rangex,rangey,1:Nz);
        Jihalf=Ji(rangex,rangey,1:Nz);
        agyro_half=agyro(rangex,rangey,1:Nz);
        rhoe_half=rhoe(rangex,rangey,1:Nz);

        JedotE_half=JedotE(rangex,rangey,1:Nz);
        JidotE_half=JidotE(rangex,rangey,1:Nz);
        JdotEp_half=JdotEp(rangex,rangey,1:Nz);
        Epar_half=Epar(rangex,rangey,1:Nz);
        vE_half=vE(rangex,rangey,1:Nz);
        vB_half=vB(rangex,rangey,1:Nz);
        B2_half=B2(rangex,rangey,1:Nz);

        Vex_half = Jex(rangex,rangey,1:Nz)./rhoe(rangex,rangey,1:Nz);


        clear rhoe rhoi Je agyro JedotE JidotE JdotEp E2 B2 Epar vE vB Ji
        clear Ex Ey Ez
    end



    di=1
    de=1/sqrt(256)


    method = 1
    target_function = abs(JedotE_half);
    target_function = Jehalf;

    figure(2001)
    for i=1:Nz
imagesc([0 Lx],[0 Ly],target_function(:,:,i)'); colorbar; axis equal; axis tight
    xlabel('x/d_i','fontsize',sizelabel)
    ylabel('y/d_i','fontsize',sizelabel)
    set(gca,'fontsize',sizeaxis)
    caxis([0 5e-3])
    title(['target function at iz=' num2str(i)],'fontsize',sizelabel)
    pause(.1)
    end
    print('-dpng','-r300','cut_jehalf.png')

    switch method
        case 1
            Je_max=max(target_function(:).^2);
            Jsat = tanh(target_function.^2./Je_max*10);

            is=Jsat>.8;
             is = target_function>max(target_function(:)/3); %prctile(target_function(:),99);

            % is = Jehalf>sqrt(Je_max)/10;
            %
        case 2
            Jcaz= imgaussfilt3(Jehalf, 10);

            is=Jehalf-Jcaz>max(Jehalf-Jcaz,[],'all')/8;

            Jcaz= imgaussfilt3(abs(target_function), 10);

            is=abs(target_function)-Jcaz> max(abs(target_function)-Jcaz,[],'all')/8;
    end

    Jsat(~is)=0;
    Jsat(is) = 1;

    [Nx,Ny,Nz]=size(Jehalf);


    [i,j,k]=ndgrid(1:Nx,1:Ny,1:Nz);

   
    % hold on
    % plot3(i(is),j(is),k(is),'k.','markersize',.1)

    X=[i(is),j(is),k(is)];

    

    figure(2002)
    imagesc([0 Lx],[0 Ly],is(:,:,1)'); colorbar; axis equal; axis tight
    xlabel('x/d_i','fontsize',sizelabel)
    ylabel('y/d_i','fontsize',sizelabel)
    set(gca,'fontsize',sizeaxis)
    title('binary is at iz=1','fontsize',sizelabel)
    print('-dpng','-r300','cut_jsat.png')
    %hold on
    %plot(X(:,1),X(:,2),'.','markersize',.1)


    figure(2003)
    imagesc([0 Lx],[0 Ly],squeeze(mean(is,3))'); colorbar; axis equal; axis tight
    xlabel('x/d_i','fontsize',sizelabel)
    ylabel('y/d_i','fontsize',sizelabel)
    set(gca,'fontsize',sizeaxis)
    title('binary is mean along z','fontsize',sizelabel)
    print('-dpng','-r300','cut_jsat.png')
    %Extract only region in a cone around the x-axis
    %ilim = X(:,1).^2<X(:,2).^2*2;
    %size(X)
    %X=X(ilim,:);
    %size(X)

    % old values
    epsilon = 5;
    minpts = 5;
    % new values
    %     minpts = 2*2; % 2 times the dimensionality of the space
    %     epsilon = 1/dx/sqrt(mass_ratio) % electron skin depth
    %     epsilon = max(epsilon, 5);
    %
    %idx=dbscan(X,epsilon,minpts);

    Np=max(size(X))
    disp('calling DBscan')
    [idx, corepts] = dbscan(X(1:Np,:),epsilon,minpts);

elseif (read_dbscan)
    %load('dec20_2021_pt1'); %full system
    load('Jun15-2022-method1.mat');% half system
else
    disp('No Data - We use data in memory')
end

dx = Lx/Nx;
dy = Ly/Ny;
dz = Lz/Nz;

disp(['Number of Clusters = ', num2str(max(idx))])
min_idx=1
Y=X(idx>min_idx,:);
Npy=max(size(Y));
idy=idx(idx>min_idx);


colormap(cmapp)
figure(100)
scatter3(Y(:,1)*dx,Y(:,2)*dx,Y(:,3)*dz,ones(Npy,1),idy,'filled')
xlabel('x/d_i','fontsize',14)
ylabel('z/d_i','fontsize',14)
zlabel('z/d_i','fontsize',14)
axis equal


min_idx=5
Y=X(idx>min_idx,:);
Npy=max(size(Y));
idy=idx(idx>min_idx);


colormap(cmapp)
figure(101)
scatter3(Y(:,1)*dx,Y(:,2)*dx,Y(:,3)*dz,ones(Npy,1),idy,'filled')
xlabel('x/d_i','fontsize',14)
ylabel('z/d_i','fontsize',14)
zlabel('z/d_i','fontsize',14)
axis equal



colors=flipud(distinguishable_colors(290));
%colors=parula(290);
set(groot,'defaultAxesColorOrder',colors);
colormap(colors)
figure(1001)
gscatter(X(1:Np,1),X(1:Np,2),idx, colors)
axis equal
figure(1002)
subplot(2,1,1)
gscatter(Y(:,1)*dx,Y(:,2)*dy,idy,colors,[],[],'off')
xlabel('x/d_i','fontsize',sizelabel)
ylabel('y/d_i','fontsize',sizelabel)
set(gca,'fontsize',sizeaxis)
axis equal
axis tight
xlim([0 Lx/2])
ylim([0 Ly])
subplot(2,1,2)
gscatter(Y(:,1)*dx,Y(:,2)*dy,idy,colors,[],[],'off')
xlabel('x/d_i','fontsize',sizelabel)
ylabel('y/d_i','fontsize',sizelabel)
set(gca,'fontsize',sizeaxis)
axis equal
axis tight
xlim([Lx/2 Lx])
ylim([0 Ly])
print('-dpng','-r300','scatterXY.png')


figure(1003)
subplot(2,1,1)
gscatter(Y(:,1)*dx,Y(:,3)*dz,idy,colors,[],[],'off')
xlabel('x/d_i','fontsize',sizelabel)
ylabel('z/d_i','fontsize',sizelabel)
set(gca,'fontsize',sizeaxis)
axis equal
axis tight
xlim([0 Lx/2])
ylim([0 Lz])
subplot(2,1,2)
gscatter(Y(:,1)*dx,Y(:,3)*dz,idy,colors,[],[],'off')
xlabel('x/d_i','fontsize',sizelabel)
ylabel('z/d_i','fontsize',sizelabel)
set(gca,'fontsize',sizeaxis)
axis equal
axis tight
xlim([Lx/2 Lx])
ylim([0 Lz])
print('-dpng','-r300','scatterXZ.png')

figure(2003)
gscatter(Y(:,1)*dx,Y(:,3)*dz,idy,colors,[],[],'off')
xlabel('x/d_i','fontsize',sizelabel)
ylabel('z/d_i','fontsize',sizelabel)
set(gca,'fontsize',sizeaxis)
axis equal
axis tight
xlim([0 Lx/2])
xlim([0 15])
ylim([0 Lz])
print('-dpng','-r300','scatterXZ.png')

figure(1004)
gscatter(Y(:,2)*dy,Y(:,3)*dz,idy,colors,[],[],'off')
xlabel('y/d_i','fontsize',sizelabel)
ylabel('z/d_i','fontsize',sizelabel)
set(gca,'fontsize',sizeaxis)
axis equal
axis tight
xlim([0 Ly])
ylim([0 Lz])
print('-dpng','-r300','scatterYZ.png')


[Nx,Ny,Nz]=size(Jehalf);
object=[];
nbin = max(idx)
hbin=[];
sizebin=[];
ibend = nbin
index = 0;
for ib=1:nbin
    xp=X(idx==ib,1);
    yp=X(idx==ib,2);
    zp=X(idx==ib,3);
    ap_mean=0;
    Epar_mean=0;
    vB_mean=0;
    vE_mean=0;
    vE_mean=1e5;
    B2_mean=0;
    jedotE_mean=0;
    jidotE_mean=0;
    jdotEp_mean=0;
    je_tot=0;
    ji_tot=0;
    np=max(size(xp));
    if(np>minpts)
        for ip= 1:np
            ap_mean=ap_mean+agyro_half(xp(ip),yp(ip),zp(ip));
            je_tot=je_tot+Jehalf(xp(ip),yp(ip),zp(ip));
            ji_tot=ji_tot+Jihalf(xp(ip),yp(ip),zp(ip));
            jedotE_mean=jedotE_mean+(JedotE_half(xp(ip),yp(ip),zp(ip)));
            jdotEp_mean=jdotEp_mean+(JdotEp_half(xp(ip),yp(ip),zp(ip)));
            jidotE_mean=jidotE_mean+(JidotE_half(xp(ip),yp(ip),zp(ip)));
            Epar_mean=Epar_mean+(Epar_half(xp(ip),yp(ip),zp(ip)));
            padding = 5;
            xrange_s = xp(ip)-padding:xp(ip)+padding;
            yrange_s = yp(ip)-padding:yp(ip)+padding;
            zrange_s = zp(ip)-padding:zp(ip)+padding;
            xrange_s = xrange_s(xrange_s>1);xrange_s = xrange_s(xrange_s<Nx);
            yrange_s = yrange_s(yrange_s>1);yrange_s = yrange_s(yrange_s<Ny);
            zrange_s = zrange_s(zrange_s>1);zrange_s = zrange_s(zrange_s<Nz);
            %vEround=1-tanh(vE_half(xrange_s,yrange_s,zrange_s)/1e2);
            %vE_mean=max(vE_mean,max(vEround(:)));
            %vE_mean=min(vE_mean,min(min(min(vE_half(xrange_s,yrange_s,zrange_s)))));
            vE_mean=vE_mean+(vE_half(xp(ip),yp(ip),zp(ip)));
            vB_mean=vB_mean+(vB_half(xp(ip),yp(ip),zp(ip)));
            B2_mean=B2_mean+(B2_half(xp(ip),yp(ip),zp(ip)));
        end
        je_tot=je_tot*dx*dy*dz;
        ji_tot=ji_tot*dx*dy*dz;
        ap_mean=ap_mean/np;
        jedotE_mean=jedotE_mean/np;
        jdotEp_mean=jdotEp_mean/np;
        jidotE_mean=jidotE_mean/np;
        Epar_mean=Epar_mean/np;
        vE_mean=vE_mean/np;
        vB_mean=vB_mean/np;
        B2_mean=B2_mean/np;
        Xin =[xp*dx yp*dy zp*dz];
        [n1 n2]=size(Xin);
        %if(n1>n2&np>10)
        index = index+1;
        gm=fitgmdist(Xin,1,'RegularizationValue',0.001*dx);
        %gm=fitgmdist(Xin,1,'RegularizationValue',0.1);
        ev = sqrt(eig(gm.Sigma));

        [veig, leig,type_eig] = eig_jac(Bx,By,Bz,gm,dx,dy,dz);


        %leig=ones(3,3);
        %type_eig=0;

        %angle(diag(leig))
        %type_eig

        %hbin(index,:)=[mean(xp)*dx mean(yp)*dy mean(zp)*dz std(xp)*dx std(yp)*dy std(zp)*dz ap_mean jedotE_mean jidotE_mean];
        hbin(index,:)=[gm.mu ev(1) ev(2) ev(3)  ap_mean jedotE_mean jidotE_mean jdotEp_mean np Epar_mean B2_mean ib diag(leig)' type_eig vE_mean vB_mean je_tot ji_tot];


        %end
    end
end

disp(['Number of Shown Clusters = ', num2str(max(size(hbin)))])

hbin = sortrows(hbin, 7,'descend');
sizebin=hbin(:,11);

dx_off = dx/2



figure(301)
subplot(2,3,1)
hist(hbin(:,4)/de,20)
xline(di/de,'-.','d_i/d_e','fontsize',sizeaxis)
xlabel('\sigma_1/d_e','fontsize',fntsz)
subplot(2,3,2)
hist(hbin(:,5)/de,20)
xline(di/de,'-.','d_i/d_e','fontsize',sizeaxis)
xlabel('\sigma_2/d_e','fontsize',fntsz)
subplot(2,3,3)
hist(hbin(:,6)/de,20)
xline(di/de,'-.','d_i/d_e','fontsize',sizeaxis)
xlabel('\sigma_1/d_e','fontsize',fntsz)
subplot(2,3,4)
hist(hbin(:,5)./hbin(:,4),20)
xlabel('\sigma_2/\sigma_1','fontsize',fntsz)
subplot(2,3,5)
hist(hbin(:,6)./hbin(:,4),20)
xlabel('\sigma_3/\sigma_1','fontsize',fntsz)
subplot(2,3,6)
hist(hbin(:,6)./hbin(:,5),20)
xlabel('\sigma_3/\sigma_2','fontsize',fntsz)



hlabel = true;
symc = false;
plotdiag = true;
xscale='log';
yscale='log';
cscale='linear';

fig_plot(1,hbin(:,4)+dx_off,hbin(:,6)+dx_off,log10(sizebin)*resize_cicles,hbin(:,7),cmap ...
    ,xscale,yscale,cscale,'\sigma_1','\sigma_3','Agyro','current_scales_agyro_13.png' ...
    ,sizeaxis,sizelabel,di,de,symc,hlabel,plotdiag)

plotdiag = false;
hlabel = false;

fig_plot(2,hbin(:,4)+dx_off,hbin(:,5)+dx_off,log10(sizebin)*resize_cicles,hbin(:,7),cmap ...
    ,xscale,yscale,cscale,'\sigma_1','\sigma_2','Agyro','current_scales_agyro_12.png' ...
    ,sizeaxis,sizelabel,di,de,symc,hlabel,plotdiag)


% resize_cicles1=25

fig_plot(3,hbin(:,4)+dx_off,hbin(:,6)./hbin(:,4),log10(sizebin)*resize_cicles,hbin(:,7),cmap ...
    ,xscale,yscale,cscale,'\sigma_1','\sigma_3/\sigma_1','Agyro','current_scales_agyro_ar1.png' ...
    ,sizeaxis,sizelabel,di,de,symc,hlabel,plotdiag)

fig_plot(4,hbin(:,4)+dx_off,hbin(:,5)./hbin(:,4),log10(sizebin)*resize_cicles,hbin(:,7),cmap ...
    ,xscale,yscale,cscale,'\sigma_1','\sigma_2/\sigma_1','Agyro','current_scales_agyro_ar2.png' ...
    ,sizeaxis,sizelabel,di,de,symc,hlabel,plotdiag)

symc = true;

fig_plot(5,hbin(:,4)+dx_off,hbin(:,5)./hbin(:,4),log10(sizebin)*resize_cicles,hbin(:,8),cmaps ...
    ,xscale,yscale,cscale,'\sigma_1','\sigma_2/\sigma_1','JeE','current_scales_jedotE_ar1.png' ...
    ,sizeaxis,sizelabel,di,de,symc,hlabel,plotdiag)


fig_plot(6,hbin(:,4)+dx_off,hbin(:,5)./hbin(:,4),log10(sizebin)*resize_cicles,hbin(:,9),cmaps ...
    ,xscale,yscale,cscale,'\sigma_1','\sigma_2/\sigma_1','JiE','current_scales_jidotE_ar1.png' ...
    ,sizeaxis,sizelabel,di,de,symc,hlabel,plotdiag)


fig_plot(7,hbin(:,4)+dx_off,hbin(:,5)./hbin(:,4),log10(sizebin)*resize_cicles,hbin(:,10),cmaps ...
    ,xscale,yscale,cscale,'\sigma_1','\sigma_2/\sigma_1','JEp','current_scales_jdotEp_ar1.png' ...
    ,sizeaxis,sizelabel,di,de,symc,hlabel,plotdiag)


fig_plot(8,hbin(:,4)+dx_off,hbin(:,5)./hbin(:,4),log10(sizebin)*resize_cicles,hbin(:,8)+hbin(:,9),cmaps ...
    ,xscale,yscale,cscale,'\sigma_1','\sigma_2/\sigma_1','JE','current_scales_jdotE_ar1.png' ...
    ,sizeaxis,sizelabel,di,de,symc,hlabel,plotdiag)

fig_plot(9,hbin(:,4)+dx_off,hbin(:,5)./hbin(:,4),log10(sizebin)*resize_cicles,hbin(:,12),cmaps ...
    ,xscale,yscale,cscale,'\sigma_1','\sigma_2/\sigma_1','E_{||}','current_scales_Epar_ar1.png' ...
    ,sizeaxis,sizelabel,di,de,symc,hlabel,plotdiag)


symc = false;
fig_plot(10,hbin(:,4)+dx_off,hbin(:,5)./hbin(:,4),log10(sizebin)*resize_cicles,log10(hbin(:,19)),winter ...
    ,xscale,yscale,cscale,'\sigma_1','\sigma_2/\sigma_1','log10(vE)','current_scales_vE_ar1.png' ...
    ,sizeaxis,sizelabel,di,de,symc,hlabel,plotdiag)



fig_plot(11,hbin(:,7),hbin(:,12)*code_E*1e3,log10(sizebin)*resize_cicles,log10(hbin(:,19)),cmapp ...
    ,xscale,yscale,cscale,'Agyrotropy','E_{||}[mV/m]','Lorentz','agyro_Epar_Lorentzng' ...
    ,sizeaxis,sizelabel,di,de,symc,hlabel,plotdiag)

fig_plot(12,hbin(:,end-1),hbin(:,end-1)./hbin(:,end),log10(sizebin)*resize_cicles,log10(hbin(:,7)),cmap ...
    ,xscale,yscale,cscale,'<J_e>','<J_e>/<J_i>','Agyrotropy','je_ji_ratio_agyro.png' ...
    ,sizeaxis,sizelabel,di,de,symc,hlabel,plotdiag)

plotdiag=true;

fig_plot(13,hbin(:,end-1),hbin(:,end),log10(sizebin)*resize_cicles,log10(hbin(:,7)),cmap ...
    ,xscale,yscale,cscale,'<J_e>','<J_i>','Agyrotropy','je_ji_agyro.png' ...
    ,sizeaxis,sizelabel,di,de,symc,hlabel,plotdiag)


cscale='log';
fig_plot(14,hbin(:,end-1),hbin(:,end),log10(sizebin)*resize_cicles,(hbin(:,19)),cmapp ...
    ,xscale,yscale,cscale,'<J_e>','<J_e>/<J_i>','Lorentz','je_ji_lorentz.png' ...
    ,sizeaxis,sizelabel,di,de,symc,hlabel,plotdiag)




if(save_all)
    close all
    save(['output' num2str(today)])
end

%function [veig, leig, type_eig] = eig_jac(B0x,B0y,B0z,Vex,Vey,Vez,Ex,Ey,Ez,gm,dx,dy,dz)
function [veig, leig, type_eig] = eig_jac(B0x,B0y,B0z,gm,dx,dy,dz)
[Nx, Ny, Nz]= size(B0x);
IJK=round(gm.mu/dx)
i1=IJK(1);
i2=IJK(2);
i3=IJK(3);

%[Bfx, Bfy, Bfz]=cross_prod(Vex(i1,i2,i3),Vey(i1,i2,i3),Vez(i1,i2,i3),Ex,Ey,Ez);

Bx = B0x; %- Bfx;
By = B0y; %- Bfy;
Bz = B0z; %- Bfz;

% bcx=Bx(i1,i2,i3);
% bcy=By(i1,i2,i3);
% bcz=Bz(i1,i2,i3);
jac=zeros(3,3)
if (i1<Nx & i1>1)
    jac(1,1)=(Bx(i1+1,i2,i3)-Bx(i1-1,i2,i3))/2/dx;
    jac(2,1)=(By(i1+1,i2,i3)-By(i1-1,i2,i3))/2/dx;
    jac(3,1)=(Bz(i1+1,i2,i3)-Bz(i1-1,i2,i3))/2/dx;
elseif (i1==Nx)
    jac(1,1)=(Bx(i1,i2,i3)-Bx(i1-1,i2,i3))/dx;
    jac(2,1)=(By(i1,i2,i3)-By(i1-1,i2,i3))/dx;
    jac(3,1)=(Bz(i1,i2,i3)-Bz(i1-1,i2,i3))/dx;
elseif (i1==1)
    jac(1,1)=(Bx(i1+1,i2,i3)-Bx(i1,i2,i3))/dx;
    jac(2,1)=(By(i1+1,i2,i3)-By(i1,i2,i3))/dx;
    jac(3,1)=(Bz(i1+1,i2,i3)-Bz(i1,i2,i3))/dx;
end

if (i2<Ny & i2>1)
    jac(1,2)=(Bx(i1,i2+1,i3)-Bx(i1,i2-1,i3))/2/dy;
    jac(2,2)=(By(i1,i2+1,i3)-By(i1,i2-1,i3))/2/dy;
    jac(3,2)=(Bz(i1,i2+1,i3)-Bz(i1,i2-1,i3))/2/dy;
elseif (i2==Ny)
    jac(1,2)=(Bx(i1,i2,i3)-Bx(i1,i2-1,i3))/dy;
    jac(2,2)=(By(i1,i2,i3)-By(i1,i2-1,i3))/dy;
    jac(3,2)=(Bz(i1,i2,i3)-Bz(i1,i2-1,i3))/dy;
elseif (i2==1)
    jac(1,2)=(Bx(i1,i2+1,i3)-Bx(i1,i2,i3))/dy;
    jac(2,2)=(By(i1,i2+1,i3)-By(i1,i2,i3))/dy;
    jac(3,2)=(Bz(i1,i2+1,i3)-Bz(i1,i2,i3))/dy;
end

if (i3<Nz & i3>1)
    jac(1,3)=(Bx(i1,i2,i3+1)-Bx(i1,i2,i3-1))/2/dz;
    jac(2,3)=(By(i1,i2,i3+1)-By(i1,i2,i3-1))/2/dz;
    jac(3,3)=(Bz(i1,i2,i3+1)-Bz(i1,i2,i3-1))/2/dz;
elseif (i3==Nz)
    jac(1,3)=(Bx(i1,i2,i3)-Bx(i1,i2,i3-1))/dz;
    jac(2,3)=(By(i1,i2,i3)-By(i1,i2,i3-1))/dz;
    jac(3,3)=(Bz(i1,i2,i3)-Bz(i1,i2,i3-1))/dz;
elseif (i2==1)
    jac(1,3)=(Bx(i1,i2,i3+1)-Bx(i1,i2,i3))/2/dz;
    jac(2,3)=(By(i1,i2,i3+1)-By(i1,i2,i3))/2/dz;
    jac(3,3)=(Bz(i1,i2,i3+1)-Bz(i1,i2,i3))/2/dz;
end


tra=trace(jac);
[veig,leig]=eig(jac);

%A,B,As,Bs
ll=diag(leig);
[lls, ill]=sort(abs(imag(ll)));
lls1=ll(ill);
if(abs(imag(lls1(2)))>1e-10)
    %As or Bs
    if(real(lls1(2))>0)
        %As
        type_eig = 3;
    else
        %Bs
        type_eig = 4;
    end
else
    %As or Bs
    [lls, ill]=sort(real(ll));
    lls1=ll(ill);
    if(real(lls1(2))>0)
        %As
        type_eig = 1;
    else
        %Bs
        type_eig = 2;
    end

end
end


