function [] = fig_plot(nfig, x,y,z,c,cmap,xscale,yscale,cscale,xlb,ylb,nome, ...
    filenome,sizeaxis,sizelabel,di,de,sym,hlabel,plotdiag)
figure(nfig)
scatter(x,y,z,c,'filled')
colormap(cmap)

xline(di,'-.','di')
xline(de,'-.','d_e','fontsize',sizeaxis)
if(hlabel)
    yline(di,'-.','di')
    yline(de,'-.','d_e','fontsize',sizeaxis)
end    
xlabel(xlb,'fontsize',sizelabel)
ylabel(ylb,'fontsize',sizelabel)
set(gca,'fontsize',sizeaxis)
if(sym)
    cmax=max(abs(c))
    caxis([-cmax cmax])
end    
if(plotdiag)
    hold on
    xmin= min(x(:))
    xmax = max(x(:))
    plot([xmin xmax], [xmin xmax])
end    
set(gca,'xscale',xscale)
set(gca,'yscale',yscale)
h=colorbar;
h.Label.String = nome
colormap(cmap)
set(gca,'ColorScale',cscale)
print('-dpng','-r300',filenome)
end
