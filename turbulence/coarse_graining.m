addpath '~/Data/codes/matanak/service'
addpath 'Colormaps'
close all
!rm myfile.h5


mean_je=[];

do_cg = false
do_sf =true

for run_number = 1:2 

    if run_number == 1
        run = 'h5HR'
        cyc = 11000;
    end  
    if run_number == 2
        run = 'h5LR'
        cyc = 8000;
    end  


    ncyc=num2str(cyc,'%06d')
            fn =['~/Data/generic7/' run '/Inset-Fields_' ncyc '.h5'];
    Ex = hdf5read(fn,'/Step#0/Block/Ex/0/');%+hdf5read(fn,'/Step#0/Block/Ex_ext/0/');
Ey = hdf5read(fn,'/Step#0/Block/Ey/0/');%+hdf5read(fn,'/Step#0/Block/Ey_ext/0/');
Ez = hdf5read(fn,'/Step#0/Block/Ez/0/');%+hdf5read(fn,'/Step#0/Block/Ez_ext/0/');

Bx = hdf5read(fn,'/Step#0/Block/Bx/0/');%+hdf5read(fn,'/Step#0/Block/Ex_ext/0/');
By = hdf5read(fn,'/Step#0/Block/By/0/');%+hdf5read(fn,'/Step#0/Block/Ey_ext/0/');
Bz = hdf5read(fn,'/Step#0/Block/Bz/0/');%+hdf5read(fn,'/Step#0/Block/Ez_ext/0/');


for species = 1:1;
nspecies = num2str(species);




        if (species==0)
Jx = hdf5read(fn,'/Step#0/Block/Jx_0/0/');%+hdf5read(fn,'/Step#0/Block/Jx_2/0/');
Jy = hdf5read(fn,'/Step#0/Block/Jy_0/0/');%+hdf5read(fn,'/Step#0/Block/Jy_2/0/');
Jz = hdf5read(fn,'/Step#0/Block/Jz_0/0/');%+hdf5read(fn,'/Step#0/Block/Jz_2/0/');

rho = abs(hdf5read(fn,['/Step#0/Block/rho_0/0/']));%+hdf5read(fn,['/Step#0/Block/rho_2/0/']));
%rho = rho + max(abs(rho(:)))/100;
Jx = Jx + hdf5read(fn,'/Step#0/Block/Vfx/0/').*rho;
Jy = Jy + hdf5read(fn,'/Step#0/Block/Vfy/0/').*rho;
Jz = Jz + hdf5read(fn,'/Step#0/Block/Vfz/0/').*rho;
        else
Jx = hdf5read(fn,'/Step#0/Block/Jx_1/0/');%+hdf5read(fn,'/Step#0/Block/Jx_3/0/');
Jy = hdf5read(fn,'/Step#0/Block/Jy_1/0/');%+hdf5read(fn,'/Step#0/Block/Jy_3/0/');
Jz = hdf5read(fn,'/Step#0/Block/Jz_1/0/');%+hdf5read(fn,'/Step#0/Block/Jz_3/0/');

rho = abs(hdf5read(fn,['/Step#0/Block/rho_1/0/']));%+hdf5read(fn,['/Step#0/Block/rho_3/0/']));
%rho = rho + max(abs(rho(:)))/100;
Jx = Jx + hdf5read(fn,'/Step#0/Block/Vfx/0/').*rho;
Jy = Jy + hdf5read(fn,'/Step#0/Block/Vfy/0/').*rho;
Jz = Jz + hdf5read(fn,'/Step#0/Block/Vfz/0/').*rho;
        end

% generic

Lx =   100.0           
Ly =   50           
Lz =   66.667 
nxc =  400              
nyc =  200             
nzc =  288 
dxg = Lx/nxc
dyg = Ly/nyc
dzg = Lz/nzc


% inset
Lx = 15;
Ly = 10;
Lz = 10;

[Nx, Ny, Nz] = size(Ex);
dx = Lx / Nx;
dy = Ly / Ny;
dz = Lz / Nz;

nbufferx = 5


Jx = Jx(nbufferx:end-nbufferx,:,:);
Jy = Jy(nbufferx:end-nbufferx,:,:);
Jz = Jz(nbufferx:end-nbufferx,:,:);
Ex = Ex(nbufferx:end-nbufferx,:,:);
Ey = Ey(nbufferx:end-nbufferx,:,:);
Ez = Ez(nbufferx:end-nbufferx,:,:);
rho = rho(nbufferx:end-nbufferx,:,:);
mask=ones(size(Ex));
Bx = Bx(nbufferx:end-nbufferx,:,:);
By = By(nbufferx:end-nbufferx,:,:);
Bz = Bz(nbufferx:end-nbufferx,:,:);

%nbufferx=10;
%mask(1:nbufferx,:,:)=0;
%mask(end:end-nbufferx,:,:)=0;


[Nx, Ny, Nz] = size(Ex);

nsx=round(Nx/2)
nsy=round(Ny/2)
nsz=round(Nz/2)

system(['rm ' run 'cyc' ncyc 'spec' nspecies '.h5'])
if (do_cg) 

Nbin=500;
cg = 150; %30;
hh=zeros(Nbin,cg);
target_function = Jx .* Ex + Jy .* Ey +Jz.* Ez;
Np = prod(size(target_function))
[hh(:,1),bins] = hist(target_function(:),Nbin);
scales = 0;
figure(4)
imagesc([0 Lx], [0 Ly], squeeze(target_function(:,:,1))')
    axis equal
    axis tight
    colorbar
    pause(.1)
print('-dpng',[run 'CG_' nspecies '_2D_' ncyc '_frame_' num2str(0,'%03d') '.png'])

mean_tt = sum(target_function(:).*mask(:));
for icg=1:cg
    smooth = (icg)*.1; 
    scales = [scales smooth];
    rhosm= imgaussfilt3(rho,smooth);
    Exsm= imgaussfilt3(Ex.*rho,smooth)./rhosm;
    Eysm= imgaussfilt3(Ey.*rho,smooth)./rhosm;
    Ezsm= imgaussfilt3(Ez.*rho,smooth)./rhosm;
    Jxsm= imgaussfilt3(Jx,smooth);
    Jysm= imgaussfilt3(Jy,smooth);
    Jzsm= imgaussfilt3(Jz,smooth);
    tsm = Jxsm .* Exsm + Jysm .* Eysm +Jzsm.* Ezsm;

    [hh(:,icg+1)] = hist(tsm(:),bins);
    mean_tt = [mean_tt; sum(tsm(:).*mask(:))];
    if(mod(icg,10)==0)
    close all
    figure(4)
    imagesc([0 Lx], [0 Ly], squeeze(tsm(:,:,1))')
    axis equal
        axis tight
    title(['Scale = ' num2str(smooth)])    
    colorbar
    pause(.1)
    print('-dpng',[run 'CG_' nspecies  '_2D_' ncyc '_frame_' num2str(icg,'%3d') '.png'])
    end
end
close all
figure(1)
semilogy(bins,hh)
xlabel('$\langle J \rangle_\ell \cdot \langle E\rangle _\ell$','Interpreter','latex','fontsize',15)
ylabel('PDF')
xline(0)
print('-dpng',[run 'CG_1D_' ncyc '.png'])

close all
figure(2)
pcolor(bins,scales,hh')
set(gca,'ColorScale','log')
set(gca,'Yscale','log')
ylabel('Coarse Graining $\ell$','Interpreter','latex','fontsize',15)
xlabel('$\langle J \rangle_\ell \cdot \langle E\rangle _\ell$','Interpreter','latex','fontsize',15)
load gist_ncar.mat
shading interp
colormap(gist_ncar)
colorbar
print('-dpng',[run 'CG_' nspecies '_FC_' ncyc '.png'])

close all
figure(3)
plot(scales,mean_tt,'linewidth',2)
yline(0)
xlabel('Coarse Graining $\ell$','Interpreter','latex','fontsize',15)
ylabel('$\int \langle J \rangle_\ell \cdot \langle E\rangle _\ell$','Interpreter','latex','fontsize',15)
print('-dpng',[run 'CG_' nspecies '_scales_' ncyc '.png'])

close all
figure(4)
imagesc(mean(target_function,3) )
mean_je =[mean_je; mean_tt'];


h5create([run 'cyc' ncyc 'spec' nspecies '.h5'],['/mean_' ncyc],size(mean_tt))
h5write([run 'cyc' ncyc 'spec' nspecies '.h5'],['/mean_' ncyc],(mean_tt))

h5create([run 'cyc' ncyc 'spec' nspecies '.h5'],['/scales_' ncyc],size(scales))
h5write([run 'cyc' ncyc 'spec' nspecies '.h5'],['/scales_' ncyc],(scales))

end
close all
figure(5)
V=sqrt(Ex.^2+Ey.^2+Ez.^2);
hV= hist(V(:),100)
semilogy(hist(V(:),100))
print('-dpng',[run 'Espctrum_' ncyc '.png'])
h5create([run 'cyc' ncyc 'spec' nspecies '.h5'],['/Espec_' ncyc],size(hV))
h5write([run 'cyc' ncyc 'spec' nspecies '.h5'],['/Espec_' ncyc],(hV))


V=sqrt(Bx.^2+By.^2+ Bz.^2);
name_var='B'
strucutre_function


V=sqrt(Ex.^2+Ey.^2+ Ez.^2);
name_var='E'
strucutre_function

V=sqrt(Jx.^2+Jy.^2+ Jz.^2)./abs(rho);
name_var='V'
strucutre_function
%V = Jx .* Ex + Jy .* Ey +Jz.* Ez;

% V=Bx;
% name_var='Bx'
% strucutre_function
% 
% V=By;
% name_var='By'
% strucutre_function
% 
% V=Bz;
% name_var='Bz'
% strucutre_function


end
end





    %close all
    %save(['CG_output' num2str(today)])
