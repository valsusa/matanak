close all

Lx =   100.0           
Ly =   50           
Lz =   66.667 
nxc =  400              
nyc =  200             
nzc =  288 
dxg = Lx/nxc
dyg = Ly/nyc
dzg = Lz/nzc

% inset
Lx = 15;
Ly = 10;
Lz = 10;

de0 = 1 /sqrt(256);


species = 0
nspecies = num2str(species);

for run_number = 1:2 

    if run_number == 1
        run = 'h5HR'
        cyc = 11000;
        line='-'
    end  
    if run_number == 2
        run = 'h5LR'
        cyc = 8000;
        line='--'
    end  


    ncyc=num2str(cyc,'%06d')
name_var='E'
elle_x = hdf5read([run 'cyc' ncyc 'spec' nspecies '.h5'],['/SF_ellex_' name_var 'cyc' ncyc]);
autX = hdf5read([run 'cyc' ncyc 'spec' nspecies '.h5'],['/SF_autX_' name_var 'cyc' ncyc]);

elle_y = hdf5read([run 'cyc' ncyc 'spec' nspecies '.h5'],['/SF_elley_' name_var 'cyc' ncyc]);
autY = hdf5read([run 'cyc' ncyc 'spec' nspecies '.h5'],['/SF_autY_' name_var 'cyc' ncyc]);

elle_z = hdf5read([run 'cyc' ncyc 'spec' nspecies '.h5'],['/SF_ellez_' name_var 'cyc' ncyc]);
autZ = hdf5read([run 'cyc' ncyc 'spec' nspecies '.h5'],['/SF_autZ_' name_var 'cyc' ncyc]);

nlx = max(size(elle_x));
nly = max(size(elle_y));
nlz = max(size(elle_z));
if(run=='h5LR')
    ii=elle_x>dxg;
    sx = nlx-max(size(elle_x(ii)));
    ii=elle_y>dyg;
    sy = nly-max(size(elle_y(ii)));
    ii=elle_z>dzg;
    sz = nlz-max(size(elle_z(ii)));

else
    sx=1;
    sy=1;
    sz=1;
end 
figure(1)
loglog(elle_x(sx:nlx),autX(sx:nlx),['r' line],elle_y(sy:nly),autY(sy:nly),['k' line],elle_z(sz:nlz),autZ(sz:nlz),['b' line])
hold on

if (do_cg)

mean_tt = hdf5read([run 'cyc' ncyc 'spec' nspecies '.h5'],['/mean_' ncyc]);
scales = hdf5read([run 'cyc' ncyc 'spec' nspecies '.h5'],['/scales_' ncyc]);


ncg = max(size(mean_tt));
%scales = (1:ncg)*dx;


if(run=='h5LR')
    ii=scales>dxg;
    sx = ncg-max(size(scales(ii)));
    sx=1
else
    sx=1;
end 
figure(2)


semilogx(scales(sx:end),mean_tt(sx:end),'linewidth',2)
yline(0)
hold on

end
end

figure(1)
legend('HRx','HRy','HRz','LRx','LRy','LRz','Location','southeast')
xlabel('$\ell/d_i$','interpreter','latex','fontsize',12)
xline(1,'_','d_{i0}')
xline(de0+dx,'_','d_{e0}')
print('-dpng',['CFR_Structure_Function' name_var 'cyc'  ncyc nspecies '.png'])


figure(2)
xlabel('Coarse Graining $\ell$','Interpreter','latex','fontsize',15)
ylabel('$\int \langle J \rangle_\ell \cdot \langle E\rangle _\ell$','Interpreter','latex','fontsize',15)
print('-dpng',[run 'CG_' nspecies '_scales_' ncyc '.png'])

