addpath 'matlab-parsek'

close all
%clear all

global mratio XSAT YSAT ZSAT
vthr=.045;

results_dir='/shared02/gianni/tred60/data1/';
%results_dir='/data2/gianni/tred81/data2D/';

filename=[results_dir 'settings.hdf']
Lx=hdf5read(filename,'/collective/Lx')
Ly=hdf5read(filename,'/collective/Ly')
Lz=hdf5read(filename,'/collective/Lz')
B0x=hdf5read(filename,'/collective/Bx0')
Dt=hdf5read(filename,'/collective/Dt')
XLEN=hdf5read(filename,'/topology/XLEN')
YLEN=hdf5read(filename,'/topology/YLEN')
ZLEN=hdf5read(filename,'/topology/ZLEN')
mratio=abs(hdf5read(filename,'/collective/species_0/qom'))
Nprocs=hdf5read(filename,'/topology/Nprocs')
vthi=hdf5read(filename,'/collective/species_1/uth')
vthe=hdf5read(filename,'/collective/species_0/uth')
% instability in cavities
ipx=XLEN/2-1; ipy=YLEN/2+2;
%ipx=9; ipy=YLEN/2+1;
%ipx=XLEN/2+3; ipy=YLEN/2;

%ipx=XLEN/2; ipy=YLEN/2;
%ipx=XLEN/2+4; ipy=YLEN/2+1;

% inflow:
ipx = XLEN/2+1
ipy = YLEN/2+3
% outflow:
ipx = XLEN/4+1
ipy = YLEN/2+1

zzp=[];
EEX=[];
EEY=[];
EEZ=[];
BBX=[];
BBY=[];
BBZ=[];
JPAR=[];
EPAR=[];
JX=[];
JY=[];
JZ=[];
JXI=[];
JYI=[];
JZI=[];
NE=[];
NI=[];
XSAT=[];YSAT=[];ZSAT=[];
for ipz=1:ZLEN;
ip=(ipx-1)*YLEN*ZLEN+(ipy-1)*ZLEN+ipz-1;

nome=[results_dir 'VirtualSatelliteTraces' num2str(ip) '.txt']
system(['gunzip ' nome])

fid=fopen(nome);
for i=1:27
x=fscanf(fid,'%f',3); 
xp(i)=x(1);
yp(i)=x(2);
zp(i)=x(3);
end
mean(xp)
mean(yp)
mean(zp)


a=fscanf(fid,'%f',[14 inf])';

fclose(fid);
skip=0;
bx=a(:,1+skip);
by=a(:,2+skip);
bz=a(:,3+skip);
ex=a(:,4+skip);
ey=a(:,5+skip);
ez=a(:,6+skip);
jxe=a(:,7+skip);
jye=a(:,8+skip);
jze=a(:,9+skip);
jxi=a(:,10+skip);
jyi=a(:,11+skip);
jzi=a(:,12+skip);
rhoe=a(:,13+skip)*4*pi;
rhoi=a(:,14+skip)*4*pi;

b=sqrt(bx.*bx+by.*by+bz.*bz);
epar=(ex.*bx+ey.*by+ez.*bz)./b;
jepar=(jxe.*bx+jye.*by+jze.*bz)./b;

[n m]=size(bx);

n0=mean(rhoi(:)-rhoe(:))/2; n0_cavity=.02
n0_cavity=n0
b0=mean(sqrt(bx(:).^2+by(:).^2+bz(:).^2));%b0=B0x;
wci=b0;
wpi=1*sqrt(n0);
wpi_cavity=1*sqrt(n0_cavity);
wce=wci*mratio;
wlh=1/sqrt(1/wce/wci+1/wpi_cavity^2);
%wpi=1 %apparently the plasma oscillations are generated elsewhere where n0=1

n1=floor(n/27)
%n1=14555
%n1=6851
%n1=12530

%n1=14000;

%n1=3200

ex=reshape(ex(1:n1*27),27,n1);
ey=reshape(ey(1:n1*27),27,n1);
ez=reshape(ez(1:n1*27),27,n1);
bx=reshape(bx(1:n1*27),27,n1);
by=reshape(by(1:n1*27),27,n1);
bz=reshape(bz(1:n1*27),27,n1);
rhoe=reshape(rhoe(1:n1*27),27,n1);
rhoi=reshape(rhoi(1:n1*27),27,n1);
epar=reshape(epar(1:n1*27),27,n1);
jepar=reshape(jepar(1:n1*27),27,n1);
jze=reshape(jze(1:n1*27),27,n1);
jye=reshape(jye(1:n1*27),27,n1);
jxe=reshape(jxe(1:n1*27),27,n1);
jzi=reshape(jzi(1:n1*27),27,n1);
jyi=reshape(jyi(1:n1*27),27,n1);
jxi=reshape(jxi(1:n1*27),27,n1);
t=linspace(0,n1,n1);




isatx=1;isaty=1;isatz=1:3;
isat=(isatx-1)*3*3+(isaty-1)*3+isatz;

%[xp(isat);yp(isat);zp(isat)]
zzp=[zzp zp(isat)]
EEX=[EEX ;ex(isat,:)];
EEY=[EEY ;ey(isat,:)];
EEZ=[EEZ ;ez(isat,:)];
BBX=[BBX ;bx(isat,:)];
BBY=[BBY ;by(isat,:)];
BBZ=[BBZ ;bz(isat,:)];
XSAT=[XSAT,xp(isat)];
YSAT=[YSAT,yp(isat)];
ZSAT=[ZSAT,zp(isat)];
EPAR=[EPAR ;epar(isat,:)];

JPAR=[JPAR ;jepar(isat,:)./rhoe(isat,:)];
JZ=[JZ ;jze(isat,:)./rhoe(isat,:)];
JY=[JY ;jye(isat,:)./rhoe(isat,:)];
JX=[JX ;jxe(isat,:)./rhoe(isat,:)];
JZI=[JZI ;jzi(isat,:)./rhoi(isat,:)];
JYI=[JYI ;jyi(isat,:)./rhoi(isat,:)];
JXI=[JXI ;jxi(isat,:)./rhoi(isat,:)];
NE=[NE ;rhoe(isat,:)];
NI=[NI ;rhoi(isat,:)];
end
SSX=EEY.*BBZ-EEZ.*BBY;
SSY=EEZ.*BBX-EEX.*BBZ;
SSZ=EEX.*BBY-EEY.*BBX;

[n1 n2]=size(EEX);
BB=sqrt(BBX.^2+BBY.^2+BBZ.^2);


epar=zeros(n1,n2);eperp1=zeros(n1,n2);eperp2=zeros(n1,n2);
bpar=zeros(n1,n2);bperp1=zeros(n1,n2);bperp2=zeros(n1,n2);

Ndetrend=300;

    SSXdet=detrend(SSX,24,Ndetrend);
    SSYdet=detrend(SSY,24,Ndetrend);
    SSZdet=detrend(SSZ,24,Ndetrend);
    
    BBXdet=detrend(BBX,24,Ndetrend);
    BBYdet=detrend(BBY,24,Ndetrend);
    BBZdet=detrend(BBZ,24,Ndetrend);
    EEXdet=detrend(EEX,24,Ndetrend);
    EEYdet=detrend(EEY,24,Ndetrend);
    EEZdet=detrend(EEZ,24,Ndetrend);
   
    BBdet=detrend(BB,24,Ndetrend);
    NEdet=detrend(NE,24,Ndetrend);
   

for i=1:24
bxavg=mean(BBX(i,:),2);
byavg=mean(BBY(i,:),2);
bzavg=mean(BBZ(i,:),2);
bavg=sqrt(bxavg.^2+byavg.^2+bzavg.^2)
bmag2D=sqrt(bxavg.^2+byavg.^2);
perp2x=bzavg.*bxavg./(bavg.*bmag2D);
perp2y=bzavg.*byavg./(bavg.*bmag2D);
perp2z=-bmag2D./bavg;

epar(i,:)=(EEXdet(i,:).*bxavg + EEYdet(i,:).*byavg + EEZdet(i,:).*bzavg)./bavg;
eperp1(i,:)=(byavg.*EEXdet(i,:)-bxavg.*EEYdet(i,:))./bmag2D;
eperp2(i,:)=perp2x.*EEXdet(i,:)+perp2y.*EEYdet(i,:)+perp2z.*EEZdet(i,:);

bpar(i,:)=(BBXdet(i,:).*bxavg + BBYdet(i,:).*byavg + BBZdet(i,:).*bzavg)./bavg;
bperp1(i,:)=(byavg.*BBXdet(i,:)-bxavg.*BBYdet(i,:))./bmag2D;
bperp2(i,:)=perp2x.*BBXdet(i,:)+perp2y.*BBYdet(i,:)+perp2z.*BBZdet(i,:);

end

space_time=false
if(space_time)
figure(1)
pcolor((1:n1)*Dt*B0x,zzp,EEX)
shading interp
colorbar
title(['EX    x= ' num2str(xp(isat(1)))  '   y=   ' num2str(yp(isat(1)))])
xlabel('\omega_{ci}t')
ylabel('z')
set(gcf,'Renderer','zbuffer');
print('-dpng','EEXsat_xy.png' )
figure(2)
pcolor((1:n1)*Dt*B0x,zzp,EEY)
shading interp
colorbar
title(['EY    x= ' num2str(xp(isat(1)))  '   y=   ' num2str(yp(isat(1)))])
xlabel('\omega_{ci}t')
ylabel('z')
set(gcf,'Renderer','zbuffer');
print('-dpng','EEYsat_xy.png' )
figure(3)
pcolor((1:n1)*Dt*B0x,zzp,EEZ)
shading interp
colorbar
title(['EZ    x= ' num2str(xp(isat(1)))  '   y=   ' num2str(yp(isat(1)))])
xlabel('\omega_{ci}t')
ylabel('z')
set(gcf,'Renderer','zbuffer');
print('-dpng','EEZsat_xy.png' )

figure(4)
pcolor((1:n1)*Dt*B0x,zzp,BBX)
shading interp
colorbar
title(['BX    x= ' num2str(xp(isat(1)))  '   y=   ' num2str(yp(isat(1)))])
xlabel('\omega_{ci}t')
ylabel('z')
set(gcf,'Renderer','zbuffer');
print('-dpng','BBXsat_xy.png' )
figure(5)
pcolor((1:n1)*Dt*B0x,zzp,BBY)
shading interp
colorbar
title(['BY    x= ' num2str(xp(isat(1)))  '   y=   ' num2str(yp(isat(1)))])
xlabel('\omega_{ci}t')
ylabel('z')
set(gcf,'Renderer','zbuffer');
print('-dpng','BBYsat_xy.png' )
figure(6)
pcolor((1:n1)*Dt*B0x,zzp,BBZ)
shading interp
colorbar
title(['BZ    x= ' num2str(xp(isat(1)))  '   y=   ' num2str(yp(isat(1)))])
xlabel('\omega_{ci}t')
ylabel('z')
set(gcf,'Renderer','zbuffer');
print('-dpng','BBZsat_xy.png' )

figure(7)
pcolor((1:n1)*Dt*B0x,zzp,NE)
shading interp
colorbar
title(['\rho_e   x= ' num2str(xp(isat(1)))  '   y=   ' num2str(yp(isat(1)))])
xlabel('\omega_{ci}t')
ylabel('z')
set(gcf,'Renderer','zbuffer');
print('-dpng','NEsat_xy.png' )

figure(8)
pcolor((1:n1)*Dt*B0x,zzp,JPAR)
shading interp
colorbar
title(['u_{e||}   x= ' num2str(xp(isat(1)))  '   y=   ' num2str(yp(isat(1)))])
xlabel('\omega_{ci}t')
ylabel('z')
set(gcf,'Renderer','zbuffer');
print('-dpng','UEPARsat_xy.png' )

figure(9)
pcolor((1:n1)*Dt*B0x,zzp,JX)
shading interp
colorbar
title(['u_{ex}   x= ' num2str(xp(isat(1)))  '   y=   ' num2str(yp(isat(1)))])
xlabel('\omega_{ci}t')
ylabel('z')
set(gcf,'Renderer','zbuffer');
print('-dpng','UEXsat_xy.png' )
close(9)

figure(9)
pcolor((1:n1)*Dt*B0x,zzp,JY)
shading interp
colorbar
title(['u_{ex}   x= ' num2str(xp(isat(1)))  '   y=   ' num2str(yp(isat(1)))])
xlabel('\omega_{ci}t')
ylabel('z')
set(gcf,'Renderer','zbuffer');
print('-dpng','UEYsat_xy.png' )
close(9)

figure(10)
pcolor((1:n1)*Dt*B0x,zzp,JZ)
shading interp
colorbar
title(['u_{ez}   x= ' num2str(xp(isat(1)))  '   y=   ' num2str(yp(isat(1)))])
xlabel('\omega_{ci}t')
ylabel('z')
set(gcf,'Renderer','zbuffer');
print('-dpng','UEZsat_xy.png' )

figure(9) 
pcolor((1:n1)*Dt*B0x,zzp,JXI)
shading interp 
colorbar 
title(['u_{ix}   x= ' num2str(xp(isat(1)))  '   y=   ' num2str(yp(isat(1)))])
xlabel('\omega_{ci}t')
ylabel('z') 
set(gcf,'Renderer','zbuffer');
print('-dpng','UIXsat_xy.png' )
close(9) 
 
figure(9) 
pcolor((1:n1)*Dt*B0x,zzp,JYI)
shading interp 
colorbar 
title(['u_{ix}   x= ' num2str(xp(isat(1)))  '   y=   ' num2str(yp(isat(1)))])
xlabel('\omega_{ci}t')
ylabel('z') 
set(gcf,'Renderer','zbuffer');
print('-dpng','UIYsat_xy.png' )
close(9) 

figure(9) 
pcolor((1:n1)*Dt*B0x,zzp,JZI)
shading interp 
colorbar 
title(['u_{ix}   x= ' num2str(xp(isat(1)))  '   y=   ' num2str(yp(isat(1)))])
xlabel('\omega_{ci}t')
ylabel('z') 
set(gcf,'Renderer','zbuffer');
print('-dpng','UIZsat_xy.png' )
close(9) 

figure(11)
pcolor((1:n1)*Dt*B0x,zzp,NI)
shading interp
colorbar
title(['\rho_i   x= ' num2str(xp(isat(1)))  '   y=   ' num2str(yp(isat(1)))])
xlabel('\omega_{ci}t')
ylabel('z')
set(gcf,'Renderer','zbuffer');
print('-dpng','NIsat_xy.png' )

figure(12)
pcolor((1:n1)*Dt*B0x,zzp,SSX)
shading interp
colorbar
title(['ExB_x   x= ' num2str(xp(isat(1)))  '   y=   ' num2str(yp(isat(1)))])
xlabel('\omega_{ci}t')
ylabel('z')
set(gcf,'Renderer','zbuffer');
print('-dpng','SXsat_xy.png' )
close(12)
pcolor((1:n1)*Dt*B0x,zzp,SSXdet)
shading interp
colorbar
title(['ExB_x_{det}   x= ' num2str(xp(isat(1)))  '   y=   ' num2str(yp(isat(1)))])
xlabel('\omega_{ci}t')
ylabel('z')
set(gcf,'Renderer','zbuffer');
print('-dpng','SXsat_detrended_xy.png' )


figure(13)
pcolor((1:n1)*Dt*B0x,zzp,SSY)
shading interp
colorbar
title(['ExB_y   x= ' num2str(xp(isat(1)))  '   y=   ' num2str(yp(isat(1)))])
xlabel('\omega_{ci}t')
ylabel('z')
set(gcf,'Renderer','zbuffer');
print('-dpng','SYsat_xy.png' )

figure(14)
pcolor((1:n1)*Dt*B0x,zzp,SSZ)
shading interp
colorbar
title(['ExB_z   x= ' num2str(xp(isat(1)))  '   y=   ' num2str(yp(isat(1)))])
xlabel('\omega_{ci}t')
ylabel('z')
set(gcf,'Renderer','zbuffer');
print('-dpng','SZsat_xy.png' )
end

fourier_space = true;
if(fourier_space)

    %
    % Spectra by probe
    %
    global ymin ymax
    ymin=0;ymax=0;

[SPET, fSPET] = plot_spect(SSX, 'SSX',Dt, NI, NE, BBX, BBY, BBZ, false);
plot_avg_spect(SPET, fSPET, 'SSX', NI, NE, BBX, BBY, BBZ);

[SPET, fSPET] = plot_spect(SSXdet, 'SSXdet', Dt, NI, NE, BBX, BBY, BBZ, false);
plot_avg_spect(SPET, fSPET, 'SSXdet', NI, NE, BBX, BBY, BBZ);
[SPET, fSPET] = plot_spect(SSXdet, 'SSXdet', Dt, NI, NE, BBX, BBY, BBZ, false);
plot_avg_spect(SPET, fSPET, 'SSXdet', NI, NE, BBX, BBY, BBZ);
[SPET, fSPET] = plot_spect(EEXdet, 'EEXdet',Dt, NI, NE, BBX, BBY, BBZ, false);
plot_avg_spect(SPET, fSPET, 'EEXdet', NI, NE, BBX, BBY, BBZ);
[SPET, fSPET] = plot_spect(EEYdet, 'EEYdet',Dt, NI, NE, BBX, BBY, BBZ, false);
plot_avg_spect(SPET, fSPET, 'EEYdet', NI, NE, BBX, BBY, BBZ);
[SPET, fSPET] = plot_spect(EEZdet, 'EEZdet',Dt, NI, NE, BBX, BBY, BBZ, false);
plot_avg_spect(SPET, fSPET, 'EEZdet', NI, NE, BBX, BBY, BBZ);

[SPET, fSPET] = plot_spect(BBYdet, 'BBYdet',Dt, NI, NE, BBX, BBY, BBZ, false);
plot_avg_spect(SPET, fSPET, 'BBXdet', NI, NE, BBX, BBY, BBZ);
[SPET, fSPET] = plot_spect(BBYdet, 'BBYdet',Dt, NI, NE, BBX, BBY, BBZ, false);
plot_avg_spect(SPET, fSPET, 'BBYdet', NI, NE, BBX, BBY, BBZ);
[SPET, fSPET] = plot_spect(BBZdet, 'BBZdet',Dt, NI, NE, BBX, BBY, BBZ, false);
plot_avg_spect(SPET, fSPET, 'BBZdet', NI, NE, BBX, BBY, BBZ);

ymin=1e-11;ymax=1e-4; %inflow
ymin=1e-9;ymax=1e-2; %outflow
[SPET, fSPET] = plot_spect(bperp1, 'bperp1',Dt, NI, NE, BBX, BBY, BBZ, false);
plot_avg_spect(SPET, fSPET, 'bpepr1', NI, NE, BBX, BBY, BBZ);
[SPET, fSPET] = plot_spect(bperp2, 'bperp2',Dt, NI, NE, BBX, BBY, BBZ, false);
plot_avg_spect(SPET, fSPET, 'bpepr2', NI, NE, BBX, BBY, BBZ);
[SPET, fSPET] = plot_spect(bpar, 'bpar',Dt, NI, NE, BBX, BBY, BBZ, false);
plot_avg_spect(SPET, fSPET, 'bpar', NI, NE, BBX, BBY, BBZ);

ymin=1e-10;ymax=1e-5; %inflow
ymin=1e-10;ymax=1e-4; %outflow
[SPET, fSPET] = plot_spect(eperp1, 'eperp1',Dt, NI, NE, BBX, BBY, BBZ, false);
plot_avg_spect(SPET, fSPET, 'epepr1', NI, NE, BBX, BBY, BBZ);
[SPET, fSPET] = plot_spect(eperp2, 'eperp2',Dt, NI, NE, BBX, BBY, BBZ, false);
plot_avg_spect(SPET, fSPET, 'epepr2', NI, NE, BBX, BBY, BBZ);
[SPET, fSPET] = plot_spect(epar, 'epar',Dt, NI, NE, BBX, BBY, BBZ, false);
plot_avg_spect(SPET, fSPET, 'epar', NI, NE, BBX, BBY, BBZ);


%
% Rotation angles
%
plot_angles(bperp1,bperp2, 'bperp1', 'bperp2', Dt, NI, NE, BBX, BBY, BBZ);
%plot_angles_due(bperp1,bperp2, 'bperp1', 'bperp2', Dt, NI, NE, BBX, BBY, BBZ);
plot_angles(eperp1,eperp2, 'eperp1', 'eperp2', Dt, NI, NE, BBX, BBY, BBZ);
%plot_angles_due(eperp1,eperp2, 'eperp1', 'eperp2', Dt, NI, NE, BBX, BBY, BBZ);

plot_angles(eperp1,bperp1, 'eperp1', 'bperp1', Dt, NI, NE, BBX, BBY, BBZ);
plot_angles(eperp2,bperp2, 'eperp2', 'bperp2', Dt, NI, NE, BBX, BBY, BBZ);


%plot_angles(SSYdet,SSZdet, 'ssx', 'ssz', Dt, NI, NE, BBX, BBY, BBZ);


end
return

figure(8)
[sx,sy] = meshgrid(15,1:1:10);   
ut=ones(size(JPAR));
h=streamline(stream2((1:n1)*Dt*B0x,zzp,ut*Dt*B0x,JZ,sx,sy,[1]));
set(h,'Color','white');
[sx,sy] = meshgrid(13,1:1:10);   
ut=ones(size(JPAR));
h=streamline(stream2((1:n1)*Dt*B0x,zzp,ut*Dt*B0x,JZ,sx,sy,[1]));
set(h,'Color','m');
set(gcf,'Renderer','zbuffer');
print('-dpng','Flowtraces_xy.png' )

function [SPET, fSPET]=plot_spect(SSX, name_var,Dt, NI, NE, BBX, BBY, BBZ, graf)
global mratio XSAT YSAT ZSAT ymin ymax
SPET=[];
[nitems,ntime]=size(SSX)
for i=1:nitems

y=SSX(i,:);
Fs = 1/Dt;                    % Sampling frequency
T = 1/Fs;                     % Sample time
L = max(size(y));             % Length of signal
t = (0:L-1)*T;                % Time vector
%y=sin(2*pi*t);
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
Y = fft(y,NFFT)/L;
f = Fs/2*linspace(0,1,NFFT/2+1);
hold off
SPET=[SPET;Y(1:round(NFFT/2+1))]; fSPET=f;

if(graf)
    figure(1)
loglog(2*f*pi,2*abs(Y(1:round(NFFT/2+1))),'k')

ax=gca;

xlabel('\omega=2\pi f')
hold on
n0=mean(NI(i,:)-NE(i,:))/2; 
b0=mean(sqrt(BBX(i,:).^2+BBY(i,:).^2+BBZ(i,:).^2));%b0=B0x;

wci=b0;
wpi=1*sqrt(n0);
wce=wci*mratio;
%wcem=max(BBX(i,:))*mratio;
wlh=1/sqrt(1/wce/wci+1/wpi^2);
loglog([wpi wpi],ax.YLim,'g')
loglog(sqrt(mratio)*[wpi wpi],ax.YLim,'g--')
%loglog(sqrt(mratio)*[wpi_cavity wpi_cavity],[1e-6, 1e0],'g--')
loglog([wlh wlh],ax.YLim,'r')
loglog([wce wce],ax.YLim,'b--')
%loglog([wcem wcem],ax.YLim,'b--')
%loglog([wci wci],ax.YLim,'b')
%loglog([1/Dt 1/Dt]*pi,ax.YLim,'m')
%legend('spectrum','\omega_{pi}','\omega_{pe}','\omega_{pe,cavity}','\omega_{lh}','\omega_{ce}','\omega_{ci}')
legend('spectrum','\omega_{pi}','\omega_{pe}','\omega_{lh}','\omega_{ce},m','\omega_{ce},M','\omega_{ci}','\pi/Dt')
title(['x= ' num2str(XSAT(i))  '   y=   ' num2str(YSAT(i)) '   z=   ' num2str(ZSAT(i))])

if(ymax>ymin)
    ylim([ymin ymax])
end

set(gcf,'Renderer','zbuffer');
print('-dpng', '-r300',['SPEC_' name_var '_sat_' num2str(i) '.png'] )

close(1)
end
end
end

function []=boldyrev(NEdet, BBdet)
% incomplete list of input variables - to be finished
fn=fft(NEdet(i,:));fn=fn(1:round(NFFT/2+1));
fb=fft(BBdet(i,:));fb=fb(1:round(NFFT/2+1));

betai=vthi^2/B0x^2*n0*2;
normaliz = betai/2*(1+vthe^2/mratio/vthi^2);
normaliz = normaliz * (1+normaliz);
loglog(2*pi*f,normaliz*fn.*conj(fn)/(n0)^2./(fb.*conj(fb))*B0x^2);
SPETcomp=[SPETcomp;normaliz*fn.*conj(fn)/(n0)^2./(fb.*conj(fb))*B0x^2];fSPETcomp=f;
ax=gca;
xlabel('\omega=2\pi f')
hold on
loglog([wpi wpi],ax.YLim,'g')
loglog(sqrt(mratio)*[wpi wpi],ax.YLim,'g--')
%loglog(sqrt(mratio)*[wpi_cavity wpi_cavity],[1e-6, 1e0],'g--')
loglog([wlh wlh],ax.YLim,'r')
loglog([wce wce],ax.YLim,'b--')
grid on
%loglog([wci wci],ax.YLim,'b')
%loglog([1/Ndetrend/Dt 1/Ndetrend/Dt]*2*pi,ax.YLim,'m')
%legend('spectrum','\omega_{pi}','\omega_{pe}','\omega_{pe,cavity}','\omega_{lh}','\omega_{ce}','\omega_{ci}')
legend('spectrum','\omega_{pi}','\omega_{pe}','\omega_{lh}','\omega_{ce}','\omega_{ci}','\omega_{detrend}','location','NorthWest')
%title(['x= ' num2str(xp(isat(i)))  '   y=   ' num2str(yp(isat(i))) '   z=   ' num2str(zp(isat(1)))])
title(['x= ' num2str(XSAT(i))  '   y=   ' num2str(YSAT(i)) '   z=   ' num2str(ZSAT(i))])

set(gcf,'Renderer','zbuffer');
print('-dpng', '-r300',['SPEC_dnodb_sat' num2str(i) '_detrended_xy.png'] )
end

function []=plot_avg_spect(SPET, fSPET, name_var, NI, NE, BBX, BBY, BBZ)
global mratio XSAT YSAT ZSAT ymin ymax
close all
figure(1)
loglog(2*pi*fSPET,mean(abs(SPET),1));
ax=gca;
xlabel('\omega=2\pi f')
hold on

n0=mean(NI(:)-NE(:))/2; 
b0=mean(sqrt(BBX(:).^2+BBY(:).^2+BBZ(:).^2));%b0=B0x;

wci=b0;
wpi=1*sqrt(n0);
wce=wci*mratio;
wlh=1/sqrt(1/wce/wci+1/wpi^2);
loglog([wpi wpi],ax.YLim,'g')
loglog(sqrt(mratio)*[wpi wpi],ax.YLim,'g--')
%loglog(sqrt(mratio)*[wpi_cavity wpi_cavity],[1e-6, 1e0],'g--')
loglog([wlh wlh],ax.YLim,'r')
loglog([wce wce],ax.YLim,'b--')
grid on
%loglog([wci wci],ax.YLim,'b')
%loglog([1/Ndetrend/Dt 1/Ndetrend/Dt]*2*pi,ax.YLim,'m')
%legend('spectrum','\omega_{pi}','\omega_{pe}','\omega_{pe,cavity}','\omega_{lh}','\omega_{ce}','\omega_{ci}')
legend('spectrum','\omega_{pi}','\omega_{pe}','\omega_{lh}','\omega_{ce}','\omega_{ci}','\omega_{detrend}','location','SouthWest')
%title(['x= ' num2str(xp(isat(i)))  '   y=   ' num2str(yp(isat(i))) '   z=   ' num2str(zp(isat(1)))])
%title('mean along z')

if(ymax>ymin)
    ylim([ymin ymax])
end
set(gcf,'Renderer','zbuffer');
print('-dpng', '-r300',['SPEC_' name_var '_mean_xy.png'] )
save(['SPEC_' name_var '_mean_xy.mat'],'fSPET','SPET','mratio','wpi','wlh','wce')
close(1)
end

function []=plot_angles(bperp1,bperp2, var1_name, var2_name, Dt, NI, NE, BBX, BBY, BBZ)
global mratio XSAT YSAT ZSAT

y=bperp1(1,:)
Fs = 1/Dt;                    % Sampling frequency
T = 1/Fs;                     % Sample time
L = max(size(y));             % Length of signal
t = (0:L-1)*T;                % Time vector
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
f = Fs/2*linspace(0,1,NFFT/2+1);
fbp1 = fft(bperp1,NFFT,2)/L;fbp1=fbp1(:,1:round(NFFT/2+1));
fbp2 = fft(bperp2,NFFT,2)/L;fbp2=fbp2(:,1:round(NFFT/2+1));

close all
%subplot(2,1,1)
scatter(2*pi*f,angle(mean(fbp1./fbp2,1))/pi*180,[],log(abs(mean(fbp1,1))),'.')
ax=gca;
hold on
n0=mean(NI(1,:)-NE(1,:))/2; 
b0=mean(sqrt(BBX(1,:).^2+BBY(1,:).^2+BBZ(1,:).^2));%b0=B0x;

wci=b0;
wpi=1*sqrt(n0);
wce=wci*mratio;
wlh=1/sqrt(1/wce/wci+1/wpi^2);
plot([wpi wpi],ax.YLim,'g')

plot(sqrt(mratio)*[wpi wpi],ax.YLim,'g--')
%loglog(sqrt(mratio)*[wpi_cavity wpi_cavity],[1e-6, 1e0],'g--')
plot([wlh wlh],ax.YLim,'r')
plot([wce wce],ax.YLim,'b--')
 set(ax,'Xscale','log')
  xlabel('\omega=2\pi f')
 ylabel(['FFT(' var1_name ')/FFT(' var2_name ')'])

%  subplot(2,1,2)
% plot(2*pi*f,angle(fbp./fbp1)/pi*180,'.')
% ax=gca;
% hold on
% n0=mean(NI(1,:)-NE(1,:))/2; 
% b0=mean(sqrt(BBX(1,:).^2+BBY(1,:).^2+BBZ(1,:).^2));%b0=B0x;
% 
% wci=b0;
% wpi=1*sqrt(n0);
% wce=wci*mratio;
% plot([wpi wpi],ax.YLim,'g')
% 
% plot(sqrt(mratio)*[wpi wpi],ax.YLim,'g--')
% %loglog(sqrt(mratio)*[wpi_cavity wpi_cavity],[1e-6, 1e0],'g--')
% plot([wlh wlh],ax.YLim,'r')
% plot([wce wce],ax.YLim,'b--')
%  set(ax,'Xscale','log')
%  xlabel('\omega=2\pi f')
%  ylabel('FFT(B_{||})/FFT(B_{\perp 1})')
 colorbar horizontal
 caxis([-26.5 -13.5])
 axis tight
 %set(gca,'fontsize',[13])
 set(gcf,'Renderer','zbuffer');
print('-dpng',[var1_name '_over_' var2_name 'Angles.png'] )

end

function []=plot_angles_due(bperp1,bperp2, var1_name, var2_name, Dt, NI, NE, BBX, BBY, BBZ)
global mratio XSAT YSAT ZSAT

y=bperp1(1,:)
Fs = 1/Dt;                    % Sampling frequency
T = 1/Fs;                     % Sample time
L = max(size(y));             % Length of signal
t = (0:L-1)*T;                % Time vector
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
f = Fs/2*linspace(0,1,NFFT/2+1);
fbp1 = fft(bperp1,NFFT,2)/L;fbp1=fbp1(:,1:round(NFFT/2+1));
fbp2 = fft(bperp2,NFFT,2)/L;fbp2=fbp2(:,1:round(NFFT/2+1));

close all
%subplot(2,1,1)
scatter(2*pi*f,angle(mean(fbp1)./mean(fbp2,1))/pi*180,[],log(abs(mean(fbp1,1))),'.')
ax=gca;
hold on
n0=mean(NI(1,:)-NE(1,:))/2; 
b0=mean(sqrt(BBX(1,:).^2+BBY(1,:).^2+BBZ(1,:).^2));%b0=B0x;

wci=b0;
wpi=1*sqrt(n0);
wce=wci*mratio;
wlh=1/sqrt(1/wce/wci+1/wpi^2);
plot([wpi wpi],ax.YLim,'g')

plot(sqrt(mratio)*[wpi wpi],ax.YLim,'g--')
%loglog(sqrt(mratio)*[wpi_cavity wpi_cavity],[1e-6, 1e0],'g--')
plot([wlh wlh],ax.YLim,'r')
plot([wce wce],ax.YLim,'b--')
 set(ax,'Xscale','log')
  xlabel('\omega=2\pi f')
 ylabel(['FFT(' var1_name ')/FFT(' var2_name ')'])

%  subplot(2,1,2)
% plot(2*pi*f,angle(fbp./fbp1)/pi*180,'.')
% ax=gca;
% hold on
% n0=mean(NI(1,:)-NE(1,:))/2; 
% b0=mean(sqrt(BBX(1,:).^2+BBY(1,:).^2+BBZ(1,:).^2));%b0=B0x;
% 
% wci=b0;
% wpi=1*sqrt(n0);
% wce=wci*mratio;
% plot([wpi wpi],ax.YLim,'g')
% 
% plot(sqrt(mratio)*[wpi wpi],ax.YLim,'g--')
% %loglog(sqrt(mratio)*[wpi_cavity wpi_cavity],[1e-6, 1e0],'g--')
% plot([wlh wlh],ax.YLim,'r')
% plot([wce wce],ax.YLim,'b--')
%  set(ax,'Xscale','log')
%  xlabel('\omega=2\pi f')
%  ylabel('FFT(B_{||})/FFT(B_{\perp 1})')
 colorbar horizontal
 caxis([-26.5 -13.5])
 axis tight
 set(gcf,'Renderer','zbuffer');
print('-dpng',[var1_name '_over_' var2_name 'Angles_Due.png'] )

end

function Ad = detrend_old(A, n, Nd)
Ad=zeros(size(A));
for i=1:n
Ad(i,:)=A(i,:)-smooth(A(i,:),Nd)';
end
end

function Ad = detrend(A, n, Nd)
Ad=zeros(size(A));
[n1 nval]=size(A);
for i=1:n1
Ad(i,:)=A(i,:)-(A(i,1)+(A(i,end)-A(i,1))*(linspace(1,nval,nval)-1)/(nval-1));
end
end