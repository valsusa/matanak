addpath(genpath('~/Matlab/matanak/'));
close all
clear all

nome='/Users/glapenta/Desktop/DIIID/165128_3000_EFIT01.mat';
nome='/Users/glapenta/Desktop/DIIID/EFIT_171274_3300_v3.mat';
nome='/Users/glapenta/Desktop/DIIID/EFIT_171274_3300_v5.mat';
load(nome)

density_adjust = false;

% Refernce Pressure in Pascal
 p_ref = 300;
%n_ref=1; %for mercury

% Reference Temperature in eV
 kT_ref = 200;


% Temperature Ratio
 TioTe = 25/200;

%B in  Tesla
 B_ref = .2;
 
 mass_ratio=64;
 
 physical_electrons = false;
 
[code_n, code_J, code_V, code_T, code_E, code_B, di0, wpi0] =   ...
       code_units_tokamak(mass_ratio, physical_electrons, p_ref, kT_ref, TioTe, B_ref);


e= 1.6022e-19;
 mu0=4*pi*1e-7;
 eps0=8.8542*1.e-12;
 cphys=1/sqrt(mu0*eps0);

kT_ref_si=kT_ref*e;

Tratio=[1,TioTe];
qom=[-mass_ratio,1];
concentration=[-1.0,1.0];

ns=2;

%size in meters
Lxout=2.54 %radius
Lxin=0.84
Ly=1.6*2 %vertical size


figure(1)
%ZLIM=min(1.15,ZLIM);
%ZLIM=-min(-ZLIM,1.15);
%RLIM=max(1.01,RLIM);
plot(RLIM,ZLIM)
axis equal



[Ny, Nx]=size(Br)

[x,y]=meshgrid(1:Nx,1:Ny);
x=Lxin + (x-1)/(Nx-1)*(Lxout-Lxin);
y=(y-1)/(Ny-1)*Ly;
dr=(Lxout-Lxin)/(Nx-1);
dz=Ly/(Ny-1);


z=zeros(size(x));
y=y-Ly/2;
RLIM(RLIM<0.95)=0.95;
[in,on] = inpolygon(x,y,RLIM,ZLIM);
Jt(~in)=0;
Jr(~in)=0;
Jz(~in)=0;

pnew = P_EFIT;
pnex(~in)=0;

% old way assuming constant temperature
dens=pnew/kT_ref_si; %in SI
dens(~in)=0;
n = dens;
%nmax=max(n);
%n=(n+nmax/10)./(1.1);

% new mehtod
dens = ne;
dens(~in)=0;
dens(1:20,:)=0
n = dens;

kTe = Te*e;
kTi = (P_EFIT - ne.*kTe)./ne;
kTi(n<.5e19)=0;

% Remove vacuum field to be put in the external field
%Bpr=Bpr-VBpr;
%Bpz=Bpz-VBpz;


%To go from r,theta, z to code x, y, z meno must be set to -1
Jt(~in)=0;
Jz(~in)=0;
Jr(~in)=0;
Jt(1:20,:)=0;
Jz(1:20,:)=0;
Jr(1:20,:)=0;
meno = -1;


Vex = - Jr./ne/e/cphys;
Vey = - Jz./ne/e/cphys;
Vez = - meno*Jt./ne/e/cphys;


Jx = Jr;
Jy = Jz;
Jz = meno* Jt;


Vex(n<.5e19)=0;
Vey(n<.5e19)=0;
Vez(n<.5e19)=0;

Vex(isnan(Vex))=0
Vex(isinf(Vex))=0

Vey(isnan(Vey))=0
Vey(isinf(Vey))=0

Vez(isnan(Vez))=0
Vez(isinf(Vez))=0

Jtest = true
if(Jtest) 
[grBpr,gzBpr]=gradient(Br,dr,dz);
[grBpz,gzBpz]=gradient(Bz,dr,dz);
[grBt,gzBt]=gradient(Bt,dr,dz);
[grBrt,gzBrt]=gradient(x.*Bt,dr,dz);

Jx_test=-gzBt/mu0;
%Jy=(grBt+Bt./x)/mu0/code_J;
Jy_test=(grBrt./x)/mu0;
Jz_test=meno*(gzBpr-grBpz)/mu0;
figure(1)
subplot(3,3,1)
imagesc(Jx);colorbar;title('Jx')
subplot(3,3,2)
imagesc(Jy);colorbar;title('Jy')
subplot(3,3,3)
imagesc(Jz);colorbar;title('Jz')
subplot(3,3,4)
imagesc(Jx_test);colorbar;title('Jx_test')
subplot(3,3,5)
imagesc(Jy_test);colorbar;title('Jy_test')
subplot(3,3,6)
imagesc(Jz_test);colorbar;title('Jz_test')
print('-dpng','test_J.png')
close(1)
end



Bx=Br/code_B;
By=Bz/code_B;
Bz= meno * Bt/code_B;
Bz_ext = Bz*0;
Bx_ext = Bx*0;
By_ext = By*0;

B2=code_B.^2*((Bx+Bx_ext).^2+(By+By_ext).^2+(Bz+Bz_ext).^2);

%Diamagnetic Drift [Bad idea, the current is mostly parallel to B]
[grp,gzp]=gradient(pnew,dr,dz);

[Jdx,Jdy,Jdz]=cross_prod(grp,gzp,zeros(size(grp)),Bx+Bx_ext,By+By_ext,Bz+Bz_ext);
Jdx = - Jdx ./B2;
Jdy = - Jdy ./B2;
Jdz = - Jdz ./B2;
% Jdx(~in)=0;
% Jdy(~in)=0;
% Jdz(~in)=0;

Jix = Jdx / code_J;
Jiy = Jdy / code_J;
Jiz = Jdz / code_J;

[Ex,Ey,Ez]=cross_prod(Jdx./n/e,Jdy./n/e,Jdz./n/e,Bx+Bx_ext,By+By_ext,Bz+Bz_ext);

Ex = - Ex /code_E;
Ey = - Ey /code_E;
Ez = - Ez /code_E;
Ex(~in)=0;
Ey(~in)=0;
Ez(~in)=0;

nodrift=true;
if(nodrift)
Ex = zeros(size(Br)) ;
Ey = zeros(size(Br)) ;
Ez = zeros(size(Br)) ;

Jix = zeros(size(Br)) ;
Jiy = zeros(size(Br)) ;
Jiz = zeros(size(Br)) ;

% Jix = Jx / code_J / mass_ratio;
% Jiy = Jy / code_J / mass_ratio;
% Jiz = Jz / code_J / mass_ratio;
end

% Assuming me<<mi
Jex = Jx / code_J - Jix;
Jey = Jy / code_J - Jiy;
Jez = Jz / code_J - Jiz;
% ii=abs(Vex)>.1; Vex(ii)=Vex(ii)./abs(Vex(ii))*.1;
% ii=abs(Vey)>.1; Vey(ii)=Vey(ii)./abs(Vey(ii))*.1;
% ii=abs(Vez)>.1; Vez(ii)=Vez(ii)./abs(Vez(ii))*.1;

%Vex=0*Vex; Vey=0*Vey; %Vez=ones(size(Vez));

p=pnew;
p(~in)=0;

% old method
kT = (p ./ dens  )/code_T ; %in code units
kT(isnan(kT))=0
kT(isinf(kT))=0


kT=kT / sum(Tratio); % To equate teh pressure of all species. 

%new method
kTe = kTe /code_T;
kTi = kTi /code_T;


n=n/code_n;

h2=figure(2)
set(h2,'Position', [440 48 451 750])
subplot(3,3,1)
pcolor(x,y,Bx);shading interp;axis equal;axis tight;colorbar;title('Bx')
title('Bx')
subplot(3,3,2)
pcolor(x,y,By);shading interp;axis equal;axis tight;colorbar;title('By')
title('By')
subplot(3,3,3)
pcolor(x,y,Bz);shading interp;axis equal;axis tight;colorbar;title('Bz')
title('Bz')
subplot(3,3,4)
pcolor(x,y,Bx_ext);shading interp;axis equal;axis tight;colorbar;title('Bx_ext')
title('Bx_{ext}')
subplot(3,3,5)
pcolor(x,y,By_ext);shading interp;axis equal;axis tight;colorbar;title('By_ext')
title('By_{ext}')
subplot(3,3,6)
pcolor(x,y,Bz_ext);shading interp;axis equal;axis tight;colorbar;title('Bz_ext')
title('Bz_{ext}')
subplot(3,3,7)
pcolor(x,y,Bx_ext+Bx);shading interp;axis equal;axis tight;colorbar;title('Bx_tot')
title('Bx_{tot}')
subplot(3,3,8)
pcolor(x,y,By_ext+By);shading interp;axis equal;axis tight;colorbar;title('By_tot')
title('By_{tot}')
subplot(3,3,9)
pcolor(x,y,Bz_ext+Bz);shading interp;axis equal;axis tight;colorbar;title('Bz_tot')
title('Bz_{tot}')

h3=figure(3)
%set(h3,'Position', [440 48 451 750])
subplot(2,3,1)
pcolor(x,y,Jx);shading interp;axis equal;axis tight;colorbar;title('Jx')
subplot(2,3,2)
pcolor(x,y,Jy);shading interp;axis equal;axis tight;colorbar;title('Jy')
subplot(2,3,3)
pcolor(x,y,Jz);shading interp;axis equal;axis tight;colorbar;title('Jz')
subplot(2,3,4)
pcolor(x,y,n);shading interp;axis equal;axis tight;colorbar;;title('n')
subplot(2,3,5)
pcolor(x,y,kTe);shading interp;axis equal;axis tight;colorbar;title('kTe')
subplot(2,3,6)
pcolor(x,y,p);shading interp;axis equal;axis tight;colorbar;;title('p')

[grp,gzp]=gradient(pnew,dr,dz);
[JxBx,JxBy,JxBz]=cross_prod(Jr,Jz,-Jt,Br,Bz,-Bt);
[divX,dummy]=gradient(x.*Br,dr,dz);
[dummy,divY]=gradient(Bz,dr,dz);
div=divX./x+divY;

% [divX,dummy]=gradient(x.*(Bpr-VBpr),dr,dz);
% [dummy,divY]=gradient((Bpz-VBpz),dr,dz);
% div=divX./x+divY;

h4=figure(4)
set(h4,'Position', [440 48 451 750])
subplot(2,3,1)
pcolor(x,y,grp);shading interp;axis equal;axis tight;colorbar;title('-\nabla_x p')
%caxis([-300 300])
subplot(2,3,2)
pcolor(x,y,gzp);shading interp;axis equal;axis tight;colorbar;title('-\nabla_y p')
%caxis([-300 300])
subplot(2,3,3)
%pcolor(x,y,Viz);shading interp;axis equal;axis tight;colorbar
subplot(2,3,4)
pcolor(x,y,JxBx);shading interp;axis equal;axis tight;colorbar;title('[JxB]_x')
%caxis([-300 300])
subplot(2,3,5)
pcolor(x,y,JxBy);shading interp;axis equal;axis tight;colorbar;title('[JxB]_y')
%caxis([-300 300])
subplot(2,3,6)
pcolor(x,y,div);shading interp;axis equal;axis tight;colorbar;title('Div(B)')
%caxis([-300 300])


figure(5) 
plot(-grp(50,:))
hold on
plot(-JxBx(50,:))
legend('\nabla p_x','JxBx')

figure(6)
plot(-gzp(:,50))
hold on
plot(-JxBy(:,50))


h7=figure(7)
%set(h4,'Position', [440 48 451 750])
subplot(3,3,1)
pcolor(x,y,Jix);title('JixMHD');shading interp;axis equal;axis tight;colorbar
subplot(3,3,2)
pcolor(x,y,Jiy);title('JiyMHD');shading interp;axis equal;axis tight;colorbar
subplot(3,3,3)
pcolor(x,y,Jiz);title('JizMHD');shading interp;axis equal;axis tight;colorbar
subplot(3,3,4)
pcolor(x,y,Jex);title('JexMHD');shading interp;axis equal;axis tight;colorbar
subplot(3,3,5)
pcolor(x,y,Jey);title('JeyMHD');shading interp;axis equal;axis tight;colorbar
subplot(3,3,6)
pcolor(x,y,Jez);title('JezMHD');shading interp;axis equal;axis tight;colorbar



Nxpic=100+1;Nypic=200+1; Nzpic=1+1; 

Lxpic=Lxout;
%Lypic=0.8; 1.5;
Lypic=Ly/2;
Lypic=1.4;
dx=Lxpic/(Nxpic-1);
dy=2*Lypic/(Nypic-1);
dz=min(dx,dy);

xpic=linspace(0,Lxpic,Nxpic);

ypic=linspace(-Lypic,Lypic,Nypic);
if(Nzpic<=2) 
    zpic=[0 0];%[(zmin+zmax)/2 (zmin+zmax)/2] ;
else
    zpic=linspace(0,dz,Nzpic);
end
if(Nypic<=2) 
    ypic=[0 0];%[(ymin+ymax)/2 (ymin+ymax)/2] ;
else
    ypic=linspace(-Lypic,Lypic,Nypic);
end
[Xpic,Ypic,Zpic]=ndgrid(xpic,ypic,zpic);

kTpic(1:Nxpic,1:Nypic,1:Nzpic,1) = interpmio2d(x,y,z,kTe,Xpic,Ypic,Zpic); 
kTpic(1:Nxpic,1:Nypic,1:Nzpic,2) = interpmio2d(x,y,z,kTi,Xpic,Ypic,Zpic); 

fourpi = 4*pi;
npic=interpmio2d(x,y,z,n,Xpic,Ypic,Zpic)/fourpi;
%npic=ones(Nxpic, Nypic, Nzpic);

[in,on] = inpolygon(Xpic,Ypic,RLIM,ZLIM);

internalbc = true;

if(internalbc)
    % uses the actual wall
Lambdapic=zeros(size(Xpic));
Lambdapic(~in)=1;
%ii=Xpic<(Lxout+Lxin)/2;Lambdapic(ii)=0;
%ii=abs(Ypic)>0.7;
ii=abs(Ypic)>1.15;
Lambdapic(ii)=1;
else
    % uses the region n=0
Lambda = zeros(size(x))
ii=n==0;
Lambda(ii) = 1;
Lambdapic=interpmio2d(x,y,z,Lambda,Xpic,Ypic,Zpic);
end

Lambdapic = smooth3Donesided(Lambdapic,10);

size(Lambdapic)



Bxpic=interpmio2d(x,y,z,Bx,Xpic,Ypic,Zpic);
Bypic=interpmio2d(x,y,z,By,Xpic,Ypic,Zpic);
Bzpic=interpmio2d(x,y,z,Bz,Xpic,Ypic,Zpic);

Bxpic_ext=interpmio2d(x,y,z,Bx_ext,Xpic,Ypic,Zpic);
Bypic_ext=interpmio2d(x,y,z,By_ext,Xpic,Ypic,Zpic);
Bzpic_ext=interpmio2d(x,y,z,Bz_ext,Xpic,Ypic,Zpic);

Expic=interpmio2d(x,y,z,Ex,Xpic,Ypic,Zpic);
Eypic=interpmio2d(x,y,z,Ey,Xpic,Ypic,Zpic);
Ezpic=interpmio2d(x,y,z,Ez,Xpic,Ypic,Zpic);

Jxpic(1:Nxpic,1:Nypic,1:Nzpic,1)=interpmio2d(x,y,z,Jex,Xpic,Ypic,Zpic)/fourpi;
Jypic(1:Nxpic,1:Nypic,1:Nzpic,1)=interpmio2d(x,y,z,Jey,Xpic,Ypic,Zpic)/fourpi;
Jzpic(1:Nxpic,1:Nypic,1:Nzpic,1)=interpmio2d(x,y,z,Jez,Xpic,Ypic,Zpic)/fourpi;

Jxpic(1:Nxpic,1:Nypic,1:Nzpic,2)=interpmio2d(x,y,z,Jix,Xpic,Ypic,Zpic)/fourpi;
Jypic(1:Nxpic,1:Nypic,1:Nzpic,2)=interpmio2d(x,y,z,Jiy,Xpic,Ypic,Zpic)/fourpi;
Jzpic(1:Nxpic,1:Nypic,1:Nzpic,2)=interpmio2d(x,y,z,Jiz,Xpic,Ypic,Zpic)/fourpi;

V0xpic(1:Nxpic,1:Nypic,1:Nzpic,1)=interpmio2d(x,y,z,Vex,Xpic,Ypic,Zpic);
V0ypic(1:Nxpic,1:Nypic,1:Nzpic,1)=interpmio2d(x,y,z,Vey,Xpic,Ypic,Zpic);
V0zpic(1:Nxpic,1:Nypic,1:Nzpic,1)=interpmio2d(x,y,z,Vez,Xpic,Ypic,Zpic);

V0xpic(1:Nxpic,1:Nypic,1:Nzpic,2)=zeros(Nxpic,Nypic,Nzpic);
V0ypic(1:Nxpic,1:Nypic,1:Nzpic,2)=zeros(Nxpic,Nypic,Nzpic);
V0zpic(1:Nxpic,1:Nypic,1:Nzpic,2)=zeros(Nxpic,Nypic,Nzpic);

% Vex=interpmio2d(x,y,z,-Jx./dens/e./code_V,Xpic,Ypic,Zpic);
% Vey=interpmio2d(x,y,z,-Jy./dens/e./code_V,Xpic,Ypic,Zpic);
% Vez=interpmio2d(x,y,z,-Jz./dens/e./code_V,Xpic,Ypic,Zpic);

 
Pxxpic=zeros(size(Jxpic));

subplot(3,3,7)
pcolor(Xpic(:,:,1),Ypic(:,:,1),Lambdapic(:,:,1));title('Lambda_pic');shading interp;axis equal;axis tight;colorbar
subplot(3,3,8)
pcolor(Xpic(:,:,1),Ypic(:,:,1),npic(:,:,1));title('npic');shading interp;axis equal;axis tight;colorbar
subplot(3,3,9)
pcolor(Xpic(:,:,1),Ypic(:,:,1),Bxpic(:,:,1));title('Bxpic');shading interp;axis equal;axis tight;colorbar


!rm Tokamak-Fields_000000.h5
opath='DIIID-Fields_000000.h5'

h5create(opath,'/Step#0/Block/Lambda/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Lambda/0',Lambdapic)

h5create(opath,'/Step#0/Block/Bx/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bx/0',Bxpic)

h5create(opath,'/Step#0/Block/By/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/By/0',Bypic)

h5create(opath,'/Step#0/Block/Bz/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bz/0',Bzpic_ext)

h5create(opath,'/Step#0/Block/Bx_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bx_ext/0',Bxpic_ext)

h5create(opath,'/Step#0/Block/By_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/By_ext/0',Bypic_ext)

h5create(opath,'/Step#0/Block/Bz_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bz_ext/0',Bzpic)

h5create(opath,'/Step#0/Block/Ex/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ex/0',Expic)

h5create(opath,'/Step#0/Block/Ey/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ey/0',Eypic)

h5create(opath,'/Step#0/Block/Ez/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ez/0',Ezpic)


h5create(opath,'/Step#0/Block/Ex_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ex_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Ey_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ey_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Ez_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ez_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,['/Step#0/Block/rho_avg/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/rho_avg/0'],zeros(Nxpic, Nypic, Nzpic))

vth= [];
for is=1:ns
h5create(opath,['/Step#0/Block/Jx_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jx_' num2str(is-1) '/0'],V0xpic(1:Nxpic,1:Nypic,1:Nzpic,is).* npic.*concentration(is))

h5create(opath,['/Step#0/Block/Jy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jy_' num2str(is-1) '/0'],V0ypic(1:Nxpic,1:Nypic,1:Nzpic,is).* npic.*concentration(is))

h5create(opath,['/Step#0/Block/Jz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jz_' num2str(is-1) '/0'],V0zpic(1:Nxpic,1:Nypic,1:Nzpic,is).* npic.*concentration(is))


h5create(opath,['/Step#0/Block/rho_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/rho_' num2str(is-1) '/0'], npic*concentration(is))

% old method of constant T
%vth2= abs(qom(is)) * Tratio(is) * kTpic; 
% new method 
vth2= abs(qom(is)) .* kTpic(1:Nxpic,1:Nypic,1:Nzpic,is); 
vth=[vth;sqrt(max(vth2(:)))];
disp(['Max thermal Speed spacies ' num2str(is) '   ' num2str(sqrt(max(vth2(:)))) ])
pxx = (V0xpic(1:Nxpic,1:Nypic,1:Nzpic,is).^2 + vth2).* npic.*concentration(is);h5create(opath,['/Step#0/Block/Pxx_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxx_' num2str(is-1) '/0'], pxx);
Pxxpic(:,:,:,is) = ( vth2).* npic.*concentration(is);

pyy = (V0ypic(1:Nxpic,1:Nypic,1:Nzpic,is).^2 + vth2).* npic.*concentration(is);
h5create(opath,['/Step#0/Block/Pyy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pyy_' num2str(is-1) '/0'], pyy);

pzz = (V0zpic(1:Nxpic,1:Nypic,1:Nzpic,is).^2 + vth2).* npic.*concentration(is);
h5create(opath,['/Step#0/Block/Pzz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pzz_' num2str(is-1) '/0'], pzz);

h5create(opath,['/Step#0/Block/Pxy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxy_' num2str(is-1) '/0'], ...
    zeros(Nxpic, Nypic, Nzpic));

h5create(opath,['/Step#0/Block/Pxz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxz_' num2str(is-1) '/0'], ...
    zeros(Nxpic, Nypic, Nzpic));

h5create(opath,['/Step#0/Block/Pyz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pyz_' num2str(is-1) '/0'], ...
    zeros(Nxpic, Nypic, Nzpic));

ppic=(pxx+pyy+pzz)/3;


figure(8)
subplot(3,2,is)
Tpic=kTpic(1:Nxpic,1:Nypic,1:Nzpic,1);
imagesc(sqrt(Tpic(:,:,1))');colorbar;title([num2str(is) 'Tpic'])
subplot(3,2,is+2)
imagesc(Jzpic(:,:,1,is)'); colorbar;title([num2str(is) 'Jzpic'])
subplot(3,2,is+4)
imagesc(npic(:,:,1)'*concentration(is)); colorbar;title([num2str(is) 'npic'])
end

Lx_code = Lxpic/di0
Ly_code = 2*Lypic/di0
Lz_code=.1
one_sec = wpi0
dx=Lx_code/(Nxpic-1)
dy=Ly_code/(Nypic-1)
dz=Lz_code/(Nzpic-1)


[grBpr,gzBpr,gphBpr]=gradient(permute(Bxpic,[2 1 3]),dx,dy,dz);
[grBpz,gzBpz,gphBpz]=gradient(permute(Bypic,[2 1 3]),dx,dy,dz);
[grBt,gzBt,gphBt]=gradient(permute(Bzpic,[2 1 3]),dx,dy,dz);
[grBrt,gzBrt,gphBrt]=gradient(permute(Xpic.*Bzpic,[2 1 3]),dx,dy,dz);



mu0=4*pi;
Jx=-meno*permute(gzBt, [2 1 3])/mu0;
%Jy=(grBt+Bt./x)/mu0/code_J;
Jy=meno*(permute(grBrt, [2 1 3])./Xpic)/mu0;
Jz=meno * permute(gzBpr-grBpz, [2 1 3])/mu0;

Jx(npic==0) = 0;
Jy(npic==0) = 0;
Jz(npic==0) = 0;

if(Jtest) 
figure(9)
subplot(2,3,1)
imagesc(Jx(:,:,1)');title('Jx computed')
colorbar
subplot(2,3,2)
imagesc(Jy(:,:,1)');title('Jy computed')
colorbar
subplot(2,3,3)
imagesc(Jz(:,:,1)');title('Jz computed')
colorbar
subplot(2,3,4)
imagesc(Jxpic(:,:,1,1)'+0*Jxpic(:,:,1,2)');title('Jx code')
colorbar
subplot(2,3,5)
imagesc(Jypic(:,:,1,1)'+0*Jypic(:,:,1,2)');title('Jy code')
colorbar
subplot(2,3,6)
imagesc(Jzpic(:,:,1,1)'+0*Jzpic(:,:,1,2)');title('Jz code')
colorbar
print('-dpng','J_test_code.png')
close(9)
end

% dr =-Xpic(1,1,1,1)+Xpic(2,1,1,1)
% dz =-Ypic(1,1,1,1)+Ypic(1,2,1,1)
[grp,gzp]=gradient(permute(Pxxpic(:,:,1,1)/qom(1)+Pxxpic(:,:,1,2)/qom(2),[2 1 3]),dx,dy);
grp=permute(grp,[2 1 3]);
gzp=permute(gzp,[2 1 3]);
[JxBx,JxBy,JxBz]=cross_prod(Jxpic(:,:,1,2)+Jxpic(:,:,1,1),(Jypic(:,:,1,2)+Jypic(:,:,1,1)),(Jzpic(:,:,1,2)+Jzpic(:,:,1,1)),Bxpic(:,:,1)+Bxpic_ext(:,:,1),Bypic(:,:,1)+Bypic_ext(:,:,1),Bzpic(:,:,1)+Bzpic_ext(:,:,1));
[divX,dummy]=gradient(Xpic(:,:,1,1).*JxBx,dr,dz);
[dummy,divY]=gradient(JxBy,dr,dz);
div=divX./Xpic(:,:,1,1)+divY;

% [divX,dummy]=gradient(x.*(Bpr-VBpr),dr,dz);
% [dummy,divY]=gradient((Bpz-VBpz),dr,dz);
% div=divX./x+divY;

h5=figure(10)
set(h5,'Position', [440 48 451 750])
subplot(2,3,1)
pcolor(Xpic(:,:,1,1),Ypic(:,:,1,1),grp);shading interp;axis equal;axis tight;colorbar;title('-\nabla_x p')
%caxis([-300 300])
subplot(2,3,2)
pcolor(Xpic(:,:,1,1),Ypic(:,:,1,1),gzp);shading interp;axis equal;axis tight;colorbar;title('-\nabla_y p')
%caxis([-300 300])
subplot(2,3,3)
%pcolor(x,y,Viz);shading interp;axis equal;axis tight;colorbar
subplot(2,3,4)
pcolor(Xpic(:,:,1,1),Ypic(:,:,1,1),JxBx);shading interp;axis equal;axis tight;colorbar;title('[JxB]_x')
%caxis([-300 300])
subplot(2,3,5)
pcolor(Xpic(:,:,1,1),Ypic(:,:,1,1),JxBy);shading interp;axis equal;axis tight;colorbar;title('[JxB]_y')
%caxis([-300 300])
subplot(2,3,6)
pcolor(Xpic(:,:,1,1),Ypic(:,:,1,1),JxBz);shading interp;axis equal;axis tight;colorbar;title('[JxB]_z')
%caxis([-300 300])

h5writeatt(opath,'/Step#0','nspec',int32(ns));

disp('Size in Code Units')


