function Tpic =  interpmio2d(x,y,z,T,Xpic,Ypic,Zpic);

Tpic=interp2(x,y,T, Xpic, Ypic);

Tpic(isnan(Tpic)) = 0; 