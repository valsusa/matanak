function [J] = smooth3Donesided(Jin,N,plus)

Jold=Jin;

for i=1:N    

J= Jold;

J(2:end-1,:,:)=.25*(2*J(2:end-1,:,:)+J(1:end-2,:,:)+J(3:end,:,:));
J(1,:,:)=.5*(J(1,:,:)+J(2,:,:));
J(end,:,:)=.5*(J(end,:,:)+J(end-1,:,:));

J(:,2:end-1,:)=.25*(2*J(:,2:end-1,:)+J(:,1:end-2,:)+J(:,3:end,:));
J(:,1,:)=.5*(J(:,1,:)+J(:,2,:));
J(:,end,:)=.5*(J(:,end,:)+J(:,end-1,:));

J(:,:,2:end-1)=.25*(2*J(:,:,2:end-1)+J(:,:,1:end-2)+J(:,:,3:end));
J(:,:,1)=(J(:,:,1)+J(:,:,2))/2;
J(:,:,end)=(J(:,:,end)+J(:,:,end-1))/2;
if(plus)
    J(Jold<J)=Jold(Jold<J);
else 
    J(Jold>J)=Jold(Jold>J);
end   

Jold =J; 
end
