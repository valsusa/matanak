opath=[nome(1:end-4) '-Fields_000000.h5']
system(['rm ' opath])
opath_rs=[nome(1:end-4) '-FixedFields.h5']
system(['rm ' opath_rs])

dx=(xmax-xmin)/Nxpic;
dy=(ymax-ymin)/Nypic;
dz=(zmax-zmin)/Nzpic;

xpic=linspace(xmin,xmax,Nxpic);
ypic=linspace(ymin,ymax,Nypic);
if(Nzpic<=2) 
    zpic=[0 0];%[(zmin+zmax)/2 (zmin+zmax)/2] ;
else
    zpic=linspace(zmin,zmax,Nzpic);
end
if(Nypic<=2) 
    ypic=[0 0];%[(ymin+ymax)/2 (ymin+ymax)/2] ;
else
    ypic=linspace(ymin,ymax,Nypic);
end
[Xpic,Ypic,Zpic]=ndgrid(xpic,ypic,zpic);

Tpic=interpmio(x,y,z,T,Xpic,Ypic,Zpic); 


npic=interpmio(x,y,z,n,Xpic,Ypic,Zpic);


Bxpic=interpmio(x,y,z,Bx,Xpic,Ypic,Zpic);
Bypic=interpmio(x,y,z,By,Xpic,Ypic,Zpic);
Bzpic=interpmio(x,y,z,Bz,Xpic,Ypic,Zpic);

Expic=interpmio(x,y,z,Ex,Xpic,Ypic,Zpic);
Eypic=interpmio(x,y,z,Ey,Xpic,Ypic,Zpic);
Ezpic=interpmio(x,y,z,Ez,Xpic,Ypic,Zpic);

Jxpic(1:Nxpic,1:Nypic,1:Nzpic,1)=interpmio(x,y,z,n.*Vex,Xpic,Ypic,Zpic);
Jypic(1:Nxpic,1:Nypic,1:Nzpic,1)=interpmio(x,y,z,n.*Vey,Xpic,Ypic,Zpic);
Jzpic(1:Nxpic,1:Nypic,1:Nzpic,1)=interpmio(x,y,z,n.*Vez,Xpic,Ypic,Zpic);

Jxpic(1:Nxpic,1:Nypic,1:Nzpic,2)=interpmio(x,y,z,n.*Vix,Xpic,Ypic,Zpic);
Jypic(1:Nxpic,1:Nypic,1:Nzpic,2)=interpmio(x,y,z,n.*Viy,Xpic,Ypic,Zpic);
Jzpic(1:Nxpic,1:Nypic,1:Nzpic,2)=interpmio(x,y,z,n.*Viz,Xpic,Ypic,Zpic);

Jxpic(1:Nxpic,1:Nypic,1:Nzpic,3:4)=0.0;
Jypic(1:Nxpic,1:Nypic,1:Nzpic,3:4)=0.0;
Jzpic(1:Nxpic,1:Nypic,1:Nzpic,3:4)=0.0;

invertYZ = false;
if(invertYZ) 
    %exchange y with z and z with -y
    Tpic=permute(Tpic,[1 3 2]);
    npic=permute(npic,[1 3 2]);
    Bxpic=permute(Bxpic,[1 3 2]);
    tmp=permute(Bzpic,[1 3 2]);
    Bzpic=-permute(Bypic,[1 3 2]);
    Bypic=tmp;
    Expic=permute(Expic,[1 3 2]);
    tmp=permute(Ezpic,[1 3 2]);
    Ezpic=-permute(Eypic,[1 3 2]);
    Eypic=tmp;
    Jxpic=permute(Jxpic,[1 3 2 4]);
    tmp=permute(Jzpic,[1 3 2 4]);
    Jzpic=-permute(Jypic,[1 3 2 4]);
    Jypic= tmp;
    tmp = Nzpic;
    Nzpic = Nypic;
    Nypic = tmp;
end    


Bxpic_ext=zeros(Nxpic, Nypic, Nzpic);
Bypic_ext=zeros(Nxpic, Nypic, Nzpic);
Bzpic_ext=zeros(Nxpic, Nypic, Nzpic);

externalon = false;

if(externalon)

    Bpic = sqrt(Bxpic.^2 +  Bypic.^2 +Bzpic.^2);

    ii=Bypic>0. ; % 1e-2;  %.015;

    Bxpic_ext(ii) = Bxpic(ii);
    Bxpic(ii) = 0;


    Bypic_ext(ii) = Bypic(ii);
    Bypic(ii) = 0;


    Bzpic_ext(ii) = Bzpic(ii);
    Bzpic(ii) = 0;

end

h5create(opath,'/Step#0/Block/Bx/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bx/0',Bxpic)

h5create(opath,'/Step#0/Block/By/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/By/0',Bypic)

h5create(opath,'/Step#0/Block/Bz/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bz/0',Bzpic)

h5create(opath_rs,'/Step#0/Block/Bx_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Bx_ext/0',Bxpic_ext)

h5create(opath_rs,'/Step#0/Block/By_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/By_ext/0',Bypic_ext)

h5create(opath_rs,'/Step#0/Block/Bz_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Bz_ext/0',Bzpic_ext)


%set_E_int = false %test
set_E_int = true %gipsy702b

if(set_E_int)
h5create(opath,'/Step#0/Block/Ex/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ex/0',Expic)

h5create(opath,'/Step#0/Block/Ey/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ey/0',Eypic)

h5create(opath,'/Step#0/Block/Ez/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ez/0',Ezpic)

h5create(opath_rs,'/Step#0/Block/Ex_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ex_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath_rs,'/Step#0/Block/Ey_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ey_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath_rs,'/Step#0/Block/Ez_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ez_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath_rs,'/Step#0/Block/Ex_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ex_rs/0',Expic)

h5create(opath_rs,'/Step#0/Block/Ey_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ey_rs/0',Eypic)

h5create(opath_rs,'/Step#0/Block/Ez_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ez_rs/0',Ezpic)

else
    
h5create(opath_rs,'/Step#0/Block/Ex_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ex_ext/0',Expic)

h5create(opath_rs,'/Step#0/Block/Ey_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ey_ext/0',Eypic)

h5create(opath_rs,'/Step#0/Block/Ez_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ez_ext/0',Ezpic)

h5create(opath,'/Step#0/Block/Ex/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ex/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Ey/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ey/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Ez/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ez/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath_rs,'/Step#0/Block/Ex_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ex_rs/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath_rs,'/Step#0/Block/Ey_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ey_rs/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath_rs,'/Step#0/Block/Ez_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ez_rs/0',zeros(Nxpic, Nypic, Nzpic))

end 
    

h5create(opath,['/Step#0/Block/rho_avg/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/rho_avg/0'],zeros(Nxpic, Nypic, Nzpic))



h5create(opath_rs,'/Step#0/Block/Lambda/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Lambda/0',Lambdapic)

h5create(opath_rs,'/Step#0/Block/Bx_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Bx_rs/0',Bxpic)

h5create(opath_rs,'/Step#0/Block/By_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/By_rs/0',Bypic)

h5create(opath_rs,'/Step#0/Block/Bz_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Bz_rs/0',Bzpic)



is=2 % ste frame for ions, species 2 in matlab
h5create(opath,['/Step#0/Block/Vfx/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Vfx/0'],Jxpic(1:Nxpic,1:Nypic,1:Nzpic,is)./npic)

h5create(opath,['/Step#0/Block/Vfy/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Vfy/0'],Jypic(1:Nxpic,1:Nypic,1:Nzpic,is)./npic)

h5create(opath,['/Step#0/Block/Vfz/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Vfz/0'],Jzpic(1:Nxpic,1:Nypic,1:Nzpic,is)./npic)


for is=1:ns
h5create(opath,['/Step#0/Block/Jx_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jx_' num2str(is-1) '/0'],Jxpic(1:Nxpic,1:Nypic,1:Nzpic,is)*concentration(is))

h5create(opath,['/Step#0/Block/Jy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jy_' num2str(is-1) '/0'],Jypic(1:Nxpic,1:Nypic,1:Nzpic,is)*concentration(is))

h5create(opath,['/Step#0/Block/Jz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jz_' num2str(is-1) '/0'],Jzpic(1:Nxpic,1:Nypic,1:Nzpic,is)*concentration(is))


h5create(opath,['/Step#0/Block/rho_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/rho_' num2str(is-1) '/0'],npic*concentration(is))

h5create(opath_rs,['/Step#0/Block/Jx_rs_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,['/Step#0/Block/Jx_rs_' num2str(is-1) '/0'],Jxpic(1:Nxpic,1:Nypic,1:Nzpic,is)*concentration(is))

h5create(opath_rs,['/Step#0/Block/Jy_rs_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,['/Step#0/Block/Jy_rs_' num2str(is-1) '/0'],Jypic(1:Nxpic,1:Nypic,1:Nzpic,is)*concentration(is))

h5create(opath_rs,['/Step#0/Block/Jz_rs_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,['/Step#0/Block/Jz_rs_' num2str(is-1) '/0'],Jzpic(1:Nxpic,1:Nypic,1:Nzpic,is)*concentration(is))


h5create(opath_rs,['/Step#0/Block/rho_rs_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,['/Step#0/Block/rho_rs_' num2str(is-1) '/0'],npic*concentration(is))

vth2= abs(qom(is)) * Tratio(is) * Tpic; 
vth_left = sqrt(vth2(1,1,1))
v0= Jxpic(1:Nxpic,1:Nypic,1:Nzpic,is)./(npic+1e-10); max(v0(:))
h5create(opath,['/Step#0/Block/Pxx_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxx_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

h5create(opath_rs,['/Step#0/Block/Pxx_rs_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,['/Step#0/Block/Pxx_rs_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

TEST =mean((v0.^2 + vth2).* npic.*concentration(is),'all')
v0 = Jypic(1:Nxpic,1:Nypic,1:Nzpic,is)./npic;
h5create(opath,['/Step#0/Block/Pyy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
disp(['Species ' num2str(is)])
v0_mean=mean(abs(v0(:)))
vth_mean=mean(sqrt(vth2(:)))
h5write(opath,['/Step#0/Block/Pyy_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

h5create(opath_rs,['/Step#0/Block/Pyy_rs_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,['/Step#0/Block/Pyy_rs_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

v0 = Jzpic(1:Nxpic,1:Nypic,1:Nzpic,is)./npic;
h5create(opath,['/Step#0/Block/Pzz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pzz_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

h5create(opath_rs,['/Step#0/Block/Pzz_rs_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,['/Step#0/Block/Pzz_rs_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

h5create(opath,['/Step#0/Block/Pxy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxy_' num2str(is-1) '/0'], ...
    zeros(Nxpic, Nypic, Nzpic));

h5create(opath,['/Step#0/Block/Pxz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxz_' num2str(is-1) '/0'], ...
    zeros(Nxpic, Nypic, Nzpic));

h5create(opath,['/Step#0/Block/Pyz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pyz_' num2str(is-1) '/0'], ...
    zeros(Nxpic, Nypic, Nzpic));

end
h5writeatt(opath,'/Step#0','nspec',int32(ns));
h5writeatt(opath_rs,'/Step#0','nspec',int32(ns));
