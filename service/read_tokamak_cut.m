close all
clear all

nome='/Users/giovannilapenta/Dropbox/Shared/tokamak/Equilibrium_Information_VEST.mat';

density_adjust = false;

% Refernce Pressure in Pascal
 p_ref = 300;
%n_ref=1; %for mercury

% Reference Temperature in eV
 kT_ref = 200;


% Temperature Ratio
 TioTe = 25/200;

%B in  Tesla
 B_ref = .2;
 
 mass_ratio=64;
 
 physical_electrons = false;
 
[code_n, code_J, code_V, code_T, code_E, code_B, di0, wpi0] =   ...
       code_units_tokamak(mass_ratio, physical_electrons, p_ref, kT_ref, TioTe, B_ref);


e= 1.6022e-19;
mu0=1.256637e-6;

kT_ref_si=kT_ref*e;

Tratio=[1,TioTe];
qom=[-mass_ratio,1];
concentration=[-1.0,1.0];

ns=2;

%size in meters
Lx=1 %radius
Ly=3 %vertical size


load(nome)

ChamberBoundary(:,2)=ChamberBoundary(:,2);

figure(1)
plot(ChamberBoundary(:,1),ChamberBoundary(:,2))
axis equal



[Ny, Nx]=size(Bpr)

[x,y]=meshgrid(1:Nx,1:Ny);
x=(x-1)/(Nx-1)*Lx;
y=(y-1)/(Ny-1)*Ly;
dr=Lx/(Nx-1);
dz=Ly/(Ny-1);
z=zeros(size(x));
y=y-Ly/2;

[in,on] = inpolygon(x,y,ChamberBoundary(:,1),ChamberBoundary(:,2));

n=Pressure/kT_ref_si; %in SI
  

%To go from r,theta, z to code x, y, z meno must be set to -1

meno = -1;

[grBpr,gzBpr]=gradient(Bpr,dr,dz);
[grBpz,gzBpz]=gradient(Bpz,dr,dz);
[grBt,gzBt]=gradient(Bt,dr,dz);
[grBrt,gzBrt]=gradient(x.*Bt,dr,dz);

Jx=-gzBt/mu0;
%Jy=(grBt+Bt./x)/mu0/code_J;
Jy=(grBrt./x)/mu0;
Jz=(gzBpr-grBpz)/mu0;
%ii=n==0;
in=n>0;
Jx(~in)=0;
Jy(~in)=0;
Jz(~in)=0;

Bx=Bpr/code_B;
By=Bpz/code_B;
Bz= meno * Bt/code_B;


Vx=zeros(size(Bpr))/code_V;
Vy=zeros(size(Bpr))/code_V;
Vz=zeros(size(Bpr))/code_V;

Ex = - (Vy.*Bz - Vz.*By);
Ey = - (Vz.*Bx - Vx.*Bz);
Ez = - meno * (Vx.*By - Vy.*Bx);

Vix = Vx / code_V;
Viy = Vy / code_V;
Viz = meno *Vz / code_V;

% Assuming me<<mi
Vex = (Vx - Jx ./(n+1e-10) /e)/ code_V;
Vey = (Vy - Jy ./(n+1e-10) /e)/ code_V;
Vez = meno * (Vz - Jz ./(n+1e-10) /e)/ code_V;
ii=abs(Vex)>.1; Vex(ii)=Vex(ii)./abs(Vex(ii))*.1;
ii=abs(Vey)>.1; Vey(ii)=Vey(ii)./abs(Vey(ii))*.1;
ii=abs(Vez)>.1; Vez(ii)=Vez(ii)./abs(Vez(ii))*.1;

Vex=0*Vex; Vey=0*Vey; %Vez=ones(size(Vez));

p=Pressure;

kT = (p ./ (n+1e-10)  ) ./ code_T;

n=n/code_n;

h2=figure(2)
set(h2,'Position', [440 48 451 750])
subplot(3,3,1)
pcolor(x,y,Bx);shading interp;axis equal;axis tight;colorbar
subplot(3,3,2)
pcolor(x,y,By);shading interp;axis equal;axis tight;colorbar
subplot(3,3,3)
pcolor(x,y,Bz);shading interp;axis equal;axis tight;colorbar
subplot(3,3,4)
pcolor(x,y,Jx);shading interp;axis equal;axis tight;colorbar
subplot(3,3,5)
pcolor(x,y,Jy);shading interp;axis equal;axis tight;colorbar
subplot(3,3,6)
pcolor(x,y,Jz);shading interp;axis equal;axis tight;colorbar
subplot(3,3,7)
pcolor(x,y,n);shading interp;axis equal;axis tight;colorbar
subplot(3,3,8)
pcolor(x,y,kT);shading interp;axis equal;axis tight;colorbar
subplot(3,3,9)
pcolor(x,y,p);shading interp;axis equal;axis tight;colorbar


h3=figure(3)
set(h3,'Position', [440 48 451 750])
subplot(3,3,1)
pcolor(x,y,Vix);shading interp;axis equal;axis tight;colorbar
subplot(3,3,2)
pcolor(x,y,Viy);shading interp;axis equal;axis tight;colorbar
subplot(3,3,3)
pcolor(x,y,Viz);shading interp;axis equal;axis tight;colorbar
subplot(3,3,4)
pcolor(x,y,Vex);shading interp;axis equal;axis tight;colorbar
subplot(3,3,5)
pcolor(x,y,Vey);shading interp;axis equal;axis tight;colorbar
subplot(3,3,6)
pcolor(x,y,Vez);shading interp;axis equal;axis tight;colorbar



Nxpic=100+1;Nypic=200+1; Nzpic=1+1; 

Lxpic=0.8;
Lypic=0.8;
dx=Lxpic/Nxpic;
dy=Lypic/Nypic;
dz=min(dx,dy);

xpic=linspace(0,Lxpic,Nxpic);

ypic=linspace(-Lypic,Lypic,Nypic)
if(Nzpic<=2) 
    zpic=[0 0];%[(zmin+zmax)/2 (zmin+zmax)/2] ;
else
    zpic=linspace(0,dz,Nzpic);
end
if(Nypic<=2) 
    ypic=[0 0];%[(ymin+ymax)/2 (ymin+ymax)/2] ;
else
    ypic=linspace(-Lypic,Lypic,Nypic);
end
[Xpic,Ypic,Zpic]=ndgrid(xpic,ypic,zpic);

kTpic=interpmio2d(x,y,z,kT,Xpic,Ypic,Zpic); 


npic=interpmio2d(x,y,z,n,Xpic,Ypic,Zpic)/4/pi;
%npic=ones(Nxpic, Nypic, Nzpic);

[in,on] = inpolygon(Xpic,Ypic,ChamberBoundary(:,1),ChamberBoundary(:,2));

internalbc = true;

if(internalbc)
    % uses the actual wall
Lambdapic=zeros(size(Xpic));
Lambdapic(~in)=1;
ii=abs(Ypic)>0.7;
Lambdapic(ii)=1;
else
    % uses the region n=0
Lambda = zeros(size(x))
ii=n==0;
Lambda(ii) = 1;
Lambdapic=interpmio2d(x,y,z,Lambda,Xpic,Ypic,Zpic);
end

size(Lambdapic)



Bxpic=interpmio2d(x,y,z,Bx,Xpic,Ypic,Zpic);
Bypic=interpmio2d(x,y,z,By,Xpic,Ypic,Zpic);
Bzpic=interpmio2d(x,y,z,Bz,Xpic,Ypic,Zpic);

Expic=interpmio2d(x,y,z,Ex,Xpic,Ypic,Zpic);
Eypic=interpmio2d(x,y,z,Ey,Xpic,Ypic,Zpic);
Ezpic=interpmio2d(x,y,z,Ez,Xpic,Ypic,Zpic);

Jxpic(1:Nxpic,1:Nypic,1:Nzpic,1)=interpmio2d(x,y,z,Vex,Xpic,Ypic,Zpic).*npic*concentration(1);
Jypic(1:Nxpic,1:Nypic,1:Nzpic,1)=interpmio2d(x,y,z,Vey,Xpic,Ypic,Zpic).*npic*concentration(1);
Jzpic(1:Nxpic,1:Nypic,1:Nzpic,1)=interpmio2d(x,y,z,Vez,Xpic,Ypic,Zpic).*npic*concentration(1);

Jxpic(1:Nxpic,1:Nypic,1:Nzpic,2)=interpmio2d(x,y,z,Vix,Xpic,Ypic,Zpic).*npic*concentration(2);
Jypic(1:Nxpic,1:Nypic,1:Nzpic,2)=interpmio2d(x,y,z,Viy,Xpic,Ypic,Zpic).*npic*concentration(2);
Jzpic(1:Nxpic,1:Nypic,1:Nzpic,2)=interpmio2d(x,y,z,Viz,Xpic,Ypic,Zpic).*npic*concentration(2);
 
subplot(3,3,7)
pcolor(Xpic(:,:,1),Ypic(:,:,1),Lambdapic(:,:,1));shading interp;axis equal;axis tight;colorbar
subplot(3,3,8)
pcolor(Xpic(:,:,1),Ypic(:,:,1),npic(:,:,1));shading interp;axis equal;axis tight;colorbar
subplot(3,3,9)
pcolor(Xpic(:,:,1),Ypic(:,:,1),Bxpic(:,:,1));shading interp;axis equal;axis tight;colorbar


!rm Tokamak-Fields_000000.h5
opath='Tokamak-Fields_000000.h5'

h5create(opath,'/Step#0/Block/Lambda/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Lambda/0',Lambdapic)

h5create(opath,'/Step#0/Block/Bx/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bx/0',Bxpic)

h5create(opath,'/Step#0/Block/By/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/By/0',Bypic)

h5create(opath,'/Step#0/Block/Bz/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bz/0',Bzpic)

h5create(opath,'/Step#0/Block/Bx_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bx_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/By_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/By_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Bz_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bz_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Ex/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ex/0',Expic)

h5create(opath,'/Step#0/Block/Ey/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ey/0',Eypic)

h5create(opath,'/Step#0/Block/Ez/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ez/0',Ezpic)


h5create(opath,'/Step#0/Block/Ex_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ex_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Ey_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ey_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Ez_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ez_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,['/Step#0/Block/rho_avg/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/rho_avg/0'],zeros(Nxpic, Nypic, Nzpic))

for is=1:ns
h5create(opath,['/Step#0/Block/Jx_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jx_' num2str(is-1) '/0'],Jxpic(1:Nxpic,1:Nypic,1:Nzpic,is))

h5create(opath,['/Step#0/Block/Jy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jy_' num2str(is-1) '/0'],Jypic(1:Nxpic,1:Nypic,1:Nzpic,is))

h5create(opath,['/Step#0/Block/Jz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jz_' num2str(is-1) '/0'],Jzpic(1:Nxpic,1:Nypic,1:Nzpic,is))


h5create(opath,['/Step#0/Block/rho_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/rho_' num2str(is-1) '/0'], npic*concentration(is))

vth2= abs(qom(is)) * Tratio(is) * kTpic; 
v0= Jxpic(1:Nxpic,1:Nypic,1:Nzpic,is)./(npic.*concentration(is)+1e-10*qom(is));
ii=abs(npic)<1e-10*abs(qom(is)); v0(ii)=0;
h5create(opath,['/Step#0/Block/Pxx_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxx_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

v0 = Jypic(1:Nxpic,1:Nypic,1:Nzpic,is)./(npic.*concentration(is)+1e-10*qom(is));
ii=abs(npic)<1e-10*abs(qom(is)); v0(ii)=0;
h5create(opath,['/Step#0/Block/Pyy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pyy_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

v0 = Jzpic(1:Nxpic,1:Nypic,1:Nzpic,is)./(npic.*concentration(is));
ii=npic==0.0; v0(ii)=0;
h5create(opath,['/Step#0/Block/Pzz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pzz_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

h5create(opath,['/Step#0/Block/Pxy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxy_' num2str(is-1) '/0'], ...
    zeros(Nxpic, Nypic, Nzpic));

h5create(opath,['/Step#0/Block/Pxz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxz_' num2str(is-1) '/0'], ...
    zeros(Nxpic, Nypic, Nzpic));

h5create(opath,['/Step#0/Block/Pyz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pyz_' num2str(is-1) '/0'], ...
    zeros(Nxpic, Nypic, Nzpic));

ppic=(v0.^2 + vth2).* npic.*concentration(is);


figure(5)
subplot(3,3,is)
Tpic=abs(ppic./(npic.*concentration(is)+1e-10*qom(is))-v0.^2);
imagesc(sqrt(Tpic(:,:,1))');colorbar
subplot(3,3,is+3)
imagesc(Jzpic(:,:,1,is)'); colorbar
subplot(3,3,is+6)
imagesc(npic(:,:,1)'*concentration(is)); colorbar
end

Lx_code = Lxpic/di0
Ly_code = 2*Lypic/di0
Lz_code=.1
one_sec = wpi0
dx=Lx_code/(Nxpic-1)
dy=Ly_code/(Nypic-1)
dy=Lz_code/(Nzpic-1)


[grBpr,gzBpr,gphBpr]=gradient(permute(Bxpic,[2 1 3]),dx,dy,dz);
[grBpz,gzBpz,gphBpz]=gradient(permute(Bypic,[2 1 3]),dx,dy,dz);
[grBt,gzBt,gphBt]=gradient(permute(Bzpic,[2 1 3]),dx,dy,dz);
[grBrt,gzBrt,gphBrt]=gradient(permute(Xpic.*Bzpic,[2 1 3]),dx,dy,dz);



mu0=4*pi;
Jx=-permute(gzBt, [2 1 3])/mu0;
%Jy=(grBt+Bt./x)/mu0/code_J;
Jy=(permute(grBrt, [2 1 3])./Xpic)/mu0;
Jz=meno * permute(gzBpr-grBpz, [2 1 3])/mu0;

Jx(npic==0) = 0
Jy(npic==0) = 0
Jz(npic==0) = 0

figure(6)
subplot(2,3,1)
imagesc(Jx(:,:,1)')
colorbar
subplot(2,3,2)
imagesc(Jy(:,:,1)')
colorbar
subplot(2,3,3)
imagesc(Jz(:,:,1)')
colorbar
subplot(2,3,4)
imagesc(Jxpic(:,:,1)')
colorbar
subplot(2,3,5)
imagesc(Jypic(:,:,1)')
colorbar
subplot(2,3,6)
imagesc(Jzpic(:,:,1)')
colorbar

h5writeatt(opath,'/Step#0','nspec',int32(ns));

disp('Size in Code Units')

