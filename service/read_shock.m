
% System Size in di
Lx = 100
Ly = 100
Lz = 1 
Nxpic=128+1; Nypic= 128+1; Nzpic= 1+1; 


phys_el = 0 % Yes physicla electrons is 1, otherwise 0
n_ref = 10; % in particle per cc
Ti_ref = 1e3;% Ion Temperature in eV
sol_factor = .1 % Factoring the speed of loght

Bx = 4.3463e-06/1000 % in T
By = 4.3463e-06/100
Bz = 0


% Temperature Ratio
 TioTe = 1;

%B in  Tesla
 B_ref = By;

 % Mass ratio 
mrcode = 100

% Physics Constants
 mu0=4*pi*1e-7;
 eps0=8.8542*1.e-12/sol_factor^2;
 cphys=1/sqrt(mu0*eps0);
 k=1.3807e-23;
 e= 1.6022e-19;
 if(phys_el==1)
%physical electorns
    me=9.1094e-31;
    mp = me * mrcode;
 else   
% physical ions 
    mp=1.6726e-27;
    me = mp / mrcode;
 end
 
 

% Convert to SI
 np=n_ref*1e6; % puts ni in m^-3
 ne=np;
 Tp=Ti_ref * 1.1604e4; % puts T in K
 Te=Tp/TioTe;

% Plasma scales

% 'Protons'
 wpp=sqrt(np*e^2/mp/eps0);
dp=cphys/wpp;
 vthp=sqrt(k*Tp/mp);
 wcp=e*B_ref/mp;
 rhop=vthp/wcp;

% 'Electrons'
 wpe=sqrt(ne*e^2/me/eps0);
 de=cphys/wpe;
 vthe=sqrt(k*Te/me);
 wce=e*B_ref/me;
 rhoe=vthe/wce;
 lde=sqrt(eps0*k*Te/ne/e*e);
 

% Normalisations
%To convert 1 in the code to SI multiply by this
%To convert 1 in SI to code units divide by this
code_E = cphys*mp*wpp/e;
code_B = mp*wpp/e;
code_J = mp*wpp/mu0/e/dp;
code_V = cphys;
code_n = n_ref*1e6;
% Assuming the reference species is ions
code_T = cphys *cphys * mp;


Va = B_ref/sqrt(mu0*np*mp)
Vx = 10*Va % in m/s
Vy = 0
Vz = 0


vthe = Va*sqrt(mrcode/TioTe) 
vthp = vthe *sqrt(TioTe/mrcode)

Ex = - (Vy.*Bz - Vz.*By);
Ey = - (Vz.*Bx - Vx.*Bz);
Ez = - (Vx.*By - Vy.*Bx);

ns=2;
Tratio=[1,1,1,1];
qom=[-mrcode,1,-mrcode,1];
concentration=[-1.0,1.0,-1.0e-3,1.0e-3];


dx=Lx/Nxpic;
dy=Ly/Nypic;
dz=Lz/Nzpic;

xpic=linspace(0,Lx,Nxpic);

ypic=linspace(0,Ly,Nypic);
if(Nzpic<=2) 
    zpic=[0 0];%[(zmin+zmax)/2 (zmin+zmax)/2] ;
else
    zpic=linspace(0,Lz,Nzpic);
end
if(Nypic<=2) 
    ypic=[0 0];%[(ymin+ymax)/2 (ymin+ymax)/2] ;
else
    ypic=linspace(0,Ly,Nypic);
end
[Xpic,Ypic,Zpic]=ndgrid(xpic,ypic,zpic);
uno = ones(Nxpic, Nypic, Nzpic);

opath='Shock-Fields_000000.h5'

system(['rm ' opath])

h5create(opath,'/Step#0/Block/Bx/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bx/0',Bx * uno /code_B)

h5create(opath,'/Step#0/Block/By/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/By/0',By * uno /code_B)

h5create(opath,'/Step#0/Block/Bz/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bz/0',Bz * uno /code_B)

h5create(opath,'/Step#0/Block/Bx_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bx_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/By_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/By_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Bz_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bz_ext/0',zeros(Nxpic, Nypic, Nzpic))


h5create(opath,'/Step#0/Block/Ex/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ex/0',Ex * uno /code_E)

h5create(opath,'/Step#0/Block/Ey/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ey/0',Ey * uno /code_E)

h5create(opath,'/Step#0/Block/Ez/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ez/0',Ez * uno /code_E)

h5create(opath,'/Step#0/Block/Ex_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ex_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Ey_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ey_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Ez_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ez_ext/0',zeros(Nxpic, Nypic, Nzpic))


h5create(opath,['/Step#0/Block/rho_avg/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/rho_avg/0'],zeros(Nxpic, Nypic, Nzpic))

Lambdapic = zeros(Nxpic, Nypic, Nzpic);
%boundary in x
Lambdapic(1:3,:,:) = 1; %left
Lambdapic(end-2:end,:,:) = 1; %right

%boundary in y
%Lambdapic(:,1:3,:) = 1;  Lambdapic(:,end-2:end,:) = 1;
%boundary in z
%Lambdapic(:,:,1:3) = 1;  Lambdapic(:,:,end-2:end) = 1;
Lambdapic=smooth3Dnew(Lambdapic,3);


%Lambdapic = (1-tanh((Xpic-xmin-4*dx)/2/dx))/2; 
%Lambdapic = Lambdapic + (1 - tanh((Ypic-ymin-4*dy)/2/dy))/2; 
%Lambdapic = Lambdapic + (1 - tanh((ymax-Ypic-4*dy)/2/dy))/2; 
%Lambdapic(Lambdapic>1)=1;
imagesc(Lambdapic(:,:,1)'); colorbar


h5create(opath,'/Step#0/Block/Lambda/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Lambda/0',Lambdapic)

npic = np/code_n / 4/ pi;

for is=1:ns
h5create(opath,['/Step#0/Block/Jx_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jx_' num2str(is-1) '/0'],Vx * uno / cphys * npic * concentration(is))

h5create(opath,['/Step#0/Block/Jy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jy_' num2str(is-1) '/0'],Vy * uno  / cphys * npic * concentration(is))

h5create(opath,['/Step#0/Block/Jz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jz_' num2str(is-1) '/0'],Vz * uno / cphys * npic * concentration(is))


h5create(opath,['/Step#0/Block/rho_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/rho_' num2str(is-1) '/0'], uno * npic * concentration(is))

if mod(is,2) == 0
    vth2 = (vthp/cphys)^2;
else
    vth2 = (vthe/cphys)^2;
end    
v0= Vx / cphys
vth2_sq=sqrt(vth2)
h5create(opath,['/Step#0/Block/Pxx_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxx_' num2str(is-1) '/0'], ...
    uno * (v0.^2 + vth2)* npic *concentration(is));

v0 = Vy / cphys;
h5create(opath,['/Step#0/Block/Pyy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pyy_' num2str(is-1) '/0'], ...
    uno * (v0.^2 + vth2).* npic.*concentration(is));

v0 = Vz / cphys;
h5create(opath,['/Step#0/Block/Pzz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pzz_' num2str(is-1) '/0'], ...
    uno * (v0.^2 + vth2).* npic.*concentration(is));

h5create(opath,['/Step#0/Block/Pxy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxy_' num2str(is-1) '/0'], ...
    zeros(Nxpic, Nypic, Nzpic));

h5create(opath,['/Step#0/Block/Pxz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxz_' num2str(is-1) '/0'], ...
    zeros(Nxpic, Nypic, Nzpic));

h5create(opath,['/Step#0/Block/Pyz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pyz_' num2str(is-1) '/0'], ...
    zeros(Nxpic, Nypic, Nzpic));

end
h5writeatt(opath,'/Step#0','nspec',int32(ns));
