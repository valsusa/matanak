function [Agyro,Agyro_aunai,Nongyro_swisdak]=compute_Agyro_par(Bx, By, Bz, ...
 PeXX, PeXY, PeXZ, PeYY, PeYZ, PeZZ ) 

disp('Computing Agyro')

tiny=1e-10;

[nx ny nz]= size(PeXX);

cellSpace = [nx, ny, nz];
numCells = prod(cellSpace);

    

for idx =  1:numCells
    [i,iy, k] = ind2sub(cellSpace, idx);
    if(i==1 & iy==1) 
        disp(['Calculating k = ' num2str(k) '  of' num2str(nz)])
    end    

p=[PeXX(i,iy,k) PeXY(i,iy,k) PeXZ(i,iy,k);
PeXY(i,iy,k) PeYY(i,iy,k) PeYZ(i,iy,k);
PeXZ(i,iy,k) PeYZ(i,iy,k) PeZZ(i,iy,k)];

b=[Bx(i,iy,k);
By(i,iy,k);
Bz(i,iy,k)];

b=reshape(b,3,1);
b=b./sqrt(sum(b.^2));

%%%%%%%%%%
% Scudder
%%%%%%%%%%

for l=1:3
N1=[cross(b,p(:,1)) cross(b,p(:,2)) cross(b,p(:,3))];
end

for l=1:3
N(:,l)=cross(b,N1(:,l));
end

lamb=sort(eig(N));
%lambda1(idx)=lamb(1);
%lambda2(idx)=lamb(2);
%lambda3(idx)=lamb(3);

Agyro(idx)= 2*(lamb(3)-lamb(2))/(lamb(3)+lamb(2));


%%%%%%%%%%%
% Aunai
%%%%%%%%%%%

Tr=(p(1,1)+p(2,2)+p(3,3));
Ppar=b'*p*b;
Pper=(Tr-Ppar)/2;
G=eye(3,3)*Pper+(Ppar-Pper)*kron(b,b');
N=p-G;
Agyro_aunai(idx)=sqrt(sum(N(:).^2))./Tr;

%%%%%%%%%%%
% Swisdak
%%%%%%%%%%%

I2=p(1,1)*p(2,2)+p(1,1)*p(3,3)+p(2,2)*p(3,3);
I2=I2-(p(1,2).^2+p(1,3).^2+p(2,3).^2);
Q=1-4*I2./((Tr-Ppar).*(Tr+3*Ppar));
Nongyro_swisdak(idx)=sqrt(Q);
% The following formula form Swisdak paper is actually wrong
%Nongyro_aunai(i,k)=sqrt(8*(p(1,2).^2+p(1,3).^2+p(2,3).^2))./(Ppar+2*Pper);

end

Agyro=reshape(Agyro, nx, ny, nz);
Agyro_aunai=reshape(Agyro_aunai, nx, ny, nz);
Agyro_aunai=reshape(Agyro_aunai, nx, ny, nz);


