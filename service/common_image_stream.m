function [J]=common_image_stream(x,y,J1,Bx,By,Bz,nlabel,name,clmt, radius, nfig)

global color_choice symmetric_color labelx labely labelc reversex Ncycle dir_out Lx Ly Lz dx dy dz color_lines

close all
J=imgaussfilt(squeeze(J1),radius);

if(nargin>8)
h=figure(nfig)
else
h=figure(1)
end

set(h, 'Position', [560 203 344 745]) %FRC
%set(h, 'Position', [167 26 515 776])
%set(h, 'Position',[204 376 764 429])

if(clmt(1) == clmt(2))
clmt(1)=min(J(:));
clmt(2)=max(J(:));
if(symmetric_color==1)
clmt(1)=-max(abs(clmt))
clmt(2)=-clmt(1)
elseif (symmetric_color==-1)
clmt(1)=-min(abs(clmt))
clmt(2)=-clmt(1)
end
end

if(clmt(1) == clmt(2))
    clmt(1)=clmt(1)-1e-10;
end
%ftrunc=-9;
%flog=sign(J).*(log10(max(abs(J),10^ftrunc))-ftrunc);
%  fmax=3;
%  fmin=-fmax;


%Ncut=max(Nsm*3,1)
Ncut=1

%imagesc(x(1,:),y(:,1),J(Ncut:end-Ncut,Ncut:end-Ncut)',clmt)
imagesc(x(1,:),y(:,1),J',clmt)

hold on
%contour(x,y,Az',20,'k')

%startx=linspace(0,max(x(:)),50);
%starty=ones(size(startx))*Ly/2;
%streamline(x,y,Ax',Ay',startx,starty)
%streamline(x,y,-Ax',-Ay',startx,starty)

tred_plot=true
if(tred_plot)

[X3 Y3 Z3] = meshgrid(0:dx:Lx-dx,0:dy:Ly-dy,0:dz:Lz-dz);
Np=50
TH = 2*pi*rand(1,Np);
PH = asin(-1+2*rand(1,Np));
R = rand(1,Np)*2;
[startx,starty,startz] = sph2cart(TH,PH,R);

startx=startx+Lx/2;
starty=starty+Ly/2;
startz=startz+Lz/2;
plot3(startx,starty,startz,'.','markersize',1)
h1=streamline(X3,Y3,Z3,permute(Bx,[2 1 3]),permute(By,[2 1 3]),permute(Bz,[2 1 3]),startx,starty,startz);
%set(h1,'Color', [1 1 1, 0.2])
set(h1,'Color', color_lines)
h2=streamline(X3,Y3,Z3,-permute(Bx,[2 1 3]),-permute(By,[2 1 3]),-permute(Bz,[2 1 3]),startx,starty,startz);
set(h2,'Color', color_lines)
end

%[X3 Y3 Z3] = meshgrid(0:dx:Lx-dx,0:dy:Ly-dy,0:dz:Lz-dz);
%streamline(X3,Y3,Z3,permute(Bx,[2 1 3]),permute(By,[2 1 3]),permute(Bz,[2 1 3]),linspace(Lx/3,Lx-Lx/3,10),ones(1,10),ones(1,10))


caxis(clmt)
%caxis([fmin fmax])

if(color_choice==0)
        colormap jet
elseif (color_choice==1)
        load cm_new
        colormap(cm_kbwrk)
elseif (color_choice==2)
        colormap hot
elseif (color_choice==3)
        load cm_multi4
        colormap(cm_cool_hot_2)
elseif (color_choice==4)
        colormap hsv
elseif (color_choice==5)
        load gist_ncar
        colormap(gist_ncar)       
elseif (color_choice==-1)
        colormap parula
end

ch=colorbar
set(get(ch,'title'),'string',labelc);

set(gca,'fontsize',[14])


xlabel(labelx,'fontsize',[14])
ylabel(labely,'fontsize',[14])


title([name '  ' nlabel ],'fontsize',[14])

axis image
axis xy

xlim([min(x(:)) max(x(:))])
ylim([min(y(:)) max(y(:))])
if(reversex==1) 
    set(gca,'xdir','reverse')
end

%set(gca,'xdir','reverse','TickDir','out')
%print('-depsc','-r300',[name '.eps'])
%set(gcf, 'Renderer', 'zbuffer');

print('-dpng','-r300',[dir_out name 'stream' Ncycle '.png'])

%saveas(gcf,[name '.fig'])

close(nfig)