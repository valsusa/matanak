function [code_n, code_J, code_V, code_T, code_E, code_B, dp, wpp] =   ....
     code_units(mrcode,phys_el,p_ref, kT_ref, TioTe, B_ref)
 



% Physics Constants
 mu0=4*pi*1e-7;
 eps0=8.8542*1.e-12;
 cphys=1/sqrt(mu0*eps0);
 k=1.3807e-23;
 e= 1.6022e-19;
 
 % Refernce Density in cc
 kT_si = kT_ref *e; % kT in Joule
 n_ref = p_ref/kT_si
%n_ref=1; %for mercury
 
 if(phys_el==true)
%physical electorns
    me=9.1094e-31;
    mp = me * mrcode;
 else   
% physical ions 
    mp=1.6726e-27;
    me = mp / mrcode;
 end
 
 

% Convert T to SI
 Te=kT_ref * 1.1604e4; % puts T in K
 Tp=Te*TioTe; % puts T in K
 np = n_ref;
 ne=np;

% Plasma scales

%disp('Protons')
 wpp=sqrt(np*e^2/mp/eps0);
dp=cphys/wpp;
 vthp=sqrt(k*Tp/mp);
 wcp=e*B_ref/mp;
 rhop=vthp/wcp;

%disp('Electrons')
 wpe=sqrt(ne*e^2/me/eps0);
 de=cphys/wpe;
 vthe=sqrt(k*Te/me);
 wce=e*B_ref/me;
 rhoe=vthe/wce;
 lde=sqrt(eps0*k*Te/ne/e*e);
 


% Normalisations
%To convert 1 in the code to SI multiply by this
%To convert 1 in SI to code units divide by this
code_E = cphys*mp*wpp/e;
code_B = mp*wpp/e;
code_J = mp*wpp/mu0/e/dp;
code_V = cphys;
code_n = n_ref;
% Assuming the reference species is ions
code_T = cphys *cphys * mp;


return

