close all
clear all

nome_old='~/Downloads/Equilibrium_Information_VEST.mat';
%nome='~/Downloads/200425_Equilibrium_Information_VEST.mat';
pnew=h5read('~/Downloads/Pressure.hdf5','/p');

%new case
nome_new='/Users/glapenta/Downloads/SNU-tokamak-3D_EQ/3D_Cartesian_Value_AT_XYZ_313.mat';

density_adjust = false;

% Refernce Pressure in Pascal
 p_ref = 300;
%n_ref=1; %for mercury

% Reference Temperature in eV
 kT_ref = 200;


% Temperature Ratio
 TioTe = 25/200;

%B in  Tesla
 B_ref = .2;
 
 mass_ratio=64;
 
 physical_electrons = false;
 
[code_n, code_J, code_V, code_T, code_E, code_B, di0, wpi0] =   ...
       code_units_tokamak(mass_ratio, physical_electrons, p_ref, kT_ref, TioTe, B_ref);


e= 1.6022e-19;
mu0=1.256637e-6;

kT_ref_si=kT_ref*e;

Tratio=[1,TioTe];
qom=[-mass_ratio,1];
concentration=[-1.0,1.0];

ns=2;

%size in meters
Lx=1 %radius
Ly=3 %vertical size

meno=-1;

load(nome_new)
By=squeeze(B_plasma_y(1,:,:))/code_B;
Bx=squeeze(B_plasma_z(1,:,:))/code_B;
Bz=zeros(size(By));
Bz_ext= meno* squeeze(B_vacuum_x(1,:,:))/code_B;
By_ext=squeeze(B_vacuum_y(1,:,:))/code_B;
Bx_ext=squeeze(B_vacuum_z(1,:,:))/code_B;
Jz=meno*squeeze(J_x(1,:,:))/code_J;
Jx=squeeze(J_z(1,:,:))/code_J;
Jy=squeeze(J_y(1,:,:))/code_J;
pnew = squeeze(P_xyz(1,:,:))/code_n/code_T;

load(nome_old,'ChamberBoundary')
ChamberBoundary(:,2)=ChamberBoundary(:,2);

figure(1)
plot(ChamberBoundary(:,1),ChamberBoundary(:,2))
axis equal



[Nx,Ny]=size(Bx)

[x,y]=ndgrid(1:Nx,1:Ny);
x=(x-1)/(Nx-1)*Lx;
y=(y-1)/(Ny-1)*Ly;
dr=Lx/(Nx-1)/di0;
dz=Ly/(Ny-1)/di0;
z=zeros(size(x));
y=y-Ly/2;

[in,on] = inpolygon(x,y,ChamberBoundary(:,1),ChamberBoundary(:,2));

n=pnew/(kT_ref_si/code_T); %in SI

  

Vx=zeros(size(Bx))/code_V;
Vy=zeros(size(Bx))/code_V;
Vz=meno* zeros(size(Bx))/code_V;

Ex = - (Vy.*Bz - Vz.*By);
Ey = - (Vz.*Bx - Vx.*Bz);
Ez = - meno * (Vx.*By - Vy.*Bx);

Vix = Vx / code_V;
Viy = Vy / code_V;
Viz = meno *Vz / code_V;

% Assuming me<<mi
Vex = (Vx - Jx ./(n+1e-10));
Vey = (Vy - Jy ./(n+1e-10));
Vez = (Vz - Jz ./(n+1e-10));
ii=abs(Vex)>.1; Vex(ii)=Vex(ii)./abs(Vex(ii))*.1;
ii=abs(Vey)>.1; Vey(ii)=Vey(ii)./abs(Vey(ii))*.1;
ii=abs(Vez)>.1; Vez(ii)=Vez(ii)./abs(Vez(ii))*.1;

%Vex=0*Vex; Vey=0*Vey; %Vez=ones(size(Vez));


kT = (pnew ./ (n+1e-10)  );

kT=kT / sum(Tratio); % To equate teh pressure of all species. 

h2=figure(2)
set(h2,'Position', [440 48 451 750])
subplot(3,3,1)
pcolor(x,y,Bx);shading interp;axis equal;axis tight;colorbar
title('Bx')
subplot(3,3,2)
pcolor(x,y,By);shading interp;axis equal;axis tight;colorbar
title('By')
subplot(3,3,3)
pcolor(x,y,Bz);shading interp;axis equal;axis tight;colorbar
title('Bz')
subplot(3,3,4)
pcolor(x,y,Bx_ext);shading interp;axis equal;axis tight;colorbar
title('Bx_{ext}')
subplot(3,3,5)
pcolor(x,y,By_ext);shading interp;axis equal;axis tight;colorbar
title('By_{ext}')
subplot(3,3,6)
pcolor(x,y,Bz_ext);shading interp;axis equal;axis tight;colorbar
title('Bz_{ext}')
subplot(3,3,7)
pcolor(x,y,Bx_ext+Bx);shading interp;axis equal;axis tight;colorbar
title('Bx_{tot}')
subplot(3,3,8)
pcolor(x,y,By_ext+By);shading interp;axis equal;axis tight;colorbar
title('By_{tot}')
subplot(3,3,9)
pcolor(x,y,Bz_ext+Bz);shading interp;axis equal;axis tight;colorbar
title('Bz_{tot}')

h3=figure(3)
%set(h3,'Position', [440 48 451 750])
subplot(2,3,1)
pcolor(x,y,Jx);shading interp;axis equal;axis tight;colorbar; title('Jx')
subplot(2,3,2)
pcolor(x,y,Jy);shading interp;axis equal;axis tight;colorbar; title('Jy')
subplot(2,3,3)
pcolor(x,y,Jz);shading interp;axis equal;axis tight;colorbar; title('Jz')
subplot(2,3,4)
pcolor(x,y,n);shading interp;axis equal;axis tight;colorbar; title('n')
subplot(2,3,5)
pcolor(x,y,kT);shading interp;axis equal;axis tight;colorbar; title('kT')
subplot(2,3,6)
pcolor(x,y,pnew);shading interp;axis equal;axis tight;colorbar; title('p')

[grp,gzp]=gradient(pnew,dr,dz);
[JxBx,JxBy,JxBz]=cross_prod(Jx,Jy,Jz,Bx+Bx_ext,By+By_ext,Bz+Bz_ext);
[divX,dummy]=gradient(x/di0.*Bx,dr,dz);
[dummy,divY]=gradient(By,dr,dz);
div=divX./x*di0+divY;

% [divX,dummy]=gradient(x.*(Bpr-VBpr),dr,dz);
% [dummy,divY]=gradient((Bpz-VBpz),dr,dz);
% div=divX./x+divY;

h5=figure(5)
set(h5,'Position', [440 48 451 750])
subplot(2,3,1)
pcolor(x,y,grp);shading interp;axis equal;axis tight;colorbar;title('\nabla_x p')
%caxis([-300 300])
subplot(2,3,2)
pcolor(x,y,gzp);shading interp;axis equal;axis tight;colorbar;title('\nabla_y p')
%caxis([-300 300])
subplot(2,3,3)
pcolor(x,y,div);shading interp;axis equal;axis tight;colorbar;title('Div(B)')
subplot(2,3,4)
pcolor(x,y,JxBx);shading interp;axis equal;axis tight;colorbar;title('[JxB]_x')
%caxis([-300 300])
subplot(2,3,5)
pcolor(x,y,JxBy);shading interp;axis equal;axis tight;colorbar;title('[JxB]_y')
%caxis([-300 300])
subplot(2,3,6)
pcolor(x,y,JxBz);shading interp;axis equal;axis tight;colorbar;title('[JxB]_z')

%caxis([-300 300])

figure(100) 
subplot(2,1,1)
plot(-grp(50,:))
hold on
plot(-JxBx(50,:))
legend('\nabla p_x','JxBx')

subplot(2,1,2)
plot(-gzp(50,:))
hold on
plot(-JxBy(50,:))


Nxpic=100+1;Nypic=200+1; Nzpic=1+1; 

Lxpic=0.8;
Lypic=0.8;
dx=Lxpic/Nxpic;
dy=Lypic/Nypic;
dz=min(dx,dy);

xpic=linspace(0,Lxpic,Nxpic);

ypic=linspace(-Lypic,Lypic,Nypic);
if(Nzpic<=2) 
    zpic=[0 0];%[(zmin+zmax)/2 (zmin+zmax)/2] ;
else
    zpic=linspace(0,dz,Nzpic);
end
if(Nypic<=2) 
    ypic=[0 0];%[(ymin+ymax)/2 (ymin+ymax)/2] ;
else
    ypic=linspace(-Lypic,Lypic,Nypic);
end
[Xpic,Ypic,Zpic]=ndgrid(xpic,ypic,zpic);

kTpic=interpmio2d(x,y,z,kT,Xpic,Ypic,Zpic); 


npic=interpmio2d(x,y,z,n,Xpic,Ypic,Zpic);%/4/pi;
%npic=ones(Nxpic, Nypic, Nzpic);

[in,on] = inpolygon(Xpic,Ypic,ChamberBoundary(:,1),ChamberBoundary(:,2));

internalbc = true;

if(internalbc)
    % uses the actual wall
Lambdapic=zeros(size(Xpic));
Lambdapic(~in)=1;
ii=abs(Ypic)>0.7;
Lambdapic(ii)=1;
else
    % uses the region n=0
Lambda = zeros(size(x))
ii=n==0;
Lambda(ii) = 1;
Lambdapic=interpmio2d(x,y,z,Lambda,Xpic,Ypic,Zpic);
end

size(Lambdapic)



Bxpic=interpmio2d(x,y,z,Bx,Xpic,Ypic,Zpic);
Bypic=interpmio2d(x,y,z,By,Xpic,Ypic,Zpic);
Bzpic=interpmio2d(x,y,z,Bz,Xpic,Ypic,Zpic);

Bxpic_ext=interpmio2d(x,y,z,Bx_ext,Xpic,Ypic,Zpic);
Bypic_ext=interpmio2d(x,y,z,By_ext,Xpic,Ypic,Zpic);
Bzpic_ext=interpmio2d(x,y,z,Bz_ext,Xpic,Ypic,Zpic);

Expic=interpmio2d(x,y,z,Ex,Xpic,Ypic,Zpic);
Eypic=interpmio2d(x,y,z,Ey,Xpic,Ypic,Zpic);
Ezpic=interpmio2d(x,y,z,Ez,Xpic,Ypic,Zpic);

Jxpic(1:Nxpic,1:Nypic,1:Nzpic,1)=interpmio2d(x,y,z,Jx,Xpic,Ypic,Zpic);
Jypic(1:Nxpic,1:Nypic,1:Nzpic,1)=interpmio2d(x,y,z,Jy,Xpic,Ypic,Zpic);
Jzpic(1:Nxpic,1:Nypic,1:Nzpic,1)=interpmio2d(x,y,z,Jz,Xpic,Ypic,Zpic);

Jxpic(1:Nxpic,1:Nypic,1:Nzpic,2)=zeros(size(Xpic));
Jypic(1:Nxpic,1:Nypic,1:Nzpic,2)=zeros(size(Xpic));
Jzpic(1:Nxpic,1:Nypic,1:Nzpic,2)=zeros(size(Xpic));
 
Pxxpic=zeros(size(Jxpic));

!rm Tokamak-Fields_000000.h5
opath='Tokamak-Fields_000000.h5'

h5create(opath,'/Step#0/Block/Lambda/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Lambda/0',Lambdapic)

h5create(opath,'/Step#0/Block/Bx/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bx/0',Bxpic)

h5create(opath,'/Step#0/Block/By/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/By/0',Bypic)

h5create(opath,'/Step#0/Block/Bz/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bz/0',Bzpic)

h5create(opath,'/Step#0/Block/Bx_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bx_ext/0',Bxpic_ext)

h5create(opath,'/Step#0/Block/By_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/By_ext/0',Bypic_ext)

h5create(opath,'/Step#0/Block/Bz_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bz_ext/0',Bzpic_ext)

h5create(opath,'/Step#0/Block/Ex/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ex/0',Expic)

h5create(opath,'/Step#0/Block/Ey/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ey/0',Eypic)

h5create(opath,'/Step#0/Block/Ez/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ez/0',Ezpic)


h5create(opath,'/Step#0/Block/Ex_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ex_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Ey_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ey_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Ez_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ez_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,['/Step#0/Block/rho_avg/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/rho_avg/0'],zeros(Nxpic, Nypic, Nzpic))

figure(7)
for is=1:ns
h5create(opath,['/Step#0/Block/Jx_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jx_' num2str(is-1) '/0'],Jxpic(1:Nxpic,1:Nypic,1:Nzpic,is))

h5create(opath,['/Step#0/Block/Jy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jy_' num2str(is-1) '/0'],Jypic(1:Nxpic,1:Nypic,1:Nzpic,is))

h5create(opath,['/Step#0/Block/Jz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jz_' num2str(is-1) '/0'],Jzpic(1:Nxpic,1:Nypic,1:Nzpic,is))


h5create(opath,['/Step#0/Block/rho_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/rho_' num2str(is-1) '/0'], npic*concentration(is))

vth2= abs(qom(is)) * Tratio(is) * kTpic; 
v0= Jxpic(1:Nxpic,1:Nypic,1:Nzpic,is)./(npic.*concentration(is)+1e-10*qom(is));
ii=abs(npic)<1e-10*abs(qom(is)); v0(ii)=0;
h5create(opath,['/Step#0/Block/Pxx_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxx_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));
Pxxpic(:,:,:,is) = ( vth2).* npic.*concentration(is);

v0 = Jypic(1:Nxpic,1:Nypic,1:Nzpic,is)./(npic.*concentration(is)+1e-10*qom(is));
ii=abs(npic)<1e-10*abs(qom(is)); v0(ii)=0;
h5create(opath,['/Step#0/Block/Pyy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pyy_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

v0 = Jzpic(1:Nxpic,1:Nypic,1:Nzpic,is)./(npic.*concentration(is));
ii=npic==0.0; v0(ii)=0;
h5create(opath,['/Step#0/Block/Pzz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pzz_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

h5create(opath,['/Step#0/Block/Pxy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxy_' num2str(is-1) '/0'], ...
    zeros(Nxpic, Nypic, Nzpic));

h5create(opath,['/Step#0/Block/Pxz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxz_' num2str(is-1) '/0'], ...
    zeros(Nxpic, Nypic, Nzpic));

h5create(opath,['/Step#0/Block/Pyz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pyz_' num2str(is-1) '/0'], ...
    zeros(Nxpic, Nypic, Nzpic));

ppic=(v0.^2 + vth2).* npic.*concentration(is);



subplot(3,2,is)
Tpic=abs(ppic./(npic.*concentration(is)+1e-10*qom(is))-v0.^2);
imagesc(sqrt(Tpic(:,:,1))');colorbar;title(['Tpic  ' num2str(is)])
subplot(3,2,is+2)
imagesc(Jzpic(:,:,1,is)'); colorbar;title(['Jzpic  ' num2str(is)])
subplot(3,2,is+4)
imagesc(npic(:,:,1)'*concentration(is)); colorbar;title(['npic  ' num2str(is)])
end

Lx_code = Lxpic/di0
Ly_code = 2*Lypic/di0
Lz_code=.1
one_sec = wpi0
dx=Lx_code/(Nxpic-1)
dy=Ly_code/(Nypic-1)
dz=Lz_code/(Nzpic-1)



% dr =-Xpic(1,1,1,1)+Xpic(2,1,1,1)
% dz =-Ypic(1,1,1,1)+Ypic(1,2,1,1)
[grp,gzp]=gradient(Pxxpic(:,:,1,1)/qom(1),dx,dy);
[JxBx,JxBy,JxBz]=cross_prod(Jxpic(:,:,1,1),Jypic(:,:,1,1),Jzpic(:,:,1,1),Bxpic(:,:,1)+Bxpic_ext(:,:,1),Bypic(:,:,1)+Bypic_ext(:,:,1),Bzpic(:,:,1)+Bzpic_ext(:,:,1));
[divX,dummy]=gradient(Xpic(:,:,1,1).*JxBx,dr,dz);
[dummy,divY]=gradient(JxBy,dr,dz);
div=divX./Xpic(:,:,1,1)+divY;

% [divX,dummy]=gradient(x.*(Bpr-VBpr),dr,dz);
% [dummy,divY]=gradient((Bpz-VBpz),dr,dz);
% div=divX./x+divY;

h5=figure(1005)
set(h5,'Position', [440 48 451 750])
subplot(2,3,1)
pcolor(Xpic(:,:,1,1),Ypic(:,:,1,1),grp);shading interp;axis equal;axis tight;colorbar;title('-\nabla_x p')
caxis([-2 2]*1e-7)
subplot(2,3,2)
pcolor(Xpic(:,:,1,1),Ypic(:,:,1,1),gzp);shading interp;axis equal;axis tight;colorbar;title('-\nabla_y p')
caxis([-2 2]*1e-7)
%subplot(2,3,3)
%pcolor(x,y,Viz);shading interp;axis equal;axis tight;colorbar
subplot(2,3,4)
pcolor(Xpic(:,:,1,1),Ypic(:,:,1,1),JxBx);shading interp;axis equal;axis tight;colorbar;title('[JxB]_x')
caxis([-2 2]*1e-7)
subplot(2,3,5)
pcolor(Xpic(:,:,1,1),Ypic(:,:,1,1),JxBy);shading interp;axis equal;axis tight;colorbar;title('[JxB]_y')
caxis([-2 2]*1e-7)
subplot(2,3,6)
pcolor(Xpic(:,:,1,1),Ypic(:,:,1,1),JxBz);shading interp;axis equal;axis tight;colorbar;title('[JxB]_z')
caxis([-2 2]*1e-7)

figure(1000) 
subplot(2,1,1)
plot(grp(50,:)*sum(Tratio))
hold on
plot(JxBx(50,:))
legend('\nabla p_x','JxBx')

subplot(2,1,2)
plot(gzp(50,:)*sum(Tratio))
hold on
plot(JxBy(50,:))

h5writeatt(opath,'/Step#0','nspec',int32(ns));

disp('Size in Code Units')


[grBpr,gzBpr,gphBpr]=gradient(permute(Bxpic,[2 1 3]),dx,dy,dz);
[grBpz,gzBpz,gphBpz]=gradient(permute(Bypic,[2 1 3]),dx,dy,dz);
[grBt,gzBt,gphBt]=gradient(permute(Bzpic,[2 1 3]),dx,dy,dz);
[grBrt,gzBrt,gphBrt]=gradient(permute(Xpic/di0.*Bzpic,[2 1 3]),dx,dy,dz);



mu0=4*pi;
Jx1=-permute(gzBt, [2 1 3])/mu0;
%Jy=(grBt+Bt./x)/mu0/code_J;
Jy1=(permute(grBrt, [2 1 3])./Xpic*di0)/mu0;
Jz1=meno * permute(gzBpr-grBpz, [2 1 3])/mu0;

Jx1(npic==0) = 0;
Jy1(npic==0) = 0;
Jz1(npic==0) = 0;

figure(8)
subplot(2,3,1)
imagesc(Jx1(:,:,1)')
colorbar
subplot(2,3,2)
imagesc(Jy1(:,:,1)')
colorbar
subplot(2,3,3)
imagesc(Jz1(:,:,1)')
colorbar
subplot(2,3,4)
imagesc(Jxpic(:,:,1)')
colorbar
subplot(2,3,5)
imagesc(Jypic(:,:,1)')
colorbar
subplot(2,3,6)
imagesc(Jzpic(:,:,1)')
colorbar

