function [J] = smooth3D(Jin,N)
J=Jin;
for i=1:N 
    i
J(2:end-1,2:end-1,2:end-1)=J(2:end-1,2:end-1,2:end-1)*.5+(J(1:end-2,2:end-1,2:end-1)+J(2:end-1,1:end-2,2:end-1)+J(3:end,2:end-1,2:end-1)+J(2:end-1,3:end,2:end-1)+J(2:end-1,2:end-1,1:end-2)+J(2:end-1,2:end-1,3:end))/12;
end
