function az=vecpot_uniform_xy(x,y,bx,by)


[ny,nx]=size(bx);
dx = (x(1,2)-x(1))

nymezzo=ceil(ny/2);
nxmezzo=ceil(ny/2);

az=zeros(size(bx));
az0=zeros(size(bx));
% for ind=2:nx;
% 	az0(nymezzo,ind)=az(nymezzo,ind-1)-trapz(x(ind-1:ind),by(nymezzo,ind-1:ind),2);
%     
% end

az(nymezzo,nxmezzo:end)=-cumtrapz(x(nxmezzo:end),by(nymezzo,nxmezzo:end),2);
az(nymezzo,nxmezzo:-1:1)=-cumtrapz(x(nxmezzo:-1:1),by(nymezzo,nxmezzo:-1:1),2);

for i=1:nx
    az(nymezzo:end,i) = az(nymezzo,i)+cumtrapz(y(nymezzo:end),bx(nymezzo:end,i));
end

for i=1:nx
    az(nymezzo:-1:1,i) = az(nymezzo,i)+cumtrapz(y(nymezzo:-1:1),bx(nymezzo:-1:1,i));
end


% for ind=nymezzo+1:ny
% %   ind;
%    az0(ind,:)=az0(ind-1,:)+trapz(y(ind-1:ind),bx(ind-1:ind,:));
% end
% for ind=nymezzo-1:-1:1
%    az0(ind,:)=az0(ind+1,:)-trapz(y(ind:ind+1),bx(ind:ind+1,:));
% end
