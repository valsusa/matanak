function  [cAx, cAy, cAz, cAv] = compute_curl(x,y,z,Ax,Ay,Az,radius,cyl)
if nargin<8
    cyl =0;
end
if nargin<7
    radius = 0;
end    
dx=x(1,2,1)-x(1,1,1);
dy=y(2,1,1)-y(1,1,1);
dz=y(1,1,2)-z(1,1,1);

small=1e-10;

if(radius == 0)
    Axsm=Ax;
    Aysm=Ay;
    Azsm=Az;
else    
Axsm=imgaussfilt3(Ax,radius,'Padding','circular');
Aysm=imgaussfilt3(Ay,radius,'Padding','circular');
Azsm=imgaussfilt3(Az,radius,'Padding','circular');
end
    [cAx, cAy, cAz] = curl(x,y,z,permute(Axsm,[2 1 3]), permute(Aysm, [2 1 3]), permute(Azsm, [2,1,3]));

cAx = permute(cAx, [2 1 3]);
cAy = permute(cAy, [2 1 3]);
cAz = permute(cAz, [2 1 3]);
end