clear all 
close all

%nome='psy400.iPic_mp_box_ttcor.0015.dat'; %Jean7 a


%nome='psy400.iPic_mp_box_ttcor.0015.dat'; %Jean13
%nome='psy501.iPic_mp_gbox.0020.dat'; %Jean17
%nome='~/Dropbox/Science/ucla/7feb09/feb0709iPICBox.035800UT.dat'; %

% 7feb09 at 035800UT
%nome='/data1/gianni/7feb09HR/feb0709iPICBox.035800UT.dat'; %7feb09

% 7feb09 at 035900UT [end del run cinetico precedente]
nome='/data1/gianni/7feb09HR/feb0709iPICBox.035900UT.dat'; %7feb09

nome='/data2/gianni/gc009/gc009iPICBox.024030UT.dat'; %gc009


% Run on Ring Current done for LWS proposal
%nome='/nobackup/glapenta/EArth/feb0709iPICBoxRC.035800UT.dat'; %7feb09 EARTH Centered
%nome='/data1/gianni/EArth/feb0709iPICBoxRC.035800UT.dat'; %7feb09 EARTH Centered on buteo



% Run on Ring Current done from Mostafa
% nome='gc012iPICBoxHR.014500UT.dat'; %gc012
% opath='gc012KHR-Fields_000000.h5'


%nome='~/Documents/storage/ucla/ucla/HRmaha3D3/feb1508iPIC.034800UT.dat'; %HRmaha3D3
%nome='~/psy501.mp_EC_3DT01_box.0020.dat'

% Generic Run without any specific event in  mind
%nome = '/nobackupp2/glapenta/generic/gc009iPICBox_lg.013000UT.dat'; % Generic run at nasa
%nome = '/data1/gianni/gc009/gc009iPICBox_lg.013000UT.dat'; % Generic run on Buteo

% BOW 25
%nome = '/Users/glapenta/Sources/dayside_2021sep_6/psy501.iPic_mp_n3box.0020.dat';
%nome = '/gpfsm/dnb31/glapenta/dayside_sep2021_7/psy501.iPic_mp_n3box.0020.dat';

%Gipsy run for Jean
%nome = '/Users/glapenta/Sources/dayside/gipsy702_2980.mp_box.dat'
%nome = '/Users/glapenta/Sources/dayside_earlier/gipsy702_2640.mp_box.dat'
%nome = '/Users/glapenta/Desktop/dayside/gipsy702_2980.mp_box2.dat'
%nome = '/Users/glapenta/Sources/dayside_2021sep/gipsy702_2980.mp_box.dat'
%nome = '/nobackup/glapenta/gipsy/gipsy702_2640.mp_box2.dat'

% case with discontinuity in solar wind, jean june 2022
%nome = '/Users/giovannilapenta/Data/codes/build_dayside/modlws001_2910.shckbox.dat' %dominio piu piccino in Y
%nome = '/Users/giovannilapenta/Data/codes/build_dayside/modlws001_2910.shckbox_2.dat' %dominio piu` grande in Y
%nome = '/Users/giovannilapenta/Data/codes/build_dayside/modlws001_2820.shckbox_2.dat' %tempo precedente senza discontinuita in B davanti allo shock
%nome = '../../dayside3D_4sept22/modlws001_2820.shckbox_3D.dat' %tempo precedente senza discontinuita in B davanti allo shock
%nome = '../../dayside3Dlambr/modlws001_2910.shckbox_3D.dat' %tempo precedente senza discontinuita in B davanti allo shock
nome = '/data1/gianni/dayside_transfer/modlws001_2820.shckbox_3D.dat' %tempo precedente senza discontinuita in B davanti allo shock

%older dayside
%nome = '/Users/giovannilapenta/Data/codes/build_dayside/modlws001_2910.shckbox.dat' %dominio piu piccino in Y
%nome = '/Users/giovannilapenta/Data/codes/build_dayside/modlws001_2910.shckbox_2.dat' %dominio piu` grande in Y
%nome = '/Users/giovannilapenta/Data/codes/build_dayside/modlws001_2820.shckbox_2.dat' %tempo precedente senza discontinuita in B davanti allo shock
%nome = '../../dayside3Dlambr/modlws001_2820.shckbox_3D.dat' %tempo precedente senza discontinuita in B davanti allo shock
%nome = '../../dayside3Dlambr/modlws001_2910.shckbox_3D.dat' %tempo precedente senza discontinuita in B davanti allo shock


% case with discontinuity in solar wind, jean june 2022
%nome = '/Users/giovannilapenta/Data/codes/build_dayside/modlws001_2910.shckbox.dat' %dominio piu piccino in Y
%nome = '/Users/giovannilapenta/Data/codes/build_dayside/modlws001_2910.shckbox_2.dat' %dominio piu` grande in Y
%nome = '/Users/giovannilapenta/Data/codes/build_dayside/modlws001_2820.shckbox_2.dat' %tempo precedente senza discontinuita in B davanti allo shock
%nome = '../../dayside3Dlambr/modlws001_2820.shckbox_3D.dat' %tempo precedente senza discontinuita in B davanti allo shock
%nome = '../../dayside3Dlambr/modlws001_2910.shckbox_3D.dat' %tempo precedente senza discontinuita in B davanti allo shock


%Case Ring Current DEc 2022

%nome = '/nobackup/glapenta/test_mostafa_dec22/gc012iPICBoxHR.014500UT.dat'
%nome = '/nobackup/glapenta/Ring_jan23/gc012iPICBoxHR.014500UT.dat'

%


phys_el = 0 % Yes physicla electrons is 1, otherwise 0
n_ref = .25; %n_ref=1; %for mercury
Ti_ref = 3.5e3;% Ion Temperature in eV
sol_factor = 1 %.1 % Factoring the speed of loght

mr=256
[code_n, code_J, code_V, code_T, code_E, code_B, momentum_corrector, dp_phys, de, wpp] =   code_units(mr,0,n_ref,Ti_ref,1.0,1.0);

[code_n, code_J, code_V, code_T, code_E, code_B, momentum_corrector, dp, de, wpp] =   code_units(mr,phys_el,n_ref,Ti_ref,sol_factor,1.0);
disp(['code_n=' num2str(code_n)])
disp(['code_J=' num2str(code_J)])
disp(['code_V=' num2str(code_V)])
disp(['code_T=' num2str(code_T)])
disp(['code_E=' num2str(code_E)])
disp(['code_B=' num2str(code_B)])
momentum_corrector=1;

dp
dp_phys

e= 1.6022e-19;

Tratio=[1/5,1,1/5,1];
qom=[-mr,1,-mr,1];
concentration=[-1.0,1.0,-1.0e-3,1.0e-3];

ns=4;

readdo=1;
if(readdo)
% Data structure:
%x(RE) y(RE) z(RE) bx(nT) by(nT) bz(nT) vx(km/s) vy(km/s) vz(km/s)
% density(cm-3) pressure(pPa) jx(nA/m2) jy(nA/m2) jz(nA/m2)
fid=fopen(nome);

s=fscanf(fid,'%s',4);
x =  fscanf(fid,'%f',4);
xmin=x(1); xmax=x(2); dx=x(3); Nx= x(4);
s=fscanf(fid,'%s',4);
x =  fscanf(fid,'%f',4);
ymin=x(1); ymax=x(2); dy=x(3); Ny= x(4);
s=fscanf(fid,'%s',4);
x =  fscanf(fid,'%f',4);
zmin=x(1); zmax=x(2); dz=x(3); Nz= x(4);



a=fscanf(fid,'%f',[14 inf])';


fclose(fid)
end

x=reshape(a(:,1),Nz,Ny,Nx);xmax=max(x(:));xmin=min(x(:));
y=reshape(a(:,2),Nz,Ny,Nx);ymax=max(y(:));ymin=min(y(:));
z=reshape(a(:,3),Nz,Ny,Nx);zmax=max(z(:));zmin=min(z(:));

n=reshape(a(:,10),Nz,Ny,Nx)*1e6;

density_adjust = true;
if(density_adjust) 
    n(n==0) = 1e5;
end   



Bx=reshape(a(:,4),Nz,Ny,Nx)*1e-9/code_B;
By=reshape(a(:,5),Nz,Ny,Nx)*1e-9/code_B;
Bz=reshape(a(:,6),Nz,Ny,Nx)*1e-9/code_B;

Jx=reshape(a(:,12),Nz,Ny,Nx)*1e-9;
Jy=reshape(a(:,13),Nz,Ny,Nx)*1e-9;
Jz=reshape(a(:,14),Nz,Ny,Nx)*1e-9;

Vx=reshape(a(:,7),Nz,Ny,Nx)*1e3*momentum_corrector;
Vy=reshape(a(:,8),Nz,Ny,Nx)*1e3*momentum_corrector;
Vz=reshape(a(:,9),Nz,Ny,Nx)*1e3*momentum_corrector;

%Add a cloud
%
%n(x<-9.5)=n(x<-9.5)*5;
%Vx(x<-9.5)=Vx(x<-9.5)*5;

Ex = - (Vy.*Bz - Vz.*By)/code_V;
Ey = - (Vz.*Bx - Vx.*Bz)/code_V;
Ez = - (Vx.*By - Vy.*Bx)/code_V;

FrameShift = false
if (FrameShift) 
    Ex = Ex*0;
    Ey = Ey*0;
    Ez = Ez*0;
    disp('Frame Shift on')
end    

Vix = Vx / code_V;
Viy = Vy / code_V;
Viz = Vz / code_V;

Vex = (Vx - Jx ./n /e)/ code_V;
Vey = (Vy - Jy ./n /e)/ code_V;
Vez = (Vz - Jz ./n /e)/ code_V;

p=reshape(a(:,11),Nz,Ny,Nx)*1e-12;

T = (p ./ n  ) ./ code_T ./(1.0+Tratio(1)/Tratio(2));
%ii=n==0; T(ii)=0;

n= n/ code_n/4/pi;

Lx=xmax-xmin
Ly=ymax-ymin
Lz=zmax-zmin

%Nxpic=129;Nypic=65; Nzpic=2; %2D 7feb09

%Nxpic=400+1;Nypic=160+1; Nzpic=160+1; %HRmaha3D3

%Nxpic=400+1; Nypic= 300+1; Nzpic=300+1; % 3D 7feb09
%Nxpic=400+1; Nypic= 200+1; Nzpic=280+1; % 3D 7feb09

Nxpic=400+1; Nypic=200+1; Nzpic= 280+1; %generic

Nxpic=150+1; Nypic= 80+1; Nzpic= 100+1; %gc009

%Nxpic=200+1; Nypic=100+1; Nzpic= 1+1; %generic 2D Low Resolution

%Nxpic=270+1; Nypic=200+1; Nzpic= 200+1; %Earth 
%Nxpic=130+1; Nypic=100+1; Nzpic= 100+1; %Earth 
%Nxpic=200+1; Nypic=150+1; Nzpic= 1+1; %Earth 2D meridian
%Nxpic=200+1; Nypic=1+1; Nzpic= 140+1; %Earth 2D equatorial

%Nxpic = 800 +1; Nypic = 260+1; NZpic = 200+1; %BOW25
Nxpic = 200 +1; Nypic = 128+1; Nzpic = 1+1; %BOW25 in 2D

%Nxpic=80+1; Nypic= 240+1; Nzpic= 1+1; %gipsy 2D run
%Nxpic=80+1; Nypic= 240+1; Nzpic= 40+1; %gipsy run 3D 

%Nxpic=120+1; Nypic= 240+1; Nzpic= 120+1; %gipsy run 3D bigger box more resolution 

% case with discontinuity in solar wind, jean june 2022
Nxpic = 140*2+1; Nypic = 40*2+1; Nzpic = 1+1;
Nxpic = 140*2+1; Nypic = 80+1; Nzpic = 1+1;

Nxpic = 140+1; Nypic = 80+1; Nzpic = 80+1;


% Ring Current Dec 2022

Nxpic = 308+1
Nypic = 90+1
Nzpic = 232+1

dx=(xmax-xmin)/Nxpic;
dy=(ymax-ymin)/Nypic;
dz=(zmax-zmin)/Nzpic;

xpic=linspace(xmin,xmax,Nxpic);

ypic=linspace(ymin,ymax,Nypic);
if(Nzpic<=2) 
    zpic=[0 0];%[(zmin+zmax)/2 (zmin+zmax)/2] ;
else
    zpic=linspace(zmin,zmax,Nzpic);
end
if(Nypic<=2) 
    ypic=[0 0];%[(ymin+ymax)/2 (ymin+ymax)/2] ;
else
    ypic=linspace(ymin,ymax,Nypic);
end
[Xpic,Ypic,Zpic]=ndgrid(xpic,ypic,zpic);

Tpic=interpmio(x,y,z,T,Xpic,Ypic,Zpic); 


npic=interpmio(x,y,z,n,Xpic,Ypic,Zpic);
%ii=npic<.01;npic(ii)=.01;
%npic(:)=1;

%imagesc(squeeze(sqrt(256*Tratio(1)*Tpic(:,:,1))));axis image;colorbar
%imagesc(squeeze(npic(:,:,1)));axis image;colorbar


Bxpic=interpmio(x,y,z,Bx,Xpic,Ypic,Zpic);
Bypic=interpmio(x,y,z,By,Xpic,Ypic,Zpic);
Bzpic=interpmio(x,y,z,Bz,Xpic,Ypic,Zpic);

Expic=interpmio(x,y,z,Ex,Xpic,Ypic,Zpic);
Eypic=interpmio(x,y,z,Ey,Xpic,Ypic,Zpic);
Ezpic=interpmio(x,y,z,Ez,Xpic,Ypic,Zpic);

Jxpic(1:Nxpic,1:Nypic,1:Nzpic,1)=interpmio(x,y,z,n.*Vex,Xpic,Ypic,Zpic);
Jypic(1:Nxpic,1:Nypic,1:Nzpic,1)=interpmio(x,y,z,n.*Vey,Xpic,Ypic,Zpic);
Jzpic(1:Nxpic,1:Nypic,1:Nzpic,1)=interpmio(x,y,z,n.*Vez,Xpic,Ypic,Zpic);

Jxpic(1:Nxpic,1:Nypic,1:Nzpic,2)=interpmio(x,y,z,n.*Vix,Xpic,Ypic,Zpic);
Jypic(1:Nxpic,1:Nypic,1:Nzpic,2)=interpmio(x,y,z,n.*Viy,Xpic,Ypic,Zpic);
Jzpic(1:Nxpic,1:Nypic,1:Nzpic,2)=interpmio(x,y,z,n.*Viz,Xpic,Ypic,Zpic);

Jxpic(1:Nxpic,1:Nypic,1:Nzpic,3:4)=0.0;
Jypic(1:Nxpic,1:Nypic,1:Nzpic,3:4)=0.0;
Jzpic(1:Nxpic,1:Nypic,1:Nzpic,3:4)=0.0;

invertYZ = false;
if(invertYZ) 
    %exchange y with z and z with -y
    Tpic=permute(Tpic,[1 3 2]);
    npic=permute(npic,[1 3 2]);
    Bxpic=permute(Bxpic,[1 3 2]);
    tmp=permute(Bzpic,[1 3 2]);
    Bzpic=-permute(Bypic,[1 3 2]);
    Bypic=tmp;
    Expic=permute(Expic,[1 3 2]);
    tmp=permute(Ezpic,[1 3 2]);
    Ezpic=-permute(Eypic,[1 3 2]);
    Eypic=tmp;
    Jxpic=permute(Jxpic,[1 3 2 4]);
    tmp=permute(Jzpic,[1 3 2 4]);
    Jzpic=-permute(Jypic,[1 3 2 4]);
    Jypic= tmp;
    tmp = Nzpic;
    Nzpic = Nypic;
    Nypic = tmp;
end    

%!rm Earth-Fields_000000.h5
%opath='Earth-Fields_000000.h5'
%opath='Initial-7feb09-0359-Fields_000000.h5'
!rm Generic4sp-Fields_000000.h5
opath='Generic4sp-Fields_000000.h5'


opath=[nome(1:end-4) '-Fields_000000.h5']
system(['rm ' opath])
opath_rs=[nome(1:end-4) '-FixedFields.h5']
system(['rm ' opath_rs])


Bxpic_ext=zeros(Nxpic, Nypic, Nzpic);
Bypic_ext=zeros(Nxpic, Nypic, Nzpic);
Bzpic_ext=zeros(Nxpic, Nypic, Nzpic);

externalon = false;

if(externalon)

Bpic = sqrt(Bxpic.^2 +  Bypic.^2 +Bzpic.^2);

ii=Bypic>0. ; % 1e-2;  %.015;

Bxpic_ext(ii) = Bxpic(ii);
Bxpic(ii) = 0;


Bypic_ext(ii) = Bypic(ii);
Bypic(ii) = 0;


Bzpic_ext(ii) = Bzpic(ii);
Bzpic(ii) = 0;

end

h5create(opath,'/Step#0/Block/Bx/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bx/0',Bxpic)

h5create(opath,'/Step#0/Block/By/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/By/0',Bypic)

h5create(opath,'/Step#0/Block/Bz/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bz/0',Bzpic)

h5create(opath_rs,'/Step#0/Block/Bx_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Bx_ext/0',Bxpic_ext)

h5create(opath_rs,'/Step#0/Block/By_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/By_ext/0',Bypic_ext)

h5create(opath_rs,'/Step#0/Block/Bz_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Bz_ext/0',Bzpic_ext)


%set_E_int = false %test
set_E_int = true %gipsy702b

if(set_E_int)
h5create(opath,'/Step#0/Block/Ex/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ex/0',Expic)

h5create(opath,'/Step#0/Block/Ey/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ey/0',Eypic)

h5create(opath,'/Step#0/Block/Ez/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ez/0',Ezpic)

h5create(opath_rs,'/Step#0/Block/Ex_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ex_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath_rs,'/Step#0/Block/Ey_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ey_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath_rs,'/Step#0/Block/Ez_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ez_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath_rs,'/Step#0/Block/Ex_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ex_rs/0',Expic)

h5create(opath_rs,'/Step#0/Block/Ey_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ey_rs/0',Eypic)

h5create(opath_rs,'/Step#0/Block/Ez_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ez_rs/0',Ezpic)

else
    
h5create(opath_rs,'/Step#0/Block/Ex_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ex_ext/0',Expic)

h5create(opath_rs,'/Step#0/Block/Ey_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ey_ext/0',Eypic)

h5create(opath_rs,'/Step#0/Block/Ez_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ez_ext/0',Ezpic)

h5create(opath,'/Step#0/Block/Ex/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ex/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Ey/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ey/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Ez/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ez/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath_rs,'/Step#0/Block/Ex_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ex_rs/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath_rs,'/Step#0/Block/Ey_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ey_rs/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath_rs,'/Step#0/Block/Ez_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ez_rs/0',zeros(Nxpic, Nypic, Nzpic))

end 
    

h5create(opath,['/Step#0/Block/rho_avg/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/rho_avg/0'],zeros(Nxpic, Nypic, Nzpic))

Lambdapic = zeros(Nxpic, Nypic, Nzpic);
%boundary in x
Lambdapic(1:3,:,:) = 1; %left
Lambdapic(end-2:end,:,:) = 1; %right

%boundary in y
%Lambdapic(:,1:3,:) = 1;  Lambdapic(:,end-2:end,:) = 1;
%boundary in z
%Lambdapic(:,:,1:3) = 1;  Lambdapic(:,:,end-2:end) = 1;
Lambdapic=smooth3Dnew(Lambdapic,3);


%Lambdapic = (1-tanh((Xpic-xmin-4*dx)/2/dx))/2; 
%Lambdapic = Lambdapic + (1 - tanh((Ypic-ymin-4*dy)/2/dy))/2; 
%Lambdapic = Lambdapic + (1 - tanh((ymax-Ypic-4*dy)/2/dy))/2; 
%Lambdapic(Lambdapic>1)=1;
figure
imagesc(Lambdapic(:,:,1)'); colorbar; axis equal; axis tight


h5create(opath_rs,'/Step#0/Block/Lambda/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Lambda/0',Lambdapic)

h5create(opath_rs,'/Step#0/Block/Bx_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Bx_rs/0',Bxpic)

h5create(opath_rs,'/Step#0/Block/By_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/By_rs/0',Bypic)

h5create(opath_rs,'/Step#0/Block/Bz_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Bz_rs/0',Bzpic)



is=2 % ste frame for ions, species 2
h5create(opath,['/Step#0/Block/Vfx/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Vfx/0'],Jxpic(1:Nxpic,1:Nypic,1:Nzpic,is)./npic)

h5create(opath,['/Step#0/Block/Vfy/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Vfy/0'],Jypic(1:Nxpic,1:Nypic,1:Nzpic,is)./npic)

h5create(opath,['/Step#0/Block/Vfz/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Vfz/0'],Jzpic(1:Nxpic,1:Nypic,1:Nzpic,is)./npic)


for is=1:ns
h5create(opath,['/Step#0/Block/Jx_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jx_' num2str(is-1) '/0'],Jxpic(1:Nxpic,1:Nypic,1:Nzpic,is)*concentration(is))

h5create(opath,['/Step#0/Block/Jy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jy_' num2str(is-1) '/0'],Jypic(1:Nxpic,1:Nypic,1:Nzpic,is)*concentration(is))

h5create(opath,['/Step#0/Block/Jz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jz_' num2str(is-1) '/0'],Jzpic(1:Nxpic,1:Nypic,1:Nzpic,is)*concentration(is))


h5create(opath,['/Step#0/Block/rho_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/rho_' num2str(is-1) '/0'],npic*concentration(is))

h5create(opath_rs,['/Step#0/Block/Jx_rs_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,['/Step#0/Block/Jx_rs_' num2str(is-1) '/0'],Jxpic(1:Nxpic,1:Nypic,1:Nzpic,is)*concentration(is))

h5create(opath_rs,['/Step#0/Block/Jy_rs_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,['/Step#0/Block/Jy_rs_' num2str(is-1) '/0'],Jypic(1:Nxpic,1:Nypic,1:Nzpic,is)*concentration(is))

h5create(opath_rs,['/Step#0/Block/Jz_rs_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,['/Step#0/Block/Jz_rs_' num2str(is-1) '/0'],Jzpic(1:Nxpic,1:Nypic,1:Nzpic,is)*concentration(is))


h5create(opath_rs,['/Step#0/Block/rho_rs_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,['/Step#0/Block/rho_rs_' num2str(is-1) '/0'],npic*concentration(is))

vth2= abs(qom(is)) * Tratio(is) * Tpic; 
vth_left = sqrt(vth2(1,1,1))
v0= Jxpic(1:Nxpic,1:Nypic,1:Nzpic,is)./(npic+1e-10); max(v0(:))
h5create(opath,['/Step#0/Block/Pxx_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxx_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

h5create(opath_rs,['/Step#0/Block/Pxx_rs_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,['/Step#0/Block/Pxx_rs_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

TEST =mean((v0.^2 + vth2).* npic.*concentration(is),'all')
v0 = Jypic(1:Nxpic,1:Nypic,1:Nzpic,is)./npic;
h5create(opath,['/Step#0/Block/Pyy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
disp(['Species ' num2str(is)])
v0_mean=mean(abs(v0(:)))
vth_mean=mean(sqrt(vth2(:)))
h5write(opath,['/Step#0/Block/Pyy_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

h5create(opath_rs,['/Step#0/Block/Pyy_rs_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,['/Step#0/Block/Pyy_rs_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

v0 = Jzpic(1:Nxpic,1:Nypic,1:Nzpic,is)./npic;
h5create(opath,['/Step#0/Block/Pzz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pzz_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

h5create(opath_rs,['/Step#0/Block/Pzz_rs_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,['/Step#0/Block/Pzz_rs_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

h5create(opath,['/Step#0/Block/Pxy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxy_' num2str(is-1) '/0'], ...
    zeros(Nxpic, Nypic, Nzpic));

h5create(opath,['/Step#0/Block/Pxz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxz_' num2str(is-1) '/0'], ...
    zeros(Nxpic, Nypic, Nzpic));

h5create(opath,['/Step#0/Block/Pyz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pyz_' num2str(is-1) '/0'], ...
    zeros(Nxpic, Nypic, Nzpic));

end
h5writeatt(opath,'/Step#0','nspec',int32(ns));
h5writeatt(opath_rs,'/Step#0','nspec',int32(ns));

% Box Size
R_E = 6.371e6*dp/dp_phys; %in meters
Lx_code = Lx *R_E /dp
Ly_code = Ly *R_E /dp
Lz_code = Lz *R_E /dp
% Lx_code = Lxpic/di0
% Ly_code = 2*Lypic/di0
% Lz_code=.1
T_Cross = (Lx * R_E) / max(Vx(:))
T_Cross * wpp
T_Cross_Code = Lx_code / max(abs(Vix(:))) % in seconds
T_Cross_Minutes = Lx *R_E / max(Vx(:)) /60  % in minues
dt=1.0
KiloCycles = T_Cross_Code /dt *1e-3

wppDT = dt;
dt_phys = wppDT/wpp

Bpic = sqrt(Bxpic.^2 + Bypic.^2 + Bzpic.^2);
Vi = sqrt(Vix.^2 + Viy.^2 + Viz.^2);
[max(Bxpic(:)), max(Vi(:)), max(npic(:))]
Va = max(Bxpic(:))/sqrt(4*pi*max(npic(:)))



vth2= abs(qom(1)) * Tratio(1) * Tpic;
v0= Jxpic(:,:,:,1)./(npic+1e-10);
vxovth = v0 ./sqrt(vth2+1e-10);

figure
imagesc(vxovth(:,:,round(Nzpic/2))); colorbar; axis equal; axis tight

return

%Year Month Day Hour Min Sec Msec Bx[nT] By[nT] Bz[nT] Vx[km/s] Vy[km/s] Vz[km/s] N[cm^(-3)] T[Kelvin]

omni=[
2018 8 21 18 40 0 0 0 -0.192 1.119 -568.055 -32.423 0.224 1.554 135564
2018 8 21 18 41 0 0 0 0.834 0.609 -568.055 -32.423 0.224 1.554 135564
2018 8 21 18 42 0 0 0 0.381 0.684 -568.055 -32.423 0.224 1.554 135564
2018 8 21 18 43 0 0 0 0.489 0.687 -568.055 -32.423 0.224 1.554 135564
2018 8 21 18 44 0 0 0 0.498 0.64 -568.055 -32.423 0.224 1.554 135564
2018 8 21 18 45 0 0 0 0.294 0.729 -568.055 -32.423 0.224 1.554 135564
2018 8 21 18 46 0 0 0 0.494 0.651 -568.055 -32.423 0.224 1.554 135564
2018 8 21 18 47 0 0 0 0.501 0.763 -568.055 -32.423 0.224 1.554 135564
2018 8 21 18 48 0 0 0 0.617 0.622 -568.055 -32.423 0.224 1.554 135564
2018 8 21 18 49 0 0 0 -0.042 1.075 -568.055 -32.423 0.224 1.554 135564
2018 8 21 18 50 0 0 0 0.315 0.801 -568.055 -32.423 0.224 1.554 135564];

%code has minus x and z y, z is are switched
omni(:,8) =  - omni(:,8);
tmp =  omni(:,10);
omni(:,10) = omni(:,9);
omni(:,9) = tmp 
omni(:,11) =  - omni(:,11);
tmp =  omni(:,12);
omni(:,12) = omni(:,13);
omni(:,13) = tmp;
b_omni = omni(:,8:10)*1e-9/code_B;
v_omni = omni(:,11:13)*1e3/code_V;
rho_omni = omni(:,14);
T_omni = omni(:,15);


t_omni = omni(:,5);
t_omni = (t_omni - t_omni(1))*60


Lx_inputfile=35.39;
Lx_code/Lx_inputfile
time_cycle=dt_phys*Lx_code/Lx_inputfile
cycle = t_omni/time_cycle;
figure
plot(t_omni/time_cycle,v_omni)

fileID = fopen('TimeFile_omni.txt','w');
%fprintf(fileID,'%6s %12s\n','x','exp(x)');
%fprintf(fileID,'%6.2f %12.8f\n',A);
%fprintf(fileID,
fclose(fileID);
