close all
clear all

nome='/Users/giovannilapenta/Dropbox/Shared/tokamak/Equilibrium_Information_VEST.mat';

density_adjust = false;

% Refernce Pressure in Pascal
 p_ref = 300;
%n_ref=1; %for mercury

% Reference Temperature in eV
 kT_ref = 200;


% Temperature Ratio
 TioTe = 25/200;

%B in  Tesla
 B_ref = .2;
 
 mass_ratio=64;
 
 physical_electrons = false;
 
[code_n, code_J, code_V, code_T, code_E, code_B, di0, wpi0] =   ...
       code_units_tokamak(mass_ratio, physical_electrons, p_ref, kT_ref, TioTe, B_ref);


e= 1.6022e-19;
mu0=1.256637e-6;

kT_ref_si=kT_ref*e;

Tratio=[1,TioTe];
qom=[-mass_ratio,1];
concentration=[-1.0,1.0];

ns=2;

%size in meters
Lx=1 %radius
Ly=3 %vertical size


load(nome)

ChamberBoundary(:,2)=ChamberBoundary(:,2)+Ly/2;

figure(1)
plot(ChamberBoundary(:,1),ChamberBoundary(:,2))
axis equal



[Ny, Nx]=size(Bpr)

[x,y]=meshgrid(1:Nx,1:Ny);
x=(x-1)/(Nx-1)*Lx;
y=(y-1)/(Ny-1)*Ly;
dr=Lx/(Nx-1);
dz=Ly/(Ny-1);
z=zeros(size(x));

[in,on] = inpolygon(x,y,ChamberBoundary(:,1),ChamberBoundary(:,2));

n=Pressure/kT_ref_si; %in SI
  


[grBpr,gzBpr]=gradient(Bpr,dr,dz);
[grBpz,gzBpz]=gradient(Bpz,dr,dz);
[grBt,gzBt]=gradient(Bt,dr,dz);
[grBrt,gzBrt]=gradient(x.*Bt,dr,dz);

Jx=-gzBt/mu0;
%Jy=(grBt+Bt./x)/mu0/code_J;
Jy=(grBrt./x)/mu0;
Jz=(gzBpr-grBpz)/mu0;
%ii=n==0;
in=n>0;
Jx(~in)=0;
Jy(~in)=0;
Jz(~in)=0;

Bx=Bpr/code_B;
By=Bpz/code_B;
Bz=Bt/code_B;

Vx=zeros(size(Bpr))/code_V;
Vy=zeros(size(Bpr))/code_V;
Vz=zeros(size(Bpr))/code_V;

Ex = - (Vy.*Bz - Vz.*By);
Ey = - (Vz.*Bx - Vx.*Bz);
Ez = - (Vx.*By - Vy.*Bx);

Vix = Vx / code_V;
Viy = Vy / code_V;
Viz = Vz / code_V;

% Assuming me<<mi
Vex = (Vx - Jx ./(n+1e-10) /e)/ code_V;
Vey = (Vy - Jy ./(n+1e-10) /e)/ code_V;
Vez = (Vz - Jz ./(n+1e-10) /e)/ code_V;
ii=abs(Vex)>.1; Vex(ii)=Vex(ii)./abs(Vex(ii))*.1;
ii=abs(Vey)>.1; Vey(ii)=Vey(ii)./abs(Vey(ii))*.1;
ii=abs(Vez)>.1; Vez(ii)=Vez(ii)./abs(Vez(ii))*.1;

p=Pressure;

kT = (p ./ (n+1e-10)  ) ./ code_T;

n=n/code_n;

h2=figure(2)
set(h2,'Position', [440 48 451 750])
subplot(3,3,1)
pcolor(x,y,Bx);shading interp;axis equal;axis tight;colorbar
subplot(3,3,2)
pcolor(x,y,By);shading interp;axis equal;axis tight;colorbar
subplot(3,3,3)
pcolor(x,y,Bz);shading interp;axis equal;axis tight;colorbar
subplot(3,3,4)
pcolor(x,y,Jx);shading interp;axis equal;axis tight;colorbar
subplot(3,3,5)
pcolor(x,y,Jy);shading interp;axis equal;axis tight;colorbar
subplot(3,3,6)
pcolor(x,y,Jz);shading interp;axis equal;axis tight;colorbar
subplot(3,3,7)
pcolor(x,y,n);shading interp;axis equal;axis tight;colorbar
subplot(3,3,8)
pcolor(x,y,kT);shading interp;axis equal;axis tight;colorbar
subplot(3,3,9)
pcolor(x,y,p);shading interp;axis equal;axis tight;colorbar


h3=figure(3)
set(h3,'Position', [440 48 451 750])
subplot(3,3,1)
pcolor(x,y,Vix);shading interp;axis equal;axis tight;colorbar
subplot(3,3,2)
pcolor(x,y,Viy);shading interp;axis equal;axis tight;colorbar
subplot(3,3,3)
pcolor(x,y,Viz);shading interp;axis equal;axis tight;colorbar
subplot(3,3,4)
pcolor(x,y,Vex);shading interp;axis equal;axis tight;colorbar
subplot(3,3,5)
pcolor(x,y,Vey);shading interp;axis equal;axis tight;colorbar
subplot(3,3,6)
pcolor(x,y,Vez);shading interp;axis equal;axis tight;colorbar



Nxpic=100+1;Nypic=300+1; Nzpic=1+1; 

dx=Lx/Nxpic;
dy=Ly/Nypic;
dz=min(dx,dy);

xpic=linspace(0,Lx,Nxpic);

ypic=linspace(0,Ly,Nypic);
if(Nzpic<=2) 
    zpic=[0 0];%[(zmin+zmax)/2 (zmin+zmax)/2] ;
else
    zpic=linspace(0,dz,Nzpic);
end
if(Nypic<=2) 
    ypic=[0 0];%[(ymin+ymax)/2 (ymin+ymax)/2] ;
else
    ypic=linspace(0,Ly,Nypic);
end
[Xpic,Ypic,Zpic]=ndgrid(xpic,ypic,zpic);

kTpic=interpmio2d(x,y,z,kT,Xpic,Ypic,Zpic); 


npic=interpmio2d(x,y,z,n,Xpic,Ypic,Zpic);

[in,on] = inpolygon(Xpic,Ypic,ChamberBoundary(:,1),ChamberBoundary(:,2));

Lambdapic=zeros(size(Xpic));
Lambdapic(~in)=1;

% Lambda = zeros(size(x))
% ii=n==0;
% Lambda(ii) = 1;
% Lambdapic=interpmio2d(x,y,z,Lambda,Xpic,Ypic,Zpic);

size(Lambdapic)

subplot(3,3,7)
pcolor(Xpic(:,:,1),Ypic(:,:,1),Lambdapic(:,:,1));shading interp;axis equal;axis tight;colorbar


Bxpic=interpmio2d(x,y,z,Bx,Xpic,Ypic,Zpic);
Bypic=interpmio2d(x,y,z,By,Xpic,Ypic,Zpic);
Bzpic=interpmio2d(x,y,z,Bz,Xpic,Ypic,Zpic);

Expic=interpmio2d(x,y,z,Ex,Xpic,Ypic,Zpic);
Eypic=interpmio2d(x,y,z,Ey,Xpic,Ypic,Zpic);
Ezpic=interpmio2d(x,y,z,Ez,Xpic,Ypic,Zpic);

Jxpic(1:Nxpic,1:Nypic,1:Nzpic,1)=interpmio2d(x,y,z,n.*Vex,Xpic,Ypic,Zpic);
Jypic(1:Nxpic,1:Nypic,1:Nzpic,1)=interpmio2d(x,y,z,n.*Vey,Xpic,Ypic,Zpic);
Jzpic(1:Nxpic,1:Nypic,1:Nzpic,1)=interpmio2d(x,y,z,n.*Vez,Xpic,Ypic,Zpic);

Jxpic(1:Nxpic,1:Nypic,1:Nzpic,2)=interpmio2d(x,y,z,n.*Vix,Xpic,Ypic,Zpic);
Jypic(1:Nxpic,1:Nypic,1:Nzpic,2)=interpmio2d(x,y,z,n.*Viy,Xpic,Ypic,Zpic);
Jzpic(1:Nxpic,1:Nypic,1:Nzpic,2)=interpmio2d(x,y,z,n.*Viz,Xpic,Ypic,Zpic);
 


!rm Tokamak-Fields_000000.h5
opath='Tokamak-Fields_000000.h5'

h5create(opath,'/Step#0/Block/Lambda/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Lambda/0',Lambdapic)

h5create(opath,'/Step#0/Block/Bx/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bx/0',Bxpic)

h5create(opath,'/Step#0/Block/By/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/By/0',Bypic)

h5create(opath,'/Step#0/Block/Bz/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bz/0',Bzpic)

h5create(opath,'/Step#0/Block/Bx_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bx_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/By_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/By_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Bz_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bz_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Ex/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ex/0',Expic)

h5create(opath,'/Step#0/Block/Ey/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ey/0',Eypic)

h5create(opath,'/Step#0/Block/Ez/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ez/0',Ezpic)


h5create(opath,'/Step#0/Block/Ex_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ex_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Ey_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ey_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Ez_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ez_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,['/Step#0/Block/rho_avg/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/rho_avg/0'],zeros(Nxpic, Nypic, Nzpic))

for is=1:ns
h5create(opath,['/Step#0/Block/Jx_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jx_' num2str(is-1) '/0'],Jxpic(1:Nxpic,1:Nypic,1:Nzpic,is)*concentration(is))

h5create(opath,['/Step#0/Block/Jy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jy_' num2str(is-1) '/0'],Jypic(1:Nxpic,1:Nypic,1:Nzpic,is)*concentration(is))

h5create(opath,['/Step#0/Block/Jz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jz_' num2str(is-1) '/0'],Jzpic(1:Nxpic,1:Nypic,1:Nzpic,is)*concentration(is))


h5create(opath,['/Step#0/Block/rho_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/rho_' num2str(is-1) '/0'],npic*concentration(is))

vth2= abs(qom(is)) * Tratio(is) * kTpic; 
v0= Jxpic(1:Nxpic,1:Nypic,1:Nzpic,is)./(npic+1e-10);
h5create(opath,['/Step#0/Block/Pxx_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxx_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

v0 = Jypic(1:Nxpic,1:Nypic,1:Nzpic,is)./npic;
h5create(opath,['/Step#0/Block/Pyy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pyy_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

v0 = Jzpic(1:Nxpic,1:Nypic,1:Nzpic,is)./npic;
h5create(opath,['/Step#0/Block/Pzz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pzz_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));


end
h5writeatt(opath,'/Step#0','nspec',int32(ns));

disp('Size in Code Units')
Lx_code = Lx/di0
Ly_code = Ly/di0
one_sec = wpi0
