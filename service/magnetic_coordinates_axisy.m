function [Apar,Aperp1,Aperp2] = magnetic_coordinates_axisy(Ax,Ay,Az,Bx,By,Bz)
%MAGNETIC_COORDINATES puts vectors in magnetic coordinates 
%assumes asix to be y

B=sqrt(Bx.^2+By.^2+Bz.^2);
B2D=sqrt(Bx.^2+Bz.^2);

if(B>0)
if(B2D>0)
perp2x=-By.*Bx./(B.*B2D);
perp2z=-Bz.*By./(B.*B2D);
perp2y=B2D./B;


Apar=(Ax.*Bx+Ay.*By+Az.*Bz)./B;

% Perp 1 is in the plane xz: ny x B
Aperp1=(Bz.*Ax-Bx.*Az)./B2D;

% Perp 2 is the direction B x
Aperp2=perp2x.*Ax+perp2y.*Ay+perp2z.*Az;

else

Apar=(Ax.*Bx+Ay.*By+Az.*Bz)./B;

% Perp 1 x
Aperp1=Ax;

% Perp 2 is z
Aperp2=Az;
end

else
    
% Par is y    
Apar= Ay;

% Perp 1 is x
Aperp1=Ax;

% Perp 2 is z
Aperp2=Az;

end

