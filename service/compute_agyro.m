function [Agyro,Agyro_aunai,Nongyro_swisdak]=compute_Agyro_par(Bx, By, Bz, ...
 PeXX, PeXY, PeXZ, PeYY, PeYZ, PeZZ ) 

disp('Computing Agyro')

tiny=1e-10;tiny=0;

[nx ny nz]= size(PeXX);

cellSpace = [nx, ny, nz];
numCells = prod(cellSpace);

    

for idx =  1:numCells
    [i,iy, k] = ind2sub(cellSpace, idx);
    if(iy==1 & i==1) 
        disp(['Calculating k = ' num2str(k) '  of' num2str(nz)])
    end    

p(1,1)=(PeXX(i,iy,k))+tiny;
p(1,2)=(PeXY(i,iy,k))+tiny;
p(1,3)=(PeXZ(i,iy,k))+tiny;
p(2,2)=(PeYY(i,iy,k))+tiny;
p(2,3)=(PeYZ(i,iy,k))+tiny;
p(3,3)=(PeZZ(i,iy,k))+tiny;
p(2,1)=p(1,2);
p(3,1)=p(1,3);
p(3,2)=p(2,3);

if trace(p)==0
    Agyro(i,iy,k)= 0;
    Agyro_aunai(i,iy,k)= 0;
    Nongyro_swisdak(i,iy,k)=0;
else
    
b(1)=(Bx(i,iy,k));
b(2)=(By(i,iy,k));
b(3)=(Bz(i,iy,k));

b=reshape(b,3,1);
b=b./sqrt(sum(b.^2));

%%%%%%%%%%
% Scudder
%%%%%%%%%%

for l=1:3
N1(l,:)=cross(b,p(l,:));
end

for l=1:3
N(:,l)=cross(b,N1(:,l));
end

lamb(1:3) = 0;
if sum(isnan(N) == 1,'all') == 0
lamb=sort(eig(N));
lambda1(i,iy,k)=lamb(1);
lambda2(i,iy,k)=lamb(2);
lambda3(i,iy,k)=lamb(3);
end


Agyro(i,iy,k)= 2*(lamb(3)-lamb(2))/(lamb(3)+lamb(2));


%%%%%%%%%%%
% Aunai
%%%%%%%%%%%

Tr=(p(1,1)+p(2,2)+p(3,3));
Ppar=b'*p*b;
Pper=(Tr-Ppar)/2;
G=eye(3,3)*Pper+(Ppar-Pper)*kron(b,b');
N=p-G;
Agyro_aunai(i,iy,k)=sqrt(sum(N(:).^2))./Tr;

%%%%%%%%%%%
% Swisdak
%%%%%%%%%%%

I2=p(1,1)*p(2,2)+p(1,1)*p(3,3)+p(2,2)*p(3,3);
I2=I2-(p(1,2).^2+p(1,3).^2+p(2,3).^2);
Q=1-4*I2./((Tr-Ppar).*(Tr+3*Ppar));
Nongyro_swisdak(i,iy,k)=sqrt(Q);
% The following formula form Swisdak paper is actually wrong
%Nongyro_aunai(i,k)=sqrt(8*(p(1,2).^2+p(1,3).^2+p(2,3).^2))./(Ppar+2*Pper);

end
end


