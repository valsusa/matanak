
dx=Lx/Nxpic;
dy=Ly/Nypic;
dz=Lz/Nzpic;

xpic=linspace(0,Lx,Nxpic);
ypic=linspace(0,Ly,Nypic);
if(Nzpic==2)
    zpic = linspace(Lz/2,Lz/2,Nzpic);
else
    zpic=linspace(0,Lz,Nzpic);
end
[Xpic,Ypic,Zpic]=ndgrid(xpic,ypic,zpic);

Tparpic=interpn(x,y,z,Tpar,Xpic,Ypic,Zpic);
Tperpic=interpn(x,y,z,Tper,Xpic,Ypic,Zpic);

npic=interpn(x,y,z,n,Xpic,Ypic,Zpic);


Bxpic=interpn(x,y,z,Bx,Xpic,Ypic,Zpic);
Bypic=interpn(x,y,z,By,Xpic,Ypic,Zpic);
Bzpic=interpn(x,y,z,Bz,Xpic,Ypic,Zpic);
Bpic = sqrt(Bxpic.^2+Bypic.^2+Bzpic.^2);
nx = abs(Bxpic./Bpic);
ny = abs(Bypic./Bpic);
nz = abs(Bzpic./Bpic);

Txpic = Tparpic .* nx + sqrt(1 - nx.^2) .* Tperpic ;
Typic = Tparpic .* ny + sqrt(1 - ny.^2) .* Tperpic ;
Tzpic = Tparpic .* nz + sqrt(1 - nz.^2) .* Tperpic ;

Expic=interpn(x,y,z,Ex,Xpic,Ypic,Zpic);
Eypic=interpn(x,y,z,Ey,Xpic,Ypic,Zpic);
Ezpic=interpn(x,y,z,Ez,Xpic,Ypic,Zpic);


Jxpic(1:Nxpic,1:Nypic,1:Nzpic,1:ns)=0.0;
Jypic(1:Nxpic,1:Nypic,1:Nzpic,1:ns)=0.0;
Jzpic(1:Nxpic,1:Nypic,1:Nzpic,1:ns)=0.0;

Jxpic(1:Nxpic,1:Nypic,1:Nzpic,1)=interpn(x,y,z,Vex,Xpic,Ypic,Zpic);
Jypic(1:Nxpic,1:Nypic,1:Nzpic,1)=interpn(x,y,z,Vey,Xpic,Ypic,Zpic);
Jzpic(1:Nxpic,1:Nypic,1:Nzpic,1)=interpn(x,y,z,Vez,Xpic,Ypic,Zpic);

Jxpic(1:Nxpic,1:Nypic,1:Nzpic,2)=interpn(x,y,z,Vix,Xpic,Ypic,Zpic);
Jypic(1:Nxpic,1:Nypic,1:Nzpic,2)=interpn(x,y,z,Viy,Xpic,Ypic,Zpic);
Jzpic(1:Nxpic,1:Nypic,1:Nzpic,2)=interpn(x,y,z,Viz,Xpic,Ypic,Zpic);



invertYZ = false;
if(invertYZ) 
    %exchange y with z and z with -y
    Tpic=permute(Tpic,[1 3 2]);
    npic=permute(npic,[1 3 2]);
    Bxpic=permute(Bxpic,[1 3 2]);
    tmp=permute(Bzpic,[1 3 2]);
    Bzpic=-permute(Bypic,[1 3 2]);
    Bypic=tmp;
    Expic=permute(Expic,[1 3 2]);
    tmp=permute(Ezpic,[1 3 2]);
    Ezpic=-permute(Eypic,[1 3 2]);
    Eypic=tmp;
    Jxpic=permute(Jxpic,[1 3 2 4]);
    tmp=permute(Jzpic,[1 3 2 4]);
    Jzpic=-permute(Jypic,[1 3 2 4]);
    Jypic= tmp;
    tmp = Nzpic;
    Nzpic = Nypic;
    Nypic = tmp;
end    


Bxpic_ext=zeros(Nxpic, Nypic, Nzpic);
Bypic_ext=zeros(Nxpic, Nypic, Nzpic);
Bzpic_ext=zeros(Nxpic, Nypic, Nzpic);

externalon = false;

if(externalon)

    Bpic = sqrt(Bxpic.^2 +  Bypic.^2 +Bzpic.^2);

    ii=Bypic>0. ; % 1e-2;  %.015;

    Bxpic_ext(ii) = Bxpic(ii);
    Bxpic(ii) = 0;


    Bypic_ext(ii) = Bypic(ii);
    Bypic(ii) = 0;


    Bzpic_ext(ii) = Bzpic(ii);
    Bzpic(ii) = 0;

end



Lambdapic=zeros(size(Xpic));
Lambdapic_border=zeros(size(Xpic));

rpsm= rp %*2/3;%-0*2*dx;
if(Nzpic>2)
    Lambdapic(sqrt((Xpic-xc).^2 + (Ypic-yc).^2 + (Zpic-zc).^2)<rpsm)=1;
else
    Lambdapic(sqrt((Xpic-xc).^2 + (Ypic-yc).^2 )<rpsm)=1;
end

around = true
if around
Lambdapic_border(1:3,:,:) = 1; %left
%Lambdapic(:,1:3,:) = 1; %bottom
%Lambdapic(:,end-3:end,:) = 1; %top
%if(Nzpic>2)
%    Lambdapic(:,:,1:3) = 1; %end left in z
%    Lambdapic(:,:,end-3:end) = 1; %end right in z
%end
end

Lambdapic = smooth3Donesided(Lambdapic,100,true);
Lambdapic = smooth3Donesided(Lambdapic,10,false);
Lambdapic = Lambdapic / max(Lambdapic(:));
%Lambdapic_border = smooth3Dnew(Lambdapic_border,3);
%Lambdapic = Lambdapic +Lambdapic_border;

size(Lambdapic)

zerocenter = true
tresh = .1
gate = 1e-5
if (zerocenter)
Bxpic(Lambdapic>tresh)=0;
Bypic(Lambdapic>tresh)=0;
Bzpic(Lambdapic>tresh)=0;
Expic(Lambdapic>gate)=0;
Eypic(Lambdapic>gate)=0;
Ezpic(Lambdapic>gate)=0;
%npic(Lambdapic>1e-3)=0;
for is = 1, ns
    tmp = Jxpic(:,:,:,is);
    tmp(Lambdapic>tresh) = 0;
    Jxpic(:,:,:,is) = tmp;
    tmp = Jypic(:,:,:,is);
    tmp(Lambdapic>tresh) = 0;
    Jypic(:,:,:,is) = tmp;
    tmp = Jzpic(:,:,:,is);
    tmp(Lambdapic>tresh) = 0;
    Jzpic(:,:,:,is) = tmp;
end
end


h5create(opath,'/Step#0/Block/Bx/0',[Nxpic, Nypic, Nzpic]);
h5create(opath,'/Step#0/Block/By/0',[Nxpic, Nypic, Nzpic]);
h5create(opath,'/Step#0/Block/Bz/0',[Nxpic, Nypic, Nzpic]);

h5create(opath_rs,'/Step#0/Block/Bx_ext/0',[Nxpic, Nypic, Nzpic]);

h5create(opath_rs,'/Step#0/Block/By_ext/0',[Nxpic, Nypic, Nzpic]);

h5create(opath_rs,'/Step#0/Block/Bz_ext/0',[Nxpic, Nypic, Nzpic]);

swtich_ext_int =false
if(swtich_ext_int)
    h5write(opath_rs,'/Step#0/Block/Bx_ext/0',Bxpic)
    h5write(opath_rs,'/Step#0/Block/By_ext/0',Bypic)
    h5write(opath_rs,'/Step#0/Block/Bz_ext/0',Bzpic)
    h5write(opath,'/Step#0/Block/Bx/0',zeros(Nxpic, Nypic, Nzpic))
    h5write(opath,'/Step#0/Block/By/0',zeros(Nxpic, Nypic, Nzpic))
    h5write(opath,'/Step#0/Block/Bz/0',zeros(Nxpic, Nypic, Nzpic))
else
    h5write(opath,'/Step#0/Block/Bx/0',Bxpic)
    h5write(opath,'/Step#0/Block/By/0',Bypic)
    h5write(opath,'/Step#0/Block/Bz/0',Bzpic)
    h5write(opath_rs,'/Step#0/Block/Bx_ext/0',zeros(Nxpic, Nypic, Nzpic))
    h5write(opath_rs,'/Step#0/Block/By_ext/0',zeros(Nxpic, Nypic, Nzpic))
    h5write(opath_rs,'/Step#0/Block/Bz_ext/0',zeros(Nxpic, Nypic, Nzpic))
end

%set_E_int = false %test
set_E_int = true %gipsy702b

if(set_E_int)
h5create(opath,'/Step#0/Block/Ex/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ex/0',Expic)

h5create(opath,'/Step#0/Block/Ey/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ey/0',Eypic)

h5create(opath,'/Step#0/Block/Ez/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ez/0',Ezpic)

h5create(opath_rs,'/Step#0/Block/Ex_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ex_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath_rs,'/Step#0/Block/Ey_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ey_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath_rs,'/Step#0/Block/Ez_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ez_ext/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath_rs,'/Step#0/Block/Ex_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ex_rs/0',Expic)

h5create(opath_rs,'/Step#0/Block/Ey_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ey_rs/0',Eypic)

h5create(opath_rs,'/Step#0/Block/Ez_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ez_rs/0',Ezpic)

else
    
h5create(opath_rs,'/Step#0/Block/Ex_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ex_ext/0',Expic)

h5create(opath_rs,'/Step#0/Block/Ey_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ey_ext/0',Eypic)

h5create(opath_rs,'/Step#0/Block/Ez_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ez_ext/0',Ezpic)

h5create(opath,'/Step#0/Block/Ex/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ex/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Ey/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ey/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Ez/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ez/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath_rs,'/Step#0/Block/Ex_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ex_rs/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath_rs,'/Step#0/Block/Ey_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ey_rs/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath_rs,'/Step#0/Block/Ez_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Ez_rs/0',zeros(Nxpic, Nypic, Nzpic))

end 
    

h5create(opath,['/Step#0/Block/rho_avg/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/rho_avg/0'],zeros(Nxpic, Nypic, Nzpic))



h5create(opath_rs,'/Step#0/Block/Lambda/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Lambda/0',Lambdapic)

h5create(opath_rs,'/Step#0/Block/Bx_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Bx_rs/0',Bxpic)

h5create(opath_rs,'/Step#0/Block/By_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/By_rs/0',Bypic)

h5create(opath_rs,'/Step#0/Block/Bz_rs/0',[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,'/Step#0/Block/Bz_rs/0',Bzpic)



is=2 % ste frame for ions, species 2 in matlab
h5create(opath,['/Step#0/Block/Vfx/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Vfx/0'],Jxpic(1:Nxpic,1:Nypic,1:Nzpic,is))

h5create(opath,['/Step#0/Block/Vfy/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Vfy/0'],Jypic(1:Nxpic,1:Nypic,1:Nzpic,is))

h5create(opath,['/Step#0/Block/Vfz/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Vfz/0'],Jzpic(1:Nxpic,1:Nypic,1:Nzpic,is))


for is=1:ns
h5create(opath,['/Step#0/Block/Jx_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jx_' num2str(is-1) '/0'],Jxpic(1:Nxpic,1:Nypic,1:Nzpic,is).*npic.*concentration(is))

h5create(opath,['/Step#0/Block/Jy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jy_' num2str(is-1) '/0'],Jypic(1:Nxpic,1:Nypic,1:Nzpic,is).*npic.*concentration(is))

h5create(opath,['/Step#0/Block/Jz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jz_' num2str(is-1) '/0'],Jzpic(1:Nxpic,1:Nypic,1:Nzpic,is).*npic.*concentration(is))


h5create(opath,['/Step#0/Block/rho_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/rho_' num2str(is-1) '/0'],npic*concentration(is))

h5create(opath_rs,['/Step#0/Block/Jx_rs_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,['/Step#0/Block/Jx_rs_' num2str(is-1) '/0'],Jxpic(1:Nxpic,1:Nypic,1:Nzpic,is).*npic.*concentration(is))

h5create(opath_rs,['/Step#0/Block/Jy_rs_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,['/Step#0/Block/Jy_rs_' num2str(is-1) '/0'],Jypic(1:Nxpic,1:Nypic,1:Nzpic,is).*npic.*concentration(is))

h5create(opath_rs,['/Step#0/Block/Jz_rs_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,['/Step#0/Block/Jz_rs_' num2str(is-1) '/0'],Jzpic(1:Nxpic,1:Nypic,1:Nzpic,is).*npic.*concentration(is))


h5create(opath_rs,['/Step#0/Block/rho_rs_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,['/Step#0/Block/rho_rs_' num2str(is-1) '/0'],npic*concentration(is))

vth2= abs(qom(is)) * Tratio(is) * Txpic;
vth_left = sqrt(vth2(1,1,1))
v0= Jxpic(1:Nxpic,1:Nypic,1:Nzpic,is); max(v0(:))
h5create(opath,['/Step#0/Block/Pxx_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxx_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

h5create(opath_rs,['/Step#0/Block/Pxx_rs_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,['/Step#0/Block/Pxx_rs_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

TEST =mean((v0.^2 + vth2).* npic.*concentration(is),'all')

vth2= abs(qom(is)) * Tratio(is) * Typic;
v0 = Jypic(1:Nxpic,1:Nypic,1:Nzpic,is);
h5create(opath,['/Step#0/Block/Pyy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
disp(['Species ' num2str(is)])
v0_mean=mean(abs(v0(:)))
vth_mean=mean(sqrt(vth2(:)))
h5write(opath,['/Step#0/Block/Pyy_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

h5create(opath_rs,['/Step#0/Block/Pyy_rs_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,['/Step#0/Block/Pyy_rs_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

vth2= abs(qom(is)) * Tratio(is) * Tzpic;
v0 = Jzpic(1:Nxpic,1:Nypic,1:Nzpic,is);
h5create(opath,['/Step#0/Block/Pzz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pzz_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

h5create(opath_rs,['/Step#0/Block/Pzz_rs_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath_rs,['/Step#0/Block/Pzz_rs_' num2str(is-1) '/0'], ...
    (v0.^2 + vth2).* npic.*concentration(is));

h5create(opath,['/Step#0/Block/Pxy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxy_' num2str(is-1) '/0'], ...
    zeros(Nxpic, Nypic, Nzpic));

h5create(opath,['/Step#0/Block/Pxz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxz_' num2str(is-1) '/0'], ...
    zeros(Nxpic, Nypic, Nzpic));

h5create(opath,['/Step#0/Block/Pyz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pyz_' num2str(is-1) '/0'], ...
    zeros(Nxpic, Nypic, Nzpic));

end
h5writeatt(opath,'/Step#0','nspec',int32(ns));
h5writeatt(opath_rs,'/Step#0','nspec',int32(ns));
