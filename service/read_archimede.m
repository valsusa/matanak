close all
clear all


density_adjust = false;

% Refernce DEnsity in m^3
 n_ref = 1e6;

% Reference Temperature in eV
 kT_ref = 8;


% Temperature Ratio
 TioTe = 1;

%B in  Tesla
 B_ref = 5.8e-9;
 
 mass_ratio=128;
 
 mp=1.6726e-27;
 me = mp / mass_ratio;
 
 physical_electrons = false;
 
[code_n, code_J, code_V, code_T, code_E, code_B, di0, wpi0] =   ...
       code_units_helio(mass_ratio, physical_electrons, n_ref, kT_ref, TioTe, B_ref);


% Physics Constants
 mu0=4*pi*1e-7;
 eps0=8.8542*1.e-12;
 cphys=1/sqrt(mu0*eps0);
 k=1.3807e-23;
 e= 1.6022e-19;

kT_ref_si=kT_ref*e;

Tratio=[1,TioTe];
qom=[-mass_ratio,1];
concentration=[-1.0,1.0];

ns=2;


Nxpic=200+1;Nypic=200+1; Nzpic=1+1; 
Nxpic=500+1;Nypic=500+1; Nzpic=1+1; 



rsol = 696340e3;
au = 1.495978707e11;

reath = au/di0;

Lxpic=4*au/di0
Lypic=4*au/di0
Lzpic=4*au/di0
dx=Lxpic/Nxpic;
dy=Lypic/Nypic;
dz=Lzpic/Nzpic;

xpic=linspace(-Lxpic/2,Lxpic/2,Nxpic);




if(Nzpic<=2) 
    zpic=[0 0];%[(zmin+zmax)/2 (zmin+zmax)/2] ;
    Lzpic = 2;
    dz = 2;
else
    zpic=linspace(-Lzpic/2,Lzpic/2,Nzpic);
end
if(Nypic<=2) 
    ypic=[0 0];%[(ymin+ymax)/2 (ymin+ymax)/2] ;
else
    ypic=linspace(-Lypic/2,Lypic/2,Nypic);
end
[Xpic,Ypic,Zpic]=ndgrid(xpic,ypic,zpic);

[X1pic,Y1pic,Z1pic]=meshgrid(xpic,ypic,zpic);

if(Nzpic>2)
    r = sqrt(Xpic.^2+Ypic.^2+Zpic.^2);
    theta = acos(Zpic./r);
else
    r = sqrt(Xpic.^2+Ypic.^2);
    theta = pi/2*ones(Nxpic,Nypic,Nzpic);
end    

phi = atan2(Ypic,Xpic);



%au = au / factor

Omega=14.713/24/60/60/360*2*pi   /wpi0;
usw=800e3/cphys;

rs=10*rsol/di0;
ii=r<rs;r(ii)=rs;
rlambda = rs*2; %30*rsol/di0;

%renorm
au0=au/di0;
Br=sin(theta).*(rs./au0).^2;
Bphi=-sin(theta).*(rs./au0).^2.*(au0-rs).*Omega.*sin(theta)./usw;
Bnorm=sqrt(Br.^2+Bphi.^2);
%Bnorm =1;

Bs=B_ref./Bnorm; % B on the source surface
Br=Bs.*sin(theta)./code_B.*(rs./r).^2;
Bphi=-Bs.*sin(theta)./code_B.*(rs./r).^2.*(r-rs).*Omega.*sin(theta)./usw;
ii=r<rs;
Br(ii)=0;
Bphi(ii)=0;



n1au=6.93e6; %particles per m^3
n=n1au./(r*di0/au).^2;

kTau = 8; %in eV
kTau = kTau*e; %in joule
kT = kTau./(r*di0/au).^(3/4);
% CGL model 
kTpar = kTau .*kT./kT;
kTperp = kTau./(r*di0/(au/3)).^2;
%emipricla model
kTpar = kT;
kTperp = kT;

%kT=kTau;

% figure(99)
% semilogy(kT(:,round(Nypic/2),1)); colorbar; hold on
% semilogy(kTpar(:,round(Nypic/2),1)); colorbar
% semilogy(kTperp(:,round(Nypic/2),1)); colorbar
% 
% return

%beta1AU = n1au * kTau / (B_ref^2/ 2/mu0)

Bx = sin(theta).*cos(phi).*Br - sin(phi).*Bphi;
By = sin(theta).*sin(phi).*Br + cos(phi).*Bphi;
Bz = cos(theta).*Br;

B2=(Bx.^2+By.^2+Bz.^2);
B=sqrt(B2);

Vr=usw;
Vphi=-(r-rs).*Omega.*sin(theta);

Vx= sin(theta).*cos(phi).*Vr - sin(phi).*Vphi;
Vy= sin(theta).*sin(phi).*Vr + cos(phi).*Vphi;
Vz = cos(theta).*Vr;

[Ex, Ey, Ez ] = cross_prod(Vx, Vy, Vz, Bx, By, Bz);

[Jx,Jy,Jz]=compute_curl(X1pic,Y1pic,Z1pic,Bx/4/pi,By/4/pi,Bz/4/pi,0,0);


nx = abs(Bx./B);
ny = abs(By./B);
nz = abs(Bz./B);

%formule errate!!!!
kTx = kTpar .* nx + sqrt(1 - nx.^2) .* kTperp ;
kTy = kTpar .* ny + sqrt(1 - ny.^2) .* kTperp ;
kTz = kTpar .* nz + sqrt(1 - nz.^2) .* kTperp ;

kTx = kT;
kTy = kT;
kTz = kT;


filename='Barchimede800.vtk';
savevtkvector_bin(Bx, By, Bz, filename,'B',dx,dy,dz,0,0,0)
filename='Varchimede800.vtk';
savevtkvector_bin(Vx, Vy, Vz, filename,'V',dx,dy,dz,0,0,0)
filename='Earchimede800.vtk';
savevtkvector_bin(Ex, Ey, Ez, filename,'E',dx,dy,dz,0,0,0)

filename='kTarchimede800.vtk';
savevtk_bin(kT, filename,'kT',dx,dy,dz,0,0,0)
filename='narchimede800.vtk';
savevtk_bin(n, filename,'n',dx,dy,dz,0,0,0)

npic=n/4/pi/1e6;


!rm Archimede-Fields_000000.h5
opath='Archimede-Fields_000000.h5'

Lambda = zeros(Nxpic, Nypic, Nzpic);
r2= rlambda *1.5;
r1= rlambda /2; 
%Lambda = (1- tanh((r-r2)./(rlambda/3)))/2;
%Lambda(Lambda<.001)=0;
ii=r<rlambda; % it was 4 solar radii
Lambda(ii)=1;
%ii=r>r2; % it was 4 solar radii
%Lambda(ii)=0;

ii= r>r1;
Lambda(ii) = bspline2((r(ii)-r1)/(r2 -r1));
ii=r<r1; % it was 4 solar radii
Lambda(ii)=1;

Lambda(round(Nxpic/2),round(Nypic/2),:)=1;
%Lambda=smooth3Dnew(Lambda,6); % it was 3 not 6
Lambda=smooth3Donesided(Lambda,10,false);


h5create(opath,'/Step#0/Block/Lambda/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Lambda/0',Lambda)

h5create(opath,'/Step#0/Block/Bx_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bx_ext/0',Bx)

h5create(opath,'/Step#0/Block/By_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/By_ext/0',By)

h5create(opath,'/Step#0/Block/Bz_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bz_ext/0',Bz)

h5create(opath,'/Step#0/Block/Bx/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bx/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/By/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/By/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Bz/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Bz/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Ex/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ex/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Ey/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ey/0',zeros(Nxpic, Nypic, Nzpic))

h5create(opath,'/Step#0/Block/Ez/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ez/0',zeros(Nxpic, Nypic, Nzpic))


h5create(opath,'/Step#0/Block/Ex_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ex_ext/0',Ex)

h5create(opath,'/Step#0/Block/Ey_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ey_ext/0',Ey)

h5create(opath,'/Step#0/Block/Ez_ext/0',[Nxpic, Nypic, Nzpic]);
h5write(opath,'/Step#0/Block/Ez_ext/0',Ez)

h5create(opath,['/Step#0/Block/rho_avg/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/rho_avg/0'],zeros(Nxpic, Nypic, Nzpic))


for is=1:ns
    if(is==1)
h5create(opath,['/Step#0/Block/Jx_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jx_' num2str(is-1) '/0'],Vx.*npic*concentration(is))%-Jx)

h5create(opath,['/Step#0/Block/Jy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jy_' num2str(is-1) '/0'],Vy.*npic*concentration(is))%-Jy)

h5create(opath,['/Step#0/Block/Jz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jz_' num2str(is-1) '/0'],Vz.*npic*concentration(is)-Jz)
    else
        h5create(opath,['/Step#0/Block/Jx_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jx_' num2str(is-1) '/0'],Vx.*npic*concentration(is))

h5create(opath,['/Step#0/Block/Jy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jy_' num2str(is-1) '/0'],Vy.*npic*concentration(is))

h5create(opath,['/Step#0/Block/Jz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Jz_' num2str(is-1) '/0'],Vz.*npic*concentration(is))
    end

h5create(opath,['/Step#0/Block/rho_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/rho_' num2str(is-1) '/0'], npic*concentration(is))

vth2= abs(qom(is)) * Tratio(is) * kTx/mp/cphys^2; 

h5create(opath,['/Step#0/Block/Pxx_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxx_' num2str(is-1) '/0'], ...
    (Vx.^2 + vth2).* npic.*concentration(is));

vth2= abs(qom(is)) * Tratio(is) * kTy/mp/cphys^2; 
h5create(opath,['/Step#0/Block/Pyy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pyy_' num2str(is-1) '/0'], ...
    (Vy.^2 + vth2).* npic.*concentration(is));

vth2= abs(qom(is)) * Tratio(is) * kTz/mp/cphys^2; 
h5create(opath,['/Step#0/Block/Pzz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pzz_' num2str(is-1) '/0'], ...
    (Vz.^2 + vth2).* npic.*concentration(is));

h5create(opath,['/Step#0/Block/Pxy_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxy_' num2str(is-1) '/0'], ...
    zeros(Nxpic, Nypic, Nzpic));

h5create(opath,['/Step#0/Block/Pxz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pxz_' num2str(is-1) '/0'], ...
    zeros(Nxpic, Nypic, Nzpic));

h5create(opath,['/Step#0/Block/Pyz_' num2str(is-1) '/0'],[Nxpic, Nypic, Nzpic]);
h5write(opath,['/Step#0/Block/Pyz_' num2str(is-1) '/0'], ...
    zeros(Nxpic, Nypic, Nzpic));
figure(100+is)
imagesc(squeeze(sqrt(vth2(:,:,round(Nzpic/2)))));colorbar
%B2=B_ref^2/code_B^2
%npic1 = npic
%npic1(:) = n1au/1e6;
beta= 8*pi*vth2.* npic.*concentration(is)/qom(is)./B2;
%beta= 8*pi*kT/code_T.* npic./B2;


figure(1)
semilogy(beta(:,round(Nypic/2),1))
colorbar
figure(2)

teta=0:pi/20:2*pi;
startx= rs*sin(teta);
starty = rs*cos(teta);
streamline(Xpic(:,:,1)',Ypic(:,:,1)',Bx(:,:,1)',By(:,:,1)',startx,starty)

figure(3)
imagesc(squeeze(Lambda(:,:,round(Nzpic/2))));colorbar

end


h5writeatt(opath,'/Step#0','nspec',int32(ns));

days_arrival=au/(usw*cphys)/60/60/24
days_rotation=2*pi/Omega/60/60/24

function [code_n, code_J, code_V, code_T, code_E, code_B, dp, wpp] =   ....
     code_units_helio(mrcode,phys_el,n_ref, kT_ref, TioTe, B_ref)
 
% Enter 
% kT_ref in eV
% n_ref in cc


% Physics Constants
 mu0=4*pi*1e-7;
 eps0=8.8542*1.e-12;
 cphys=1/sqrt(mu0*eps0);
 k=1.3807e-23;
 e= 1.6022e-19;
 

 
%n_ref=1; %for mercury
 
 if(phys_el==true)
%physical electorns
    me=9.1094e-31;
    mp = me * mrcode;
 else   
% physical ions 
    mp=1.6726e-27;
    me = mp / mrcode;
 end
 
 

% Convert T to SI
 Te=kT_ref * 1.1604e4; % puts T in K
 Tp=Te*TioTe; % puts T in K
 np = n_ref;
 ne=np;

% Plasma scales

%disp('Protons')
 wpp=sqrt(np*e^2/mp/eps0);
dp=cphys/wpp;
 vthp=sqrt(k*Tp/mp);
 wcp=e*B_ref/mp;
 rhop=vthp/wcp;

%disp('Electrons')
 wpe=sqrt(ne*e^2/me/eps0);
 de=cphys/wpe;
 vthe=sqrt(k*Te/me);
 wce=e*B_ref/me;
 rhoe=vthe/wce;
 lde=sqrt(eps0*k*Te/ne/e*e);
 


% Normalisations
%To convert 1 in the code to SI multiply by this
%To convert 1 in SI to code units divide by this
code_E = cphys*mp*wpp/e
code_B = mp*wpp/e
code_J = mp*wpp/mu0/e/dp
code_V = cphys
code_n = n_ref
% Assuming the reference species is ions
code_T = cphys *cphys * mp


return
end
