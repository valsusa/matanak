function [b2] = bspline2(xin)

x = abs(xin);

b2 = zeros(size(x));


ii= x<=1.5;
b2(ii) = 0.5 *(1.5 - x(ii)).^2;

ii= x<=0.5;
b2(ii) = 0.75 - x(ii).^2;



b2 = b2 /.75;