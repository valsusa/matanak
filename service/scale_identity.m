%scale_identity



R_mercury = 2439.7e3; %in meters
R_earth = 6371e3; %in meters


%EARTH

mrcode = 256;
phys_el = false;
[code_n, code_J, code_V, code_T, code_E, code_B, momentum_corrector, dp, de, wpi] =   code_units(mrcode,phys_el);

%Simulation Box in Planet radii

Dim = [16+11, 20, 20] * R_earth;

%Simulation Box in code unis (dp)

Dim_code = [ 108 80 80]*dp;

Earth_smaller=Dim./Dim_code

dt = .5;
ncycle = 20000;
T =dt * ncycle /wpi 
T * Earth_smaller


%EARTH DAYSIDE

mrcode = 256;
phys_el = false;
[code_n, code_J, code_V, code_T, code_E, code_B, momentum_corrector, dp, de, wpi] =   code_units(mrcode,phys_el);

%Simulation Box in Planet radii

Dim = [15-7, 5, 2] * R_earth;

%Simulation Box in code unis (dp)

Dim_code = [ 32 20 8]*dp;

Earth_smaller=Dim./Dim_code

dt = .2;
ncycle = 1000;
T =dt * ncycle /wpi 
T * Earth_smaller


% Mercury Run
mrcode = 128;
phys_el = false;
[code_n, code_J, code_V, code_T, code_E, code_B, momentum_corrector, dp, de, wpi] =   code_units(mrcode,phys_el);


Dim_code = [ 240 300 300]*dp/4; % must divide by 4
Dim = [7.3+3.9 14 14]* R_mercury;
Mercury_smaller=Dim./Dim_code
