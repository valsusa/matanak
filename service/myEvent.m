function [value, isterminal, direction] = myEvent(T, Y)
global Nx
value      = (Y(1) <= 10 | Y(1)>= Nx-10);
isterminal = 1;   % Stop the integration
direction  = 0;
