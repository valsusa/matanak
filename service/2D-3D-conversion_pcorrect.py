################################################################################
################################################################################
####                                                                        ####
####  Create a field file for restart in 3D from the results in 2D in       ####
####  cylindrical coordinates.                                              ####
####                                                                        ####
####  Version for H5hut files                                               ####
####                                                                        ####
####  Diego Gonzalez                                                        ####
####  July 2018                                                             ####
####                                                                        ####
####------------------------------------------------------------------------####
####                                                                        ####
####  WARINING: in the output file only the magnitudes used in the restart  ####
####            are right (charge, current, diagonal elements of pressure   ####
####            electric and magnetic field), the rest contains -1.         ####
####                                                                        ####
####------------------------------------------------------------------------####
####  USAGE:                                                                ####
####    Set the name of the input files and the cycle (2D cylindrical).     ####
####    Set the resolution in 3D cartesian.                                 ####
####    Execute the script                                                  ####
####                                                                        ####
################################################################################
################################################################################
import numpy as np
import h5py
import sys

# Set the basename, range and final filename
def readField(field, file):
	key = 'Step#0/Block/'+ field + '/0'
	data  = file.get(key)[...]
	return data

def computeValuesVector(Bx_3d, By_3d, Bz_3d, Bx, By, Bz, i, j, k, i0, j0, i1, j1, w00, w01, w10, w11, alpha): 
	ValR = w00*Bx[0][j0][i0] + w01*Bx[0][j0][i1] + w10*Bx[0][j1][i0] + w11*Bx[0][j1][i1]
	ValZ = w00*By[0][j0][i0] + w01*By[0][j0][i1] + w10*By[0][j1][i0] + w11*By[0][j1][i1]
	ValT = w00*Bz[0][j0][i0] + w01*Bz[0][j0][i1] + w10*Bz[0][j1][i0] + w11*Bz[0][j1][i1]
	cA = np.cos(alpha)
	sA = np.sin(alpha)
	Bx_3d[k][j][i] = cA*ValR - sA*ValT
	By_3d[k][j][i] = ValZ
	Bz_3d[k][j][i] = sA*ValR + cA*ValT
	
def computeValuesP(Pxx_3d, Pzz_3d, Prr, Ptt, Prt, i, j, k, i0, j0, i1, j1, w00, w01, w10, w11, alpha):
	ValR = w00*Prr[0][j0][i0] + w01*Prr[0][j0][i1] + w10*Prr[0][j1][i0] + w11*Prr[0][j1][i1]
	ValT = w00*Ptt[0][j0][i0] + w01*Ptt[0][j0][i1] + w10*Ptt[0][j1][i0] + w11*Ptt[0][j1][i1]
	ValRT = w00*Prt[0][j0][i0] + w01*Prt[0][j0][i1] + w10*Prt[0][j1][i0] + w11*Prt[0][j1][i1]
	cA = np.cos(alpha)
	sA = np.sin(alpha)
	Pxx_3d[k][j][i] = cA*cA*ValR + sA*sA*ValT - 2*sA*cA*ValRT
	Pzz_3d[k][j][i] = sA*sA*ValR + cA*cA*ValT + 2*sA*cA*ValRT
def computeValuesScalar(A_3d, A, i, j, k, i0, j0, i1, j1, w00, w01, w10, w11): 
	val = w00*A[0][j0][i0] + w01*A[0][j0][i1] + w10*A[0][j1][i0] + w11*A[0][j1][i1]
	A_3d[k][j][i] = val

################################################################################
##    	MAIN
################################################################################

#---------- INPUT DATA --------------------------------------------------------
# Name of the files (without -Fields_######.h5)
directory = "./"
name  = "Tokamak"
cycle = 00000

# Number of cells of the output file ( <= dimensions of the input)
nxc_3D      = 100
nyc_3D      = 100
nzc_3D      = 100
listKeys = ["rho", "J", "P", "E", "B", "E_ext", "B_ext", "Lambda"]
#------------------------------------------------------------------------------

nxn_3D = nxc_3D + 1
nyn_3D = nyc_3D + 1
nzn_3D = nzc_3D + 1

# Reading original data
listData = []
listOutput = []
filename = directory + name + "-Fields_%06d.h5" % (cycle)
new_file = directory + name + "_3D-Fields_%06d.h5" % (cycle)
fileIn   = h5py.File(filename,'r')
ns = np.int(fileIn['Step#0'].attrs['nspec'])
print(np.int(ns))
# Read the original data in 2D
for k in listKeys:
  if (k == "E" or k == "B" ):
    d = readField(k+"x",fileIn) 
    listData.append(d)
    d = readField(k+"y",fileIn) 
    listData.append(d)
    d = readField(k+"z",fileIn) 
    listData.append(d)
    
    d = np.empty((nzn_3D,nyn_3D,nxn_3D))
    listOutput.append(d)
    d = np.empty((nzn_3D,nyn_3D,nxn_3D))
    listOutput.append(d)
    d = np.empty((nzn_3D,nyn_3D,nxn_3D))
    listOutput.append(d)
    
  elif (k == "E_ext" or k == "B_ext"):
    d = readField(k[0]+"x_ext",fileIn)
    listData.append(d)
    d = readField(k[0]+"y_ext",fileIn)
    listData.append(d)
    d = readField(k[0]+"z_ext",fileIn)
    listData.append(d)
  
    d = np.empty((nzn_3D,nyn_3D,nxn_3D))
    listOutput.append(d)
    d = np.empty((nzn_3D,nyn_3D,nxn_3D))
    listOutput.append(d)
    d = np.empty((nzn_3D,nyn_3D,nxn_3D))
    listOutput.append(d)

  elif (k == "J"):
    for s in range(ns):
      print("J_"+str(s))
      d = readField(k[0]+"x_"+str(s),fileIn)
      listData.append(d)
      d = readField(k[0]+"y_"+str(s),fileIn)
      listData.append(d)
      d = readField(k[0]+"z_"+str(s),fileIn)
      listData.append(d)

      d = np.empty((nzn_3D,nyn_3D,nxn_3D))
      listOutput.append(d)
      d = np.empty((nzn_3D,nyn_3D,nxn_3D))
      listOutput.append(d)
      d = np.empty((nzn_3D,nyn_3D,nxn_3D))
      listOutput.append(d)

  elif (k == "P"):
    for s in range(ns):
      print("P_"+str(s))
      d = readField(k[0]+"xx_"+str(s),fileIn)
      listData.append(d)
      d = readField(k[0]+"yy_"+str(s),fileIn)
      listData.append(d)
      d = readField(k[0]+"zz_"+str(s),fileIn)
      listData.append(d)
      d = readField(k[0]+"xz_"+str(s),fileIn)
      listData.append(d)
      
      d = np.empty((nzn_3D,nyn_3D,nxn_3D))
      listOutput.append(d)
      d = np.empty((nzn_3D,nyn_3D,nxn_3D))
      listOutput.append(d)
      d = np.empty((nzn_3D,nyn_3D,nxn_3D))
      listOutput.append(d)      
      d = np.empty((nzn_3D,nyn_3D,nxn_3D))
      listOutput.append(d) 
      
  elif (k == "rho"):
    for s in range(ns):
      print("rho_"+str(s))
      d = readField(k + "_" + str(s),fileIn)
      listData.append(d)
      d = np.empty((nzn_3D,nyn_3D,nxn_3D))
      listOutput.append(d)
      
  elif (k == "Lambda"):
        print("Lambda")
        d = readField(k,fileIn)
        listData.append(d)
        d = np.empty((nzn_3D,nyn_3D,nxn_3D))
        listOutput.append(d)


nxn_2D = listData[0].shape[2]
nyn_2D = listData[0].shape[1]
nzn_2D = listData[0].shape[0]
nxc_2D = nxn_2D - 1
nyc_2D = nyn_2D - 1
nzc_2D = nzn_2D - 1

nx = (nxc_2D)*2
ny = nyc_2D
nz = nx
dx = float(nx)/nxc_3D
dy = float(ny)/nyc_3D
dz = float(nz)/nzc_3D
x_center = nxc_3D/2

if (nx < nxc_3D or ny < nyc_3D or nz < nzc_3D):
  print("ERROR: spatial resolution in 3D must be smaller than the one in 2D (%d x %d x %d  <= %d x %d x %d)" % (nxc_3D, nyc_3D, nzc_3D, nx, ny, nz) )
  sys.exit(0)

ntn = nxn_3D*nyn_3D*nzn_3D
nt = 0

for i in range(0,nxn_3D):
  for j in range(0,nyn_3D):
    for k in range(0,nzn_3D):
      x_org = (i - x_center)*dx
      y_org = j*dy
      z_org = (k - x_center)*dz
      r_org = np.sqrt(x_org*x_org + z_org*z_org)
      if (r_org>= nxn_2D):
        r_org = nxn_2D-1e-10
      alpha = np.arctan2(z_org, x_org)
      i0 = int(r_org)
      i1 = i0 + 1 
      j0 = int(y_org)
      j1 = j0 + 1 
      if (r_org < nxn_2D):
        # index is in order j than i
        w00 = (j1 - y_org)*(i1 - r_org)
        w10 = (y_org - j0)*(i1 - r_org)
        w01 = (j1 - y_org)*(r_org - i0)
        w11 = (y_org - j0)*(r_org - i0)
        if (i1 > nxn_2D - 1): i1 = i0
        if (j1 > nyn_2D - 1): j1 = j0
        
        indD = 0
        for key in listKeys:
          if (key == "E" or key=="B" or key=="E_ext" or key=="B_ext"):
            computeValuesVector(listOutput[indD], listOutput[indD+1], listOutput[indD+2], \
                                listData[indD]  , listData[indD+1]  , listData[indD+2], \
                                i, j, k, i0, j0, i1, j1, w00, w01, w10, w11, alpha)
            indD = indD + 3
          elif (key=="J"):
            for s in range(ns):
              computeValuesVector(listOutput[indD], listOutput[indD+1], listOutput[indD+2], \
                                  listData[indD]  , listData[indD+1]  , listData[indD+2], \
                                  i, j, k, i0, j0, i1, j1, w00, w01, w10, w11, alpha)
              indD = indD + 3
          elif (key=="P"):
            for s in range(ns):
                # pressure xx,zz
               computeValuesP(listOutput[indD], listOutput[indD+2],listData[indD], listData[indD+2], listData[indD+3], i, j, k, i0, j0, i1, j1, w00, w01, w10, w11, alpha)
                # pressure yy
               computeValuesScalar(listOutput[indD+1], listData[indD+1], i, j, k, i0, j0, i1, j1, w00, w01, w10, w11)
               indD = indD + 4
          elif (key=="rho"): 
            for s in range(ns):
              computeValuesScalar(listOutput[indD], listData[indD], i, j, k, i0, j0, i1, j1, w00, w01, w10, w11)
              indD = indD + 1
          elif (key=="Lambda"):
                computeValuesScalar(listOutput[indD], listData[indD], i, j, k, i0, j0, i1, j1, w00, w01, w10, w11)
                indD = indD + 1

      nt = nt + 1
      if (np.mod(nt,500) == 0 or nt==ntn):
        prog = float(nt)/(ntn)*100
        msg = "\r   " + filename + " ( %.1f %% )" % (prog)
        sys.stdout.write(msg)


# Output
print("   Writing data ...")
fileOut = h5py.File(new_file,"w");
maingrp = fileOut.create_group('Step#0/Block')

indD = 0
for key in listKeys:
  if (key == "E" or key=="B"):
    grp = maingrp.create_group(key + "x")
    grp.create_dataset('0', data=listOutput[indD])
    grp = maingrp.create_group(key + "y")
    grp.create_dataset('0', data=listOutput[indD+1])
    grp = maingrp.create_group(key + "z")
    grp.create_dataset('0', data=listOutput[indD+2])
    indD = indD + 3
  elif (key=="E_ext" or key=="B_ext"):
    grp = maingrp.create_group(key[0] + "x_ext")
    grp.create_dataset('0', data=listOutput[indD])
    grp = maingrp.create_group(key[0] + "y_ext")
    grp.create_dataset('0', data=listOutput[indD+1])
    grp = maingrp.create_group(key[0] + "z_ext")
    grp.create_dataset('0', data=listOutput[indD+2])
    indD = indD + 3
  elif (key == "J"): 
    for s in range(ns):
      grp = maingrp.create_group(key[0] + "x_" + str(s))
      grp.create_dataset('0', data=listOutput[indD])
      grp = maingrp.create_group(key[0] + "y_" + str(s))
      grp.create_dataset('0', data=listOutput[indD+1])
      grp = maingrp.create_group(key[0] + "z_" + str(s))
      grp.create_dataset('0', data=listOutput[indD+2])
      indD = indD + 3
  elif (key == "P"): 
    for s in range(ns):
      grp = maingrp.create_group(key[0] + "xx_" + str(s))
      grp.create_dataset('0', data=listOutput[indD])
      grp = maingrp.create_group(key[0] + "yy_" + str(s))
      grp.create_dataset('0', data=listOutput[indD+1])
      grp = maingrp.create_group(key[0] + "zz_" + str(s))
      grp.create_dataset('0', data=listOutput[indD+2])
      indD = indD + 4
  elif (key == "rho"):
    for s in range(ns):
      grp = maingrp.create_group(key + "_" + str(s))
      grp.create_dataset('0', data=listOutput[indD])
      indD = indD + 1
  elif (key == "Lambda"):
    grp = maingrp.create_group(key)
    grp.create_dataset('0', data=listOutput[indD])
    indD = indD + 1
grpin  = fileIn['/Step#0']
grpout =fileOut['/Step#0']
grpout.attrs['nspec'] = grpin.attrs['nspec']

#fileOut['Step#0'].attrs['nspec'] = ns
fileIn.close()
fileOut.close()
print("   DONE")
