function [Uth, Ubulk, divQbulk, divQenth, divQhf,  udivP, PgradV, ugradp, pdivv, divUP] = compute_energy_balance( ...
    rho, Jx, Jy, Jz,... 
    Qbulkx, Qbulky, Qbulkz, Qenthx, Qenthy, Qenthz, Qhfx, Qhfy, Qhfz, ...
    Pxx, Pyy, Pzz, Pxy, Pxz, Pyz, x, y, z, dx, dy, dz, qom, radius,cyl)

%divQbulk = compute_div(x,y,z,smooth3(Qbulkx,'box',5),smooth3(Qbulky,'box',5),smooth3(Qbulkz,'box',5));    
divQbulk = compute_div(x,y,z,Qbulkx,Qbulky,Qbulkz, radius, cyl);
divQenth = compute_div(x,y,z,Qenthx,Qenthy,Qenthz, radius, cyl);
divQhf = compute_div(x,y,z,Qhfx,Qhfy,Qhfz, radius, cyl);
Uth = imgaussfilt3((Pxx + Pyy + Pzz)/2,radius);
Ubulk = imgaussfilt3(((Jx./rho).^2 + (Jy./rho).^2 + (Jz./rho).^2) .* rho /2/qom,radius);  
    

Vx=imgaussfilt3(Jx./rho,radius);
Vy=imgaussfilt3(Jy./rho,radius);
Vz=imgaussfilt3(Jz./rho,radius);
psxx=imgaussfilt3(Pxx,radius);
psyy=imgaussfilt3(Pyy,radius);
pszz=imgaussfilt3(Pzz,radius);
psxy=imgaussfilt3(Pxy,radius);
psxz=imgaussfilt3(Pxz,radius);
psyz=imgaussfilt3(Pyz,radius);

tmp = compute_div(x,y,z,Pxx,Pxy,Pxz, radius, cyl);
udivP = tmp.* Vx;
tmp = compute_div(x,y,z,Pxy,Pyy,Pyz, radius, cyl);
udivP = udivP + tmp.* Vy;
tmp = compute_div(x,y,z,Pxz,Pyz,Pzz, radius, cyl);
udivP = udivP + tmp.* Vz;

p=(Pxx+Pyy+Pzz)/3;
pdivv = imgaussfilt3(p,radius).*compute_div(x,y,z,Vx,Vy,Vz, radius, cyl);

[tx, ty, tz] = gradient(imgaussfilt3(permute(p,[2 1 3]),radius), dx, dy, dz);
tx=permute(tx,[2 1 3]);ty=permute(ty,[2 1 3]);tz=permute(tz,[2 1 3]);
ugradp = tx.*Vx + ty.*Vy + tz.*Vz;

[tx, ty, tz] = gradient(permute(Vx,[2 1 3]), dx, dy, dz);
tx=permute(tx,[2 1 3]);ty=permute(ty,[2 1 3]);tz=permute(tz,[2 1 3]);
PgradV = tx.*psxx+ ty.*psxy +tz.* psxz;
[tx, ty, tz] = gradient(permute(Vy,[2 1 3]), dx, dy, dz);
tx=permute(tx,[2 1 3]);ty=permute(ty,[2 1 3]);tz=permute(tz,[2 1 3]);
PgradV = PgradV + tx.*psxy + ty.*psyy +tz.* psyz;
[tx, ty, tz] = gradient(permute(Vz,[2 1 3]), dx, dy, dz);
tx=permute(tx,[2 1 3]);ty=permute(ty,[2 1 3]);tz=permute(tz,[2 1 3]);
PgradV = PgradV + tx.*psxz + ty.*psyz +tz.* pszz;

divUP = compute_div(x,y,z,psxx.*Vx,psxy.*Vy,psxz.*Vz, 0, cyl);
divUP = divUP + compute_div(x,y,z,psxy.*Vx,psyy.*Vy,psyz.*Vz, 0, cyl);
divUP = divUP + compute_div(x,y,z,psxz.*Vx,psyz.*Vy,pszz.*Vz, 0, cyl);
