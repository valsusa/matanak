close all
addpath(genpath('/Users/glapenta/Matlab/matanak'));


agyro_write = false;
agyro= false
radial_v = false
currents= true
external_fields = true


list_sim=["cfr_inj_decay"; "cfr_base" ;"cfr_stj" ;"cfr_ebi_j2"; "cfr_ebi_j4" ;"cfr_nbi"];

list_sim=["N1_194"; "N8_194"; "N8_197"]
list_sim =["2X-8NBI-ebeam-57k"; "2X-8NBI-ebeam-43k";"2X-8NBI-ebeam-37k"]
list_sim =["2X-8NBI-ebeam"; "0X-NBI-ebeam";"8NBI-ebeam"]

list_sim = "FRC-CD"
for it =[43,45,53,62]%[35,37.5,40,43]
for lsim = 1:1

    sim_name=char(list_sim(lsim))
read_case

cycle=it*1000

dir_out = ['/Users/glapenta/Data/compare_nov4/' sim_name '/']
dir_out = ['/Users/glapenta/Data/2X-8NBI-ebeam/' sim_name '/']
dir_out = ['/Users/glapenta/Data/FRC3D-NBI-ebeam/analisi' num2str(it) '/' sim_name '/']
dir_out = [dir 'analisi_matlab/' num2str(cycle) '/']


unix(['mkdir ' dir_out])


ncycle=num2str(cycle,'%06i\n')
qom=-64 

leggo='h5'

import_h5_binvtk

% FRC
Lx=8;Ly=14;Lz=8
xr=[Lx/5 Lx-Lx/5];
yr=[Ly/10 Ly-Ly/10];

dx=Lx/Nx;
dy=Ly/Ny;
dz=Lz/Nz;

[X Y] = meshgrid(0:dx:Lx-dx,0:dy:Ly-dy);
[X3 Y3 Z3] = ndgrid(0:dx:Lx-dx,0:dy:Ly-dy,0:dz:Lz-dz);
[X2 Z2] = meshgrid(0:dx:Lx-dx,0:dz:Lz-dz);
[x,y,z]=meshgrid(0:dx:Lx-dx,0:dy:Ly-dy,0:dz:Lz-dz);


ir=round(xr./dx);ir=min(ir):max(ir);
jr=round(yr./dy);jr=min(jr):max(jr);
iz=round((Nz-1)/2);

color_choice=1
global color_choice symmetric_color labelx labely Ncycle Lx Ly Lz dx dy dz dir_out

color_lines = [0 0 0 0.2];
global color_lines
tiny = 1e-10;
Nsm=0;
cyl=0;
labelx='x/d_i';
labely='y/d_i';
Ncycle =num2str(cycle);Ncycle='';

[iw,jw]=meshgrid(1:Nx,1:Ny);
sx=Nx;sy=Ny;
window = exp(-(iw-Nx/2).^2/sx-(jw-Ny/2).^2/sy);
navg= floor(Nx/8);

radius = 3;

VexbX = (Ey.*Bz - Ez.*By)./(B.^2+tiny);
VexbY = (Ez.*Bx - Ex.*Bz)./(B.^2+tiny);
VexbZ = (Ex.*By - Ey.*Bx)./(B.^2+tiny);

Va=B./sqrt(4*pi.*rhoi);


Vex= Jex./(rhoe-tiny) ;
Vey= Jey./(rhoe-tiny) ;
Vez= Jez./(rhoe-tiny) ;

Je=sqrt(Jex.^2+Jey.^2+Jez.^2);

Vpar= (Vex.*Bx+Vey.*By+Vez.*Bz)./(B +tiny) ;

Veperpx= Vex - Vpar .*Bx./(B +tiny);
Veperpy= Vey - Vpar .*By./(B +tiny);
Veperpz= Vez - Vpar .*Bz./(B +tiny);

Epar = (Ex.*Bx+ Ey.*By+ Ez.*Bz)./(B +tiny);

[Sx, Sy, Sz] = cross_prod(Ex, Ey, Ez, Bx, By, Bz);
Sx = Sx /4/pi; Sy = Sy /4/pi; Sz = Sz /4/pi;
divS = compute_div(x,y,z,Sx,Sy,Sz,radius, cyl);

% Compute J dot E

Exsm = imgaussfilt3(Ex,radius);
Eysm = imgaussfilt3(Ey,radius);
Ezsm = imgaussfilt3(Ez,radius);

JedotE=dot(Jex,Jey,Jez,Ex,Ey,Ez);


JedotEsm=dot(imgaussfilt3(Jex,radius),imgaussfilt3(Jey,radius),imgaussfilt3(Jez,radius), ...
    Exsm, Eysm , Ezsm);


JidotE=dot(Jix,Jiy,Jiz,Ex,Ey,Ez);

JidotEsm=dot(imgaussfilt3(Jix,radius),imgaussfilt3(Jiy,radius),imgaussfilt3(Jiz,radius), ...
    Exsm, Eysm , Ezsm);


JbdotE=dot(Jbx,Jby,Jbz,Ex,Ey,Ez);


JbdotEsm=dot(imgaussfilt3(Jbx,radius),imgaussfilt3(Jby,radius),imgaussfilt3(Jbz,radius), ...
    Exsm, Eysm , Ezsm);

JdotE=JedotE+JidotE+JbdotE;
JdotEsm=JedotEsm+JidotEsm+JbdotEsm;

Vex= Jex./(rhoe-tiny) ;
Vey= Jey./(rhoe-tiny) ;
Vez= Jez./(rhoe-tiny) ;
Epx = Ex + (Vey.*Bz - Vez.*By);
Epy = Ey + (Vez.*Bx - Vex.*Bz);
Epz = Ez + (Vex.*By - Vey.*Bx);

JdotEp=imgaussfilt3(Jex+Jix,radius).*imgaussfilt3(Epx,radius) + imgaussfilt3(Jey+Jiy,radius).*imgaussfilt3(Epy,radius) + imgaussfilt3(Jez+Jiz,radius).*imgaussfilt3(Epz,radius);

[dEx,dEy,dEz]=compute_curl(x,y,z,Bx,By,Bz,radius);
dE2dt1 = (Exsm.*dEx + Eysm.*dEy + Ezsm.*dEz)/4/pi;
dE2dt2 = -Exsm.*imgaussfilt3(Jex+Jix,radius) - Eysm.*(Jey+Jiy) - Ezsm.*imgaussfilt3(Jez+Jiz,radius);
dE2dt= dE2dt2+ dE2dt1;

[dBx,dBy,dBz]=compute_curl(x,y,z,Ex,Ey,Ez,radius);
dB2dt = -(Bx.*dBx + By.*dBy + Bz.*dBz)/4/pi;

dE2dt_computed = -divS/4/pi -JdotE - dB2dt;


[Sx, Sy, Sz] = cross_prod(Ex, Ey, Ez, Bx, By, Bz);
Sx = Sx /4/pi; Sy = Sy /4/pi; Sz = Sz /4/pi;
divS = compute_div(x,y,z,Sx,Sy,Sz,radius, cyl);

xc=linspace(0, Lx, Nx);
yc=linspace(0, Ly, Ny);
AAz=vecpot(xc,yc,Bx(:,:,iz),By(:,:,iz));
Nsm=0;
radius=5

V1=imgaussfilt3(Vex,radius);V1=V1(:,:,iz);
V2=imgaussfilt3(Vey,radius);V2=V2(:,:,iz);
Psiz=vecpot(xc,yc,V1,V2);



color_choice=3
symmetric_color=1



figure(1)
p=(Pexx+Peyy+Pezz+Pixx+Piyy+Pizz)/3;
pB=B.^2/8/pi;

subplot(2,2,1)
tmp=imagesc([0 Lx],[0 Lz],log10(squeeze(pB(:,round(Ny/2),:))));colorbar
xlabel('x')
ylabel('z')
title('pB=B^2/8\pi')
subplot(2,2,2)
tmp=imagesc([0 Lx],[0 Lz],log10(squeeze(tiny+p(:,round(Ny/2),:))));colorbar
xlabel('x')
ylabel('z')
title('p= Tr(Pe+Pi)/3')
subplot(2,2,3)
plot(X2(1,:),p(:,round(Ny/2),round(Nz/2)),X2(1,:),pB(:,round(Ny/2),round(Nz/2)),X2(1,:),pB(:,round(Ny/2),round(Nz/2))+p(:,round(Ny/2),round(Nz/2)))
xlabel('x')
legend('p','pB','p+pB')
subplot(2,2,4)
plot(Y(:,1),p(round(Nx/2),:,round(Nz/2)),Y(:,1),pB(round(Nx/2),:,round(Nz/2)),Y(:,1),pB(round(Nx/2),:,round(Nz/2))+p(round(Nx/2),:,round(Nz/2)))
xlabel('y')
legend('p','pB','p+pB')
print('-dpng',[dir_out 'pressure_with4pi.png'])

%
% The figure 2 is done to test. The correct defintion of pB is with
% B^2/8/pi as done above. The figure 2 is wrong.
%
figure(2)
p=(Pexx+Peyy+Pezz+Pixx+Piyy+Pizz)/3;
pB=B.^2/2;

subplot(2,2,1)
tmp=imagesc([0 Lx],[0 Lz],log10(squeeze(pB(:,round(Ny/2),:))));colorbar
xlabel('x')
ylabel('z')
title('pB=B^2/2')
subplot(2,2,2)
tmp=imagesc([0 Lx],[0 Lz],log10(squeeze(tiny+p(:,round(Ny/2),:))));colorbar
xlabel('x')
ylabel('z')
title('p= Tr(Pe+Pi)/3')
subplot(2,2,3)
plot(X2(1,:),p(:,round(Ny/2),round(Nz/2)),X2(1,:),pB(:,round(Ny/2),round(Nz/2)),X2(1,:),pB(:,round(Ny/2),round(Nz/2))+p(:,round(Ny/2),round(Nz/2)))
xlabel('x')
legend('p','pB','p+pB')
subplot(2,2,4)
plot(Y(:,1),p(round(Nx/2),:,round(Nz/2)),Y(:,1),pB(round(Nx/2),:,round(Nz/2)),Y(:,1),pB(round(Nx/2),:,round(Nz/2))+p(round(Nx/2),:,round(Nz/2)))
xlabel('y')
legend('p','pB','p+pB')

print('-dpng',[dir_out 'pressure_without4pi.png'])

if(external_fields) 
% tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(Ex_ext(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'E_{x,ext}','Exext',[-1 1]*0, Nsm, 1);
% tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(Ey_ext(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'E_{y,ext}','Eyext',[-1 1]*0, Nsm, 1);
% tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(Ez_ext(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'E_{z,ext}','Ezext',[-1 1]*0, Nsm, 1);

cp1=imgaussfilt3(Ex,radius);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp1(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'E_{x}','Ex',[-1 1]*0, Nsm, 1);

cp1=imgaussfilt3(Ey,radius);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp1(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'E_{y}','Ey',[-1 1]*0, Nsm, 1);

cp1=imgaussfilt3(Ez,radius);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp1(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'E_{z}','Ez',[-1 1]*0, Nsm, 1);

[cp1,cp2,cp3] = magnetic_coordinates_axisy(Ex_ext,Ey_ext, Ez_ext, Bx, By, Bz); 

tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp1(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'E_{||,ext}','Eparext',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp2(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'E_{\perp1,ext}','Eperp1ext',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp3(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'E_{\perp2,ext}','Eperp2ext',[-1 1]*0, Nsm, 1);

[cp1,cp2,cp3] = magnetic_coordinates_axisy(Ex,Ey, Ez, Bx, By, Bz); 
cp1=imgaussfilt3(cp1,radius);
cp2=imgaussfilt3(cp2,radius);
cp3=imgaussfilt3(cp3,radius);

tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp1(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'E_{||}','Epar',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp2(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'E_{\perp1}','Eperp1',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp3(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'E_{\perp2}','Eperp2',[-1 1]*0, Nsm, 1);

end


if(currents)

cp1=imgaussfilt3(Jex-rhoe.*VexbX,radius);V1=cp1(:,:,iz-navg:iz+navg);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(V1,3), Bx,By, Bz,'(J_e-J_{ExB})_x','JexbX',[-1 1]*0, Nsm, 1);

cp2=imgaussfilt3(Jey-rhoe.*VexbY,radius);V2=cp2(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V2,3), AAz(ir,jr),Psiz(ir,jr),'(J_e-J_{ExB})_y','JexbY',[-1 1]*0, Nsm, 2);

cp3=imgaussfilt3(Jez-rhoe.*VexbZ,radius);V3=cp3(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V3,3), AAz(ir,jr),Psiz(ir,jr),'(J_e-J_{ExB})_z','JexbZ',[-1 1]*0, Nsm, 3);
savevtkvector_bin(cp1,cp2,cp3, [dir_out  'JeSLIP' Ncycle '.vtk'],'JeSLIP',dx,dy,dz,0,0,0)

symmetric_color=0
color_choice=-1 % 5
color_lines = [1 1 1 0.4];
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(sqrt(V1.^2+V2.^2+V3.^2),3), AAz(ir,jr),Psiz(ir,jr),'(J_e-J_{ExB})','Jexb',[-1 1]*0, Nsm, 3);
color_choice=3
symmetric_color=1
color_lines = [0 0 0 0.2];

[cp1,cp2,cp3] = magnetic_coordinates_axisy(cp1,cp2, cp3, Bx, By, Bz); 
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp1(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'(J_e-J_{ExB})_{||}','Jexbpar',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp2(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'(J_e-J_{ExB})_{\perp1}','Jexbperp1',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp3(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'(J_e-J_{ExB})_{\perp2}','Jexbpep2',[-1 1]*0, Nsm, 1);
savevtkvector_bin(cp1,cp2,cp3, [dir_out  'JeSLIPMC' Ncycle '.vtk'],'JeSLIPMC',dx,dy,dz,0,0,0)

symmetric_color=0
color_choice=-1
color_lines = [1 1 1 0.4];
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(sqrt(cp1(:,:,iz-navg:iz+navg).^2+cp2(:,:,iz-navg:iz+navg).^2+cp3(:,:,iz-navg:iz+navg).^2),3), Bx,By, Bz,'(J_e-J_{ExB})','Jexbmc',[-1 1]*0, Nsm, 1);
color_choice=3
symmetric_color=1
color_lines = [0 0 0 0.2];

cp1=imgaussfilt3((Jex-rhoe.*VexbX)./(rhoe-tiny),radius);V1=cp1(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1,3), AAz(ir,jr),Psiz(ir,jr),'(V_e-V_{ExB})_x','VexbX',[-1 1]*0, Nsm, 11);

cp2=imgaussfilt3((Jey-rhoe.*VexbY)./(rhoe-tiny),radius);V2=cp2(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V2,3), AAz(ir,jr),Psiz(ir,jr),'(V_e-V_{ExB})_y','VexbY',[-1 1]*0, Nsm, 12);

cp3=imgaussfilt3((Jez-rhoe.*VexbZ)./(rhoe-tiny),radius);V3=cp3(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V3,3), AAz(ir,jr),Psiz(ir,jr),'(V_e-V_{ExB})_z','VexbZ',[-1 1]*0, Nsm, 13);
savevtkvector_bin(cp1,cp2,cp3, [dir_out  'VeSLIP'  Ncycle '.vtk'],'VeSLIP',dx,dy,dz,0,0,0)

symmetric_color=0
color_choice=-1
color_lines = [1 1 1 0.4];
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(sqrt(V1.^2+V2.^2+V3.^2),3), AAz(ir,jr),Psiz(ir,jr),'(V_e-V_{ExB})','Vexb',[-1 1]*0, Nsm, 13);
color_choice=3
symmetric_color=1
color_lines = [0 0 0 0.2];

[cp1,cp2,cp3] = magnetic_coordinates_axisy(cp1,cp2, cp3, Bx, By, Bz); 
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp1(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'(V_e-V_{ExB})_{||}','Vexbpar',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp2(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'(V_e-V_{ExB})_{\perp1}','Vexbperp1',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp3(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'(V_e-V_{ExB})_{\perp2}','Vexbpep2',[-1 1]*0, Nsm, 1);
savevtkvector_bin(cp1,cp2,cp3, [dir_out 'VeSLIPMC' Ncycle '.vtk'],'VeSLIPMC',dx,dy,dz,0,0,0)

symmetric_color=0
color_choice=-1
color_lines = [1 1 1 0.4];
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(sqrt(cp1(:,:,iz-navg:iz+navg).^2+cp2(:,:,iz-navg:iz+navg).^2+cp3(:,:,iz-navg:iz+navg).^2),3), Bx,By, Bz,'(V_e-V_{ExB})','Vexbmc',[-1 1]*0, Nsm, 1);
color_choice=3
symmetric_color=1
color_lines = [0 0 0 0.2];

cp1=imgaussfilt3(Jex./(rhoe-tiny),radius);V1=cp1(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1,3), AAz(ir,jr),Psiz(ir,jr),'Vex','Vex',[-1 1]*0, Nsm, 14);
cp2=imgaussfilt3(Jey./(rhoe-tiny),radius);V1=cp2(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1,3), AAz(ir,jr),Psiz(ir,jr),'Vey','Vey',[-1 1]*0, Nsm, 15);
cp3=imgaussfilt3(Jez./(rhoe-tiny),radius);V1=cp3(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1,3), AAz(ir,jr),Psiz(ir,jr),'Vez','Vez',[-1 1]*0, Nsm, 16);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp1(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'V_e_{x}','Vex',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp2(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'V_e_{y}','Vey',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp3(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'V_e_{z}','Vez',[-1 1]*0, Nsm, 1);
savevtkvector_bin(cp1,cp2,cp3, [dir_out  'Ve' Ncycle '.vtk'],'Ve',dx,dy,dz,0,0,0)

[cp1,cp2,cp3] = magnetic_coordinates_axisy(cp1,cp2, cp3, Bx, By, Bz); 
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp1(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'V_e_{||}','Vepar',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp2(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'V_e_{\perp1}','Veperp1',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp3(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'V_e_{\perp2}','Veperp2',[-1 1]*0, Nsm, 1);


cp1=imgaussfilt3(Jex,radius);V1=cp1(:,:,iz);
%tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1, AAz(ir,jr),Psiz(ir,jr),'Vex','Vex',[-1 1]*0, Nsm, 14);
cp2=imgaussfilt3(Jey,radius);V1=cp2(:,:,iz);
%tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1, AAz(ir,jr),Psiz(ir,jr),'Vey','Vey',[-1 1]*0, Nsm, 15);
cp3=imgaussfilt3(Jez,radius);V1=cp3(:,:,iz);
%tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1, AAz(ir,jr),Psiz(ir,jr),'Vez','Vez',[-1 1]*0, Nsm, 16);

tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp1(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'J_e_{x}','Jex',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp2(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'J_e_{y}','Jey',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp3(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'J_e_{z}','Jez',[-1 1]*0, Nsm, 1);
savevtkvector_bin(cp1,cp2,cp3, [dir_out  'Je' Ncycle '.vtk'],'Je',dx,dy,dz,0,0,0)

[cp1,cp2,cp3] = magnetic_coordinates_axisy(cp1,cp2, cp3, Bx, By, Bz); 
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp1(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'J_e_{||}','Jepar',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp2(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'J_e_{\perp1}','Jeperp1',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(cp3(:,:,iz-navg:iz+navg),3).*window', Bx,By, Bz,'J_e_{\perp2}','Jeperp2',[-1 1]*0, Nsm, 1);


%V1=imgaussfilt3(Epar,radius);V1=V1(:,:,iz);
%tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1, AAz(ir,jr),Psiz(ir,jr),'Epar','Epar',[-1 1]*0, Nsm, 16);
V1=imgaussfilt3(JdotEp,radius);V1=V1(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1,3), AAz(ir,jr),Psiz(ir,jr),'JEp','JEp',[-1 1]*0, Nsm, 16);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(V1,3).*window', Bx,By, Bz,'JEp','JEp',[-1 1]*.3e-8*0, Nsm, 1);

V1=imgaussfilt3(JdotE,radius);V1=V1(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1,3), AAz(ir,jr),Psiz(ir,jr),'JE','JE',[-1 1]*0, Nsm, 16);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(V1,3).*window', Bx,By, Bz,'JE','JE',[-1 1]*.3e-8*0, Nsm, 1);

V1=imgaussfilt3(JedotE,radius);V1=V1(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1,3), AAz(ir,jr),Psiz(ir,jr),'JeE','JeE',[-1 1]*0, Nsm, 16);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(V1,3).*window', Bx,By, Bz,'JeE','JeE',[-1 1]*.3e-8*0, Nsm, 1);

V1=imgaussfilt3(JidotE,radius);V1=V1(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1,3), AAz(ir,jr),Psiz(ir,jr),'JiE','JiE',[-1 1]*0, Nsm, 16);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(V1,3).*window', Bx,By, Bz,'JiE','JiE',[-1 1]*.3e-8*0, Nsm, 1);

V1=imgaussfilt3(JbdotE,radius);V1=V1(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1,3), AAz(ir,jr),Psiz(ir,jr),'JbE','JbE',[-1 1]*0, Nsm, 16);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(V1,3).*window', Bx,By, Bz,'JbE','JbE',[-1 1]*.3e-8*0, Nsm, 1);

V1=imgaussfilt3(JdotEp,radius);V1=V1(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1,3), AAz(ir,jr),Psiz(ir,jr),'JEp','JEp',[-1 1]*0, Nsm, 16);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(V1,3).*window', Bx,By, Bz,'JEp','JEp',[-1 1]*.3e-8*0, Nsm, 1);

V1=JdotEsm;V1=V1(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1,3), AAz(ir,jr),Psiz(ir,jr),'JEsm','JEsm',[-1 1]*0, Nsm, 16);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(V1,3).*window', Bx,By, Bz,'JEsm','JEsm',[-1 1]*.3e-8*0, Nsm, 1);

V1=JedotEsm;V1=V1(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1,3), AAz(ir,jr),Psiz(ir,jr),'JeEsm','JeEsm',[-1 1]*0, Nsm, 16);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(V1,3).*window', Bx,By, Bz,'JeEsm','JeEsm',[-1 1]*.3e-8*0, Nsm, 1);

V1=JidotEsm;V1=V1(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1,3), AAz(ir,jr),Psiz(ir,jr),'JiEsm','JiEsm',[-1 1]*0, Nsm, 16);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(V1,3).*window', Bx,By, Bz,'JiEsm','JiEsm',[-1 1]*.3e-8*0, Nsm, 1);

V1=JbdotEsm;V1=V1(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1,3), AAz(ir,jr),Psiz(ir,jr),'JbEsm','JbEsm',[-1 1]*0, Nsm, 16);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(V1,3).*window', Bx,By, Bz,'JbEsm','JbEsm',[-1 1]*.3e-8*0, Nsm, 1);

V1=imgaussfilt3(dB2dt,radius);V1=V1(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1,3), AAz(ir,jr),Psiz(ir,jr),'dB2dt','dB2dt',[-1 1]*0, Nsm, 16);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(V1,3).*window', Bx,By, Bz,'dB2dt','dB2dt',[-1 1]*.3e-8*0, Nsm, 1);

V1=imgaussfilt3(dE2dt,radius);V1=V1(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1,3), AAz(ir,jr),Psiz(ir,jr),'dE2dt','dE2dt',[-1 1]*0, Nsm, 16);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(V1,3).*window', Bx,By, Bz,'dE2dt','dE2dt',[-1 1]*.3e-8*0, Nsm, 1);

V1=imgaussfilt3(divS,radius);V1=V1(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1,3), AAz(ir,jr),Psiz(ir,jr),'divS','divS',[-1 1]*0, Nsm, 16);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(V1,3).*window', Bx,By, Bz,'divS','divS',[-1 1]*.3e-8*0, Nsm, 1);

symmetric_color=1
color_choice=-1
color_lines = [1 1 1 0.4];
V1=imgaussfilt3(Je,radius);V1=V1(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1,3), AAz(ir,jr),Psiz(ir,jr),'Je','Je',[-1 1]*0, Nsm, 4);

end



if(radial_v)
symmetric_color=1
color_choice=3

ii=abs(Y3-Ly/2)<Lz/5;
cfrcx=mean(X3(ii).*rhoi(ii))./mean(rhoi(ii));
cfrcz=mean(Z3(ii).*rhoi(ii))./mean(rhoi(ii));
X3=X3-cfrcx;
Z3=Z3-cfrcz;
theta=atan2(Z3,X3);
R3=sqrt(X3.^2+Z3.^2);


Ver=(cos(theta).*Jex+sin(theta).*Jez)./(rhoe-1e-10);
V1=imgaussfilt3(Ver,radius);
V1=V1(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1,3), AAz(ir,jr),Psiz(ir,jr),'Ver','Ver',[-1 1]*1e-3, Nsm, 4);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(V1,3), Bx,By, Bz,'Ver','Ver',[-1 1]*1e-3, Nsm, 1);

Veth=(-sin(theta).*Jex+cos(theta).*Jez)./(rhoe-1e-10);
V1=imgaussfilt3(Veth,radius);
V1=V1(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1,3), AAz(ir,jr),Psiz(ir,jr),'Veth','Veth',[-1 1]*3e-3, Nsm, 4);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(V1,3), Bx,By, Bz,'Veth','Veth',[-1 1]*3e-3, Nsm, 1);

Vir=(cos(theta).*Jix+sin(theta).*Jiz)./(rhoi+1e-10);
V1=imgaussfilt3(Vir,radius);
V1=V1(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1,3), AAz(ir,jr),Psiz(ir,jr),'Vir','Vir',[-1 1]*3e-4, Nsm, 4);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(V1,3), Bx,By, Bz,'Vir','Vir',[-1 1]*3e-4, Nsm, 1);

Vith=(-sin(theta).*Jix+cos(theta).*Jiz)./(rhoi+1e-10);
V1=imgaussfilt3(Vith,radius);
V1=V1(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1,3), AAz(ir,jr),Psiz(ir,jr),'Vith','Vith',[-1 1]*6e-4, Nsm, 4);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(V1,3), Bx,By, Bz,'Vith','Vith',[-1 1]*6e-4, Nsm, 1);

end
symmetric_color=1
color_choice =1;
Peani=(Peper1+Peper1)/2./(Pepar+1e-10);
Piani=(Piper1+Piper1)/2./(Pipar+1e-10);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(real(Peani(ir,jr,iz-navg:iz+navg)),3)-1,Bx,By,Bz,['midZ'],'PeaniM1',[0 0], radius,1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(real(Piani(ir,jr,iz-navg:iz+navg)),3)-1,Bx,By,Bz,['midZ'],'PianiM1',[0 0], radius,1);
V=(Peani-1).*abs(rhoe);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(V(ir,jr,iz-navg:iz+navg),3),Bx,By,Bz,['midZ'],'PeaniM1n',[0 0], radius,1);
V=(Piani-1).*abs(rhoi);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(V(ir,jr,iz-navg:iz+navg),3),Bx,By,Bz,['midZ'],'PianiM1n',[0 0], radius,1);
Pediff=(Peper1+Peper1)/2 - (Pepar);
Pidiff=(Piper1+Piper1)/2. - (Pipar);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(Pediff(ir,jr,iz-navg:iz+navg),3),Bx,By,Bz,['midZ'],'Pediff',[0 0], radius,1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(Pidiff(ir,jr,iz-navg:iz+navg),3),Bx,By,Bz,['midZ'],'Pidiff',[0 0], radius,1);


if(agyro)
    navg = 0
if(agyro_write)    
    [Agyro,Agyro_aunai,Nongyro_swisdak]=compute_agyro(Bx, By, Bz, imgaussfilt3(Pexx,radius), ...
    imgaussfilt3(Pexy,radius), imgaussfilt3(Pexz,radius), imgaussfilt3(Peyy,radius),  ...
    imgaussfilt3(Peyz,radius), imgaussfilt3(Pezz,radius) ) ;
    opath=[dir_out 'Agyro' ncycle '.h5']; 
h5create(opath,'/Step#0/Block/SmAgyro/0',[Nx, Ny, Nz]);
h5write(opath,'/Step#0/Block/SmAgyro/0',real(Agyro))
h5create(opath,'/Step#0/Block/SmAgyro_aunai/0',[Nx, Ny, Nz]);
h5write(opath,'/Step#0/Block/SmAgyro_aunai/0',real(Agyro_aunai))
h5create(opath,'/Step#0/Block/SmNongyro_swisdak/0',[Nx, Ny, Nz]);
h5write(opath,'/Step#0/Block/SmNongyro_swisdak/0',real(Nongyro_swisdak))
savevtk_bin(real(Agyro), [dir_out 'Agyro' Ncycle '.vtk'],'Agyro',dx,dy,dz,0,0,0)
savevtkvector_bin(Bx,By,Bz, [dir_out 'B' Ncycle '.vtk'],'B',dx,dy,dz,0,0,0)
else
    opath=[dir_out 'Agyro' ncycle '.h5'];
    Agyro_aunai=hdf5read(opath,'/Step#0/Block/SmAgyro_aunai/0/');
    Agyro=hdf5read(opath,'/Step#0/Block/SmAgyro/0/');
    Nongyro_swisdak=hdf5read(opath,'/Step#0/Block/SmNongyro_swisdak/0/');
end
symmetric_color=0
color_choice=5
radius=1
V1=imgaussfilt3(real(Agyro),radius);
savevtk_bin(V1, [dir_out 'AgyroV' Ncycle ' .vtk'],'AgyroV1',dx,dy,dz,0,0,0)
V1=V1(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1,3), AAz(ir,jr),Psiz(ir,jr),'Agyro','Agyro',[0 .4], Nsm, 5);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(V1,3).*window',Bx,By,Bz,['midZ'],'Agyro',[0 .4], radius,1);


V1=imgaussfilt3(real(Agyro).*abs(rhoe),radius);V1=V1(:,:,iz-navg:iz+navg);
tmp=common_image_due(X(jr,ir),Y(jr,ir),mean(V1,3), AAz(ir,jr),Psiz(ir,jr),'AgyroW','AgyroW',[0 0], Nsm, 6);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),mean(V1,3).*window',Bx,By,Bz,['midZ'],'AgyroW',[0 0], radius,1);

xcoord = (X(jr,ir));
ycoord = (Y(jr,ir));

tmp=common_image_stream(xcoord,ycoord,mean(real(Agyro(ir,jr,iz-navg:iz+navg)),3),Bx,By,Bz,['midZ'],'Agyro',[0 .4], radius,1);
tmp=common_image_stream(xcoord,ycoord,mean(real(Agyro(ir,jr,iz-navg:iz+navg)).*Je(ir,jr,iz-navg:iz+navg),3),Bx,By,Bz,['midZ'],'AgyroW',[0 0], radius,1);
tmp=common_image_stream(xcoord,ycoord,mean(rhoe(ir,jr,iz-navg:iz+navg),3),Bx,By,Bz,['midZ'],'Rhoe',[0 0], radius,1);
end

%system(['mkdir ' ncycle])
%system(['mv *.png ' ncycle])

end
end
return

[divPx]=divergence(X,Y,permute(Pexx,[2 1]),permute(Pexy,[2 1 ])); % Valid only because dx=dy=dz
[divPy]=divergence(X,Y,permute(Pexy,[2 1]),permute(Peyy,[2 1 ])); % Valid only because dx=dy=dz
[divPz]=divergence(X,Y,permute(Pexz,[2 1]),permute(Peyz,[2 1 ])); % Valid only because dx=dy=dz

divPx=permute(divPx,[2 1])./rhoe;
divPy=permute(divPy,[2 1])./rhoe;
divPz=permute(divPz,[2 1])./rhoe;

VddX = -(divPx.*Bz - divPz.*By)./B.^2;
VddY = -(divPz.*Bx - divPx.*Bz)./B.^2;
VddZ = -(divPx.*By - divPy.*Bx)./B.^2;

color_choice=-1
tmp=common_image_due(X(jr,ir),Y(jr,ir),VddZ(ir,jr), Az(ir,jr),Psiz(ir,jr),'V_{dd}_Z','VdZ',[-1 1]*Vnorm, Nsm, 2);
tmp=common_image_due(X(jr,ir),Y(jr,ir),Veperpz(ir,jr), Az(ir,jr),Psiz(ir,jr),'V_{\perp Z}','VeZ',[-1 1]*Vnorm, Nsm, 4);



tmp=common_image_due(X(jr,ir),Y(jr,ir),Ex(ir,jr), Az(ir,jr),Psiz(ir,jr),'E_{X}','Ex',[-1 1]*0, Nsm, 6);


figure(100)
jxpt=1594;
jband=jxpt-5:jxpt+5
%jband=jxpt;

h(1) = subplot(2,1,1); % upper plot
plot(xc(:),mean(Veperpz(:,jband),2),xc(:),mean(VexbZ(:,jband),2),xc(:),mean(VddZ(:,jband),2))
xlim([12 12.5]);
title(['Y=' num2str(mean(yc(jband)))])
legend('V_{e\perp z}','V_{ExBz}','V_{DDz}')
h(2) = subplot(2,1,2); % lower plot
plot(xc(:),mean(Vex(:,jband),2),xc(:),mean(VddX(:,jband)+VexbX(:,jband),2),xc(:),mean(Ex(:,jband),2),xc(:),mean(By(:,jband),2)/10);
xlim([12 12.5]);
legend('V_{ex}','V_{ExBx}+V_{DDx}',' E_x','By/10','location','SouthWest')
grid on

linkaxes(h,'x'); % link the axes in x direction (just for convenience)
xlim([12 12.5]);
set(h(1),'xticklabel',[]);
pos=get(h,'position');
bottom=pos{2}(2);
top=pos{1}(2)+pos{1}(4);
plotspace=top-bottom;
pos{2}(4)=plotspace/2;
pos{1}(4)=plotspace/2;
pos{1}(2)=bottom+plotspace/2;

set(h(1),'position',pos{1});
set(h(2),'position',pos{2});

print -dpng flythroughs

h=figure(101)
set(h, 'Position', [167 26 515 776])
contour(xc(ir),yc(jr),Psiz(ir,jr)',30,'m')
hold on
contour(xc(ir),yc(jr),Az(ir,jr)',30,'k')
contour(xc(ir),yc(jr),Psiz(ir,jr)',30,'m')

print -dpng contours
