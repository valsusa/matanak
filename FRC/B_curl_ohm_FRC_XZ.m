%
% Energy plots in the XZ plane averaging on the whole range in y
%
!rm *.png
close all
addpath(genpath('../')); % Point to the directory where the iPic3D toolbox is
%dir='/data1/gianni/HRmaha3D3/vtk/'; %directory where the files are

addpath(genpath('../../ipic3d_toolbox'));

addpath(genpath('/Users/glapenta/Matlab/matanak'));

list_sim=["cfr_base" ;"cfr_stj" ;"cfr_ebi_j2"; "cfr_ebi_j4" ;"cfr_nbi"];
%list_sim=["cfr_ebi_j4" ;"cfr_nbi"];
%list_sim=["cfr_ebi_j2"]
list_sim =["2X-8NBI-ebeam-57k"; "2X-8NBI-ebeam-43k";"2X-8NBI-ebeam-37k"]
list_sim =["2X-8NBI-ebeam"; "0X-NBI-ebeam";"8NBI-ebeam"]

for it =[43,57]
for lsim = 1:3

    sim_name=char(list_sim(lsim))
read_case

%dir_out = ['/Users/glapenta/Data/compare/' sim_name '/']
dir_out = ['/Users/glapenta/Data/2X-8NBI-ebeam/OHM/' sim_name '/']
dir_out = ['/Users/glapenta/Data/FRC3D-NBI-ebeam/analisiXZ' num2str(it) '/' sim_name '/']
cycle=it*1000

unix(['mkdir /Users/glapenta/Data/FRC3D-NBI-ebeam/analisiXZ' num2str(it)])
unix(['mkdir ' dir_out])



qom=-64 

leggo='h5';



cyl = 0; % cartesian geometry

% Prepare string
ntime = num2str(cycle,'%06d');
ncycle=num2str(cycle,'%06i\n')
Ncycle = ncycle;


import_h5_binvtk  

tiny=1e-10;
rhoe=rhoe-tiny;
rhoi=rhoi+tiny;

% FRC
Lx=8;Ly=14;Lz=8
xr=[Lx/4 Lx-Lx/4];
yr=[Ly/10 Ly-Ly/10];
zr=[Lz/4 Lz-Lz/4];

Xgsmrange= [0 Lx];
Zgsmrange= [0 Ly];
Ygsmrange= [0 Lz];

global Xgsmrange Zgsmrange Ygsmrange Lx Ly Lz dx dy dz

dx=Lx/Nx;
dy=Ly/Ny;
dz=Lz/Nz;

[X Z] = meshgrid(0:dx:Lx-dx,0:dz:Lz-dz);


ir=round(xr./dx);ir=min(ir):max(ir); ir=1:Nx
jr=round((Ny-1)/2);
kr=round(zr./dz);kr=min(kr):max(kr); kr=1:Nz

iy = round(Ny/2+10);
ix = round(Nx/2);deltax=20;

radius=5; %radius=4


global color_choice symmetric_color labelx labely labelc reversex reversey Ncycle skip dir_out color_lines

reversey=1;
symmetric_color=1;
color_choice =1;

    labelx ='x/d_i';
    labely ='y/d_i';
    labelc_flux = 'c.u.';
    labelc_power = 'c.u.';
    signx = 1;
    Wm3 = 1; %4pi is due to the usal division by 4pi of the density
    nWm3 = 1;
    mWm2= 1;
    reversex=0;

skip=1;
color_lines = [0 0 0 0.2];



iy = round(Ny/3);
ix = round(Nx/2);deltax=20;


xcoord = (X(kr,ir));
zcoord = (Z(kr,ir));
xc=linspace(0, Lx, Nx);
zc=linspace(0, Lz, Nz);
AAz=vecpot(xc,zc,Bx(:,jr,:),Bz(:,jr,:));
AAz = B(:,jr,:);
%AAz=(Bx.^2+By.^2+Bz.^2);AAz=AAz(:,:,kr);
[x,y,z]=meshgrid(0:dx:Lx-dx,0:dy:Ly-dy,0:dz:Lz-dz);
r = sqrt((x-Lx/2).^2+(y-Ly/2).^2+(z-Lz/2).^2);

[X3 Y3 Z3] = meshgrid(0:dx:Lx-dx,0:dy:Ly-dy,0:dz:Lz-dz);

% na=30
% 
% ra=2
% angle =0:2*pi/(na-1):2*pi;
% na=max(size(angle));
% startx = Lx/2+ra*cos(angle);
% startz = Lz/2+ra*sin(angle);
% starty = ones(1,na)*Ly/2;
% streamline(X3,Y3,Z3,permute(Bx,[2 1 3]),permute(By,[2 1 3]),permute(Bz,[2 1 3]),startx,starty,startz);view(3)
% %streamline(X3,Y3,Z3,permute(Bx,[2 1 3]),permute(By,[2 1 3]),permute(Bz,[2 1 3]),Lx/2+linspace(-2,2,20),ones(1,20)*5,Lz/2-linspace(-2,2,20));view(3)
% hold on 
% plot3(startx,starty,startz)
% 
% ra=.2
% startx = Lx/2+ra*cos(angle);
% startz = Lz/2+ra*sin(angle);
% starty = ones(1,na)*Ly/2;
% streamline(X3,Y3,Z3,permute(Bx,[2 1 3]),permute(By,[2 1 3]),permute(Bz,[2 1 3]),startx,starty,startz);view(3)
% %streamline(X3,Y3,Z3,permute(Bx,[2 1 3]),permute(By,[2 1 3]),permute(Bz,[2 1 3]),Lx/2+linspace(-2,2,20),ones(1,20)*5,Lz/2-linspace(-2,2,20));view(3)
% hold on 
% plot3(startx,starty,startz)
% 
% 
% close all

Np=50
TH = 2*pi*rand(1,Np);
PH = asin(-1+2*rand(1,Np));
R = rand(1,Np)*2;
[startx,starty,startz] = sph2cart(TH,PH,R);

startx=startx+Lx/2;
starty=starty+Ly/2;
startz=startz+Lz/2;
%plot3(startx,starty,startz,'.','markersize',1)
h1 = streamline(X3,Y3,Z3,permute(Bx,[2 1 3]),permute(By,[2 1 3]),permute(Bz,[2 1 3]),startx,starty,startz);view(3)
h2 = streamline(X3,Y3,Z3,-permute(Bx,[2 1 3]),-permute(By,[2 1 3]),-permute(Bz,[2 1 3]),startx,starty,startz);view(3)
%h3 = streamline(X3,Y3,Z3,permute(Jex,[2 1 3]),permute(Jey,[2 1 3]),permute(Jez,[2 1 3]),startx,starty,startz);view(3)
%h4 = streamline(X3,Y3,Z3,-permute(Jex,[2 1 3]),-permute(Jey,[2 1 3]),-permute(Jez,[2 1 3]),startx,starty,startz);view(3)
axis equal 
set(h1,'Color', [0 0 0, 0.2])
set(h2,'Color', [0 0 0, 0.2])
%set(h3,'Color', [1 0 0, 0.2])
%set(h4,'Color', [1 0 0, 0.2])
print('-dpng','-r300',[dir_out 'magnetic_lines' Ncycle '.png'])


[npx,npy]=size(Bx(ir,jr,kr)); [iw,jw]=meshgrid(1:npx,1:npy);
sx=npx/4;sy=npy/4
window = exp(-(iw-npx/2).^2/sx-(jw-npy/2).^2/sy);
navg= floor(npx/4);

[X3 Y3 Z3] = meshgrid(0:dx:Lx-dx,0:dy:Ly-dy,0:dz:Lz-dz);
sx=Lx/4;sy=Ly/4;sz=Lz/4;
window3 = exp(-(X3-Lx/2).^2/sx-(Y3-Ly/2).^2/sy-(Z3-Lz/2).^2/sz);


%Poyn ting theorem
vnorm = 1e-8
[Sx, Sy, Sz] = cross_prod(Ex, Ey, Ez, Bx, By, Bz);
Sx = Sx /4/pi; Sy = Sy /4/pi; Sz = Sz /4/pi;
divS = compute_div(x,y,z,Sx,Sy,Sz,radius*0, cyl);
JdotE=(Jex+Jix).*Ex + (Jey+Jiy).*Ey + (Jez+Jiz).*Ez;
cazzo = JdotE .* permute(window3,[2 1 3]);

[dEx,dEy,dEz]=compute_curl(x,y,z,Bx,By,Bz);
dE2dt1 = (Ex.*dEx + Ey.*dEy + Ez.*dEz)/4/pi;
dE2dt2 = -Ex.*(Jex+Jix) - Ey.*(Jey+Jiy) - Ez.*(Jez+Jiz);
dE2dt= dE2dt2+ dE2dt1;
%tmp=common_image_stream(xcoord,zcoord,squeeze(dE2dt1(ir,jr,kr)).*window',Bx,By,Bz,['midZ'],'dE2dt_1',[-1 1]*vnorm, radius,1);
%tmp=common_image_stream(xcoord,zcoord,squeeze(dE2dt2(ir,jr,kr)).*window',Bx,By,Bz,['midZ'],'dE2dt_2',[-1 1]*vnorm, radius,1);
%tmp=common_image_stream(xcoord,zcoord,squeeze(dE2dt(ir,jr,kr)).*window',Bx,By,Bz,['midZ'],'dE2dt',[-1 1]*vnorm, radius,1);
tmp=common_image(xcoord,zcoord,dE2dt(ir,jr,kr),AAz(ir,kr),['midZ'],'dE2dt',[-1 1]*vnorm, radius,1);

% Assuming n>0 and e>0
% (-e n ve_ = Je)xB

%vnorm = 1e-6
axis equal
[cp1, cp2, cp3] = cross_prod(Jex+Jix, Jey+Jiy, Jez+Jiz, Bx, By, Bz);
[dBx,dBy,dBz]=compute_curl(x,y,z,-cp1./rhoe,-cp2./rhoe,-cp3./rhoe);
dB2dt = -(Bx.*dBx + By.*dBy + Bz.*dBz)/4/pi;

labelc = labelc_power;
tmp=common_image(xcoord,zcoord,squeeze(dB2dt(ir,jr,kr)),AAz(ir,kr),['midZ'],'BcurlOHMJxB',[-1 1]*vnorm, radius,1);
%tmp=common_image_stream(xcoord,zcoord,squeeze(dB2dt(ir,jr,kr)).*window',Bx,By,Bz,['midZ'],'BcurlOHMJxB',[-1 1]*vnorm, radius,1);

[dBx,dBy,dBz]=compute_curl(x,y,z,Ex,Ey,Ez);
dB2dt = -(Bx.*dBx + By.*dBy + Bz.*dBz)/4/pi;

dE2dt_computed = -divS-JdotE - dB2dt;

%tmp=common_image_stream(xcoord,zcoord,squeeze(divS(ir,jr,kr)).*window',Bx,By,Bz,['midZ'],'divS',[-1 1]*vnorm, radius,1);
%tmp=common_image_stream(xcoord,zcoord,squeeze(JdotE(ir,jr,kr)).*window',Bx,By,Bz,['midZ'],'JdotE',[-1 1]*vnorm, radius,1);
%tmp=common_image_stream(xcoord,zcoord,squeeze(dB2dt(ir,jr,kr)).*window',Bx,By,Bz,['midZ'],'dB2dt',[-1 1]*vnorm, radius,1);
%tmp=common_image_stream(xcoord,zcoord,squeeze(dE2dt_computed(ir,jr,kr)).*window',Bx,By,Bz,['midZ'],'dE2dt_computed',[-1 1]*vnorm, radius,1);

V1=imgaussfilt3(dB2dt,radius);
savevtk_bin(V1, [dir_out  'dB2dt' Ncycle '.vtk'],'dB2dt',dx,dy,dz,0,0,0);
V1=imgaussfilt3(dE2dt,radius);
savevtk_bin(V1, [dir_out  'dE2dt' Ncycle '.vtk'],'dE2dt',dx,dy,dz,0,0,0);
V1=imgaussfilt3(JdotE,radius);
savevtk_bin(V1, [dir_out  'JdotE' Ncycle '.vtk'],'JdotE',dx,dy,dz,0,0,0);
V1=imgaussfilt3(divS,radius);
savevtk_bin(V1, [dir_out  'divS' Ncycle '.vtk'],'divS',dx,dy,dz,0,0,0);

tmp=common_image(xcoord,zcoord,divS(ir,jr,kr),AAz(ir,kr),['midZ'],'divS',[-1 1]*vnorm, radius,1);
tmp=common_image(xcoord,zcoord,JdotE(ir,jr,kr),AAz(ir,kr),['midZ'],'JdotE',[-1 1]*vnorm, radius,1);
tmp=common_image(xcoord,zcoord,dB2dt(ir,jr,kr),AAz(ir,kr),['midZ'],'dB2dt',[-1 1]*vnorm, radius,1);
tmp=common_image(xcoord,zcoord,dE2dt_computed(ir,jr,kr),AAz(ir,kr),['midZ'],'dE2dt_computed',[-1 1]*vnorm, radius,1);


cp2sm=smooth3(dB2dt,'box',3);
ohmJexBY= cp2sm(:,iy,kr);
ohmJexBYy= cp2sm(ix,:,kr);



axis equal 
[cp1, cp2, cp3] = cross_prod(Jix, Jiy, Jiz, Bx, By, Bz);
[dBx,dBy,dBz]=compute_curl(x,y,z,cp1./rhoe,cp2./rhoe,cp3./rhoe);
dB2dt = -(Bx.*dBx + By.*dBy + Bz.*dBz)/4/pi;
labelc = labelc_power;
tmp=common_image(xcoord,zcoord,dB2dt(ir,jr,kr),AAz(ir,kr),['midZ'],'BcurlOHMJixB',[-1 1]*vnorm, radius,1);
%tmp=common_image_stream(xcoord,zcoord,squeeze(dB2dt(ir,jr,kr)).*window',Bx,By,Bz,['midZ'],'BcurlOHMJixB',[-1 1]*vnorm, radius,1);

cp2sm=smooth3(dB2dt,'box',3);
ohmJixBY= cp2sm(:,iy,kr);
ohmJixBYy= cp2sm(ix,:,kr);


[cp1, cp2, cp3] = cross_prod(Jex, Jey, Jez, Bx, By, Bz);
[dBx,dBy,dBz]=compute_curl(x,y,z,-cp1./rhoe,-cp2./rhoe,-cp3./rhoe);
dB2dt = -(Bx.*dBx + By.*dBy + Bz.*dBz)/4/pi;
term_ve = dB2dt;
labelc = labelc_power;
tmp=common_image(xcoord,zcoord,dB2dt(ir,jr,kr),AAz(ir,kr),['midZ'],'BcurlOHMVexB',[-1 1]*vnorm, radius,1);
%tmp=common_image_stream(xcoord,zcoord,squeeze(dB2dt(ir,jr,kr)).*window',Bx,By,Bz,['midZ'],'BcurlOHMVexB',[-1 1]*vnorm, radius,1);

cp2sm=smooth3(dB2dt,'box',3);
ohmJixBY= cp2sm(:,iy,kr);
ohmJixBYy= cp2sm(ix,:,kr);

%balance magnetic energy d(B^2/2)/dt = B.dB/dt = -B curl(E)
[dBx,dBy,dBz]=compute_curl(x,y,z,Ex,Ey,Ez);
dB2dt = -(Bx.*dBx + By.*dBy + Bz.*dBz)/4/pi;

term_E = dB2dt;

tmp=common_image(xcoord,zcoord,dB2dt(ir,jr,kr),AAz(ir,kr),['midZ'],'BcurlOHME',[-1 1]*vnorm, radius,1);
%tmp=common_image_stream(xcoord,zcoord,squeeze(dB2dt(ir,jr,kr)).*window',Bx,By,Bz,['midZ'],'BcurlOHME',[-1 1]*vnorm, radius,1);

cp2sm=smooth3(dB2dt,'box',3);
ohmnEY= cp2sm(:,iy,kr);
ohmnEYy= cp2sm(ix,:,kr);

% me/qe (dVe/dt) = me/ qe  ve  * grad (Ve) =  ve grad(Ve) /qom

[tx, ty, tz] = gradient(imgaussfilt3(permute(Jex./rhoe,[2 1 3]),radius), dx, dy, dz);
tx=permute(tx,[2 1 3]);ty=permute(ty,[2 1 3]); tz=permute(tz,[2 1 3]);
cp1 = (tx.*Jex + ty.*Jey + tz.*Jez) ./rhoe /qom;

[tx, ty, tz] = gradient(imgaussfilt3(permute(Jey./rhoe,[2 1 3]),radius), dx, dy, dz);
tx=permute(tx,[2 1 3]);ty=permute(ty,[2 1 3]); tz=permute(tz,[2 1 3]);
cp2 = (tx.*Jex + ty.*Jey + tz.*Jez) ./rhoe /qom;

[tx, ty, tz] = gradient(imgaussfilt3(permute(Jez./rhoe,[2 1 3]),radius), dx, dy, dz);
tx=permute(tx,[2 1 3]);ty=permute(ty,[2 1 3]); tz=permute(tz,[2 1 3]);
cp3 = (tx.*Jex + ty.*Jey + tz.*Jez) ./rhoe /qom;

[dBx,dBy,dBz]=compute_curl(x,y,z,cp1,cp2,cp3);
dB2dt = -(Bx.*dBx + By.*dBy + Bz.*dBz)/4/pi;

ohmi= dB2dt(:,iy,kr);
ohmiy= dB2dt(ix,:,kr);

tmp=common_image(xcoord,zcoord,dB2dt(ir,jr,kr),AAz(ir,kr),['midZ'],'BcurlOHMiner',[-1 1]*vnorm, radius,1);
%tmp=common_image_stream(xcoord,zcoord,squeeze(dB2dt(ir,jr,kr)).*window',Bx,By,Bz,['midZ'],'BcurlOHMiner',[-1 1]*vnorm, radius,1);

% -div (Pe)
[cp1, cp2, cp3] = compute_divtensor(x,y,z,Pexx,Pexy,Pexz,Peyy, ...
    Peyz,Pezz,radius,0);

[dBx,dBy,dBz]=compute_curl(x,y,z,cp1./rhoe,cp2./rhoe,cp3./rhoe);
dB2dt = -(Bx.*dBx + By.*dBy + Bz.*dBz)/4/pi;
term_pe = dB2dt;

tmp=common_image(xcoord,zcoord,dB2dt(ir,jr,kr),AAz(ir,kr),['midZ'],'BcurlOHMdivP',[-1 1]*vnorm, radius,1);
%tmp=common_image_stream(xcoord,zcoord,squeeze(dB2dt(ir,jr,kr)).*window',Bx,By,Bz,['midZ'],'BcurlOHMdivP',[-1 1]*vnorm, radius,1);

ohmdivP= dB2dt(:,iy,kr);
ohmdivPy= dB2dt(ix,:,kr);



% Diagonal term
pe = (Pexx + Peyy + Pezz)/3;
[cp1,cp2,cp3] = gradient(permute(imgaussfilt3(pe,radius),[2 1 3]),dx,dy,dz);
cp1 =  permute(cp1, [2 1 3]); 
cp2 =  permute(cp2, [2 1 3]);
cp3 =  permute(cp3, [2 1 3]);

[dBx,dBy,dBz]=compute_curl(x,y,z,cp1./rhoe,cp2./rhoe,cp3./rhoe);
dB2dt = -(Bx.*dBx + By.*dBy + Bz.*dBz)/4/pi;

tmp=common_image(xcoord,zcoord,dB2dt(ir,jr,kr),AAz(ir,kr),['midZ'],'BcurlOHMgradp',[-1 1]*vnorm, radius,1);
%tmp=common_image_stream(xcoord,zcoord,squeeze(dB2dt(ir,jr,kr)).*window',Bx,By,Bz,['midZ'],'BcurlOHMgradp',[-1 1]*vnorm, radius,1);


ohmgradp= dB2dt(:,iy,kr);
ohmgradpy= dB2dt(ix,:,kr);

% -deviatoric
[cp1, cp2, cp3] = compute_divtensor(x,y,z,Pexx-pe,Pexy,Pexz,Peyy-pe, ...
    Peyz,Pezz-pe,radius,0);

[dBx,dBy,dBz]=compute_curl(x,y,z,cp1./rhoe,cp2./rhoe,cp3./rhoe);
dB2dt = -(Bx.*dBx + By.*dBy + Bz.*dBz)/4/pi;

tmp=common_image(xcoord,zcoord,dB2dt(ir,jr,kr),AAz(ir,kr),['midZ'],'BcurlOHMdivPdev',[-1 1]*vnorm, radius,1);
%tmp=common_image_stream(xcoord,zcoord,squeeze(dB2dt(ir,jr,kr)).*window',Bx,By,Bz,['midZ'],'BcurlOHMdivPdev',[-1 1]*vnorm, radius,1);

OHMdivPdev= dB2dt(:,iy,kr);
OHMdivPdevy= dB2dt(ix,:,kr);

term_iner = term_E - term_ve - term_pe;
tmp=common_image(xcoord,zcoord,term_iner(ir,jr,kr),AAz(ir,kr),['midZ'],'BcurlOHMtotiner',[-1 1]*vnorm, radius,1);
%tmp=common_image_stream(xcoord,zcoord,squeeze(dB2dt(ir,jr,kr)).*window',Bx,By,Bz,['midZ'],'BcurlOHMtotiner',[-1 1]*vnorm, radius,1);


end
end

function [dp] = fluct(p)
p_avg=squeeze(p,3);
[Nx Ny Nz]=size(p);
dp=p;
for k=1:Nz
    dp(:,:,k)=p(:,:,k)-p_avg;
end
end

function [p_avg] = mean_z(p)
p_avg=squeeze(p,3);
end

