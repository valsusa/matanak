function V1 =  imgaussfilt3(V, radius,dummy,dymmy2)
if(radius ==0) 
    V1=V;
else
    V1=smooth3(V,'box',radius);
end    
end