%
% Energy plots in the XZ plane averaging on the whole range in y
%
!rm *.png
close all
addpath(genpath('../')); % Point to the directory where the iPic3D toolbox is
%dir='/data1/gianni/HRmaha3D3/vtk/'; %directory where the files are

addpath(genpath('../../ipic3d_toolbox'));

addpath(genpath('/Users/glapenta/Matlab/matanak'));

list_sim=["cfr_base" ;"cfr_stj" ;"cfr_ebi_j2"; "cfr_ebi_j4" ;"cfr_nbi"];

for lsim = 1:5

    sim_name=char(list_sim(lsim))
read_case

dir_out = ['/Users/glapenta/Data/compare/' sim_name '/']
unix(['mkdir ' dir_out])


qom=-64 

leggo='h5';

tiny = 1e-10;
% FRC
Lx=8;Ly=14;Lz=8
dx=Lx/Nx;
dy=Ly/Ny;
dz=Lz/Nz;

cyl = 0; % cartesian geometry

% Prepare string
ntime = num2str(cycle,'%06d');
ncycle = num2str(cycle,'%06d');
Ncycle = ncycle;


import_h5_binvtk 



savevtkvector_bin(Bx,By,Bz, [dir_out 'Bn' ncycle '.vtk'],'B',dx,dy,dz,0,0,0)
savevtkvector_bin(Ex,Ey,Ez, [dir_out 'En' ncycle '.vtk'],'E',dx,dy,dz,0,0,0)
savevtk_bin(rhoe, [dir_out 'rhoe' ncycle '.vtk'],'rhoe',dx,dy,dz,0,0,0)
savevtk_bin(rhoe, [dir_out 'rhoi' ncycle '.vtk'],'rhoi',dx,dy,dz,0,0,0)
savevtkvector(Jex,Jey,Jez, [dir_out ' Je' ncycle '.vtk'],'Je',dx,dy,dz,0,0,0)

savevtkvector_bin(Jex./(rhoe-tiny),Jey./(rhoe-tiny),Jez./(rhoe-tiny), [dir_out ' Veraw' ncycle '.vtk'],'Veraw',dx,dy,dz,0,0,0)
savevtkvector_bin(Jix./(rhoi+tiny),Jiy./(rhoi+tiny),Jiz./(rhoi+tiny), [dir_out ' Viraw' ncycle '.vtk'],'Viraw',dx,dy,dz,0,0,0)

end