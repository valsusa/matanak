
addpath(genpath('/Users/glapenta/Matlab/matanak'));


dir='/Users/glapenta/Sources/ECsim-Hi/build/data/';
case_name='FRC3D'

for cycle=0:10:400
qom=-64 

Lx=8;Ly=64;Lz=8


% Prepare string
ntime = num2str(cycle,'%06d');
ncycle = num2str(cycle,'%06d');
leggo='h5';

import_h5_binvtk  

dx=Lx/Nx;
dy=Ly/Ny;
dz=Lz/Nz;

savevtkvector_bin(Bx,By,Bz, [dir 'B' ncycle '.vtk'],'B',dx,dy,dz,0,0,0)
savevtkvector_bin(Ex,Ey,Ez, [dir 'E' ncycle '.vtk'],'E',dx,dy,dz,0,0,0)
savevtk_bin(rhoe, [dir 'rhoe' ncycle '.vtk'],'rhoe',dx,dy,dz,0,0,0)

[X3 Y3 Z3] = meshgrid(0:dx:Lx-dx,0:dy:Ly-dy,0:dz:Lz-dz);

Np=50
TH = 2*pi*rand(1,Np);
PH = asin(-1+2*rand(1,Np));
R = rand(1,Np)*2;
[startx,starty,startz] = sph2cart(TH,PH,R);

startx=startx+Lx/2;
starty=starty+Ly/8;
startz=startz+Lz/2;
%plot3(startx,starty,startz,'.','markersize',1)
h1 = streamline(X3,Y3,Z3,permute(Bx,[2 1 3]),permute(By,[2 1 3]),permute(Bz,[2 1 3]),startx,starty,startz);view(3)
h2 = streamline(X3,Y3,Z3,-permute(Bx,[2 1 3]),-permute(By,[2 1 3]),-permute(Bz,[2 1 3]),startx,starty,startz);view(3)
axis equal 
set(h1,'Color', [0 0 0, 0.2])
set(h2,'Color', [0 0 0, 0.2])
print('-dpng','-r300',[dir 'magnetic_lines' ncycle '.png'])

[X Y] = meshgrid(0:dx:Lx-dx,0:dy:Ly-dy);
iz=round(Nz/2);

Ncycle=ncycle
global color_choice symmetric_color labelx labely labelc reversex Ncycle dir Lx Ly Lz dx dy dz

color_choice = 1
symmetric_color = 1
tmp=common_image_stream(X,Y,rhoe(:,:,iz),Bx,By,Bz,['midZ'],'rhoe',[-1 1]*0, 3,1);
tmp=common_image_stream(X,Y,Ex(:,:,iz),Bx,By,Bz,['midZ'],'Ex',[-1 1]*0, 3,1);


end