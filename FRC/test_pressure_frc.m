close all
addpath(genpath('/Users/glapenta/Matlab/matanak'));

list_sim=["cfr_base" ;"cfr_stj" ;"cfr_ebi_j2"; "cfr_ebi_j4" ;"cfr_nbi"];

for lsim = 1:5

    sim_name=char(list_sim(lsim))
read_case

dir_out = ['/Users/glapenta/Data/compare/' sim_name '/']
unix(['mkdir ' dir_out])


ncycle=num2str(cycle)
qom=-64 

leggo='h5'

import_h5_binvtk

% FRC
Lx=8;Ly=14;Lz=8
xr=[Lx/5 Lx-Lx/5];
yr=[Ly/10 Ly-Ly/10];

dx=Lx/Nx;
dy=Ly/Ny;
dz=Lz/Nz;

[X Y] = meshgrid(0:dx:Lx-dx,0:dy:Ly-dy);
[X3 Y3 Z3] = ndgrid(0:dx:Lx-dx,0:dy:Ly-dy,0:dz:Lz-dz);
[X2 Z2] = meshgrid(0:dx:Lx-dx,0:dz:Lz-dz);


ir=round(xr./dx);ir=min(ir):max(ir);
jr=round(yr./dy);jr=min(jr):max(jr);
iz=round(Nz/2);

color_choice=1
global color_choice symmetric_color labelx labely Ncycle Lx Ly Lz dx dy dz dir_out

tiny = 1e-10;
Nsm=0;
labelx='x/d_i';
labely='y/d_i';
Ncycle =num2str(cycle);

VexbX = (Ey.*Bz - Ez.*By)./(B.^2+tiny);
VexbY = (Ez.*Bx - Ex.*Bz)./(B.^2+tiny);
VexbZ = (Ex.*By - Ey.*Bx)./(B.^2+tiny);

Va=B./sqrt(4*pi.*rhoi);


Vex= Jex./(rhoe-tiny) ;
Vey= Jey./(rhoe-tiny) ;
Vez= Jez./(rhoe-tiny) ;

Je=sqrt(Jex.^2+Jey.^2+Jez.^2);

Vpar= (Vex.*Bx+Vey.*By+Vez.*Bz)./(B +tiny) ;

Veperpx= Vex - Vpar .*Bx./(B +tiny);
Veperpy= Vey - Vpar .*By./(B +tiny);
Veperpz= Vez - Vpar .*Bz./(B +tiny);

Epar = (Ex.*Bx+ Ey.*By+ Ez.*Bz)./(B +tiny);

Epx = Ex + (Vey.*Bz - Vez.*By);
Epy = Ey + (Vez.*Bx - Vex.*Bz);
Epz = Ez + (Vex.*By - Vey.*Bx);

JdotEp=(Jex+Jix).*Epx + (Jey+Jiy).*Epy + (Jez+Jiz).*Epz;


xc=linspace(0, Lx, Nx);
yc=linspace(0, Ly, Ny);
AAz=vecpot(xc,yc,Bx(:,:,iz),By(:,:,iz));
Nsm=0;
radius=5

V1=imgaussfilt3(Vex,radius);V1=V1(:,:,iz);
V2=imgaussfilt3(Vey,radius);V2=V2(:,:,iz);
Psiz=vecpot(xc,yc,V1,V2);



color_choice=3
symmetric_color=1

agyro_write=false;
agyro= false
radial_v = false
currents= false
external_fields = false

figure(1)
p=(Pexx+Peyy+Pezz+Pixx+Piyy+Pizz)/3;
pB=B.^2/8/pi;

subplot(2,2,1)
tmp=imagesc([0 Lx],[0 Lz],log10(squeeze(pB(:,round(Ny/2),:))));colorbar
xlabel('x')
ylabel('z')
title('pB=B^2/8\pi')
subplot(2,2,2)
tmp=imagesc([0 Lx],[0 Lz],log10(squeeze(tiny+p(:,round(Ny/2),:))));colorbar
xlabel('x')
ylabel('z')
title('p=(Tr(Pe+Pi)/3')
subplot(2,2,3)
plot(X2(1,:),p(:,round(Ny/2),round(Nz/2)),X2(1,:),pB(:,round(Ny/2),round(Nz/2)),X2(1,:),pB(:,round(Ny/2),round(Nz/2))+p(:,round(Ny/2),round(Nz/2)))
xlabel('x')
legend('p','pB','p+pB')
subplot(2,2,4)
plot(Y(:,1),p(round(Nx/2),:,round(Nz/2)),Y(:,1),pB(round(Nx/2),:,round(Nz/2)),Y(:,1),pB(round(Nx/2),:,round(Nz/2))+p(round(Nx/2),:,round(Nz/2)))
xlabel('y')
legend('p','pB','p+pB')
print('-dpng','pressure_with4pi.png')

figure(2)
p=(Pexx+Peyy+Pezz+Pixx+Piyy+Pizz)/3;
pB=B.^2/2;

subplot(2,2,1)
tmp=imagesc([0 Lx],[0 Lz],log10(squeeze(pB(:,round(Ny/2),:))));colorbar
xlabel('x')
ylabel('z')
title('pB=B^2/2')
subplot(2,2,2)
tmp=imagesc([0 Lx],[0 Lz],log10(squeeze(tiny+p(:,round(Ny/2),:))));colorbar
xlabel('x')
ylabel('z')
title('p=(Tr(Pe+Pi)/3')
subplot(2,2,3)
plot(X2(1,:),p(:,round(Ny/2),round(Nz/2)),X2(1,:),pB(:,round(Ny/2),round(Nz/2)),X2(1,:),pB(:,round(Ny/2),round(Nz/2))+p(:,round(Ny/2),round(Nz/2)))
xlabel('x')
legend('p','pB','p+pB')
subplot(2,2,4)
plot(Y(:,1),p(round(Nx/2),:,round(Nz/2)),Y(:,1),pB(round(Nx/2),:,round(Nz/2)),Y(:,1),pB(round(Nx/2),:,round(Nz/2))+p(round(Nx/2),:,round(Nz/2)))
xlabel('y')
legend('p','pB','p+pB')

print('-dpng','pressure_without4pi.png')
if(external_fields) 
tmp=common_image_stream(X(jr,ir),Y(jr,ir),Ex_ext(:,:,iz), Bx,By, Bz,'E_{x,ext}','Exext',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),Ey_ext(:,:,iz), Bx,By, Bz,'E_{y,ext}','Eyext',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),Ez_ext(:,:,iz), Bx,By, Bz,'E_{z,ext}','Ezext',[-1 1]*0, Nsm, 1);

[cp1,cp2,cp3] = magnetic_coordinates_axisy(Ex_ext,Ey_ext, Ez_ext, Bx, By, Bz); 

tmp=common_image_stream(X(jr,ir),Y(jr,ir),cp1(:,:,iz), Bx,By, Bz,'E_{||,ext}','Eparext',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),cp2(:,:,iz), Bx,By, Bz,'E_{\perp1,ext}','Eperp1ext',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),cp3(:,:,iz), Bx,By, Bz,'E_{\perp2,ext}','Eperp2ext',[-1 1]*0, Nsm, 1);

end


if(currents)

cp1=imgaussfilt3(Jex-rhoe.*VexbX,radius);V1=cp1(:,:,iz);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),V1, Bx,By, Bz,'(J_e-J_{ExB})_x','JexbX',[-1 1]*0, Nsm, 1);

cp2=imgaussfilt3(Jey-rhoe.*VexbY,radius);V2=cp2(:,:,iz);
tmp=common_image_due(X(jr,ir),Y(jr,ir),V2, AAz(ir,jr),Psiz(ir,jr),'(J_e-J_{ExB})_y','JexbY',[-1 1]*0, Nsm, 2);

cp3=imgaussfilt3(Jez-rhoe.*VexbZ,radius);V3=cp3(:,:,iz);
tmp=common_image_due(X(jr,ir),Y(jr,ir),V3, AAz(ir,jr),Psiz(ir,jr),'(J_e-J_{ExB})_z','JexbZ',[-1 1]*0, Nsm, 3);

[cp1,cp2,cp3] = magnetic_coordinates_axisy(cp1,cp2, cp3, Bx, By, Bz); 
tmp=common_image_stream(X(jr,ir),Y(jr,ir),cp1(:,:,iz), Bx,By, Bz,'(J_e-J_{ExB})_{||}','Jexbpar',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),cp2(:,:,iz), Bx,By, Bz,'(J_e-J_{ExB})_{\perp1}','Jexbperp1',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),cp3(:,:,iz), Bx,By, Bz,'(J_e-J_{ExB})_{\perp2}','Jexbpep2',[-1 1]*0, Nsm, 1);


cp1=imgaussfilt3((Jex-rhoe.*VexbX)./(rhoe-tiny),radius);V1=cp1(:,:,iz);
tmp=common_image_due(X(jr,ir),Y(jr,ir),V1, AAz(ir,jr),Psiz(ir,jr),'(V_e-V_{ExB})_x','VexbX',[-1 1]*0, Nsm, 11);

cp2=imgaussfilt3((Jey-rhoe.*VexbY)./(rhoe-tiny),radius);V1=cp2(:,:,iz);
tmp=common_image_due(X(jr,ir),Y(jr,ir),V1, AAz(ir,jr),Psiz(ir,jr),'(V_e-V_{ExB})_y','VexbY',[-1 1]*0, Nsm, 12);

cp3=imgaussfilt3((Jez-rhoe.*VexbZ)./(rhoe-tiny),radius);V1=cp3(:,:,iz);
tmp=common_image_due(X(jr,ir),Y(jr,ir),V1, AAz(ir,jr),Psiz(ir,jr),'(V_e-V_{ExB})_z','VexbZ',[-1 1]*0, Nsm, 13);

[cp1,cp2,cp3] = magnetic_coordinates_axisy(cp1,cp2, cp3, Bx, By, Bz); 
tmp=common_image_stream(X(jr,ir),Y(jr,ir),cp1(:,:,iz), Bx,By, Bz,'(V_e-V_{ExB})_{||}','Vexbpar',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),cp2(:,:,iz), Bx,By, Bz,'(V_e-V_{ExB})_{\perp1}','Vexbperp1',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),cp3(:,:,iz), Bx,By, Bz,'(V_e-V_{ExB})_{\perp2}','Vexbpep2',[-1 1]*0, Nsm, 1);

cp1=imgaussfilt3(Jex./(rhoe-tiny),radius);V1=cp1(:,:,iz);
tmp=common_image_due(X(jr,ir),Y(jr,ir),V1, AAz(ir,jr),Psiz(ir,jr),'Vex','Vex',[-1 1]*0, Nsm, 14);
cp2=imgaussfilt3(Jey./(rhoe-tiny),radius);V1=cp2(:,:,iz);
tmp=common_image_due(X(jr,ir),Y(jr,ir),V1, AAz(ir,jr),Psiz(ir,jr),'Vey','Vey',[-1 1]*0, Nsm, 15);
cp3=imgaussfilt3(Jez./(rhoe-tiny),radius);V1=cp3(:,:,iz);
tmp=common_image_due(X(jr,ir),Y(jr,ir),V1, AAz(ir,jr),Psiz(ir,jr),'Vez','Vez',[-1 1]*0, Nsm, 16);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),cp1(:,:,iz), Bx,By, Bz,'V_e_{x}','Vex',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),cp2(:,:,iz), Bx,By, Bz,'V_e_{y}','Vey',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),cp3(:,:,iz), Bx,By, Bz,'V_e_{z}','Vez',[-1 1]*0, Nsm, 1);
savevtkvector_bin(cp1,cp2,cp3, [dir_out Ncycle 'Ve.vtk'],'Ve',dx,dy,dz,0,0,0)

[cp1,cp2,cp3] = magnetic_coordinates_axisy(cp1,cp2, cp3, Bx, By, Bz); 
tmp=common_image_stream(X(jr,ir),Y(jr,ir),cp1(:,:,iz), Bx,By, Bz,'V_e_{||}','Vepar',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),cp2(:,:,iz), Bx,By, Bz,'V_e_{\perp1}','Veperp1',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),cp3(:,:,iz), Bx,By, Bz,'V_e_{\perp2}','Veperp2',[-1 1]*0, Nsm, 1);


cp1=imgaussfilt3(Jex,radius);V1=cp1(:,:,iz);
%tmp=common_image_due(X(jr,ir),Y(jr,ir),V1, AAz(ir,jr),Psiz(ir,jr),'Vex','Vex',[-1 1]*0, Nsm, 14);
cp2=imgaussfilt3(Jey,radius);V1=cp2(:,:,iz);
%tmp=common_image_due(X(jr,ir),Y(jr,ir),V1, AAz(ir,jr),Psiz(ir,jr),'Vey','Vey',[-1 1]*0, Nsm, 15);
cp3=imgaussfilt3(Jez,radius);V1=cp3(:,:,iz);
%tmp=common_image_due(X(jr,ir),Y(jr,ir),V1, AAz(ir,jr),Psiz(ir,jr),'Vez','Vez',[-1 1]*0, Nsm, 16);

tmp=common_image_stream(X(jr,ir),Y(jr,ir),cp1(:,:,iz), Bx,By, Bz,'J_e_{x}','Jex',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),cp2(:,:,iz), Bx,By, Bz,'J_e_{y}','Jey',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),cp3(:,:,iz), Bx,By, Bz,'J_e_{z}','Jez',[-1 1]*0, Nsm, 1);
savevtkvector_bin(cp1,cp2,cp3, [dir_out Ncycle 'Je.vtk'],'Je',dx,dy,dz,0,0,0)

[cp1,cp2,cp3] = magnetic_coordinates_axisy(cp1,cp2, cp3, Bx, By, Bz); 
tmp=common_image_stream(X(jr,ir),Y(jr,ir),cp1(:,:,iz), Bx,By, Bz,'J_e_{||}','Jepar',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),cp2(:,:,iz), Bx,By, Bz,'J_e_{\perp1}','Jeperp1',[-1 1]*0, Nsm, 1);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),cp3(:,:,iz), Bx,By, Bz,'J_e_{\perp2}','Jeperp2',[-1 1]*0, Nsm, 1);


%V1=imgaussfilt3(Epar,radius);V1=V1(:,:,iz);
%tmp=common_image_due(X(jr,ir),Y(jr,ir),V1, AAz(ir,jr),Psiz(ir,jr),'Epar','Epar',[-1 1]*0, Nsm, 16);
V1=imgaussfilt3(JdotEp,radius);V1=V1(:,:,iz);
tmp=common_image_due(X(jr,ir),Y(jr,ir),V1, AAz(ir,jr),Psiz(ir,jr),'JEp','JEp',[-1 1]*0, Nsm, 16);

symmetric_color=1
color_choice=5
V1=imgaussfilt3(Je,radius);V1=V1(:,:,iz);
tmp=common_image_due(X(jr,ir),Y(jr,ir),V1, AAz(ir,jr),Psiz(ir,jr),'Je','Je',[-1 1]*0, Nsm, 4);

end



if(radial_v)
symmetric_color=1
color_choice=3

ii=abs(Y3-Ly/2)<Lz/5;
cfrcx=mean(X3(ii).*rhoi(ii))./mean(rhoi(ii));
cfrcz=mean(Z3(ii).*rhoi(ii))./mean(rhoi(ii));
X3=X3-cfrcx;
Z3=Z3-cfrcz;
theta=atan2(Z3,X3);
R3=sqrt(X3.^2+Z3.^2);


Ver=(cos(theta).*Jex+sin(theta).*Jez)./(rhoe-1e-10);
V1=imgaussfilt3(Ver,radius);
V1=V1(:,:,iz);
tmp=common_image_due(X(jr,ir),Y(jr,ir),V1, AAz(ir,jr),Psiz(ir,jr),'Ver','Ver',[-1 1]*1e-3, Nsm, 4);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),V1, Bx,By, Bz,'Ver','Ver',[-1 1]*1e-3, Nsm, 1);

Veth=(-sin(theta).*Jex+cos(theta).*Jez)./(rhoe-1e-10);
V1=imgaussfilt3(Veth,radius);
V1=V1(:,:,iz);
tmp=common_image_due(X(jr,ir),Y(jr,ir),V1, AAz(ir,jr),Psiz(ir,jr),'Veth','Veth',[-1 1]*3e-3, Nsm, 4);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),V1, Bx,By, Bz,'Veth','Veth',[-1 1]*3e-3, Nsm, 1);

Vir=(cos(theta).*Jix+sin(theta).*Jiz)./(rhoi+1e-10);
V1=imgaussfilt3(Vir,radius);
V1=V1(:,:,iz);
tmp=common_image_due(X(jr,ir),Y(jr,ir),V1, AAz(ir,jr),Psiz(ir,jr),'Vir','Vir',[-1 1]*3e-4, Nsm, 4);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),V1, Bx,By, Bz,'Vir','Vir',[-1 1]*3e-4, Nsm, 1);

Vith=(-sin(theta).*Jix+cos(theta).*Jiz)./(rhoi+1e-10);
V1=imgaussfilt3(Vith,radius);
V1=V1(:,:,iz);
tmp=common_image_due(X(jr,ir),Y(jr,ir),V1, AAz(ir,jr),Psiz(ir,jr),'Vith','Vith',[-1 1]*6e-4, Nsm, 4);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),V1, Bx,By, Bz,'Vith','Vith',[-1 1]*6e-4, Nsm, 1);

end


if(agyro)
if(agyro_write)    
    [Agyro,Agyro_aunai,Nongyro_swisdak]=compute_agyro(Bx, By, Bz, imgaussfilt3(Pexx,radius), ...
    imgaussfilt3(Pexy,radius), imgaussfilt3(Pexz,radius), imgaussfilt3(Peyy,radius),  ...
    imgaussfilt3(Peyz,radius), imgaussfilt3(Pezz,radius) ) ;
    opath=[dir_out 'Agyro' ncycle '.h5']; 
h5create(opath,'/Step#0/Block/SmAgyro/0',[Nx, Ny, Nz]);
h5write(opath,'/Step#0/Block/SmAgyro/0',real(Agyro))
h5create(opath,'/Step#0/Block/SmAgyro_aunai/0',[Nx, Ny, Nz]);
h5write(opath,'/Step#0/Block/SmAgyro_aunai/0',real(Agyro_aunai))
h5create(opath,'/Step#0/Block/SmNongyro_swisdak/0',[Nx, Ny, Nz]);
h5write(opath,'/Step#0/Block/SmNongyro_swisdak/0',real(Nongyro_swisdak))
savevtk_bin(real(Agyro).*Je, 'Agyro.vtk','Agyro',dx,dy,dz,0,0,0)
savevtkvector_bin(Bx,By,Bz, 'B.vtk','B',dx,dy,dz,0,0,0)
else
    opath=[dir_out 'Agyro' ncycle '.h5'];
    Agyro_aunai=hdf5read(opath,'/Step#0/Block/SmAgyro_aunai/0/');
    Agyro=hdf5read(opath,'/Step#0/Block/SmAgyro/0/');
    Nongyro_swisdak=hdf5read(opath,'/Step#0/Block/SmNongyro_swisdak/0/');
end
symmetric_color=0
color_choice=5
radius=1
V1=imgaussfilt3(real(Agyro),radius);V1=V1(:,:,iz);
tmp=common_image_due(X(jr,ir),Y(jr,ir),V1, AAz(ir,jr),Psiz(ir,jr),'Agyro','Agyro',[0 .4], Nsm, 5);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),V1,Bx,By,Bz,['midZ'],'Agyro',[0 .4], radius,1);

V1=imgaussfilt3(real(Agyro).*abs(rhoe),radius);V1=V1(:,:,iz);
tmp=common_image_due(X(jr,ir),Y(jr,ir),V1, AAz(ir,jr),Psiz(ir,jr),'AgyroW','AgyroW',[0 0], Nsm, 6);
tmp=common_image_stream(X(jr,ir),Y(jr,ir),V1,Bx,By,Bz,['midZ'],'AgyroW',[0 0], radius,1);

xcoord = (X(jr,ir));
ycoord = (Y(jr,ir));
tmp=common_image_stream(xcoord,ycoord,real(Agyro(ir,jr,iz)),Bx,By,Bz,['midZ'],'Agyro',[0 .4], radius,1);
tmp=common_image_stream(xcoord,ycoord,real(Agyro(ir,jr,iz)).*Je(ir,jr,iz),Bx,By,Bz,['midZ'],'AgyroW',[0 0], radius,1);
tmp=common_image_stream(xcoord,ycoord,rhoe(ir,jr,iz),Bx,By,Bz,['midZ'],'Rhoe',[0 0], radius,1);
end

%system(['mkdir ' ncycle])
%system(['mv *.png ' ncycle])

end
return

[divPx]=divergence(X,Y,permute(Pexx,[2 1]),permute(Pexy,[2 1 ])); % Valid only because dx=dy=dz
[divPy]=divergence(X,Y,permute(Pexy,[2 1]),permute(Peyy,[2 1 ])); % Valid only because dx=dy=dz
[divPz]=divergence(X,Y,permute(Pexz,[2 1]),permute(Peyz,[2 1 ])); % Valid only because dx=dy=dz

divPx=permute(divPx,[2 1])./rhoe;
divPy=permute(divPy,[2 1])./rhoe;
divPz=permute(divPz,[2 1])./rhoe;

VddX = -(divPx.*Bz - divPz.*By)./B.^2;
VddY = -(divPz.*Bx - divPx.*Bz)./B.^2;
VddZ = -(divPx.*By - divPy.*Bx)./B.^2;

color_choice=-1
tmp=common_image_due(X(jr,ir),Y(jr,ir),VddZ(ir,jr), Az(ir,jr),Psiz(ir,jr),'V_{dd}_Z','VdZ',[-1 1]*Vnorm, Nsm, 2);
tmp=common_image_due(X(jr,ir),Y(jr,ir),Veperpz(ir,jr), Az(ir,jr),Psiz(ir,jr),'V_{\perp Z}','VeZ',[-1 1]*Vnorm, Nsm, 4);



tmp=common_image_due(X(jr,ir),Y(jr,ir),Ex(ir,jr), Az(ir,jr),Psiz(ir,jr),'E_{X}','Ex',[-1 1]*0, Nsm, 6);


figure(100)
jxpt=1594;
jband=jxpt-5:jxpt+5
%jband=jxpt;

h(1) = subplot(2,1,1); % upper plot
plot(xc(:),mean(Veperpz(:,jband),2),xc(:),mean(VexbZ(:,jband),2),xc(:),mean(VddZ(:,jband),2))
xlim([12 12.5]);
title(['Y=' num2str(mean(yc(jband)))])
legend('V_{e\perp z}','V_{ExBz}','V_{DDz}')
h(2) = subplot(2,1,2); % lower plot
plot(xc(:),mean(Vex(:,jband),2),xc(:),mean(VddX(:,jband)+VexbX(:,jband),2),xc(:),mean(Ex(:,jband),2),xc(:),mean(By(:,jband),2)/10);
xlim([12 12.5]);
legend('V_{ex}','V_{ExBx}+V_{DDx}',' E_x','By/10','location','SouthWest')
grid on

linkaxes(h,'x'); % link the axes in x direction (just for convenience)
xlim([12 12.5]);
set(h(1),'xticklabel',[]);
pos=get(h,'position');
bottom=pos{2}(2);
top=pos{1}(2)+pos{1}(4);
plotspace=top-bottom;
pos{2}(4)=plotspace/2;
pos{1}(4)=plotspace/2;
pos{1}(2)=bottom+plotspace/2;

set(h(1),'position',pos{1});
set(h(2),'position',pos{2});

print -dpng flythroughs

h=figure(101)
set(h, 'Position', [167 26 515 776])
contour(xc(ir),yc(jr),Psiz(ir,jr)',30,'m')
hold on
contour(xc(ir),yc(jr),Az(ir,jr)',30,'k')
contour(xc(ir),yc(jr),Psiz(ir,jr)',30,'m')

print -dpng contours