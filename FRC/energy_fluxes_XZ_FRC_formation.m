%
% Energy plots in the XZ plane averaging on a partial range in Y (code coordinates)
%

close all
addpath(genpath('/Users/glapenta/Matlab/matanak'));

list_sim=["cfr_inj_decay"; "cfr_base" ;"cfr_stj" ;"cfr_ebi_j2"; "cfr_ebi_j4" ;"cfr_nbi"];

list_sim=["N1_194"; "N8_194"; "N8_197"]

list_sim =["2X-8NBI-ebeam-57k"; "2X-8NBI-ebeam-43k";"2X-8NBI-ebeam-37k"]
list_sim =["2X-8NBI-ebeam"; "0X-NBI-ebeam";"8NBI-ebeam"]
list_sim = "FRC-CD"
for lsim = 1:1

    sim_name=char(list_sim(lsim))
read_case

%dir_out = ['/Users/glapenta/Data/compare_nov4/energy/' sim_name '/']
dir_out = ['/Users/glapenta/Data/2X-8NBI-ebeam/' sim_name '/']
dir_out = ['/Users/glapenta/Data/FRC3D-NBI-ebeam/analisi57/' sim_name '/']
dir_out = [dir 'analisi_matlab/']
cycle=35000

unix(['mkdir ' dir_out])
global dir_out

qom=-64 

ncycle=num2str(cycle,'%06i\n')

leggo='h5'

import_h5_binvtk

% FRC
tiny = 1e-10;
Lx=8;Ly=14;Lz=8
xr=[Lx/5 Lx-Lx/5];
yr=[Ly/10 Ly-Ly/10];

Xgsmrange= [0 Lx];
Zgsmrange= [0 Ly];
Ygsmrange= [0 Lz];
global Lx Ly Lz Xgsmrange Ygsmrange Zgsmrange dx dy dz XLEN YLEN ZLEN initial_time Nx Ny Nz Dt dir

dx=Lx/(Nx-1);
dy=Ly/(Ny-1);
dz=Lz/(Nz-1);
e=1.6e-19;
mu0 = 4*pi*1e-7;
code_E = 1;
code_B = 1;
%code_B=code_B *1e9; % to convert from Tesla to nT
code_J = 1;
%code_J = code_J*4*pi;
%code_J = code_J*1e9; % to convert to nA/m^2
code_V = 1;
%code_V=code_V/1e3; %to convert to Km/s
code_T = 1;
code_n = 1;
code_dp=1;

[X Y] = meshgrid(0:dx:Lx,0:dy:Ly);
[X3 Y3 Z3] = ndgrid(0:dx:Lx,0:dy:Ly,0:dz:Lz);
[X2 Z2] = meshgrid(0:dx:Lx-dx,0:dz:Lz-dz);



external_fields= true;
poynting=true;
electrons=true;
ions=true;
saveVTK=false;


cyl = 0; % cartesian geometry

% Prepare string
ntime = num2str(cycle,'%06d');
ncycle = num2str(cycle,'%06d');



[X Z] = meshgrid(0:dx:Lx,0:dz:Lz);

bufferX=round(Nx/20);
bufferY=round(Ny/2.5);
bufferZ=round(Nz/10);
ir=bufferX:Nx-bufferX;

jr=round(Ny/2)+(-3:3);
kr=bufferZ:Nz-bufferZ;

radius=5; %radius=4






global color_choice symmetric_color labelx labely labelc reversex reversey Ncycle skip

reversey=0;
symmetric_color=1;
color_choice =3;


labelx ='x/d_i';
labely ='z/d_i';
labelc_flux = 'c.u.';
labelc_power = 'c.u.';
signx = 1;
Wm3 = 1; 
nWm3 = 1;
mWm2= 1;

skip=2;


% Call the heavy lifting
energy_workhorse


%
% Radial coordinates
%
ii=abs(Y3-Ly/2)<Ly/5;
cfrcx=mean(X3(ii).*rhoi(ii))./mean(rhoi(ii));
cfrcz=mean(Z3(ii).*rhoi(ii))./mean(rhoi(ii));
X3=X3-cfrcx;
Z3=Z3-cfrcz;
theta=atan2(Z3,X3);
R3=sqrt(X3.^2+Z3.^2);
[Rcg,Tcg]=ndgrid(linspace(0,min([Lx Lz])/2*3/4,50),linspace(0,2*pi,100));
Xcg=cos(Tcg).*Rcg;
Zcg=sin(Tcg).*Rcg;
Ycg=ones(size(Rcg))*Ly/2;
%
%Define velocities on the 2D plane of plotting
%
Vex=smooth3(Vex,'box',radius);
Vey=smooth3(Vey,'box',radius);
Vez=smooth3(Vez,'box',radius);
Ver=(cos(theta).*Jex+sin(theta).*Jez)./(rhoe-tiny);
Veth=(-sin(theta).*Jex+cos(theta).*Jez)./(rhoe-tiny);

Vex=signx*squeeze(mean(Vex(:,jr,:),2));
Vey=squeeze(mean(Vey(:,jr,:),2));
Vez=squeeze(mean(Vez(:,jr,:),2));
%AAze=vecpot(xc,zc,Vex,Vez);

tmp=common_image_vel(X,Z,Vex,Vex,Vez ,'midZ','Vex',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,Vey,Vex,Vez ,'midZ','Vey',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,Vez,Vex,Vez ,'midZ','Vez',[-1 1]*0e-9, radius, 2);


tmp=common_image_vel(X,Z,mean(Ver(:,jr,:),2),Vex,Vez ,'midZ','Ver',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(Veth(:,jr,:),2),Vex,Vez ,'midZ','Veth',[-1 1]*0e-9, radius, 2);

Ver_cg=interpn(X3,Y3,Z3,Ver,Xcg,Ycg,Zcg);
Veth_cg=interpn(X3,Y3,Z3,Veth,Xcg,Ycg,Zcg);
tmp=common_image_vel_sum(Tcg,Rcg,Ver_cg',Veth_cg',Ver_cg' ,'midZ','Ver_cg',[-1 1]*0e-9, radius, 2);

Vir=(cos(theta).*Vix+sin(theta).*Viz);
Vith=(-sin(theta).*Vix+cos(theta).*Viz);
Vix=signx*imgaussfilt(squeeze(mean(Vix(:,jr,:),2)),radius);
Viy=imgaussfilt(squeeze(mean(Viy(:,jr,:),2)),radius);
Viz=imgaussfilt(squeeze(mean(Viz(:,jr,:),2)),radius);
%AAzi=vecpot(xc,zc,Vix,Viz);

tmp=common_image_vel(X,Z,Vix,Vix,Viz ,'midZ','Vix',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,Viy,Vix,Viz ,'midZ','Viy',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,Viz,Vix,Viz ,'midZ','Viz',[-1 1]*0e-9, radius, 2);


tmp=common_image_vel(X,Z,mean(Vir(:,jr,:),2),Vix,Viz ,'midZ','Vir',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(Vith(:,jr,:),2),Vix,Viz ,'midZ','Vith',[-1 1]*0e-9, radius, 2);

Vir_cg=interpn(X3,Y3,Z3,Vir,Xcg,Ycg,Zcg);
Vith_cg=interpn(X3,Y3,Z3,Vith,Xcg,Ycg,Zcg);
tmp=common_image_vel_sum(Tcg,Rcg,Vir_cg',Vith_cg',Vir_cg' ,'midZ','Vir_cg',[-1 1]*0e-9, radius, 2);



%
% External Fields
%

if(external_fields) 
    
cp1=imgaussfilt3(Ex,radius);

cp2=imgaussfilt3(Ey,radius);

cp3=imgaussfilt3(Ez,radius);

tmp=common_image_vel(X,Z,mean(cp1(:,jr,:),2),Vex,Vez ,'midZ','Ex',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(cp2(:,jr,:),2),Vex,Vez ,'midZ','Ey',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(cp3(:,jr,:),2),Vex,Vez ,'midZ','Ey',[-1 1]*0e-9, radius, 2);


[cp1,cp2,cp3] = magnetic_coordinates_axisy(Ex,Ey, Ez, Bx, By, Bz); 
cp1=imgaussfilt3(cp1,radius);
cp2=imgaussfilt3(cp2,radius);
cp3=imgaussfilt3(cp3,radius);

tmp=common_image_vel(X,Z,mean(cp1(:,jr,:),2),Vex,Vez ,'midZ','Epar',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(cp2(:,jr,:),2),Vex,Vez ,'midZ','Eperp1',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(cp3(:,jr,:),2),Vex,Vez ,'midZ','Eperp2',[-1 1]*0e-9, radius, 2);

Epar_cg=interpn(X3,Y3,Z3,cp1,Xcg,Ycg,Zcg);
tmp=common_image_vel_sum(Tcg,Rcg,Epar_cg',Veth_cg',Ver_cg' ,'midZ','Epar_cg',[-1 1]*0e-9, radius, 2);
Eperp1_cg=interpn(X3,Y3,Z3,cp2,Xcg,Ycg,Zcg);
tmp=common_image_vel_sum(Tcg,Rcg,Eperp1_cg',Veth_cg',Ver_cg' ,'midZ','Eperp1_cg',[-1 1]*0e-9, radius, 2);
Eperp2_cg=interpn(X3,Y3,Z3,cp3,Xcg,Ycg,Zcg);
tmp=common_image_vel_sum(Tcg,Rcg,Eperp2_cg',Veth_cg',Ver_cg' ,'midZ','Eperp2_cg',[-1 1]*0e-9, radius, 2);

end


%
%Elm energy
%
if(poynting)
Sx=Sx/4/pi;
Sy=Sy/4/pi;
Sz=Sz/4/pi;
divS = divS*nWm3;
% the poynting flux is not in W/m^2 that is in SI unit.
%
% I verified that if instead one computes from code units, then divides it
% by 4pi and rescale it with mWm2 like all other fluxes the result is
% identical.
%

%
% n, J and p from the code need to be multiplied by 4pi and then
% renormalized because of the 4pi division in the code from the MHD density
% By the same token, all particle energy fluxes need the 4 pi
% multiplication, but not the Poynting flux that is based on the fields.
%



labelc = labelc_power;
tmp=common_image_vel(X,Z,mean(JdotE(:,jr,:),2)*nWm3,Vex,Vez,'midZ','JE',[-1 1]*0e-10, radius,1);
tmp=common_image_vel(X,Z,mean(JdotEp(:,jr,:),2)*nWm3,Vex,Vez,'midZ','JEp',[-1 1]*0e-10, radius,1);
tmp=common_image_vel(X,Z,mean(divS(:,jr,:),2),Vex,Vez ,'midZ','divS',[-1 1]*0e-9, radius, 4);


% The poynting flux is in SI units, W/m^3 so we need multiplication by 1e3
% to have it in mW/m^2
labelc = labelc_flux;
tmp=common_image_vel(X,Z,signx*mean(Sx(:,jr,:),2),Vex,Vez ,'midZ','Sx',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(Sy(:,jr,:),2),Vex,Vez , 'midZ','Sy',[-1 1]*0e-9, radius, 3);
tmp=common_image_vel(X,Z,mean(Sz(:,jr,:),2),Vex,Vez ,'midZ','Sz',[-1 1]*0e-9, radius, 4);

Sr=(cos(theta).*Sx+sin(theta).*Sz);
tmp=common_image_vel(X,Z,mean(Sr(:,jr,:),2),Vex,Vez ,'midZ','Sr',[-1 1]*0e-9, radius, 2);
Sth=(-sin(theta).*Sx+cos(theta).*Sz);
tmp=common_image_vel(X,Z,mean(Sth(:,jr,:),2),Vex,Vez ,'midZ','Sth',[-1 1]*0e-9, radius, 2);

Sperp1=(By.*Sx-Bx.*Sy)./B2D;
tmp=common_image_vel(X,Z,mean(Sperp1(:,jr,:),2),Vex,Vez , 'midZ','Sperp1',[-1 1]*0e-9, radius, 2);
Sperp2=perp2x.*Sx+perp2y.*Sy+perp2z.*Sz;
tmp=common_image_vel(X,Z,mean(Sperp2(:,jr,:),2),Vex,Vez ,'midZ','Sperp2',[-1 1]*0e-9, radius, 2);

%balance magnetic energy d(B^2/2)/dt = B.dB/dt = -B curl(E)
[dBx,dBy,dBz]=compute_curl(x,y,z,Ex,Ey,Ez)
dB2dt = Bx.*dBx + By.*dBy + Bz.*dBz
tmp=common_image_vel(X,Z,mean(dB2dt(:,jr,:),2),Vex,Vez ,'midZ','dB2dt',[-1 1]*0e-9, radius, 2);

end

if(electrons)

labelc = labelc_power;
tmp=common_image_vel(X,Z,mean(JedotE(:,jr,:),2)*nWm3,Vex,Vez,'midZ','JeE',[-1 1]*0e-10, radius,1);
tmp=common_image_vel(X,Z,mean(divQbulk(:,jr,:),2)*nWm3,Vex,Vez,'midZ','divQbulke',[-1 1]*0e-10, radius,1);
tmp=common_image_vel(X,Z,mean(divQenth(:,jr,:),2)*nWm3,Vex,Vez,'midZ','divQenthe',[-1 1]*0e-10, radius,1);
tmp=common_image_vel(X,Z,mean(divQhf(:,jr,:),2)*nWm3,Vex,Vez,'midZ','divQhfe',[-1 1]*0e-10, radius,1);

labelc = labelc_flux;
tmp=common_image_vel(X,Z,signx*mean(Qbulkex(:,jr,:),2)*mWm2,Vex,Vez ,'midZ','Qbulkex',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(Qbulkey(:,jr,:),2)*mWm2,Vex,Vez , 'midZ','Qbulkey',[-1 1]*0e-9, radius, 3);
tmp=common_image_vel(X,Z,mean(Qbulkez(:,jr,:),2)*mWm2,Vex,Vez ,'midZ','Qbulkez',[-1 1]*0e-9, radius, 4);

Sr=(cos(theta).*Qbulkex+sin(theta).*Qbulkez);
tmp=common_image_vel(X,Z,mean(Sr(:,jr,:),2),Vex,Vez ,'midZ','Qbulker',[-1 1]*0e-9, radius, 2);
Sth=(-sin(theta).*Qbulkex+cos(theta).*Qbulkez);
tmp=common_image_vel(X,Z,mean(Sth(:,jr,:),2),Vex,Vez ,'midZ','Qbulketh',[-1 1]*0e-9, radius, 2);

Sr_cg=interpn(X3,Y3,Z3,Sr,Xcg,Ycg,Zcg);
tmp=common_image_vel_sum(Tcg,Rcg,Sr_cg',Vith_cg',Vir_cg' ,'midZ','Qbulker_cg',[-1 1]*0e-9, radius, 2);


tmp=common_image_vel(X,Z,signx*mean(Qenthex(:,jr,:),2)*mWm2,Vex,Vez , 'midZ','Qenthex',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(Qenthey(:,jr,:),2)*mWm2,Vex,Vez , 'midZ','Qenthey',[-1 1]*0e-9, radius, 3);
tmp=common_image_vel(X,Z,mean(Qenthez(:,jr,:),2)*mWm2,Vex,Vez , 'midZ','Qenthez',[-1 1]*0e-9, radius, 4);

Sr=(cos(theta).*Qenthex+sin(theta).*Qenthez);
tmp=common_image_vel(X,Z,mean(Sr(:,jr,:),2),Vex,Vez ,'midZ','Qenther',[-1 1]*0e-9, radius, 2);
Sth=(-sin(theta).*Qenthex+cos(theta).*Qenthez);
tmp=common_image_vel(X,Z,mean(Sth(:,jr,:),2),Vex,Vez ,'midZ','Qentheth',[-1 1]*0e-9, radius, 2);

Sr_cg=interpn(X3,Y3,Z3,Sr,Xcg,Ycg,Zcg);
tmp=common_image_vel_sum(Tcg,Rcg,Sr_cg',Vith_cg',Vir_cg' ,'midZ','Qenther_cg',[-1 1]*0e-9, radius, 2);

tmp=common_image_vel(X,Z,signx*mean(Qhfex(:,jr,:),2)*mWm2,Vex,Vez , 'midZ','Qhfex',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(Qhfey(:,jr,:),2)*mWm2,Vex,Vez , 'midZ','Qhfey',[-1 1]*0e-9, radius, 3);
tmp=common_image_vel(X,Z,mean(Qhfez(:,jr,:),2)*mWm2,Vex,Vez , 'midZ','Qhfez',[-1 1]*0e-9, radius, 4);

Sr=(cos(theta).*Qhfex+sin(theta).*Qhfez);
tmp=common_image_vel(X,Z,mean(Sr(:,jr,:),2),Vex,Vez ,'midZ','Qhfer',[-1 1]*0e-9, radius, 2);
Sth=(-sin(theta).*Qhfex+cos(theta).*Qhfez);
tmp=common_image_vel(X,Z,mean(Sth(:,jr,:),2),Vex,Vez ,'midZ','Qhfeth',[-1 1]*0e-9, radius, 2);

Sr_cg=interpn(X3,Y3,Z3,Sr,Xcg,Ycg,Zcg);
tmp=common_image_vel_sum(Tcg,Rcg,Sr_cg',Vith_cg',Vir_cg' ,'midZ','Qhfer_cg',[-1 1]*0e-9, radius, 2);


% Qenthepar= dot(Qenthex,Qenthey,Qenthez,Bx,By,Bz)./B;
% tmp=common_image_vel(X,Z,mean(Qenthepar(:,jr,:),2)*mWm2,Vex,Vez , 'midZ','Qenthepar',[-1 1]*0e-9, radius, 2);
% Qentheperp1=(By.*Qenthex-Bx.*Qenthey)./B2D;
% tmp=common_image_vel(X,Z,mean(Qentheperp1(:,jr,:),2)*mWm2,Vex,Vez , 'midZ','Qentheprp1',[-1 1]*0e-9, radius, 2);
% Qentheperp2=perp2x.*Qenthex+perp2y.*Qenthey+perp2z.*Qenthez;
% tmp=common_image_vel(X,Z,mean(Qentheperp2(:,jr,:),2)*mWm2,Vex,Vez , 'midZ','Qentheprp2',[-1 1]*0e-9, radius, 2);
% 
% 
% Qbulkepar= dot(Qbulkex,Qbulkey,Qbulkez,Bx,By,Bz)./B;
% tmp=common_image_vel(X,Z,mean(Qbulkepar(:,jr,:),2)*mWm2,Vex,Vez,'midZ','Qbulkepar',[-1 1]*0e-9, radius, 2);
% Qbulkeperp1=(By.*Qbulkex-Bx.*Qbulkey)./B2D;
% tmp=common_image_vel(X,Z,mean(Qbulkeperp1(:,jr,:),2)*mWm2,Vex,Vez,'midZ','Qbulkeprp1',[-1 1]*0e-9, radius, 2);
% Qbulkeperp2=perp2x.*Qbulkex+perp2y.*Qbulkey+perp2z.*Qbulkez;
% tmp=common_image_vel(X,Z,mean(Qbulkeperp2(:,jr,:),2)*mWm2,Vex,Vez, 'midZ','Qbulkeprp2',[-1 1]*0e-9, radius, 2);


labelc = labelc_power;

tmp=common_image_vel(X,Z,mean(divUP(:,jr,:),2)*nWm3,Vex,Vez, 'midZ','divUPe',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(udivP(:,jr,:),2)*nWm3,Vex,Vez, 'midZ','UdivPe',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(ugradp(:,jr,:),2)*nWm3,Vex,Vez, 'midZ','Ugradpe',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(udivP(:,jr,:)-ugradp(:,jr,:),2)*nWm3,Vex,Vez, 'midZ','offUdivPe',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(PgradV(:,jr,:),2)*nWm3,Vex,Vez, 'midZ','PgradVe',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(pdivv(:,jr,:),2)*nWm3,Vex,Vez, 'midZ','pdivVe',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(PgradV(:,jr,:)-pdivv(:,jr,:),2)*nWm3,Vex,Vez, 'midZ','offPgradVe',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(Ubulk(:,jr,:).*divVe(:,jr,:),2)*nWm3,Vex,Vez, 'midZ','UbulkdivVe',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(Uth(:,jr,:).*divVe(:,jr,:),2)*nWm3,Vex,Vez, 'midZ','UthdivVe',[-1 1]*0e-9, radius, 2);


tmp=common_image_vel(X,Z,mean(DUbulkDt(:,jr,:),2)*nWm3,Vex,Vez, 'midZ','DUbulkeDt',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(DUthDt(:,jr,:),2)*nWm3,Vex,Vez, 'midZ','DUtheDt',[-1 1]*0e-9, radius, 2);

end

if(ions)
[Uth, Ubulk, divQbulk, divQenth, divQhf,  udivP, PgradV, ugradp, pdivv, divUP] = compute_energy_balance( ...
    rhoi, Jix, Jiy, Jiz,... 
    Qbulkix, Qbulkiy, Qbulkiz, Qenthix, Qenthiy, Qenthiz, Qhfix, Qhfiy, Qhfiz, ...
    Pixx, Piyy, Pizz, Pixy, Pixz, Piyz, x, y, z, dx, dy, dz, 1.0, radius,cyl);

DUbulkDt = JidotE - Ubulk.*divVi - udivP;
DUthDt = -Uth.*divVi -PgradV;

end

if(ions)

labelc = labelc_power;
tmp=common_image_vel(X,Z,mean(JidotE(:,jr,:),2)*nWm3,Vix,Viz,'midZ','JiE',[-1 1]*0e-10, radius,1);
tmp=common_image_vel(X,Z,mean(divQbulk(:,jr,:),2)*nWm3,Vix,Viz, 'midZ','divQbulki',[-1 1]*0e-10, radius,1);
tmp=common_image_vel(X,Z,mean(divQenth(:,jr,:),2)*nWm3,Vix,Viz,'midZ','divQenthi',[-1 1]*0e-10, radius,1);
tmp=common_image_vel(X,Z,mean(divQhf(:,jr,:),2)*nWm3,Vix,Viz, 'midZ','divQhfi',[-1 1]*0e-10, radius,1);



labelc = labelc_flux;
tmp=common_image_vel(X,Z,signx*mean(Qbulkix(:,jr,:),2)*mWm2,Vix,Viz , 'midZ','Qbulkix',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(Qbulkiy(:,jr,:),2)*mWm2,Vix,Viz , 'midZ','Qbulkiy',[-1 1]*0e-9, radius, 3);
tmp=common_image_vel(X,Z,mean(Qbulkiz(:,jr,:),2)*mWm2,Vix,Viz , 'midZ','Qbulkiz',[-1 1]*0e-9, radius, 4);

Sr=(cos(theta).*Qbulkix+sin(theta).*Qbulkiz);
tmp=common_image_vel(X,Z,mean(Sr(:,jr,:),2),Vix,Viz ,'midZ','Qbulkir',[-1 1]*0e-9, radius, 2);
Sth=(-sin(theta).*Qbulkix+cos(theta).*Qbulkiz);
tmp=common_image_vel(X,Z,mean(Sth(:,jr,:),2),Vix,Viz ,'midZ','Qbulkith',[-1 1]*0e-9, radius, 2);

Sr_cg=interpn(X3,Y3,Z3,Sr,Xcg,Ycg,Zcg);
tmp=common_image_vel_sum(Tcg,Rcg,Sr_cg',Vith_cg',Vir_cg' ,'midZ','Qbulkir_cg',[-1 1]*0e-9, radius, 2);



tmp=common_image_vel(X,Z,signx*mean(Qenthix(:,jr,:),2)*mWm2,Vix,Viz , 'midZ','Qenthix',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(Qenthiy(:,jr,:),2)*mWm2,Vix,Viz ,'midZ','Qenthiy',[-1 1]*0e-9, radius, 3);
tmp=common_image_vel(X,Z,mean(Qenthiz(:,jr,:),2)*mWm2,Vix,Viz , 'midZ','Qenthiz',[-1 1]*0e-9, radius, 4);

Sr=(cos(theta).*Qenthix+sin(theta).*Qenthiz);
tmp=common_image_vel(X,Z,mean(Sr(:,jr,:),2),Vix,Viz ,'midZ','Qenthir',[-1 1]*0e-9, radius, 2);
Sth=(-sin(theta).*Qenthix+cos(theta).*Qenthiz);
tmp=common_image_vel(X,Z,mean(Sth(:,jr,:),2),Vix,Viz ,'midZ','Qenthith',[-1 1]*0e-9, radius, 2);

Sr_cg=interpn(X3,Y3,Z3,Sr,Xcg,Ycg,Zcg);
tmp=common_image_vel_sum(Tcg,Rcg,Sr_cg',Vith_cg',Vir_cg' ,'midZ','Qenthir_cg',[-1 1]*0e-9, radius, 2);

tmp=common_image_vel(X,Z,signx*mean(Qhfix(:,jr,:),2)*mWm2,Vix,Viz ,'midZ','Qhfix',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(Qhfiy(:,jr,:),2)*mWm2,Vix,Viz , 'midZ','Qhfiy',[-1 1]*0e-9, radius, 3);
tmp=common_image_vel(X,Z,mean(Qhfiz(:,jr,:),2)*mWm2,Vix,Viz ,'midZ','Qhfiz',[-1 1]*0e-9, radius, 4);

Sr=(cos(theta).*Qhfix+sin(theta).*Qhfiz);
tmp=common_image_vel(X,Z,mean(Sr(:,jr,:),2),Vix,Viz ,'midZ','Qhfir',[-1 1]*0e-9, radius, 2);
Sth=(-sin(theta).*Qhfix+cos(theta).*Qhfiz);
tmp=common_image_vel(X,Z,mean(Sth(:,jr,:),2),Vix,Viz ,'midZ','Qhfith',[-1 1]*0e-9, radius, 2);

Sr_cg=interpn(X3,Y3,Z3,Sr,Xcg,Ycg,Zcg);
tmp=common_image_vel_sum(Tcg,Rcg,Sr_cg',Vith_cg',Vir_cg' ,'midZ','Qhfir_cg',[-1 1]*0e-9, radius, 2);


% Qenthipar= dot(Qenthix,Qenthiy,Qenthiz,Bx,By,Bz)./B;
% tmp=common_image_vel(X,Z,mean(Qenthipar(:,jr,:),2)*mWm2,Vix,Viz ,'midZ','Qenthipar',[-1 1]*0e-9, radius, 2);
% Qenthiperp1=(By.*Qenthix-Bx.*Qenthiy)./B2D;
% tmp=common_image_vel(X,Z,mean(Qenthiperp1(:,jr,:),2)*mWm2,Vix,Viz , 'midZ','Qenthiprp1',[-1 1]*0e-9, radius, 2);
% Qenthiperp2=perp2x.*Qenthix+perp2y.*Qenthiy+perp2z.*Qenthiz;
% tmp=common_image_vel(X,Z,mean(Qenthiperp2(:,jr,:),2)*mWm2,Vix,Viz ,'midZ','Qenthiprp2',[-1 1]*0e-9, radius, 2);
% 
% 
% Qbulkipar= dot(Qbulkix,Qbulkiy,Qbulkiz,Bx,By,Bz)./B;
% tmp=common_image_vel(X,Z,mean(Qbulkipar(:,jr,:),2)*mWm2,Vix,Viz , 'midZ','Qbulkipar',[-1 1]*0e-9, radius, 2);
% Qbulkiperp1=(By.*Qbulkix-Bx.*Qbulkiy)./B2D;
% tmp=common_image_vel(X,Z,mean(Qbulkiperp1(:,jr,:),2)*mWm2,Vix,Viz ,'midZ','Qbulkiprp1',[-1 1]*0e-9, radius, 2);
% Qbulkiperp2=perp2x.*Qbulkix+perp2y.*Qbulkiy+perp2z.*Qbulkiz;
% tmp=common_image_vel(X,Z,mean(Qbulkiperp2(:,jr,:),2)*mWm2,Vix,Viz ,'midZ','Qbulkiprp2',[-1 1]*0e-9, radius, 2);


labelc = labelc_power;
tmp=common_image_vel(X,Z,mean(divUP(:,jr,:),2)*nWm3,Vix,Viz, 'midZ','divUPi',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(udivP(:,jr,:),2)*nWm3,Vix,Viz, 'midZ','UdivPi',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(ugradp(:,jr,:),2)*nWm3,Vix,Viz, 'midZ','Ugradpi',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(udivP(:,jr,:)-ugradp(:,jr,:),2)*nWm3,Vix,Viz, 'midZ','offUdivPi',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(PgradV(:,jr,:),2)*nWm3,Vix,Viz, 'midZ','PgradVi',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(pdivv(:,jr,:),2)*nWm3,Vix,Viz, 'midZ','pdivVi',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(PgradV(:,jr,:)-pdivv(:,jr,:),2)*nWm3,Vix,Viz, 'midZ','offPgradVi',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(Ubulk(:,jr,:).*divVe(:,jr,:),2)*nWm3,Vix,Viz, 'midZ','UbulkdivVi',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(Uth(:,jr,:).*divVe(:,jr,:),2)*nWm3,Vix,Viz, 'midZ','UthdivVi',[-1 1]*0e-9, radius, 2);

tmp=common_image_vel(X,Z,mean(DUbulkDt(:,jr,:),2)*nWm3,Vix,Viz, 'midZ','DUbulkiDt',[-1 1]*0e-9, radius, 2);
tmp=common_image_vel(X,Z,mean(DUthDt(:,jr,:),2)*nWm3,Vix,Viz, 'midZ','DUthiDt',[-1 1]*0e-9, radius, 2);

end

labelc = labelc_power;
tmp=common_image_vel(X,Z,mean(JbdotE(:,jr,:),2)*nWm3,Vix,Viz,'midZ','JbE',[-1 1]*0e-10, radius,1);

!/usr/local/bin/convert \( PgradVe.png -trim pdivVe.png -trim offPgradVe.png -trim -append \)  \( UdivPe.png -trim Ugradpe.png -trim offUdivPe.png -trim -append \) divUPe.png -trim +append comboe.png

!/usr/local/bin/convert \( PgradVi.png -trim pdivVi.png -trim offPgradVi.png -trim -append \)  \( UdivPi.png -trim Ugradpi.png -trim offUdivPi.png -trim -append \) divUPi.png -trim +append comboi.png

!convert \( PgradVe.png -trim pdivVe.png -trim offPgradVe.png -trim -append \)  \( UdivPe.png -trim Ugradpe.png -trim offUdivPe.png -trim -append \) divUPe.png -trim +append comboe.png

!convert \( PgradVi.png -trim pdivVi.png -trim offPgradVi.png -trim -append \)  \( UdivPi.png -trim Ugradpi.png -trim offUdivPi.png -trim -append \) divUPi.png -trim +append comboi.png

end
