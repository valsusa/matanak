%
% Energy plots in the XZ plane averaging on the whole range in y
%
!rm *.png
close all
addpath(genpath('../')); % Point to the directory where the iPic3D toolbox is
%dir='/data1/gianni/HRmaha3D3/vtk/'; %directory where the files are

addpath(genpath('../../ipic3d_toolbox'));

addpath(genpath('/Users/glapenta/Matlab/matanak'));

list_sim=["cfr_base" ;"cfr_stj" ;"cfr_ebi_j2"; "cfr_ebi_j4" ;"cfr_nbi"];
%list_sim=["cfr_ebi_j4" ;"cfr_nbi"];
%list_sim=["cfr_ebi_j2"]
list_sim =["2X-8NBI-ebeam-57k"; "2X-8NBI-ebeam-43k";"2X-8NBI-ebeam-37k"]
for lsim = 1:3

    sim_name=char(list_sim(lsim))
read_case

%dir_out = ['/Users/glapenta/Data/compare/' sim_name '/']
dir_out = ['/Users/glapenta/Data/2X-8NBI-ebeam/OHM/' sim_name '/']
unix(['mkdir ' dir_out])



qom=-64 

leggo='h5';



cyl = 0; % cartesian geometry

% Prepare string
ntime = num2str(cycle,'%06d');
ncycle=num2str(cycle,'%06i\n')
Ncycle = ncycle;


import_h5_binvtk  

tiny=1e-10;
rhoe=rhoe-tiny;
rhoi=rhoi+tiny;

% FRC
Lx=8;Ly=14;Lz=8
xr=[Lx/4 Lx-Lx/4];
yr=[Ly/10 Ly-Ly/10];

Xgsmrange= [0 Lx];
Zgsmrange= [0 Ly];
Ygsmrange= [0 Lz];

global Xgsmrange Zgsmrange Ygsmrange Lx Ly Lz dx dy dz

dx=Lx/Nx;
dy=Ly/Ny;
dz=Lz/Nz;

[X Y] = meshgrid(0:dx:Lx-dx,0:dy:Ly-dy);


ir=round(xr./dx);ir=min(ir):max(ir); %ir=1:Nx
jr=round(yr./dy);jr=min(jr):max(jr); %jr=1:Ny
iz=round((Nz-1)/2);

iy = round(Ny/2+10);
ix = round(Nx/2);deltax=20;

radius=5; %radius=4


global color_choice symmetric_color labelx labely labelc reversex reversey Ncycle skip dir_out color_lines

reversey=1;
symmetric_color=1;
color_choice =3;

    labelx ='x/d_i';
    labely ='y/d_i';
    labelc_flux = 'c.u.';
    labelc_power = 'c.u.';
    signx = 1;
    Wm3 = 1; %4pi is due to the usal division by 4pi of the density
    nWm3 = 1;
    mWm2= 1;
    reversex=0;

skip=1;
color_lines = [0 0 0 0.2];



iy = round(Ny/3);
ix = round(Nx/2);deltax=20;


xcoord = (X(jr,ir));
ycoord = (Y(jr,ir));
xc=linspace(0, Lx, Nx);
yc=linspace(0, Ly, Ny);
AAz=vecpot(xc,yc,Bx(:,:,iz),By(:,:,iz));
%AAz=(Bx.^2+By.^2+Bz.^2);AAz=AAz(:,:,iz);
[x,y,z]=meshgrid(0:dx:Lx-dx,0:dy:Ly-dy,0:dz:Lz-dz);


[X3 Y3 Z3] = meshgrid(0:dx:Lx-dx,0:dy:Ly-dy,0:dz:Lz-dz);

% na=30
% 
% ra=2
% angle =0:2*pi/(na-1):2*pi;
% na=max(size(angle));
% startx = Lx/2+ra*cos(angle);
% startz = Lz/2+ra*sin(angle);
% starty = ones(1,na)*Ly/2;
% streamline(X3,Y3,Z3,permute(Bx,[2 1 3]),permute(By,[2 1 3]),permute(Bz,[2 1 3]),startx,starty,startz);view(3)
% %streamline(X3,Y3,Z3,permute(Bx,[2 1 3]),permute(By,[2 1 3]),permute(Bz,[2 1 3]),Lx/2+linspace(-2,2,20),ones(1,20)*5,Lz/2-linspace(-2,2,20));view(3)
% hold on 
% plot3(startx,starty,startz)
% 
% ra=.2
% startx = Lx/2+ra*cos(angle);
% startz = Lz/2+ra*sin(angle);
% starty = ones(1,na)*Ly/2;
% streamline(X3,Y3,Z3,permute(Bx,[2 1 3]),permute(By,[2 1 3]),permute(Bz,[2 1 3]),startx,starty,startz);view(3)
% %streamline(X3,Y3,Z3,permute(Bx,[2 1 3]),permute(By,[2 1 3]),permute(Bz,[2 1 3]),Lx/2+linspace(-2,2,20),ones(1,20)*5,Lz/2-linspace(-2,2,20));view(3)
% hold on 
% plot3(startx,starty,startz)
% 
% 
% close all

Np=50
TH = 2*pi*rand(1,Np);
PH = asin(-1+2*rand(1,Np));
R = rand(1,Np)*2;
[startx,starty,startz] = sph2cart(TH,PH,R);

startx=startx+Lx/2;
starty=starty+Ly/2;
startz=startz+Lz/2;
%plot3(startx,starty,startz,'.','markersize',1)
h1 = streamline(X3,Y3,Z3,permute(Bx,[2 1 3]),permute(By,[2 1 3]),permute(Bz,[2 1 3]),startx,starty,startz);view(3)
h2 = streamline(X3,Y3,Z3,-permute(Bx,[2 1 3]),-permute(By,[2 1 3]),-permute(Bz,[2 1 3]),startx,starty,startz);view(3)
%h3 = streamline(X3,Y3,Z3,permute(Jex,[2 1 3]),permute(Jey,[2 1 3]),permute(Jez,[2 1 3]),startx,starty,startz);view(3)
%h4 = streamline(X3,Y3,Z3,-permute(Jex,[2 1 3]),-permute(Jey,[2 1 3]),-permute(Jez,[2 1 3]),startx,starty,startz);view(3)
axis equal 
set(h1,'Color', [0 0 0, 0.2])
set(h2,'Color', [0 0 0, 0.2])
%set(h3,'Color', [1 0 0, 0.2])
%set(h4,'Color', [1 0 0, 0.2])
print('-dpng','-r300',[dir_out 'magnetic_lines' Ncycle '.png'])


% Assuming n>0 and e>0
% (-e n ve_ = Je)xB

vnorm = 1e-6
axis equal
[cp1, cp2, cp3] = cross_prod(Jex, Jey, Jez, Bx, By, Bz);
labelc = labelc_power;
tmp=common_image(xcoord,ycoord,cp1(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMJexB_X',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp1(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMJexB_X',[-1 1]*vnorm, radius,1);
tmp=common_image(xcoord,ycoord,cp2(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMJexB_Y',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp2(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMJexB_Y',[-1 1]*vnorm, radius,1);
tmp=common_image(xcoord,ycoord,cp3(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMJexB_Z',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp3(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMJexB_Z',[-1 1]*vnorm, radius,1);

cp2sm=smooth3(cp2,'box',3);
ohmJexBY= cp2sm(:,iy,iz);
ohmJexBYy= cp2sm(ix,:,iz);


[cp1,cp2,cp3] = magnetic_coordinates_axisy(cp1,cp2, cp3, Bx, By, Bz); 
tmp=common_image(xcoord,ycoord,cp1(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMJexB_{par}',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp1(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMJexB_{par}',[-1 1]*vnorm, radius,1);
tmp=common_image(xcoord,ycoord,cp2(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMJexB_{perp1}',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp2(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMJexB_{perp1}',[-1 1]*vnorm, radius,1);
tmp=common_image(xcoord,ycoord,cp3(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMJexB_{perp2}',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp3(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMJexB_{perp2}',[-1 1]*vnorm, radius,1);


axis equal 
[cp1, cp2, cp3] = cross_prod(Jix, Jiy, Jiz, Bx, By, Bz);
labelc = labelc_power;
tmp=common_image(xcoord,ycoord,cp1(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMJixB_X',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp1(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMJixB_X',[-1 1]*vnorm, radius,1);
tmp=common_image(xcoord,ycoord,cp2(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMJixB_Y',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp2(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMJixB_Y',[-1 1]*vnorm, radius,1);
tmp=common_image(xcoord,ycoord,cp3(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMJixB_Z',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp3(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMJixB_Z',[-1 1]*vnorm, radius,1);

cp2sm=smooth3(cp2,'box',3);
ohmJixBY= cp2sm(:,iy,iz);
ohmJixBYy= cp2sm(ix,:,iz);


[cp1,cp2,cp3] = magnetic_coordinates_axisy(cp1,cp2, cp3, Bx, By, Bz); 
tmp=common_image(xcoord,ycoord,cp1(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMJixB_{par}',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp1(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMJixB_{par}',[-1 1]*vnorm, radius,1);
tmp=common_image(xcoord,ycoord,cp2(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMJixB_{perp1}',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp2(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMJixB_{perp1}',[-1 1]*vnorm, radius,1);
tmp=common_image(xcoord,ycoord,cp3(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMJixB_{perp2}',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp3(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMJixB_{perp2}',[-1 1]*vnorm, radius,1);

% en E = -rhoe E

cp1 = -imgaussfilt3(rhoe.*Ex,radius);
tmp=common_image(xcoord,ycoord,cp1(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMnE_X',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp1(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMnE_X',[-1 1]*vnorm, radius,1);
cp2 = -imgaussfilt3(rhoe.*Ey,radius);
tmp=common_image(xcoord,ycoord,cp2(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMnE_Y',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp2(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMnE_Y',[-1 1]*vnorm, radius,1);
cp3 = -imgaussfilt3(rhoe.*Ez,radius);
tmp=common_image(xcoord,ycoord,cp3(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMnE_Z',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp3(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMnE_Z',[-1 1]*vnorm, radius,1);


ohmnEY= cp2(:,iy,iz);
ohmnEYy= cp2(ix,:,iz);

[cp1,cp2,cp3] = magnetic_coordinates_axisy(cp1,cp2, cp3, Bx, By, Bz); 
tmp=common_image(xcoord,ycoord,cp1(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMnE_{par}',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp1(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMnE_{par}',[-1 1]*vnorm, radius,1);
tmp=common_image(xcoord,ycoord,cp2(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMnE_{perp1}',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp2(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMnE_{perp1}',[-1 1]*vnorm, radius,1);
tmp=common_image(xcoord,ycoord,cp3(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMnE_{perp2}',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp3(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMnE_{perp2}',[-1 1]*vnorm, radius,1);

% -me ne (dVe/dt) = -me/qe qe ve  * grad (Ve) = - Je grad(Ve) /qom

[tx, ty, tz] = gradient(imgaussfilt3(permute(Jex./rhoe,[2 1 3]),radius), dx, dy, dz);
tx=permute(tx,[2 1 3]);ty=permute(ty,[2 1 3]); tz=permute(tz,[2 1 3]);
cp1 = - tx.*Jex - ty.*Jey - tz.*Jez;

[tx, ty, tz] = gradient(imgaussfilt3(permute(Jey./rhoe,[2 1 3]),radius), dx, dy, dz);
tx=permute(tx,[2 1 3]);ty=permute(ty,[2 1 3]); tz=permute(tz,[2 1 3]);
cp2 = - tx.*Jex - ty.*Jey - tz.*Jez;

[tx, ty, tz] = gradient(imgaussfilt3(permute(Jez./rhoe,[2 1 3]),radius), dx, dy, dz);
tx=permute(tx,[2 1 3]);ty=permute(ty,[2 1 3]); tz=permute(tz,[2 1 3]);
cp3 = - tx.*Jex - ty.*Jey - tz.*Jez;

ohmi= cp2(:,iy,iz)/qom;
ohmiy= cp2(ix,:,iz)/qom;

tmp=common_image(xcoord,ycoord,cp1(ir,jr,iz)/qom,AAz(ir,jr),['midZ'],'OHMinert_X',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp1(ir,jr,iz)/qom,Bx,By,Bz,['midZ'],'OHMinert_X',[-1 1]*vnorm, radius,1);
tmp=common_image(xcoord,ycoord,cp2(ir,jr,iz)/qom,AAz(ir,jr),['midZ'],'OHMinert_Y',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp2(ir,jr,iz)/qom,Bx,By,Bz,['midZ'],'OHMinert_Y',[-1 1]*vnorm, radius,1);
tmp=common_image(xcoord,ycoord,cp3(ir,jr,iz)/qom,AAz(ir,jr),['midZ'],'OHMinert_Z',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp3(ir,jr,iz)/qom,Bx,By,Bz,['midZ'],'OHMinert_Z',[-1 1]*vnorm, radius,1);

[cp1,cp2,cp3] = magnetic_coordinates_axisy(cp1,cp2, cp3, Bx, By, Bz); 

tmp=common_image(xcoord,ycoord,cp1(ir,jr,iz)/qom,AAz(ir,jr),['midZ'],'OHMinert_{par}',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp1(ir,jr,iz)/qom,Bx,By,Bz,['midZ'],'OHMinert_{par}',[-1 1]*vnorm, radius,1);
tmp=common_image(xcoord,ycoord,cp2(ir,jr,iz)/qom,AAz(ir,jr),['midZ'],'OHMinert_{perp1}',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp2(ir,jr,iz)/qom,Bx,By,Bz,['midZ'],'OHMinert_{perp1}',[-1 1]*vnorm, radius,1);
tmp=common_image(xcoord,ycoord,cp3(ir,jr,iz)/qom,AAz(ir,jr),['midZ'],'OHMinert_{perp2}',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp3(ir,jr,iz)/qom,Bx,By,Bz,['midZ'],'OHMinert_{perp2}',[-1 1]*vnorm, radius,1);

% -div (Pe)
[cp1, cp2, cp3] = compute_divtensor(x,y,z,Pexx,Pexy,Pexz,Peyy, ...
    Peyz,Pezz,radius,0);
cp1=-cp1;cp2=-cp2;cp3=-cp3;
savevtkvector_bin(cp1,cp2,cp3, 'negdivP.vtk','negdivP',dx,dy,dz,0,0,0)

tmp=common_image(xcoord,ycoord,cp1(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMdivP_X',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp1(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMdivP_X',[-1 1]*vnorm, radius,1);

tmp=common_image(xcoord,ycoord,cp2(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMdivP_Y',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp2(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMdivP_Y',[-1 1]*vnorm, radius,1);

tmp=common_image(xcoord,ycoord,cp3(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMdivP_Z',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp3(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMdivP_Z',[-1 1]*vnorm, radius,1);

ohmdivP= cp2(:,iy,iz);
ohmdivPy= cp2(ix,:,iz);

[cp1,cp2,cp3] = magnetic_coordinates_axisy(cp1,cp2, cp3, Bx, By, Bz); 

tmp=common_image(xcoord,ycoord,cp1(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMdivP_{par}',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp1(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMdivP_{par}',[-1 1]*vnorm, radius,1);

tmp=common_image(xcoord,ycoord,cp2(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMdivP_{perp1}',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp2(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMdivP_{perp1}',[-1 1]*vnorm, radius,1);

tmp=common_image(xcoord,ycoord,cp3(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMdivP_{perp2}',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp3(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMdivP_{perp2}',[-1 1]*vnorm, radius,1);


% Diagonal term
pe = (Pexx + Peyy + Pezz)/3;
[cp1,cp2,cp3] = gradient(permute(imgaussfilt3(pe,radius),[2 1 3]),dx,dy,dz);
cp1 = - permute(cp1, [2 1 3]); 
cp2 = - permute(cp2, [2 1 3]);
cp3 = - permute(cp3, [2 1 3]);


tmp=common_image(xcoord,ycoord,cp1(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMgradp_X',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp1(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMgradp_X',[-1 1]*vnorm, radius,1);

tmp=common_image(xcoord,ycoord,cp2(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMgradp_Y',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp2(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMgradp_Y',[-1 1]*vnorm, radius,1);

tmp=common_image(xcoord,ycoord,cp3(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMgradp_Z',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp3(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMgradp_Z',[-1 1]*vnorm, radius,1);

ohmgradp= cp2(:,iy,iz);
ohmgradpy= cp2(ix,:,iz);
[cp1,cp2,cp3] = magnetic_coordinates_axisy(cp1,cp2, cp3, Bx, By, Bz); 

tmp=common_image(xcoord,ycoord,cp1(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMgradp_{par}',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp1(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMgradp_{par}',[-1 1]*vnorm, radius,1);

tmp=common_image(xcoord,ycoord,cp2(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMgradp_{perp1}',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp2(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMgradp_{perp1}',[-1 1]*vnorm, radius,1);

tmp=common_image(xcoord,ycoord,cp3(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMgradp_{perp2}',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp3(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMgradp_{perp2}',[-1 1]*vnorm, radius,1);


% -deviatoric
[cp1, cp2, cp3] = compute_divtensor(x,y,z,Pexx-pe,Pexy,Pexz,Peyy-pe, ...
    Peyz,Pezz-pe,radius,0);
cp1=-cp1;cp2=-cp2;cp3=-cp3;
savevtkvector_bin(cp1,cp2,cp3, 'negdivP.vtk','negdivP',dx,dy,dz,0,0,0)

tmp=common_image(xcoord,ycoord,cp1(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMdivPdev_X',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp1(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMdivPdev_X',[-1 1]*vnorm, radius,1);

tmp=common_image(xcoord,ycoord,cp2(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMdivPdev_Y',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp2(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMdivPdev_Y',[-1 1]*vnorm, radius,1);

tmp=common_image(xcoord,ycoord,cp3(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMdivPdev_Z',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp3(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMdivPdev_Z',[-1 1]*vnorm, radius,1);

OHMdivPdev= cp2(:,iy,iz);
OHMdivPdevy= cp2(ix,:,iz);

[cp1,cp2,cp3] = magnetic_coordinates_axisy(cp1,cp2, cp3, Bx, By, Bz); 

tmp=common_image(xcoord,ycoord,cp1(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMdivPdev_{par}',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp1(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMdivPdev_{par}',[-1 1]*vnorm, radius,1);

tmp=common_image(xcoord,ycoord,cp2(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMdivPdev_{perp1}',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp2(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMdivPdev_{perp1}',[-1 1]*vnorm, radius,1);

tmp=common_image(xcoord,ycoord,cp3(ir,jr,iz),AAz(ir,jr),['midZ'],'OHMdivPdev_{perp2}',[-1 1]*vnorm, radius,1);
tmp=common_image_stream(xcoord,ycoord,cp3(ir,jr,iz),Bx,By,Bz,['midZ'],'OHMdivPdev_{perp2}',[-1 1]*vnorm, radius,1);



figure(100)
xrange=linspace(0,Lx,Nx);
%subplot(2,1,1)
plot(xrange, ohmnEY, xrange, ohmJixBY,xrange, ohmJexBY,xrange, ohmi, xrange, ohmdivP,  xrange, ohmgradp)
legend({'nE', 'JixB','JexB','ndV/dt','div P', 'gradP'},'Location','NorthEast')%,'NumColumns',2)
%subplot(2,1,2)
%plot(xrange, -ohmdJxcBZ, xrange, -ohmdndEZ,xrange, -ohmdi)
%legend('\delta Jx\delta B','\delta n\delta E','\delta n \delta V/dt','location','North')
xlabel(labelx,'fontsize',[14])
%ylabel(labely,'fontsize',[14])
print('-dpng','-r300',[dir_out 'OHMalongX.png'])
%subplot(3,1,3)
%semilogy(xrange, abs(ohmdJxcBZ./ohmnEZ), xrange, abs(ohmdndEZ./ohmnEZ),xrange, abs(ohmdi./ohmnEZ))
%legend('\delta Jx\delta B','\delta n\delta E','\delta nd \delta V/dt','location','EastOutside')
%ylim([.01, 1])


figure(101)
yrange=linspace(0,Ly,Ny);
%subplot(2,1,1)
plot(yrange, ohmnEYy, yrange, ohmJixBYy, yrange, ohmJexBYy, yrange,  ohmiy, yrange, ohmdivPy , yrange, ohmgradpy)
legend({'nE','JixB','JexB','ndV/dt','div P', 'grad p'},'Location','NorthEast')%,'NumColumns',2)
%subplot(2,1,2)
%plot(yrange, -ohmdJxcBZy, yrange, -ohmdndEZy, yrange, -ohmdiy)
%legend('\delta Jx\delta B','\delta n\delta E','\delta n  \delta V/dt','location','North')
xlabel(labely,'fontsize',[14])
%xlim([5 10])
print('-dpng','-r300',[dir_out 'OHMalongY.png'])

end

function [dp] = fluct(p)
p_avg=mean(p,3);
[Nx Ny Nz]=size(p);
dp=p;
for k=1:Nz
    dp(:,:,k)=p(:,:,k)-p_avg;
end
end

function [p_avg] = mean_z(p)
p_avg=mean(p,3);
end

