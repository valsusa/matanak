close all
V1=-imgaussfilt3(divS,radius);
V2=imgaussfilt3(dB2dt,radius);
[Nx, Ny, Nz] = size(V1);
ixc=round(Nx/2);
izc=round(Nz/2);
%V1 = V1(ixc-3:ixc+3,:,izc-3:izc+3);
%V2 = V2(ixc-3:ixc+3,:,izc-3:izc+3);
plot(mean(mean(V1,1),3)); 
hold on; 
plot(mean(mean(V2,1),3));
legend('div S','dB/dt')
grid on