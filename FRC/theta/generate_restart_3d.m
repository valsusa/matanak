close all
clear all
%addpath(genpath('~/Matlab/Matanak/matlab/ipic3d_toolbox'))


Lx =   15.2            
Ly =   84.0            
Lz =   15.2             

opath='FRC2D_3D-Partcl_001000.h5'
inpath='FRC2D-Partcl_001000.h5'
system(['rm ' opath])

h5disp(inpath)
h5info(inpath)

nspec = h5readatt(inpath,'/Step#0','nspec')
for is=1:nspec
npart(is) = double(h5readatt(inpath,'/Step#0',['npart_' num2str(is-1)]))
end

for is=1:nspec
 
var=h5read(inpath,['/Step#0/q_' num2str(is-1)]);
h5create(opath,['/Step#0/q_' num2str(is-1)],double(npart(is)));
h5write(opath,['/Step#0/q_' num2str(is-1) ],var,1,npart(is)) 


x=h5read(inpath,['/Step#0/x_' num2str(is-1)]);
theta=rand(size(x))*2*pi;
h5create(opath,['/Step#0/x_' num2str(is-1) ],npart(is));
h5write(opath,['/Step#0/x_' num2str(is-1) ],Lx/2+x.*cos(theta),1,npart(is)) 

ii=(Lz/2+x.*cos(theta)<0);
disp(['out of range in x: x<0  ->  ' num2str(sum(ii,'all'))])
ii=(Lz/2+x.*cos(theta)>Lz);
disp(['out of range in x: x>Lx  ->  ' num2str(sum(ii,'all'))])

h5create(opath,['/Step#0/z_' num2str(is-1) ],npart(is));
h5write(opath,['/Step#0/z_' num2str(is-1) ],Lz/2+x.*sin(theta),1,npart(is)) 

ii=(Lz/2+x.*sin(theta)<0);
disp(['out of range in z: z<0  ->  ' num2str(sum(ii,'all'))])
ii=(Lz/2+x.*sin(theta)>Lz);
disp(['out of range in z: z>Lz  ->  ' num2str(sum(ii,'all'))])

clear x

var=h5read(inpath,['/Step#0/y_' num2str(is-1)]);
h5create(opath,['/Step#0/y_' num2str(is-1) ],npart(is));
h5write(opath,['/Step#0/y_' num2str(is-1) ],var,1,npart(is)) 


vx=h5read(inpath,['/Step#0/u_' num2str(is-1)]);
vz=h5read(inpath,['/Step#0/w_' num2str(is-1)]);

h5create(opath,['/Step#0/u_' num2str(is-1) ],npart(is));
h5write(opath,['/Step#0/u_' num2str(is-1) ],vx.*cos(theta)-vz.*sin(theta),1,npart(is)) 

h5create(opath,['/Step#0/w_' num2str(is-1) ],npart(is));
h5write(opath,['/Step#0/w_' num2str(is-1) ],vx.*sin(theta)+vz.*cos(theta),1,npart(is)) 

clear vx vz

var=h5read(inpath,['/Step#0/v_' num2str(is-1)]);
h5create(opath,['/Step#0/v_' num2str(is-1) ],npart(is));
h5write(opath,['/Step#0/v_' num2str(is-1) ],var,1,npart(is)) 


end


for is=1:nspec
    h5writeatt(opath,'/Step#0',['npart_' num2str(is-1)] ,int32(npart(is)));
end    
h5writeatt(opath,'/Step#0','nspec',int32(nspec));
