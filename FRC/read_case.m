


switch sim_name  
        case '54376'
        dir='/data1/gianni/frc-54376/h5/'
        case_name='FRC3D'
        cycle=23500
        electrons =0; 
    case '55852'
        dir='/data1/gianni/frc-55852_4/DEC2022/'
        case_name='FRC3D'
        cycle=38000
        electrons =0; 
    case 'FRC3D10'
        dir='/gpfsm/dnb31/glapenta/frc3d10/dataHR/'
        case_name='FRC3D'
        electrons =0; 
    case 'FRC-CD'
        dir='/Users/glapenta/Data/FRC-CD-analysis/'
        case_name='FRC3D'
        cycle=35000
    case '0X-NBI-ebeam'
        dir='/Users/glapenta/Data/FRC3D-NBI-ebeam/0X-NBI-ebeam/'
        case_name='FRC3D'
        cycle=57000
    case '2X-8NBI-ebeam'
        dir='/Users/glapenta/Data/FRC3D-NBI-ebeam/2X-8NBI-ebeam/'
        case_name='FRC3D'
        cycle=57000
    case '8NBI-ebeam'
        dir='/Users/glapenta/Data/FRC3D-NBI-ebeam/8NBI-ebeam/'
        case_name='FRC3D'
        cycle=57000
    case '2X-8NBI-ebeam-57k'
        dir='/Users/glapenta/Data/2X-8NBI-ebeam/'
        case_name='FRC3D'
        cycle=57000
    case '2X-8NBI-ebeam-43k'
        dir='/Users/glapenta/Data/2X-8NBI-ebeam/'
        case_name='FRC3D'
        cycle=43000
    case '2X-8NBI-ebeam-37k'
        dir='/Users/glapenta/Data/2X-8NBI-ebeam/'
        case_name='FRC3D'
        cycle=37000
    case 'V1'
dir='/Users/glapenta/Data/FRC3D-v6-i3-re284k-V1/';
case_name='FRC3D'
cycle=288000
 
    case 'V2'
dir='/Users/glapenta/Data/FRC3D-v6-i3-re284k-V2/';
case_name='FRC3D'
cycle=288000
 
    case 'V4'
dir='/Users/glapenta/Data/FRC3D-v6-i4-B2-V4-re190k/';
cycle=193000;
case_name='FRC3D'
    
    case 'B2'
dir='/Users/glapenta/Data/FRC3D-v6-i4-B2-re210k/';
case_name='FRC3D'
cycle=210100
 
    case 'j2'
dir='/Users/glapenta/Data/FRC3D-j2-re190k/';
case_name='FRC3D'
cycle=193000

    case 'cfr_base'
dir='/Users/glapenta/Data/compare/theory/ecsim/results/FRC3D-v6-i4-B2-170k-214k-baseline/45996.ibm-h2/FRC3D-v6-i4-B2-re170k/';
case_name='FRC3D'
cycle=190000

case 'cfr_stj'
dir='/Users/glapenta/Data/compare/theory/ecsim/results/FRC3D-v6-i4-B2-P1-re190k/46412.ibm-h2/FRC3D-v6-i4-B2-P1-re190k/'
case_name='FRC3D'
cycle=206000

case 'cfr_ebi_j2'
dir='/Users/glapenta/Data/compare/theory/ecsim/results/FRC3D-j2-re190k/47098.ibm-h2/FRC3D-v6-j2-P0-re191k/'
case_name='FRC3D'
cycle=192000

case 'cfr_ebi_j4'
dir='/Users/glapenta/Data/compare/theory/ecsim/results/FRC3D-j4-re185k/47204.ibm-h2/FRC3D-v6-j4-P0-re185k/'
case_name='FRC3D'
cycle=191000

case 'cfr_nbi'
dir='/Users/glapenta/Data/compare/theory/ecsim/results/FRC3D-v6-i4-B2-V0-NBI2/46748.ibm-h2/FRC3D-v6-i4-B2-V0-NBI2-re190k/'
case_name='FRC3D'
cycle=198000

case 'cfr_inj_decay'
%dir='/Users/glapenta/Data/compare/theory/ecsim/results/FRC3D-v6-i4-B2-N4-P1/'
case_name='FRC3D'
cycle=194000

    case 'N1_194'
        dir='/Users/glapenta/Data/compare_nov4/'
        case_name='FRC3D-N1-Avg'
        cycle=194000
    case 'N8_194'
        dir='/Users/glapenta/Data/compare_nov4/'
        case_name='FRC3D-N8-Avg'
        cycle=194000
    case 'N8_197'
        dir='/Users/glapenta/Data/compare_nov4/'
        case_name='FRC3D-N8-Avg'
        cycle=197000
otherwise
    print('no recognised case selected')
end
