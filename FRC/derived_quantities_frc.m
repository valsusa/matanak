%
% Energy plots in the XZ plane averaging on the whole range in y
%
!rm *.png
close all
addpath(genpath('../')); % Point to the directory where the iPic3D toolbox is
%dir='/data1/gianni/HRmaha3D3/vtk/'; %directory where the files are

%addpath(genpath('../../ipic3d_toolbox'));

%addpath(genpath('/Users/glapenta/Matlab/matanak'));

list_sim=["cfr_base" ;"cfr_stj" ;"cfr_ebi_j2"; "cfr_ebi_j4" ;"cfr_nbi"];
%list_sim=["cfr_ebi_j4" ;"cfr_nbi"];
%list_sim=["cfr_ebi_j2"]
list_sim =["2X-8NBI-ebeam-57k"; "2X-8NBI-ebeam-43k";"2X-8NBI-ebeam-37k"]
list_sim =["2X-8NBI-ebeam"; "0X-NBI-ebeam";"8NBI-ebeam"]
list_sim=["FRC3D10"]
list_sim=["55852"]
list_sim=["54376"]

%for it =200:2:320
%for it =322:2:450
%for it=226:2:318
%for it=200:2:450

for it =1:1
for lsim = 1:1

    sim_name=char(list_sim(lsim))
read_case

%dir_out = ['/Users/glapenta/Data/compare/' sim_name '/']
dir_out = ['/Users/glapenta/Data/2X-8NBI-ebeam/OHM/' sim_name '/']
dir_out = ['/Users/glapenta/Data/FRC3D-NBI-ebeam/analisi_vtk/' num2str(it) '/' sim_name '/']
dir_out ='frc3d10HRp3/';
dir_out ='/data1/gianni/frc-55852_4/DEC2022/'
dir_out ='/data1/gianni/frc-54376/vtk/'

%cycle=it*100

unix(['mkdir -p ' dir_out])
%unix(['rm ' dir_out '*.vtk'])


qom=-64 
qoms=[-64, 1, 2]
%55852
qoms=[-64, 1, 1]
leggo='h5';



cyl = 0; % cartesian geometry

% Prepare string
ntime = num2str(cycle,'%06d');
ncycle=num2str(cycle,'%06i\n')
Ncycle = ncycle;


radius =3 %.3; % 1; %radius=3; %radius=4

import_h5_binvtk  

% FRC
Lx=8;Ly=14;Lz=8
%FRC3D
Lx=7.6*2;
Ly=84;
Lz=Lx;
%55852
Lx=21.5;
Ly=127.17;
Lz=Lx;

dx=Lx/Nx;
dy=Ly/Ny;
dz=Lz/Nz;


tiny=1e-10;
rhoe=rhoe-tiny;
rhoi=rhoi+tiny;



[x,y,z]=meshgrid(0:dx:Lx-dx,0:dy:Ly-dy,0:dz:Lz-dz);


%Poynting theorem

[Sx, Sy, Sz] = cross_prod(Ex, Ey, Ez, Bx, By, Bz);
Sx = Sx /4/pi; Sy = Sy /4/pi; Sz = Sz /4/pi;

divS = compute_div(x,y,z,Sx,Sy,Sz,radius, cyl);
JdotE=(Jex+Jix+Jbx).*Ex + (Jey+Jiy+Jby).*Ey + (Jez+Jiz+Jbz).*Ez;

Epar = (Ex.*Bx+ Ey.*By+ Ez.*Bz)./(B +tiny);

Vex= Jex./(rhoe-tiny) ;
Vey= Jey./(rhoe-tiny) ;
Vez= Jez./(rhoe-tiny) ;
Epx = Ex + (Vey.*Bz - Vez.*By);
Epy = Ey + (Vez.*Bx - Vex.*Bz);
Epz = Ez + (Vex.*By - Vey.*Bx);

JdotEp=(Jex+Jix+Jbx).*Epx + (Jey+Jiy+Jby).*Epy + (Jez+Jiz+Jbz).*Epz;



[dEx,dEy,dEz]=compute_curl(x,y,z,Bx,By,Bz);
dE2dt1 = (Ex.*dEx + Ey.*dEy + Ez.*dEz)/4/pi;
dE2dt2 = -Ex.*(Jex+Jix+Jbx) - Ey.*(Jey+Jiy+Jby) - Ez.*(Jez+Jiz+Jbz);
dE2dt= dE2dt2+ dE2dt1;

% Assuming n>0 and e>0
% (-e n ve_ = Je)xB

[cp1, cp2, cp3] = cross_prod(Jex+Jix+Jbx, Jey+Jiy+Jby, Jez+Jiz+Jbz, Bx, By, Bz);
[dBx,dBy,dBz]=compute_curl(x,y,z,-cp1./rhoe,-cp2./rhoe,-cp3./rhoe);
dB2dt = -(Bx.*dBx + By.*dBy + Bz.*dBz);

V1=imgaussfilt3(dB2dt,radius);
savevtk_bin(V1, [dir_out  'BcurlOHMJxB' Ncycle '.vtk'],'BcurlOHMJxB',dx,dy,dz,0,0,0);

[dBx,dBy,dBz]=compute_curl(x,y,z,Ex,Ey,Ez);
dB2dt = -(Bx.*dBx + By.*dBy + Bz.*dBz)/4/pi;

dE2dt_computed = -divS -JdotE - dB2dt;

V1=imgaussfilt3(dB2dt,radius);
savevtk_bin(V1, [dir_out  'dB2dt' Ncycle '.vtk'],'dB2dt',dx,dy,dz,0,0,0);
V1=imgaussfilt3(dE2dt,radius);
savevtk_bin(V1, [dir_out  'dE2dt' Ncycle '.vtk'],'dE2dt',dx,dy,dz,0,0,0);

V1=imgaussfilt3(dE2dt1,radius);
savevtk_bin(V1, [dir_out  'dE2dt1' Ncycle '.vtk'],'dE2dt1',dx,dy,dz,0,0,0);
V1=imgaussfilt3(dE2dt2,radius);
savevtk_bin(V1, [dir_out  'dE2dt2' Ncycle '.vtk'],'dE2dt2',dx,dy,dz,0,0,0);

V1=imgaussfilt3(JdotE,radius);
savevtk_bin(V1, [dir_out  'JdotE' Ncycle '.vtk'],'JdotE',dx,dy,dz,0,0,0);
V1=imgaussfilt3(JdotEp,radius);
savevtk_bin(V1, [dir_out  'JdotEp' Ncycle '.vtk'],'JdotEp',dx,dy,dz,0,0,0);
V1=imgaussfilt3(divS,radius);
savevtk_bin(V1, [dir_out  'divS' Ncycle '.vtk'],'divS',dx,dy,dz,0,0,0);

do_ohm=false
if(do_ohm)

[cp1, cp2, cp3] = cross_prod(Jix, Jiy, Jiz, Bx, By, Bz);
[dBx,dBy,dBz]=compute_curl(x,y,z,cp1./rhoi,cp2./rhoi,cp3./rhoi);
V1 = -(Bx.*dBx + By.*dBy + Bz.*dBz);

V1=imgaussfilt3(V1,radius);
savevtk_bin(V1, [dir_out  'BcurlOHMVixB' Ncycle '.vtk'],'BcurlOHMVixB',dx,dy,dz,0,0,0);



[cp1, cp2, cp3] = cross_prod(Jex, Jey, Jez, Bx, By, Bz);
[dBx,dBy,dBz]=compute_curl(x,y,z,-cp1./rhoe,-cp2./rhoe,-cp3./rhoe);
term_ve = -(Bx.*dBx + By.*dBy + Bz.*dBz);

V1=imgaussfilt3(term_ve,radius);
savevtk_bin(V1, [dir_out  'BcurlOHMVexB' Ncycle '.vtk'],'BcurlOHMVexB',dx,dy,dz,0,0,0);


%balance magnetic energy d(B^2/2)/dt = B.dB/dt = -B curl(E)
[dBx,dBy,dBz]=compute_curl(x,y,z,Ex,Ey,Ez);
term_E = -(Bx.*dBx + By.*dBy + Bz.*dBz);

V1=imgaussfilt3(term_E,radius);
savevtk_bin(V1, [dir_out  'BcurlOHME' Ncycle '.vtk'],'BcurlOHME',dx,dy,dz,0,0,0);


% me/qe (dVe/dt) = me/ qe  ve  * grad (Ve) =  ve grad(Ve) /qom

[tx, ty, tz] = gradient(imgaussfilt3(permute(Jex./rhoe,[2 1 3]),radius), dx, dy, dz);
tx=permute(tx,[2 1 3]);ty=permute(ty,[2 1 3]); tz=permute(tz,[2 1 3]);
cp1 = (tx.*Jex + ty.*Jey + tz.*Jez) ./rhoe /qom;

[tx, ty, tz] = gradient(imgaussfilt3(permute(Jey./rhoe,[2 1 3]),radius), dx, dy, dz);
tx=permute(tx,[2 1 3]);ty=permute(ty,[2 1 3]); tz=permute(tz,[2 1 3]);
cp2 = (tx.*Jex + ty.*Jey + tz.*Jez) ./rhoe /qom;

[tx, ty, tz] = gradient(imgaussfilt3(permute(Jez./rhoe,[2 1 3]),radius), dx, dy, dz);
tx=permute(tx,[2 1 3]);ty=permute(ty,[2 1 3]); tz=permute(tz,[2 1 3]);
cp3 = (tx.*Jex + ty.*Jey + tz.*Jez) ./rhoe /qom;

[dBx,dBy,dBz]=compute_curl(x,y,z,cp1,cp2,cp3);
V1 = -(Bx.*dBx + By.*dBy + Bz.*dBz);

V1=imgaussfilt3(V1,radius);
savevtk_bin(V1, [dir_out  'BcurlOHMiner' Ncycle '.vtk'],'BcurlOHMiner',dx,dy,dz,0,0,0);


% -div (Pe)
[cp1, cp2, cp3] = compute_divtensor(x,y,z,Pexx,Pexy,Pexz,Peyy, ...
    Peyz,Pezz,radius,0);

[dBx,dBy,dBz]=compute_curl(x,y,z,cp1./rhoe,cp2./rhoe,cp3./rhoe);
term_pe = -(Bx.*dBx + By.*dBy + Bz.*dBz);

V1=imgaussfilt3(term_pe,radius);
savevtk_bin(V1, [dir_out  'BcurlOHMdivP' Ncycle '.vtk'],'BcurlOHMdivP',dx,dy,dz,0,0,0);


% Diagonal term
pe = (Pexx + Peyy + Pezz)/3;
[cp1,cp2,cp3] = gradient(permute(imgaussfilt3(pe,radius),[2 1 3]),dx,dy,dz);
cp1 =  permute(cp1, [2 1 3]); 
cp2 =  permute(cp2, [2 1 3]);
cp3 =  permute(cp3, [2 1 3]);

[dBx,dBy,dBz]=compute_curl(x,y,z,cp1./rhoe,cp2./rhoe,cp3./rhoe);
V1 = -(Bx.*dBx + By.*dBy + Bz.*dBz);

V1=imgaussfilt3(V1,radius);
savevtk_bin(V1, [dir_out  'BcurlOHMgradp' Ncycle '.vtk'],'BcurlOHMgradp',dx,dy,dz,0,0,0);


% -deviatoric
[cp1, cp2, cp3] = compute_divtensor(x,y,z,Pexx-pe,Pexy,Pexz,Peyy-pe, ...
    Peyz,Pezz-pe,radius,0);

[dBx,dBy,dBz]=compute_curl(x,y,z,cp1./rhoe,cp2./rhoe,cp3./rhoe);
V1 = -(Bx.*dBx + By.*dBy + Bz.*dBz);

V1=imgaussfilt3(V1,radius);
savevtk_bin(V1, [dir_out  'BcurlOHMdivPdev' Ncycle '.vtk'],'BcurlOHMdivPdev',dx,dy,dz,0,0,0);


term_iner = term_E - term_ve - term_pe;

V1=imgaussfilt3(term_iner,radius);
savevtk_bin(V1, [dir_out  'BcurlOHMtotiner' Ncycle '.vtk'],'BcurlOHMtotiner',dx,dy,dz,0,0,0);

end

agyro = true;

if(agyro) 
    [Agyro,Agyro_aunai,Nongyro_swisdak]=compute_agyro(Bx, By, Bz, imgaussfilt3(Pexx,radius), ...
    imgaussfilt3(Pexy,radius), imgaussfilt3(Pexz,radius), imgaussfilt3(Peyy,radius),  ...
    imgaussfilt3(Peyz,radius), imgaussfilt3(Pezz,radius) ) ;
%V1=imgaussfilt3(Agyro,radius);
V1 = real(Agyro);
savevtk_bin(V1, [dir_out  'Agyro' Ncycle '.vtk'],'Agyro',dx,dy,dz,0,0,0);
%V1=imgaussfilt3(Agyro_aunai,radius);
V1 = real(Agyro_aunai);
savevtk_bin(V1, [dir_out  'Agyro_aunai' Ncycle '.vtk'],'Agyro_aunai',dx,dy,dz,0,0,0);
%V1=imgaussfilt3(Nongyro_swisdak,radius);
V1 = real(Nongyro_swisdak);
savevtk_bin(V1, [dir_out  'Nongyro_swisdak' Ncycle '.vtk'],'Nongyro_swisdak',dx,dy,dz,0,0,0);

end


energy= true;
if (energy)


Vex=Jex./rhoe;Vey=Jey./rhoe;Vez=Jez./rhoe;
divVe = compute_div(x,y,z,Vex,Vey,Vez, radius, cyl);
Vix=Jix./rhoi;Viy=Jiy./rhoi;Viz=Jiz./rhoi;
divVi = compute_div(x,y,z,Vix,Viy,Viz, radius, cyl);
Vbx=Jbx./rhob;Vby=Jby./rhob;Vbz=Jbz./rhob;
divVb = compute_div(x,y,z,Vbx,Vby,Vbz, radius, cyl);

savevtkvector_bin(Bx,By,Bz, [dir_out 'B' Ncycle '.vtk'],'B',dx,dy,dz,0,0,0)
savevtkvector_bin(Ex,Ey,Ez, [dir_out 'E' Ncycle '.vtk'],'E',dx,dy,dz,0,0,0)
savevtkvector_bin(Jex,Jey,Jez, [dir_out 'Je' Ncycle '.vtk'],'Je',dx,dy,dz,0,0,0)
savevtkvector_bin(Jix,Jiy,Jiz, [dir_out 'Ji' Ncycle '.vtk'],'Ji',dx,dy,dz,0,0,0)
savevtkvector_bin(Jbx,Jby,Jbz, [dir_out 'Jb' Ncycle '.vtk'],'Jb',dx,dy,dz,0,0,0)
savevtk_bin(rhoe, [dir_out  'rhoe' Ncycle '.vtk'],'rhoe',dx,dy,dz,0,0,0);
savevtk_bin(rhoi, [dir_out  'rhoi' Ncycle '.vtk'],'rhoi',dx,dy,dz,0,0,0);
savevtk_bin(rhob, [dir_out  'rhob' Ncycle '.vtk'],'rhob',dx,dy,dz,0,0,0);

V1=imgaussfilt3(divVe,radius);
savevtk_bin(V1, [dir_out  'divVe' Ncycle '.vtk'],'divV',dx,dy,dz,0,0,0);
V1=imgaussfilt3(divVi,radius);
savevtk_bin(V1, [dir_out  'divVi' Ncycle '.vtk'],'divV',dx,dy,dz,0,0,0);

    
Vmhdx=(Vix+Vex/abs(qom))/(1+1/abs(qom));
Vmhdy=(Viy+Vey/abs(qom))/(1+1/abs(qom));
Vmhdz=(Viz+Vez/abs(qom))/(1+1/abs(qom));

Epx = Ex + (Vmhdy.*Bz - Vmhdz.*By);
Epy = Ey + (Vmhdz.*Bx - Vmhdx.*Bz);
Epz = Ez + (Vmhdx.*By - Vmhdy.*Bx);

JdotEpmhd=(Jex+Jix+Jbx).*Epx + (Jey+Jiy+Jby).*Epy + (Jez+Jiz+Jbz).*Epz;
V1=imgaussfilt3(JdotEpmhd,radius);
savevtk_bin(V1, [dir_out  'JdotEpmhd' Ncycle '.vtk'],'JdotEpmhd',dx,dy,dz,0,0,0);

%
% Electrons
%

[Uth, Ubulk, divQbulk, divQenth, divQhf,  udivP, PgradV, ugradp, pdivv, divUP] = compute_energy_balance( ...
    rhoe, Jex, Jey, Jez,... 
    Qbulkex, Qbulkey, Qbulkez, Qenthex, Qenthey, Qenthez, Qhfex, Qhfey, Qhfez, ...
    Pexx, Peyy, Pezz, Pexy, Pexz, Peyz, x, y, z, dx, dy, dz, qom, radius,cyl);
    
    
JedotE=(Jex).*Ex + (Jey).*Ey + (Jez).*Ez;
DUbulkDt = JedotE - Ubulk.*divVe - udivP;
DUthDt = -Uth.*divVe -PgradV;

V1=imgaussfilt3((Uth+Ubulk).*divVe,radius);
savevtk_bin(V1, [dir_out  'EdivVe' Ncycle '.vtk'],'EdivV',dx,dy,dz,0,0,0);

V1=imgaussfilt3(divUP,radius);
savevtk_bin(V1, [dir_out  'divUPe' Ncycle '.vtk'],'divUP',dx,dy,dz,0,0,0);
V1=imgaussfilt3(pdivv,radius);
savevtk_bin(V1, [dir_out  'pdivve' Ncycle '.vtk'],'pdivv',dx,dy,dz,0,0,0);
V1=imgaussfilt3(ugradp,radius);
savevtk_bin(V1, [dir_out  'ugradpe' Ncycle '.vtk'],'ugradp',dx,dy,dz,0,0,0);
V1=imgaussfilt3(PgradV,radius);
savevtk_bin(V1, [dir_out  'PgradVe' Ncycle '.vtk'],'PgradV',dx,dy,dz,0,0,0);
V1=imgaussfilt3(udivP,radius);
savevtk_bin(V1, [dir_out  'udivPe' Ncycle '.vtk'],'udivP',dx,dy,dz,0,0,0);
V1=imgaussfilt3(JedotE,radius);
savevtk_bin(V1, [dir_out  'JedotE' Ncycle '.vtk'],'JedotE',dx,dy,dz,0,0,0);

%
% Ions
%
[Uth, Ubulk, divQbulk, divQenth, divQhf,  udivP, PgradV, ugradp, pdivv, divUP] = compute_energy_balance( ...
    rhoi, Jix, Jiy, Jiz,... 
    Qbulkix, Qbulkiy, Qbulkiz, Qenthix, Qenthiy, Qenthiz, Qhfix, Qhfiy, Qhfiz, ...
    Pixx, Piyy, Pizz, Pixy, Pixz, Piyz, x, y, z, dx, dy, dz, 1.0, radius,cyl);

JidotE=(Jix).*Ex + (Jiy).*Ey + (Jiz).*Ez;
DUbulkDt = JidotE - Ubulk.*divVi - udivP;
DUthDt = -Uth.*divVi -PgradV;

V1=imgaussfilt3((Uth+Ubulk).*divVi,radius);
savevtk_bin(V1, [dir_out  'EdivVi' Ncycle '.vtk'],'EdivV',dx,dy,dz,0,0,0);

V1=imgaussfilt3(divUP,radius);
savevtk_bin(V1, [dir_out  'divUPi' Ncycle '.vtk'],'divUP',dx,dy,dz,0,0,0);
V1=imgaussfilt3(JidotE,radius);
savevtk_bin(V1, [dir_out  'JidotE' Ncycle '.vtk'],'JidotE',dx,dy,dz,0,0,0);

V1=imgaussfilt3(pdivv,radius);
savevtk_bin(V1, [dir_out  'pdivvi' Ncycle '.vtk'],'pdivv',dx,dy,dz,0,0,0);
V1=imgaussfilt3(ugradp,radius);
savevtk_bin(V1, [dir_out  'ugradpi' Ncycle '.vtk'],'ugradp',dx,dy,dz,0,0,0);
V1=imgaussfilt3(PgradV,radius);
savevtk_bin(V1, [dir_out  'PgradVi' Ncycle '.vtk'],'PgradV',dx,dy,dz,0,0,0);
V1=imgaussfilt3(udivP,radius);
savevtk_bin(V1, [dir_out  'udivPi' Ncycle '.vtk'],'udivP',dx,dy,dz,0,0,0);
V1=imgaussfilt3(JidotE,radius);
savevtk_bin(V1, [dir_out  'JidotE' Ncycle '.vtk'],'JedotE',dx,dy,dz,0,0,0);

%
% Beam
%
[Uth, Ubulk, divQbulk, divQenth, divQhf,  udivP, PgradV, ugradp, pdivv, divUP] = compute_energy_balance( ...
    rhob, Jbx, Jby, Jbz,...
    Qbulkbx, Qbulkby, Qbulkbz, Qenthbx, Qenthby, Qenthbz, Qhfbx, Qhfby, Qhfbz, ...
    Pbxx, Pbyy, Pbzz, Pbxy, Pbxz, Pbyz, x, y, z, dx, dy, dz, qoms(3), radius,cyl);

JbdotE=(Jbx).*Ex + (Jby).*Ey + (Jbz).*Ez;
DUbulkDt = JidotE - Ubulk.*divVb - udivP;
DUthDt = -Uth.*divVb -PgradV;

V1=imgaussfilt3((Uth+Ubulk).*divVb,radius);
savevtk_bin(V1, [dir_out  'EdivVb' Ncycle '.vtk'],'EdivV',dx,dy,dz,0,0,0);

V1=imgaussfilt3(divUP,radius);
savevtk_bin(V1, [dir_out  'divUPb' Ncycle '.vtk'],'divUP',dx,dy,dz,0,0,0);
V1=imgaussfilt3(JbdotE,radius);
savevtk_bin(V1, [dir_out  'JbdotE' Ncycle '.vtk'],'JbdotE',dx,dy,dz,0,0,0);

V1=imgaussfilt3(pdivv,radius);
savevtk_bin(V1, [dir_out  'pdivvb' Ncycle '.vtk'],'pdivv',dx,dy,dz,0,0,0);
V1=imgaussfilt3(ugradp,radius);
savevtk_bin(V1, [dir_out  'ugradpb' Ncycle '.vtk'],'ugradp',dx,dy,dz,0,0,0);
V1=imgaussfilt3(PgradV,radius);
savevtk_bin(V1, [dir_out  'PgradVb' Ncycle '.vtk'],'PgradV',dx,dy,dz,0,0,0);
V1=imgaussfilt3(udivP,radius);
savevtk_bin(V1, [dir_out  'udivPb' Ncycle '.vtk'],'udivP',dx,dy,dz,0,0,0);
V1=imgaussfilt3(JbdotE,radius);
savevtk_bin(V1, [dir_out  'JbdotE' Ncycle '.vtk'],'JedotE',dx,dy,dz,0,0,0);


end    

end
end


