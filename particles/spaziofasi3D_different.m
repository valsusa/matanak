function [vdf,urange,vrange,wrange]=spaziofasi3D_different(u,v,w,q,ndivu,ndivv,ndivw)

umin=min(u(:));umax=max(u(:));
vmin=min(v(:));vmax=max(v(:));
wmin=min(w(:));wmax=max(w(:));
umin=prctile(u,.1)
umax=prctile(u,99.9)
vmin=prctile(v,.1)
vmax=prctile(v,99.9)
wmin=prctile(w,.1)
wmax=prctile(w,99.9)

du=(-umin+umax)/(ndivu);
dv=(-vmin+vmax)/(ndivv);
dw=(-wmin+wmax)/(ndivw);
urange=linspace(umin,umax,ndivu);
vrange=linspace(vmin,vmax,ndivv);
wrange=linspace(wmin,wmax,ndivw);
np=max(size(u));
vdf=zeros(ndivu,ndivv,ndivw);
for ip=1:np

ivx=floor((u(ip)-umin)/du)+1;
ivy=floor((v(ip)-vmin)/dv)+1;
ivz=floor((w(ip)-wmin)/dw)+1;

if(ivx>0 & ivx<=ndivu & ivy>0 & ivy<=ndivv & ivz>0 & ivz<=ndivw)

          vdf(ivx,ivy,ivz)= vdf(ivx,ivy,ivz)+abs(q(ip));

end
end
          
          %vdf=vdf./sum(vdf(:));

return
